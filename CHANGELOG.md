## Version 0.9.2 - 29/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added options to show 404
- adjust routing to support query parameters

## Version 0.9.2 - 22/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust form_text_form_field formfieldvalidator
- added shared to header_bar_change_notifier
- added max height to popup_menu_custom
- cell rule display elaborate value async

## Version 0.9.2 - 21/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- replace shared prefer and secure storage with HiveDB
- added configHandler
- some code cleanup
- added place manager for google apis

## Version 0.9.2 - 20/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- removed cordico core dep
- added userschemamodel
- adjust data table search feature
- adjust data table export feature
- replace validator with formFieldValidator
- added requirements text inside formfieldtext
- added pickImageActions to ImageContainerView

## Version 0.9.2 - 18/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- bug fix
- custom widget to textFormFieldItemsProportionally and also new options added
- new options to IGLayoutItemTypeMarkdownOptions
- new IGLayoutItemWidgetHelper

## Version 0.9.2 - 14/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust ImageContainerView setState

## Version 0.9.2 - 11/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added imageWidget to ActionMenuButtonOption

## Version 0.9.2 - 10/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- update some functions

## Version 0.9.2 - 09/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- Flutter 2.8
- Markdown support to FormTextFormField

## Version 0.9.2 - 07/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust image container view pick

## Version 0.9.2 - 04/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust data table view change tab

## Version 0.9.2 - 02/12/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- improved filters popup with a new all checkbox
- keep data table state between navigation
- searchfield improvements
- added headerRules to SideItem
- some data table view nullability
- data table view code cleaning and structuring

## Version 0.9.2 - 30/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- improved data_table_view performance
- create json schema utilities
- added new textfield for handle JSON creation
- added IGFormFieldValidatorType.json

## Version 0.9.2 - 28/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added sendFeedbackHandler
- adjust FeedbackFormSheet

## Version 0.9.2 - 26/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust side item path detection

## Version 0.9.2 - 25/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- replace "it" with "en

## Version 0.9.2 - 23/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- formsheet improvements

## Version 0.9.2 - 18/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added refreshInternals to webConfigurator
- adjust header_rule sort feature to support keys with .

## Version 0.9.2 - 05/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust data_table_view generated rows
- added limitHorizontalList to cell_display_rule

## Version 0.9.2 - 04/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- created IGTreeView from JSON Viewer

## Version 0.9.2 - 03/11/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added IGLayoutItemTypeImagePickFilesOptions inside ImageContainerView
- adjust selectedUrl when is empty to null inside ImageContainerView

## Version 0.9.2 - 28/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust form field validator array

## Version 0.9.2 - 27/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added isOptional to enumTextFormFieldItemsProportionally

## Version 0.9.2 - 25/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjust formTextFormField from enum values to format the value if necessary

## Version 0.9.2 - 23/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- formTextFormField from enum values
- use firstWhereOrNull instead firstWhere

## Version 0.9.2 - 20/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- adjusted IGButtonRow

## Version 0.9.2 - 17/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- added markdown to IGLayoutItem
- added IGLayoutItemTypeMarkdownOptions

## Version 0.9.2 - 16/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- some adjustments for US language defaults

## Version 0.9.2 - 11/10/2020 - Luca Iaconelli ([IGLU](https://www.iglu.dev/en/))

- first version
