/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class LocalNotificationsChangeNotifier
    extends ArrayValueObserve<LocalNotification> {}
