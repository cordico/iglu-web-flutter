/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

class LocalNotification {
  String? title;
  String? message;
  String? externalId;
  Function()? onTap;
  LocalNotification({this.message, this.title, this.externalId, this.onTap});
}
