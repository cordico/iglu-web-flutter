/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/widgets.dart";

class MapValueObserve<K, V> extends ChangeNotifier {
  Map<K, V> _value = {};
  MapValueObserve();

  Map<K, V> get value => _value;

  set value(Map<K, V> v) {
    _value = v;
    notifyListeners();
  }

  V? operator [](K key) {
    return _value[key];
  }

  void operator []=(K key, V value) {
    _value[key] = value;
    notifyListeners();
  }

  update(K key, V value) {
    _value.update(key, (v) => value, ifAbsent: () => value);
    notifyListeners();
  }

  clear() {
    _value.clear();
    notifyListeners();
  }

  remove(K key) {
    _value.remove(key);
    notifyListeners();
  }

  removeWhere(bool Function(K, V) predicate) {
    _value.removeWhere(predicate);
    notifyListeners();
  }

  refresh() {
    notifyListeners();
  }
}
