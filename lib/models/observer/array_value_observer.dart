/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/widgets.dart";

class ArrayValueObserve<T> extends ChangeNotifier {
  List<T>? _value;
  ArrayValueObserve() {
    value = [];
  }

  List<T>? get value => _value;

  set value(List<T>? v) {
    _value = v;
    notifyListeners();
  }

  add(T element) {
    _value!.add(element);
    notifyListeners();
  }

  addAll(List<T> elements) {
    _value!.addAll(elements);
    notifyListeners();
  }

  insert(int index, T element) {
    _value!.insert(index, element);
    notifyListeners();
  }

  clear() {
    _value!.clear();
    notifyListeners();
  }

  remove(T element) {
    _value!.remove(element);
    notifyListeners();
  }

  removeWhere(bool Function(T) test) {
    _value!.removeWhere(test);
    notifyListeners();
  }

  removeAt(int index) {
    _value!.removeAt(index);
    notifyListeners();
  }

  refresh() {
    notifyListeners();
  }
}
