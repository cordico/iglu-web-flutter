/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/widgets.dart";

class ArrayClassChangeNotifier<T> extends ChangeNotifier {
  List<T>? _value;
  ChangeNotifier? outsideNotifier;
  ArrayClassChangeNotifier({this.outsideNotifier}) {
    value = [];
  }

  List<T>? get value => _value;

  _notify() {
    if (outsideNotifier != null) {
      outsideNotifier!.notifyListeners();
    } else {
      notifyListeners();
    }
  }

  set value(List<T>? v) {
    _value = v;
    _notify();
  }

  add(T element) {
    var index = _value!.indexOf(element);
    if (index > -1) {
      _value!.removeAt(index);
      _value!.insert(index, element);
    } else {
      _value!.add(element);
    }
    _notify();
  }

  update(T element) {
    var index = _value!.indexOf(element);
    if (index > -1) {
      _value!.removeAt(index);
      _value!.insert(index, element);
    }
    _notify();
  }

  addAll(List<T> elements) {
    _value!.addAll(elements);
    _notify();
  }

  insert(int index, T element) {
    _value!.insert(index, element);
    _notify();
  }

  clear() {
    _value!.clear();
    _notify();
  }

  remove(T element) {
    _value!.remove(element);
    _notify();
  }

  int indexOf(T element) {
    return _value!.indexOf(element);
  }

  int indexWhere(bool Function(T) test) {
    return _value!.indexWhere(test);
  }

  removeWhere(bool Function(T) test) {
    _value!.removeWhere(test);
    _notify();
  }

  removeAt(int index) {
    _value!.removeAt(index);
    _notify();
  }

  refresh() {
    _notify();
  }
}
