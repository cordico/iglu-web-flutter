/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/widgets.dart";

class ValueObserve<T> extends ChangeNotifier {
  T? _value;
  ValueObserve();

  T? get value => _value;

  set value(T? v) {
    _value = v;
    notifyListeners();
  }

  refresh() {
    notifyListeners();
  }
}
