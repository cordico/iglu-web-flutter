/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:json_annotation/json_annotation.dart';
part 'attach.g.dart';

@JsonSerializable(explicitToJson: true)
class Attach {
  String? externalID;
  String? url;
  String? originalUrl;
  String? thumbUrl;
  String? originalFileName;
  String? thumbFileName;
  String? endpoint;
  String? mimetype;
  double? size;
  String? previewBase64;
  String? expireDate;
  String? externalSource;
  num? width;
  num? height;
  num? density;
  num? exifOrientation;
  bool? isLandscape;

  Attach();

  factory Attach.fromJson(Map<String, dynamic> json) => _$AttachFromJson(json);

  Map<String, dynamic> toJson() => _$AttachToJson(this);

  static Attach fromJSON(Map<String, dynamic> json) {
    return Attach.fromJson(json);
  }
}
