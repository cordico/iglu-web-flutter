import 'package:json_annotation/json_annotation.dart';
part 'user_schema_model.g.dart';

@JsonSerializable(explicitToJson: true)
class UserSchemaModel extends JsonSerializable {
  UserSchemaModel();

  factory UserSchemaModel.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaModelFromJson(json);

  String? email;
  String? id;
  String? identifier;
  String? sessionToken;

  String? userTypeId;

  List<UserSchemaModelRole>? roles;

  @JsonKey(name: 'last_name')
  String? lastName;
  @JsonKey(name: 'first_name')
  String? firstName;

  @JsonKey(name: 'organization_id')
  String? organizationId;

  @JsonKey(name: 'organization_ids')
  List<String>? organizationIds;

  @JsonKey(name: 'connected_organizations')
  List<UserSchemaModelConnectedOrganizations>? connectedOrganizations;

  @JsonKey(name: 'lid_identifier')
  String? lidIdentifier;

  UserSchemaModelCredentials? credentials;

  UserSchemaModelOrganization? organization;

  dynamic name;

  @override
  Map<String, dynamic> toJson() => _$UserSchemaModelToJson(this);

  static UserSchemaModel fromJSON(Map<String, dynamic> json) {
    return UserSchemaModel.fromJson(json);
  }

  List<String> allOrganizations() {
    final a = organizationIds ?? [];
    if (organizationId != null) {
      a.add(organizationId!);
    }
    if (connectedOrganizations != null) {
      a.addAll(
          connectedOrganizations!.map((e) => e.organizationId ?? '').toList());
    }
    return a.toSet().toList();
  }

  String? get rFirstName {
    if (name is UserSchemaModelNameSchema) {
      return firstName ?? (name as UserSchemaModelNameSchema).first;
    }
    return firstName ?? name as String?;
  }

  String? get rLastName {
    if (name is UserSchemaModelNameSchema) {
      return lastName ?? (name as UserSchemaModelNameSchema).last;
    }
    return lastName ?? name as String?;
  }

  String? get fullName {
    if (rFirstName == null && rLastName == null) {
      return null;
    }
    return '${rFirstName ?? ''} ${rLastName ?? ''}'.trim();
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaModelNameSchema {
  UserSchemaModelNameSchema();

  factory UserSchemaModelNameSchema.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaModelNameSchemaFromJson(json);

  String? first;
  String? last;

  Map<String, dynamic> toJson() => _$UserSchemaModelNameSchemaToJson(this);

  static UserSchemaModelNameSchema fromJSON(Map<String, dynamic> json) {
    return UserSchemaModelNameSchema.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaModelCredentials {
  UserSchemaModelCredentials();

  factory UserSchemaModelCredentials.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaModelCredentialsFromJson(json);

  String? identifier;
  String? password;

  Map<String, dynamic> toJson() => _$UserSchemaModelCredentialsToJson(this);

  static UserSchemaModelCredentials fromJSON(Map<String, dynamic> json) {
    return UserSchemaModelCredentials.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaModelOrganization {
  UserSchemaModelOrganization();

  factory UserSchemaModelOrganization.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaModelOrganizationFromJson(json);

  UserSchemaModelOrganizationData? data;
  String? name;

  @JsonKey(name: 'organization_type')
  String? organizationType;

  Map<String, dynamic> toJson() => _$UserSchemaModelOrganizationToJson(this);

  static UserSchemaModelOrganization fromJSON(Map<String, dynamic> json) {
    return UserSchemaModelOrganization.fromJson(json);
  }
}

@JsonSerializable(explicitToJson: true)
class UserSchemaModelOrganizationData {
  UserSchemaModelOrganizationData();

  factory UserSchemaModelOrganizationData.fromJson(Map<String, dynamic> json) =>
      _$UserSchemaModelOrganizationDataFromJson(json);

  String? id;

  Map<String, dynamic> toJson() => _$UserSchemaModelOrganizationDataToJson(this);

  static UserSchemaModelOrganizationData fromJSON(Map<String, dynamic> json) {
    return UserSchemaModelOrganizationData.fromJson(json);
  }
}

enum UserSchemaModelRole { admin, editor, user }

@JsonSerializable(explicitToJson: true)
class UserSchemaModelConnectedOrganizations {
  UserSchemaModelConnectedOrganizations();

  factory UserSchemaModelConnectedOrganizations.fromJson(
          Map<String, dynamic> json) =>
      _$UserSchemaModelConnectedOrganizationsFromJson(json);

  List<String>? roles;
  @JsonKey(name: 'organization_id')
  String? organizationId;

  Map<String, dynamic> toJson() =>
      _$UserSchemaModelConnectedOrganizationsToJson(this);

  static UserSchemaModelConnectedOrganizations fromJSON(Map<String, dynamic> json) {
    return UserSchemaModelConnectedOrganizations.fromJson(json);
  }
}
