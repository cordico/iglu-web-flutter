/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class Config {
  Const appConfiguration = Const();

  Config();

  Config.fromJson(Map<String, dynamic> json) {
    if (json['appConfiguration'] != null &&
        (json['appConfiguration'] as Map).isNotEmpty) {
      this.appConfiguration = Const.fromJson(json['appConfiguration']);
    }
  }

  static Config fromJSON(Map<String, dynamic> json) {
    return Config.fromJson(json);
  }
}
