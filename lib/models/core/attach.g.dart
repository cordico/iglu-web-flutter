// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attach.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Attach _$AttachFromJson(Map<String, dynamic> json) {
  return Attach()
    ..externalID = json['externalID'] as String?
    ..url = json['url'] as String?
    ..originalUrl = json['originalUrl'] as String?
    ..thumbUrl = json['thumbUrl'] as String?
    ..originalFileName = json['originalFileName'] as String?
    ..thumbFileName = json['thumbFileName'] as String?
    ..endpoint = json['endpoint'] as String?
    ..mimetype = json['mimetype'] as String?
    ..size = (json['size'] as num?)?.toDouble()
    ..previewBase64 = json['previewBase64'] as String?
    ..expireDate = json['expireDate'] as String?
    ..externalSource = json['externalSource'] as String?
    ..width = json['width'] as num?
    ..height = json['height'] as num?
    ..density = json['density'] as num?
    ..exifOrientation = json['exifOrientation'] as num?
    ..isLandscape = json['isLandscape'] as bool?;
}

Map<String, dynamic> _$AttachToJson(Attach instance) => <String, dynamic>{
      'externalID': instance.externalID,
      'url': instance.url,
      'originalUrl': instance.originalUrl,
      'thumbUrl': instance.thumbUrl,
      'originalFileName': instance.originalFileName,
      'thumbFileName': instance.thumbFileName,
      'endpoint': instance.endpoint,
      'mimetype': instance.mimetype,
      'size': instance.size,
      'previewBase64': instance.previewBase64,
      'expireDate': instance.expireDate,
      'externalSource': instance.externalSource,
      'width': instance.width,
      'height': instance.height,
      'density': instance.density,
      'exifOrientation': instance.exifOrientation,
      'isLandscape': instance.isLandscape,
    };
