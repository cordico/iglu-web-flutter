/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class PaginatedQuery<T> {
  PaginatedQuery({this.handler, required this.fromJSON});

  Function()? handler;
  Function fromJSON;
  //TODO: QUERY
  dynamic query;
  var page = 1;
  int? totalElements = 0;
  List<T> elements = List<T>.from([], growable: true);

  var isLoading = false;
  var isEnd = false;
  var isTooEarly = false;

  var keepOnlyOnePage = false;

  reset() {
    page = 1;
    totalElements = 0;
    isLoading = false;
    isTooEarly = false;
    isEnd = false;
    elements.clear();
  }

  Future<void> restart() async {
    reset();
    await perform();
  }

  Future<void> perform(
      {bool isFirst = false,
      bool handlePage = true,
      bool forceRefresh = false,
      bool loadAlsoOtherPages = false,
      Function? additionHandler}) async {
    if (!forceRefresh && (isLoading || isEnd || isTooEarly)) {
      return;
    }
    isTooEarly = true;

    if (query != null && (!isEnd || forceRefresh) && !isLoading) {
      if (isFirst && page > 1) {
        isLoading = false;
        if (handler != null) {
          handler!();
        }
        return;
      }
      isLoading = true;
      if (additionHandler != null) {
        additionHandler();
      }
      query!.setPage(page);
      logger("LOADING QUERY  - page:$page");
      var snap = await (query!.getObjects());
      totalElements = snap.nbHits;
      if (snap.hits.isEmpty) {
        isLoading = false;
        isEnd = true;
        if (handler != null) {
          handler!();
        }
        logger(
            " Hits count: ${snap.nbHits} - Page: ${snap.offset ?? snap.page} - Hit per Page: ${snap.limit} - Hit in this Page: ${snap.hits.length} - Results: ${elements.length}");
        return;
      }
      if (snap.hits.length < query!.limit) {
        isEnd = true;
      }
      isLoading = false;
      if (handlePage) {
        page += 1;
      }
      if (keepOnlyOnePage) {
        elements.clear();
      }
      snap.hits.forEach((f) {
        var item = f;
        elements.remove(item);
        elements.add(item);
      });
      logger(
          "Hits count: ${snap.nbHits} - Page: ${snap.offset ?? snap.page} - Hit per Page: ${snap.limit} - Hit in this Page: ${snap.hits.length} - Results: ${elements.length}");
      if (handler != null) {
        handler!();
      }
      if (additionHandler != null) {
        additionHandler();
      }
      if (page % 2 == 1 && loadAlsoOtherPages) {
        Future.delayed(Duration(milliseconds: 800), () {
          perform(isFirst: false, additionHandler: additionHandler);
        });
      }
      Future.delayed(Duration(seconds: 2), () {
        isTooEarly = false;
      });
    }
  }
}
