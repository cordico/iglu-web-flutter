/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/extensions/double_ext.dart';
import 'package:iglu_web_flutter/extensions/string_ext.dart';

class Const {
  double boxRadius = 8;
  double boxBigRadius = 8;
  double hPadding = 20;
  double hIntPadding = 20;
  double hViewsPadding = 12;
  double vViewsPadding = 12;
  double hBigPadding = 40;
  double topPadding = 20;
  double bottomPadding = 20;
  double topIntPadding = 16;
  double bottomIntPadding = 16;
  bool materialOnly = false;

  //NAVIGATION BAR:--------------------
  double navBarHeight = 80.0;
  double navBarTitleSize = 26.0;
  FontWeight navBarTitleWeight = FontWeight.w600;
  double navBarTopPadding = 28.0;
  double navBarHPadding = 16.0;
  bool navBarSeparator = false;
  double navBarSeparatorHeight = 0.5;
  bool navBarHasShadow = true;
  bool navBariOSStyle = true;
  BoxShadow? navBarShadow = BoxShadow();

  //BUTTON:-----------------------
  double buttonHeight = 44.0;
  double buttonElevation = 0.0;
  double buttonRadius = 30.0;
  bool buttonHasBorder = false;
  double buttonBorderWidth = 2.0;
  double buttonTextSize = 15.0;
  double buttonIconSize = 24.0;
  FontWeight buttonTextWeight = FontWeight.w500;

  //HEADER TEXT:---------------
  FontWeight headerTextWeight = FontWeight.w700;
  double headerTextSize = 20.0;

  //SETTINGS OPTION:---------------
  FontWeight settingsOptionTextWeight = FontWeight.w400;
  double settingsOptionTextSize = 18.0;
  double settingsOptionIconSize = 24.0;
  double settingsOptionVPadding = 16.0;
  bool settingsOptionMultiline = false;

  //SEPARATOR:---------------
  double separatorThickness = 0.3;
  double separatorHeight = 0.4;
  double separatorIndent = 20.0;

  //POPUP:---------------
  double popupTitleSize = 18.0;
  double popupMessageSize = 16.0;
  FontWeight popupTitleWeight = FontWeight.w600;
  FontWeight popupMessageWeight = FontWeight.w400;

  //COLORS:---------------------
  Color? primaryColor;
  Color? primaryDarkColor;
  Color? accentColor;
  Color? backgroundColor;
  Color? primaryBackgroundColor;
  Color? backgroundSecondaryColor;
  Color? backgroundDarkColor;
  Color? backgroundSecondaryDarkColor;
  Color? backgroundMainColor;
  Color? backgroundMainDarkColor;
  Color? textColor;
  Color? textSecondaryColor;
  Color? textDarkColor;
  Color? textSecondaryDarkColor;
  Color? hintColor;
  Color? hintDarkColor;
  Color? iconColor;
  Color? iconDarkColor;
  Color? tabBarColor;
  Color? tabBarDarkColor;
  Color? tabBarTintColor;
  Color? tabBarTintDarkColor;
  Color? navBarColor;
  Color? navBarDarkColor;
  Color? navBarTintColor;
  Color? navBarTintDarkColor;
  Color? navBarSeparatorColor;
  Color? navBarSeparatorDarkColor;
  Color? separatorColor;
  Color? separatorDarkColor;
  Color? buttonColor;
  Color? buttonDisabledColor;
  Color? buttonDarkColor;
  Color? buttonDisabledDarkColor;

  Color? buttonBorderColor;
  Color? buttonBorderDarkColor;
  Color? buttonTextColor;
  Color? buttonTextDarkColor;
  Color? buttonOnlyTextColor;
  Color? buttonOnlyTextDarkColor;
  Color? buttonHighlightColor;
  Color? buttonHighlightDarkColor;

  Color? popupBackgroundColor;
  Color? popupBackgroundDarkColor;
  Color? popupTitleColor;
  Color? popupTitleDarkColor;
  Color? popupMessageColor;
  Color? popupMessageDarkColor;
  Color? popupButtonTextColor;
  Color? popupButtonTextDarkColor;

  Brightness? statusLightBrightness;
  Brightness? statusDarkBrightness;

  Map<String, dynamic>? colorsMap;
  Map<String, dynamic>? metadata;

  Map<String, dynamic>? appStructureConfig;
  Map<String, dynamic>? profileFields;
  Map<String, dynamic>? memberInfosFields;

  Const({
    this.boxRadius = 8,
    this.boxBigRadius = 16,
    this.hPadding = 20,
    this.hIntPadding = 20,
    this.hViewsPadding = 12,
    this.vViewsPadding = 12,
    this.hBigPadding = 40,
    this.topPadding = 20,
    this.bottomPadding = 20,
    this.topIntPadding = 16,
    this.bottomIntPadding = 16,
    this.primaryColor,
    this.accentColor,
    this.backgroundColor,
    this.backgroundDarkColor,
    this.backgroundSecondaryColor,
    this.backgroundSecondaryDarkColor,
    this.textColor,
    this.textDarkColor,
    this.textSecondaryColor,
    this.textSecondaryDarkColor,
  });

  Const.fromJson(Map<String, dynamic> json) {
    if (json['layout'] != null) {
      this.boxRadius = json['layout']['boxRadius'] != null
          ? json['layout']['boxRadius'].toDouble()
          : 8;
      this.boxBigRadius = json['layout']['boxBigRadius'] != null
          ? json['layout']['boxBigRadius'].toDouble()
          : 8;
      this.hPadding = json['layout']['hPadding'] != null
          ? json['layout']['hPadding'].toDouble()
          : 20;
      this.hIntPadding = json['layout']['hIntPadding'] != null
          ? json['layout']['hIntPadding'].toDouble()
          : 20;
      this.hViewsPadding = json['layout']['hViewsPadding'] != null
          ? json['layout']['hViewsPadding'].toDouble()
          : 12;
      this.vViewsPadding = json['layout']['vViewsPadding'] != null
          ? json['layout']['vViewsPadding'].toDouble()
          : 12;
      this.hBigPadding = json['layout']['hBigPadding'] != null
          ? json['layout']['hBigPadding'].toDouble()
          : 40;
      this.topPadding = json['layout']['topPadding'] != null
          ? json['layout']['topPadding'].toDouble()
          : 20;
      this.bottomPadding = json['layout']['bottomPadding'] != null
          ? json['layout']['bottomPadding'].toDouble()
          : 20;
      this.topIntPadding = json['layout']['topIntPadding'] != null
          ? json['layout']['topIntPadding'].toDouble()
          : 16;
      this.bottomIntPadding = json['layout']['bottomIntPadding'] != null
          ? json['layout']['bottomIntPadding'].toDouble()
          : 16;

      this.materialOnly = json['layout']['materialOnly'] != null
          ? json['layout']['materialOnly']
          : false;

      this.navBarHeight = json['layout']['navBarHeight'] != null
          ? json['layout']['navBarHeight'].toDouble()
          : 80;

      this.navBarTitleSize = json['layout']['navBarTitleSize'] != null
          ? json['layout']['navBarTitleSize'].toDouble()
          : 26;

      this.navBarTitleWeight = (json['layout']['navBarTitleWeight'] != null
          ? ((json['layout']['navBarTitleWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w600);

      this.navBarTopPadding = json['layout']['navBarTopPadding'] != null
          ? json['layout']['navBarTopPadding'].toDouble()
          : 28;

      this.navBarHPadding = json['layout']['navBarHPadding'] != null
          ? json['layout']['navBarHPadding'].toDouble()
          : 16;

      this.navBarSeparator = json['layout']['navBarSeparator'] != null
          ? json['layout']['navBarSeparator']
          : false;

      this.navBarSeparatorHeight =
          json['layout']['navBarSeparatorHeight'] != null
              ? json['layout']['navBarSeparatorHeight'].toDouble()
              : 0.5;

      this.navBarHasShadow = json['layout']['navBarHasShadow'] != null
          ? json['layout']['navBarHasShadow']
          : true;

      this.navBariOSStyle = json['layout']['navBariOSStyle'] != null
          ? json['layout']['navBariOSStyle']
          : false;

      this.buttonHeight = json['layout']['buttonHeight'] != null
          ? json['layout']['buttonHeight'].toDouble()
          : 44;

      this.buttonElevation = json['layout']['buttonElevation'] != null
          ? json['layout']['buttonElevation'].toDouble()
          : 0;

      this.buttonRadius = json['layout']['buttonRadius'] != null
          ? json['layout']['buttonRadius'].toDouble()
          : 30;

      this.buttonHasBorder = json['layout']['buttonHasBorder'] != null
          ? json['layout']['buttonHasBorder']
          : false;

      this.buttonBorderWidth = json['layout']['buttonBorderWidth'] != null
          ? json['layout']['buttonBorderWidth'].toDouble()
          : 2.0;

      this.buttonTextSize = json['layout']['buttonTextSize'] != null
          ? json['layout']['buttonTextSize'].toDouble()
          : 15.0;

      this.buttonIconSize = json['layout']['buttonIconSize'] != null
          ? json['layout']['buttonIconSize'].toDouble()
          : 24.0;

      this.buttonTextWeight = (json['layout']['buttonTextWeight'] != null
          ? ((json['layout']['buttonTextWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w500);

      this.headerTextWeight = (json['layout']['headerTextWeight'] != null
          ? ((json['layout']['headerTextWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w700);

      this.headerTextSize = json['layout']['headerTextSize'] != null
          ? json['layout']['headerTextSize'].toDouble()
          : 20.0;

      this.settingsOptionTextWeight = (json['layout']
                  ['settingsOptionTextWeight'] !=
              null
          ? ((json['layout']['settingsOptionTextWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w400);

      this.settingsOptionTextSize =
          json['layout']['settingsOptionTextSize'] != null
              ? json['layout']['settingsOptionTextSize'].toDouble()
              : 18.0;

      this.settingsOptionIconSize =
          json['layout']['settingsOptionIconSize'] != null
              ? json['layout']['settingsOptionIconSize'].toDouble()
              : 24.0;

      this.settingsOptionVPadding =
          json['layout']['settingsOptionVPadding'] != null
              ? json['layout']['settingsOptionVPadding'].toDouble()
              : 16.0;

      this.settingsOptionMultiline =
          json['layout']['settingsOptionMultiline'] != null
              ? json['layout']['settingsOptionMultiline']
              : false;

      this.separatorThickness = json['layout']['separatorThickness'] != null
          ? json['layout']['separatorThickness'].toDouble()
          : 0.3;

      this.separatorHeight = json['layout']['separatorHeight'] != null
          ? json['layout']['separatorHeight'].toDouble()
          : 0.4;

      this.separatorIndent = json['layout']['separatorIndent'] != null
          ? json['layout']['separatorIndent'].toDouble()
          : 20.0;

      this.popupTitleSize = json['layout']['popupTitleSize'] != null
          ? json['layout']['popupTitleSize'].toDouble()
          : 18.0;
      this.popupMessageSize = json['layout']['popupMessageSize'] != null
          ? json['layout']['popupMessageSize'].toDouble()
          : 16.0;
      this.popupTitleWeight = (json['layout']['popupTitleWeight'] != null
          ? ((json['layout']['popupTitleWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w600);
      this.popupMessageWeight = (json['layout']['popupMessageWeight'] != null
          ? ((json['layout']['popupMessageWeight'].toDouble() as double?)!
              .toFontWeight())
          : FontWeight.w400);
    }

    //COLORS:--------------------
    this.colorsMap = json['colors'];
    this.metadata = json['metadata'];

    if (json['colors'] != null) {
      this.primaryColor =
          (json['colors']['primaryColor'] as String?)?.getColor();
      this.primaryDarkColor =
          (json['colors']['primaryDarkColor'] as String?)?.getColor();
      this.accentColor = (json['colors']['accentColor'] as String?)?.getColor();
      this.primaryBackgroundColor =
          (json['colors']['primaryBackground'] as String?)?.getColor();
      this.backgroundColor = (json['colors']['back'] as String?)?.getColor();
      this.backgroundDarkColor =
          (json['colors']['backDark'] as String?)?.getColor();
      this.backgroundSecondaryColor =
          (json['colors']['backSecondary'] as String?)?.getColor();
      this.backgroundSecondaryDarkColor =
          (json['colors']['backSecondaryDark'] as String?)?.getColor();
      this.backgroundMainColor =
          (json['colors']['backMainColor'] as String?)?.getColor();
      this.backgroundMainDarkColor =
          (json['colors']['backMainDark'] as String?)?.getColor();
      this.textColor = (json['colors']['text'] as String?)?.getColor();
      this.textDarkColor = (json['colors']['textDark'] as String?)?.getColor();
      this.textSecondaryColor =
          (json['colors']['textSecondary'] as String?)?.getColor();
      this.textSecondaryDarkColor =
          (json['colors']['textSecondaryDark'] as String?)?.getColor();
      this.hintColor = (json['colors']['hint'] as String?)?.getColor();
      this.hintDarkColor = (json['colors']['hintDark'] as String?)?.getColor();
      this.iconColor = (json['colors']['icon'] as String?)?.getColor();
      this.iconDarkColor = (json['colors']['iconDark'] as String?)?.getColor();
      this.tabBarColor = (json['colors']['tabBar'] as String?)?.getColor();
      this.tabBarDarkColor =
          (json['colors']['tabBarDark'] as String?)?.getColor();
      this.tabBarTintColor =
          (json['colors']['tabBarTint'] as String?)?.getColor();
      this.tabBarTintDarkColor =
          (json['colors']['tabBarTintDark'] as String?)?.getColor();
      this.navBarColor = (json['colors']['navBar'] as String?)?.getColor();
      this.navBarDarkColor =
          (json['colors']['navBarDark'] as String?)?.getColor();
      this.navBarTintColor =
          (json['colors']['navBarTint'] as String?)?.getColor();
      this.navBarTintDarkColor =
          (json['colors']['navBarTintDark'] as String?)?.getColor();
      this.navBarSeparatorColor =
          (json['colors']['navBarSeparatorColor'] as String?)?.getColor();
      this.navBarSeparatorDarkColor =
          (json['colors']['navBarSeparatorDarkColor'] as String?)?.getColor();
      this.separatorColor =
          (json['colors']['separator'] as String?)?.getColor();
      this.separatorDarkColor =
          (json['colors']['separatorDark'] as String?)?.getColor();
      this.buttonColor = (json['colors']['button'] as String?)?.getColor();
      this.buttonDisabledColor =
          (json['colors']['buttonDisabled'] as String?)?.getColor();
      this.buttonDarkColor =
          (json['colors']['buttonDark'] as String?)?.getColor();
      this.buttonDisabledDarkColor =
          (json['colors']['buttonDisabledDark'] as String?)?.getColor();

      this.buttonBorderColor =
          (json['colors']['buttonBorderColor'] as String?)?.getColor();
      this.buttonBorderDarkColor =
          (json['colors']['buttonBorderDarkColor'] as String?)?.getColor();
      this.buttonTextColor =
          (json['colors']['buttonTextColor'] as String?)?.getColor();
      this.buttonTextDarkColor =
          (json['colors']['buttonTextDarkColor'] as String?)?.getColor();
      this.buttonOnlyTextColor =
          (json['colors']['buttonOnlyTextColor'] as String?)?.getColor();
      this.buttonOnlyTextDarkColor =
          (json['colors']['buttonOnlyTextDarkColor'] as String?)?.getColor();
      this.buttonHighlightColor =
          (json['colors']['buttonHighlightColor'] as String?)?.getColor();
      this.buttonHighlightDarkColor =
          (json['colors']['buttonHighlightDarkColor'] as String?)?.getColor();

      this.popupBackgroundColor =
          (json['colors']['popupBackgroundColor'] as String?)?.getColor();
      this.popupBackgroundDarkColor =
          (json['colors']['popupBackgroundDarkColor'] as String?)?.getColor();
      this.popupTitleColor =
          (json['colors']['popupTitleColor'] as String?)?.getColor();
      this.popupTitleDarkColor =
          (json['colors']['popupTitleDarkColor'] as String?)?.getColor();
      this.popupMessageColor =
          (json['colors']['popupMessageColor'] as String?)?.getColor();
      this.popupMessageDarkColor =
          (json['colors']['popupMessageDarkColor'] as String?)?.getColor();
      this.popupButtonTextColor =
          (json['colors']['popupButtonTextColor'] as String?)?.getColor();
      this.popupButtonTextDarkColor =
          (json['colors']['popupButtonTextDarkColor'] as String?)?.getColor();

      if (json['colors']['statusDarkBrightness'] != null) {
        if (json['colors']['statusDarkBrightness'] == 'dark') {
          statusDarkBrightness = Brightness.dark;
        } else if (json['colors']['statusDarkBrightness'] == 'light') {
          statusDarkBrightness = Brightness.light;
        }
      } else {
        statusDarkBrightness = Brightness.dark;
      }

      if (json['colors']['statusLightBrightness'] != null) {
        if (json['colors']['statusLightBrightness'] == 'dark') {
          statusLightBrightness = Brightness.dark;
        } else if (json['colors']['statusLightBrightness'] == 'light') {
          statusLightBrightness = Brightness.light;
        }
      } else {
        statusLightBrightness = Brightness.light;
      }
    }

    this.appStructureConfig = json['appConfig'];
    this.profileFields = json['profileFields'];
    this.memberInfosFields = json['memberInfosFields'];
  }
}
