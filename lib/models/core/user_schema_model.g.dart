// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_schema_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSchemaModel _$UserSchemaModelFromJson(Map<String, dynamic> json) {
  return UserSchemaModel()
    ..email = json['email'] as String?
    ..id = json['id'] as String?
    ..identifier = json['identifier'] as String?
    ..sessionToken = json['sessionToken'] as String?
    ..userTypeId = json['userTypeId'] as String?
    ..roles = (json['roles'] as List<dynamic>?)
        ?.map((e) => _$enumDecode(_$UserSchemaModelRoleEnumMap, e))
        .toList()
    ..lastName = json['last_name'] as String?
    ..firstName = json['first_name'] as String?
    ..organizationId = json['organization_id'] as String?
    ..organizationIds = (json['organization_ids'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList()
    ..connectedOrganizations =
        (json['connected_organizations'] as List<dynamic>?)
            ?.map((e) => UserSchemaModelConnectedOrganizations.fromJson(
                e as Map<String, dynamic>))
            .toList()
    ..lidIdentifier = json['lid_identifier'] as String?
    ..credentials = json['credentials'] == null
        ? null
        : UserSchemaModelCredentials.fromJson(
            json['credentials'] as Map<String, dynamic>)
    ..organization = json['organization'] == null
        ? null
        : UserSchemaModelOrganization.fromJson(
            json['organization'] as Map<String, dynamic>)
    ..name = json['name'];
}

Map<String, dynamic> _$UserSchemaModelToJson(UserSchemaModel instance) =>
    <String, dynamic>{
      'email': instance.email,
      'id': instance.id,
      'identifier': instance.identifier,
      'sessionToken': instance.sessionToken,
      'userTypeId': instance.userTypeId,
      'roles':
          instance.roles?.map((e) => _$UserSchemaModelRoleEnumMap[e]).toList(),
      'last_name': instance.lastName,
      'first_name': instance.firstName,
      'organization_id': instance.organizationId,
      'organization_ids': instance.organizationIds,
      'connected_organizations':
          instance.connectedOrganizations?.map((e) => e.toJson()).toList(),
      'lid_identifier': instance.lidIdentifier,
      'credentials': instance.credentials?.toJson(),
      'organization': instance.organization?.toJson(),
      'name': instance.name,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$UserSchemaModelRoleEnumMap = {
  UserSchemaModelRole.admin: 'admin',
  UserSchemaModelRole.editor: 'editor',
  UserSchemaModelRole.user: 'user',
};

UserSchemaModelNameSchema _$UserSchemaModelNameSchemaFromJson(
    Map<String, dynamic> json) {
  return UserSchemaModelNameSchema()
    ..first = json['first'] as String?
    ..last = json['last'] as String?;
}

Map<String, dynamic> _$UserSchemaModelNameSchemaToJson(
        UserSchemaModelNameSchema instance) =>
    <String, dynamic>{
      'first': instance.first,
      'last': instance.last,
    };

UserSchemaModelCredentials _$UserSchemaModelCredentialsFromJson(
    Map<String, dynamic> json) {
  return UserSchemaModelCredentials()
    ..identifier = json['identifier'] as String?
    ..password = json['password'] as String?;
}

Map<String, dynamic> _$UserSchemaModelCredentialsToJson(
        UserSchemaModelCredentials instance) =>
    <String, dynamic>{
      'identifier': instance.identifier,
      'password': instance.password,
    };

UserSchemaModelOrganization _$UserSchemaModelOrganizationFromJson(
    Map<String, dynamic> json) {
  return UserSchemaModelOrganization()
    ..data = json['data'] == null
        ? null
        : UserSchemaModelOrganizationData.fromJson(
            json['data'] as Map<String, dynamic>)
    ..name = json['name'] as String?
    ..organizationType = json['organization_type'] as String?;
}

Map<String, dynamic> _$UserSchemaModelOrganizationToJson(
        UserSchemaModelOrganization instance) =>
    <String, dynamic>{
      'data': instance.data?.toJson(),
      'name': instance.name,
      'organization_type': instance.organizationType,
    };

UserSchemaModelOrganizationData _$UserSchemaModelOrganizationDataFromJson(
    Map<String, dynamic> json) {
  return UserSchemaModelOrganizationData()..id = json['id'] as String?;
}

Map<String, dynamic> _$UserSchemaModelOrganizationDataToJson(
        UserSchemaModelOrganizationData instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

UserSchemaModelConnectedOrganizations
    _$UserSchemaModelConnectedOrganizationsFromJson(Map<String, dynamic> json) {
  return UserSchemaModelConnectedOrganizations()
    ..roles =
        (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList()
    ..organizationId = json['organization_id'] as String?;
}

Map<String, dynamic> _$UserSchemaModelConnectedOrganizationsToJson(
        UserSchemaModelConnectedOrganizations instance) =>
    <String, dynamic>{
      'roles': instance.roles,
      'organization_id': instance.organizationId,
    };
