import 'package:flutter/material.dart';

class AnimationPage extends Page {
  final Key? valueKey;
  final Widget? child;
  final Duration transitionDuration;
  final Duration reverseTransitionDuration;
  final bool opaque;
  final bool barrierDismissible;
  final Color? barrierColor;
  final String? barrierLabel;
  final bool? maintainState;
  final bool fullscreenDialog;

  AnimationPage(
      {this.valueKey,
      this.child,
      this.barrierColor,
      this.barrierLabel,
      this.opaque = true,
      this.barrierDismissible = false,
      this.maintainState = true,
      this.fullscreenDialog = false,
      this.transitionDuration = const Duration(milliseconds: 300),
      this.reverseTransitionDuration = const Duration(milliseconds: 300)})
      : super(key: valueKey as LocalKey?);

  Route createRoute(BuildContext context) {
    return PageRouteBuilder(
      settings: this,
      barrierColor: barrierColor,
      barrierDismissible: barrierDismissible,
      barrierLabel: barrierLabel,
      fullscreenDialog: fullscreenDialog,
      maintainState: maintainState!,
      opaque: opaque,
      reverseTransitionDuration: reverseTransitionDuration,
      transitionDuration: transitionDuration,
      pageBuilder: (context, animation, animation2) {
        return FadeTransition(opacity: animation, child: child);
      },
    );
  }
}
