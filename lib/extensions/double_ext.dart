import 'package:flutter/rendering.dart';

/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

extension DoubleExtension on double? {
  String toEuroText({String currency = "€", bool forceSymbol = true}) {
    var realC = currency;
    if (realC.toLowerCase() == "eur") {
      realC = "€";
    }
    return this!.toStringAsFixed(2).replaceAll('.', ',') + ' $realC';
  }

  String getTimeString() {
    var hour = this!.toInt() ~/ 60;
    var minutes = this!.toInt() % 60;

    String hourString = hour < 10
        ? '0${hour.toStringAsFixed(0)}'
        : '${hour.toStringAsFixed(0)}';
    String minuteString = minutes < 10
        ? '0${minutes.toStringAsFixed(0)}'
        : '${minutes.toStringAsFixed(0)}';

    return hourString + ':' + minuteString;
  }

  int getHours() {
    int hour = this!.toInt() ~/ 60;
    return hour;
  }

  int getMinutes() {
    var minutes = this!.toInt() % 60;
    return minutes;
  }

  double toStripeFormat() {
    return this! / 100;
  }

  FontWeight toFontWeight() {
    if (this == 100) {
      return FontWeight.w100;
    } else if (this == 200) {
      return FontWeight.w200;
    } else if (this == 300) {
      return FontWeight.w300;
    } else if (this == 400) {
      return FontWeight.w400;
    } else if (this == 500) {
      return FontWeight.w500;
    } else if (this == 600) {
      return FontWeight.w600;
    } else if (this == 700) {
      return FontWeight.w700;
    } else if (this == 800) {
      return FontWeight.w800;
    } else if (this == 900) {
      return FontWeight.w900;
    } else {
      return FontWeight.w600;
    }
  }
}
