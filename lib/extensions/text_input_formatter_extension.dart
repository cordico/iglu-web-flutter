/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/services.dart";

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

class LimitLenghtTextFormatter extends TextInputFormatter {
  int maxLength;
  LimitLenghtTextFormatter({required this.maxLength});

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > maxLength) {
      return TextEditingValue(
        text: newValue.text.substring(0, maxLength + 1),
        selection: newValue.selection,
      );
    } else {
      return TextEditingValue(
        text: newValue.text,
        selection: newValue.selection,
      );
    }
  }
}
