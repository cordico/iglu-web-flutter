/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

class ArrayUtils {
  static List<T> listDiff<T>(List<T> l1, List<T> l2) => (l1.toSet()..addAll(l2))
      .where((i) => !l1.contains(i) || !l2.contains(i))
      .toList();
}
