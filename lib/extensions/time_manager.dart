/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

class TimeManager {
  TimeManager();

  Map<String, LookupMessages> _lookupMessagesMap = {
    'en': EnMessages(),
    'en_short': EnShortMessages(),
    'en_time_spent': EnMessagesTimeSpent(),
    'it': ItMessages(),
    'it_short': ItShortMessages(),
    'it_time_spent': ItMessagesTimeSpent()
  };

  /// Sets a [locale] with the provided [lookupMessages] to be available when
  /// using the [format] function.
  ///
  /// Example:
  /// ```dart
  /// setLocaleMessages('fr', FrMessages())
  /// ```
  ///
  /// If you want to define locale message implement [LookupMessages] interface
  /// with the desired messages
  ///
  void setLocaleMessages(String locale, LookupMessages lookupMessages) {
    _lookupMessagesMap[locale] = lookupMessages;
  }

  /// Formats provided [date] to a fuzzy time like 'a moment ago'
  ///
  /// - If [locale] is passed will look for message for that locale, if you want
  ///   to add or override locales use [setLocaleMessages]. Defaults to 'en'
  /// - If [clock] is passed this will be the point of reference for calculating
  ///   the elapsed time. Defaults to DateTime.now()
  /// - If [allowFromNow] is passed, format will use the From prefix, ie. a date
  ///   5 minutes from now in 'en' locale will display as "5 minutes from now"
  String format(DateTime date,
      {String? locale, DateTime? clock, bool? allowFromNow}) {
    final _locale = locale ?? 'en';
    final _allowFromNow = allowFromNow ?? false;
    final messages = _lookupMessagesMap[_locale] ?? EnMessages();
    final _clock = clock ?? DateTime.now();
    var elapsed = _clock.millisecondsSinceEpoch - date.millisecondsSinceEpoch;

    String prefix, suffix;

    if (_allowFromNow && elapsed < 0) {
      elapsed = date.isBefore(_clock) ? elapsed : elapsed.abs();
      prefix = messages.prefixFromNow();
      suffix = messages.suffixFromNow();
    } else {
      prefix = messages.prefixAgo();
      suffix = messages.suffixAgo();
    }

    final num seconds = elapsed / 1000;
    final num minutes = seconds / 60;
    final num hours = minutes / 60;
    final num days = hours / 24;
    final num months = days / 30;
    final num years = days / 365;

    String result;
    if (seconds < 45)
      result = messages.lessThanOneMinute(seconds.round());
    else if (seconds < 90)
      result = messages.aboutAMinute(minutes.round());
    else if (minutes < 45)
      result = messages.minutes(minutes.round());
    else if (minutes < 90)
      result = messages.aboutAnHour(minutes.round());
    else if (hours < 24)
      result = messages.hours(hours.round());
    else if (hours < 48)
      result = messages.aDay(hours.round());
    else if (days < 30)
      result = messages.days(days.round());
    else if (days < 60)
      result = messages.aboutAMonth(days.round());
    else if (days < 365)
      result = messages.months(months.round());
    else if (years < 2)
      result = messages.aboutAYear(months.round());
    else
      result = messages.years(years.round());

    return [prefix, result, suffix]
        .where((str) => str.isNotEmpty)
        .join(messages.wordSeparator());
  }
}

//----

abstract class LookupMessages {
  String prefixAgo();
  String prefixFromNow();
  String suffixAgo();
  String suffixFromNow();
  String lessThanOneMinute(int seconds);
  String aboutAMinute(int minutes);
  String minutes(int minutes);
  String aboutAnHour(int minutes);
  String hours(int hours);
  String aDay(int hours);
  String days(int days);
  String aboutAMonth(int days);
  String months(int months);
  String aboutAYear(int year);
  String years(int years);
  String wordSeparator() => ' ';
}

//---- IT
class ItMessagesTimeSpent implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => '';
  String suffixFromNow() => '';
  String lessThanOneMinute(int seconds) => 'meno di un minuto';
  String aboutAMinute(int minutes) => 'circa un minuto';
  String minutes(int minutes) => '$minutes minuti';
  String aboutAnHour(int minutes) => "circa un'ora";
  String hours(int hours) => '$hours ore';
  String aDay(int hours) => 'circa un giorno';
  String days(int days) => '$days giorni';
  String aboutAMonth(int days) => 'circa un mese';
  String months(int months) => '$months mesi';
  String aboutAYear(int year) => 'circa un anno';
  String years(int years) => '$years anni';
  String wordSeparator() => ' ';
}

class ItMessages implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => 'tra';
  String suffixAgo() => 'fa';
  String suffixFromNow() => '';
  String lessThanOneMinute(int seconds) => 'meno di un minuto';
  String aboutAMinute(int minutes) => 'circa un minuto';
  String minutes(int minutes) => '$minutes minuti';
  String aboutAnHour(int minutes) => "circa un'ora";
  String hours(int hours) => '$hours ore';
  String aDay(int hours) => 'circa un giorno';
  String days(int days) => '$days giorni';
  String aboutAMonth(int days) => 'circa un mese';
  String months(int months) => '$months mesi';
  String aboutAYear(int year) => 'circa un anno';
  String years(int years) => '$years anni';
  String wordSeparator() => ' ';
}

class ItShortMessages implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => '';
  String suffixFromNow() => '';
  String lessThanOneMinute(int seconds) => 'ora';
  String aboutAMinute(int minutes) => '1 m';
  String minutes(int minutes) => '$minutes m';
  String aboutAnHour(int minutes) => '~1 o';
  String hours(int hours) => '$hours o';
  String aDay(int hours) => '~1 g';
  String days(int days) => '$days g';
  String aboutAMonth(int days) => '~1 m';
  String months(int months) => '$months m';
  String aboutAYear(int year) => '~1 a';
  String years(int years) => '$years a';
  String wordSeparator() => ' ';
}

//---- EN
class EnMessagesTimeSpent implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => '';
  String suffixFromNow() => '';
  String lessThanOneMinute(int seconds) => 'a moment';
  String aboutAMinute(int minutes) => 'a minute';
  String minutes(int minutes) => '$minutes minutes';
  String aboutAnHour(int minutes) => 'about an hour';
  String hours(int hours) => '$hours hours';
  String aDay(int hours) => 'a day';
  String days(int days) => '$days days';
  String aboutAMonth(int days) => 'about a month';
  String months(int months) => '$months months';
  String aboutAYear(int year) => 'about a year';
  String years(int years) => '$years years';
  String wordSeparator() => ' ';
}

class EnMessages implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => 'ago';
  String suffixFromNow() => 'from now';
  String lessThanOneMinute(int seconds) => 'a moment';
  String aboutAMinute(int minutes) => 'a minute';
  String minutes(int minutes) => '$minutes minutes';
  String aboutAnHour(int minutes) => 'about an hour';
  String hours(int hours) => '$hours hours';
  String aDay(int hours) => 'a day';
  String days(int days) => '$days days';
  String aboutAMonth(int days) => 'about a month';
  String months(int months) => '$months months';
  String aboutAYear(int year) => 'about a year';
  String years(int years) => '$years years';
  String wordSeparator() => ' ';
}

class EnShortMessages implements LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => '';
  String suffixFromNow() => '';
  String lessThanOneMinute(int seconds) => 'now';
  String aboutAMinute(int minutes) => '1 min';
  String minutes(int minutes) => '$minutes min';
  String aboutAnHour(int minutes) => '~1 h';
  String hours(int hours) => '$hours h';
  String aDay(int hours) => '~1 d';
  String days(int days) => '$days d';
  String aboutAMonth(int days) => '~1 mo';
  String months(int months) => '$months mo';
  String aboutAYear(int year) => '~1 yr';
  String years(int years) => '$years yr';
  String wordSeparator() => ' ';
}
