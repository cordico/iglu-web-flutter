/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

extension IntExtension on int {
  String toEuroText() {
    return this.toStringAsFixed(2).replaceAll('.', ',') + ' €';
  }

  String getTimeString() {
    var hour = this ~/ 60;
    var minutes = this % 60;

    String hourString = hour < 10
        ? '0${hour.toStringAsFixed(0)}'
        : '${hour.toStringAsFixed(0)}';
    String minuteString = minutes < 10
        ? '0${minutes.toStringAsFixed(0)}'
        : '${minutes.toStringAsFixed(0)}';

    return hourString + ':' + minuteString;
  }

  int getHours() {
    int hour = this ~/ 60;
    return hour;
  }

  int getMinutes() {
    var minutes = this % 60;
    return minutes;
  }

  int getAllMinutes() {
    return (this.getHours() * 60) + this.getMinutes();
  }

  /// A method returns a human readable string representing a file this
  String toFileSize([int round = 2]) {
    /** 
   * [size] can be passed as number or as string
   *
   * the optional parameter [round] specifies the number 
   * of digits after comma/point (default is 2)
   */
    var divider = 1024;

    if (this < divider) {
      return '$this B';
    }

    if (this < divider * divider && this % divider == 0) {
      return '${(this / divider).toStringAsFixed(0)} KB';
    }

    if (this < divider * divider) {
      return '${(this / divider).toStringAsFixed(round)} KB';
    }

    if (this < divider * divider * divider && this % divider == 0) {
      return '${(this / (divider * divider)).toStringAsFixed(0)} MB';
    }

    if (this < divider * divider * divider) {
      return '${(this / divider / divider).toStringAsFixed(round)} MB';
    }

    if (this < divider * divider * divider * divider && this % divider == 0) {
      return '${(this / (divider * divider * divider)).toStringAsFixed(0)} GB';
    }

    if (this < divider * divider * divider * divider) {
      return '${(this / divider / divider / divider).toStringAsFixed(round)} GB';
    }

    if (this < divider * divider * divider * divider * divider &&
        this % divider == 0) {
      num r = this / divider / divider / divider / divider;
      return '${r.toStringAsFixed(0)} TB';
    }

    if (this < divider * divider * divider * divider * divider) {
      num r = this / divider / divider / divider / divider;
      return '${r.toStringAsFixed(round)} TB';
    }

    if (this < divider * divider * divider * divider * divider * divider &&
        this % divider == 0) {
      num r = this / divider / divider / divider / divider / divider;
      return '${r.toStringAsFixed(0)} PB';
    } else {
      num r = this / divider / divider / divider / divider / divider;
      return '${r.toStringAsFixed(round)} PB';
    }
  }
}
