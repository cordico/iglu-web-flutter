/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

extension NestedScrollFixExtension on Widget {
  Widget fixNestedDoubleScrollbar() {
    return NotificationListener<ScrollNotification>(
      onNotification: (notification) => true,
      child: this,
    );
  }
}
