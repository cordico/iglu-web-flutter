/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:intl/intl.dart';

class DateExtension {
  static String getHourString(String stringDate,
      {String format = 'yyyy-MM-ddTHH:mm:ss.SSSZ', bool toLocal = true}) {
    var date = getDate(stringDate, format: format, toLocal: toLocal);
    var hourString = "00";
    if ((date?.hour ?? 0) < 10) {
      hourString = "0${date?.hour.toString() ?? "00"}";
    } else {
      hourString = "${date?.hour.toString() ?? "00"}";
    }
    var minuteString = "00";
    if ((date?.minute ?? 0) < 10) {
      minuteString = "0${date?.minute.toString() ?? "00"}";
    } else {
      minuteString = "${date?.minute.toString() ?? "00"}";
    }
    var secondString = "00";
    if ((date?.second ?? 0) < 10) {
      secondString = "0${date?.second.toString() ?? "00"}";
    } else {
      secondString = "${date?.second.toString() ?? "00"}";
    }
    return "$hourString:$minuteString:$secondString";
  }

  static String getHourStringFromDate(DateTime date) {
    return "${date.hour}:${date.minute}:${date.second}";
  }

  static DateTime toLocalTimeZone(DateTime date) {
    var hours = DateTime.now().timeZoneOffset.inHours;
    return date.subtract(Duration(hours: hours));
  }

  static DateTime formatDBDate(String dateString, {bool toLocal = true}) {
    DateFormat dateFormat = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSZ');
    DateTime date = dateFormat.parse(dateString, true);
    if (toLocal) {
      return date.toLocal();
    }
    return date;
  }

  static DateTime? getDate(String dateString,
      {String format = 'dd/MM/yyyy', bool toLocal = true}) {
    DateFormat dateFormat = DateFormat(format);
    try {
      DateTime date = dateFormat.parse(dateString, true);
      if (toLocal) {
        return date.toLocal();
      }
      return date;
    } catch (err) {
      return null;
    }
  }

  static FormattedDate returnFormattedDate(String dateString,
      {bool toLocal = true}) {
    DateFormat dateFormat = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSZ');
    DateTime date = dateFormat.parse(dateString, true);
    if (toLocal) {
      date = date.toLocal();
    }
    var secondsString = date.minute < 10 ? '0${date.second}' : '${date.second}';
    var minuteString = date.minute < 10 ? '0${date.minute}' : '${date.minute}';
    var hourString = date.hour < 10 ? '0${date.hour}' : '${date.hour}';
    var dayString = date.day < 10 ? '0${date.day}' : '${date.day}';
    var monthString = date.month < 10 ? '0${date.month}' : '${date.month}';
    return FormattedDate(
      seconds: secondsString,
      minutes: minuteString,
      hours: hourString,
      days: dayString,
      month: monthString,
      year: date.year.toString(),
    );
  }

  static String toDBString(
      {required DateTime date, String format = 'yyyy-MM-dd HH:mm:ss'}) {
    DateFormat dateFormat = DateFormat(format);
    return dateFormat.format(date);
  }

  static String toDBPerfectString(
      {required DateTime date, String format = 'yyyy-MM-ddTHH:mm:ss'}) {
    DateFormat dateFormat = DateFormat(format);
    return dateFormat.format(date);
  }

  static String formatDate(
      {DateTime? date,
      String format = 'dd/MM/yyyy',
      String? stringDate,
      bool toLocal = true}) {
    if (stringDate != null) {
      return DateExtension.formatDate(
          date: DateExtension.formatDBDate(stringDate, toLocal: toLocal),
          format: format);
    }
    return DateFormat(format).format(date!);
  }
}

class FormattedDate {
  String? seconds;
  String? minutes;
  String? hours;
  String? days;
  String? month;
  String? year;

  FormattedDate({
    this.seconds,
    this.minutes,
    this.hours,
    this.days,
    this.month,
    this.year,
  });
}
