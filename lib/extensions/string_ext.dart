/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

extension StringExtension on String? {
  bool get isEmail {
    var regExp = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    if (regExp.hasMatch(this!))
      return true;
    else
      return false;
  }

  int get wordsCount {
    var regExp = RegExp(r'[\w-]+');
    return regExp.allMatches(this!).length;
  }

  List<String> get words {
    var regExp = RegExp(r'[\w-]+');
    return regExp
        .allMatches(this!)
        .toList()
        .map((e) => this!.substring(e.start, e.end))
        .toList();
  }

  String capitalize() {
    return "${this![0].toUpperCase()}${this!.substring(1)}";
  }

  bool get isNumeric {
    if (this == null) {
      return false;
    }
    return double.tryParse(this!) != null;
  }

  String getOnlyNumbers() {
    String cleanedText = this!;
    if (cleanedText.isNotEmpty) {
      var onlyNumbersRegex = RegExp(r'[^\d]');
      cleanedText = cleanedText.replaceAll(onlyNumbersRegex, '');
    }
    return cleanedText;
  }

  double numberValue({int precision = 2}) {
    var numbers = getOnlyNumbers();
    if (numbers.isEmpty) {
      return 0.0;
    }
    List<String> parts = numbers.split('').toList(growable: true);
    if (parts.length > 1) {
      parts.insert(parts.length - precision, '.');
      return double.parse(parts.join());
    }
    return double.parse(parts.first);
  }

  Map<String, String>? getQueryParameters() {
    var map = Map<String, String>();
    if (this == null) {
      return null;
    }
    var arr = this!.split("?");
    if (arr.length > 1) {
      var first = arr.reversed.first;
      if (first.length > 0) {
        var queryElements = first.split("&");
        if (queryElements.isNotEmpty) {
          for (var i = 0; i < queryElements.length; i++) {
            var element = queryElements[i];
            var split = element.split("=");
            if (split.length > 1) {
              map[split[0]] = split[1];
            }
          }
          if (map.isEmpty) {
            return null;
          }
          return map;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  bool get hasURL {
    var regExp = RegExp(
      r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
      caseSensitive: false,
      multiLine: true,
    );
    return regExp.hasMatch(this!);
  }

  String get returnURLOrNull {
    var regExp = RegExp(
        r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
        caseSensitive: false,
        multiLine: true);
    var u = regExp.firstMatch(this!)!;
    String url = this!.substring(u.start, u.end);
    return url;
  }

  String get replaceURL {
    var regExp = RegExp(
      r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
      caseSensitive: false,
      multiLine: true,
    );
    var newString = this!;

    regExp.allMatches(this!).forEach((element) {
      String single = this!.substring(element.start, element.end);
      newString = newString.replaceFirst(single, '');
    });
    return newString;
  }

  String? get replaceFirstURL {
    var regExp = RegExp(
      r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
      caseSensitive: false,
      multiLine: true,
    );
    var newString = this!;
    var u = regExp.firstMatch(this!);
    if (u != null) {
      String url = this!.substring(u.start, u.end);
      newString = newString.replaceFirst(url, '');
      return newString;
    } else {
      return null;
    }
  }

  String? get getFirstURL {
    var regExp = RegExp(
      r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
      caseSensitive: false,
      multiLine: true,
    );
    var u = regExp.firstMatch(this!);
    if (u != null) {
      String url = this!.substring(u.start, u.end);
      return url.fullURL;
    } else {
      return null;
    }
  }

  String? get getNotFullFirstURL {
    var regExp = RegExp(
      r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?",
      caseSensitive: false,
      multiLine: true,
    );
    var u = regExp.firstMatch(this!);
    if (u != null) {
      String url = this!.substring(u.start, u.end);
      return url;
    } else {
      return null;
    }
  }

  String get fullURL {
    String url = this!;

    if (!this!.toLowerCase().startsWith('http://') &&
        !this!.toLowerCase().startsWith('https://')) {
      url = 'https://' + this!;
    }
    return url;
  }

  //PEOPLE TAG
  String? get getLastTaggable {
    var regExp = RegExp(
      //"(?!\\n)(?:^|\\s)(@([·・ー_ぁ-んァ-ンa-zA-Z0-9一-龠０-９ａ-ｚＡ-ＺáàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ]+))",
      r'([@])\w+',
      caseSensitive: false,
      multiLine: true,
    );
    var u = regExp.allMatches(this!);
    if (u.isNotEmpty) {
      String url = this!.substring(u.last.start, u.last.end);
      return url;
    }
    return null;
  }

  bool get isPeopleTag {
    var regExp = RegExp(
      "(?!\\n)(?:^|\\s)(@([·・ー_ぁ-んァ-ンa-zA-Z0-9一-龠０-９ａ-ｚＡ-ＺáàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ]+))",
      caseSensitive: false,
      multiLine: true,
    );
    return regExp.hasMatch(this!);
  }

  bool get isLastWordTaggable {
    List<String> a = this!.split(' ').toList();
    if (a.isNotEmpty) {
      return a.last.isPeopleTag;
    }
    return false;
  }

  //HASHTAG

  String? get getLastHashtagable {
    final regExp = RegExp(
        "(?!\\n)(?:^|\\s)(#([·・ー_ぁ-んァ-ンa-zA-Z0-9一-龠０-９ａ-ｚＡ-ＺáàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ]+))",
        multiLine: true,
        caseSensitive: false);

    var u = regExp.allMatches(this!);
    if (u.isNotEmpty) {
      String url = this!.substring(u.last.start, u.last.end);
      return url;
    }
    return null;
  }

  List<String> get getAllHashtagable {
    final regExp = RegExp(
        "(?!\\n)(?:^|\\s)(#([·・ー_ぁ-んァ-ンa-zA-Z0-9一-龠０-９ａ-ｚＡ-ＺáàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ]+))",
        multiLine: true,
        caseSensitive: false);

    List<String> matches = List<String>.from([], growable: true);

    regExp.allMatches(this!).forEach((element) {
      String word =
          this!.substring(element.start, element.end).replaceAll(' ', '');
      matches.add(word.trim());
    });
    return matches;
  }

  bool get isHashtag {
    final regExp = RegExp(
        "(?!\\n)(?:^|\\s)(#([·・ー_ぁ-んァ-ンa-zA-Z0-9一-龠０-９ａ-ｚＡ-ＺáàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ]+))",
        multiLine: true,
        caseSensitive: false);
    return regExp.hasMatch(this!);
  }

  bool get isLastWordHashtag {
    List<String> a = this!.split(' ').toList();
    if (a.isNotEmpty) {
      return a.last.isHashtag;
    }
    return false;
  }

  Size getSize(
      {required BuildContext context,
      required TextStyle style,
      double minWidth = 0.0,
      double maxWidth = double.infinity,
      int maxLines = 1,
      double textScaleFactor = 1,
      TextDirection textDirection = TextDirection.ltr}) {
    return (TextPainter(
            text: TextSpan(text: this, style: style),
            maxLines: maxLines,
            textScaleFactor: textScaleFactor,
            textDirection: textDirection)
          ..layout(minWidth: minWidth, maxWidth: maxWidth))
        .size;
  }

  Color? getColor({Color? defaultColor}) {
    if (this != null) {
      var hex = this!.replaceAll("#", "");
      if (hex.length == 6) {
        return Color(int.parse("0xFF" + hex));
      } else if (hex.length == 8) {
        return Color(int.parse("0x" + hex));
      } else {
        if (defaultColor != null) {
          return defaultColor;
        }
        return null;
      }
    } else {
      if (defaultColor != null) {
        return defaultColor;
      }
      return null;
    }
  }
}
