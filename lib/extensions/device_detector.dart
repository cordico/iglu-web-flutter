/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

import 'package:flutter/widgets.dart';

enum ScreenSizeType { L, M, S }

//COPIED FROM APP CENTER WEBSITE
class SizeDetector {
  static bool isTooSmall(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return width <= 300;
  }

  static bool isMini(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return width <= 320;
  }

  static bool isMobile(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return MediaQuery.of(context).size.width <= 425;
  }

  static double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static bool isLargeScreen(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return width >= 1290.0;
  }

  static bool isMediumScreen(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return width > 768.0 && width < 1290.0;
  }

  static bool isSmallScreen(BuildContext context, {double? width}) {
    if (width == null || width == double.infinity) {
      width = MediaQuery.of(context).size.width;
    }
    return width <= 768.0;
  }
}
