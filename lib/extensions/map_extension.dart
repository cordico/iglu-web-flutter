/// IGLU WEB FLUTTER
///
/// Copyright © 2020 - 2021 IGLU. All rights reserved.
/// Copyright © 2020 - 2021 IGLU
///

void removeNullAndEmptyParams(Map<dynamic, dynamic> mapToEdit) {
  final keys = mapToEdit.keys.toList(growable: false);
  for (final dynamic key in keys) {
    final dynamic value = mapToEdit[key];
    if (value == null) {
      mapToEdit.remove(key);
    } else if (value is String) {
      if (value.isEmpty) {
        mapToEdit.remove(key);
      }
    } else if (value is Map) {
      removeNullAndEmptyParams(value);
      if (value.isEmpty) {
        mapToEdit.remove(key);
      }
    } else if (value is List<Map>) {
      for (final element in value) {
        removeNullAndEmptyParams(element);
      }
      if (value.isEmpty) {
        mapToEdit.remove(key);
      }
    }
  }
}
