/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/widgets.dart";
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

extension GlobalKeyEx on GlobalKey {
  Rect? get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null && renderObject?.paintBounds != null) {
      return renderObject?.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else {
      return null;
    }
  }
}

extension BuildContextEx on BuildContext {
  Rect? get globalPaintBounds {
    final renderObject = findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null && renderObject?.paintBounds != null) {
      return renderObject?.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else {
      return null;
    }
  }

  SideItem? sideItem({String? location}) {
    var settings = location ??
        webConfigurator
            .kingRouteDelegate.currentConfiguration?.routeInformation.location;
    return webConfigurator.sideItemManager.sideItems.hasPath(settings);
  }
}
