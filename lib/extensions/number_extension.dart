/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

extension Range on num {
  bool isBetween(num from, num to) {
    return from < this && this < to;
  }
}
