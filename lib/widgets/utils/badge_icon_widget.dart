/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class BadgeIconWidget extends StatelessWidget {
  BadgeIconWidget({this.buttonView, this.badgeCounter, this.iconSize});
  final double? iconSize;
  final int? badgeCounter;
  final IGButtonView? buttonView;

  @override
  Widget build(BuildContext context) {
    return badgeCounter != null && badgeCounter! > 0
        ? Stack(
            children: <Widget>[
              Container(height: iconSize! + 12, width: iconSize! + 12),
              Positioned(left: 0, bottom: 0, child: buttonView!),
              if (badgeCounter != null)
                Positioned(
                  right: 0,
                  top: 0,
                  child: Container(
                    width: iconSize! - 6.0,
                    height: iconSize! - 6.0,
                    child: Center(
                      child: Text(
                        "$badgeCounter",
                        style: IGTextStyles.boldStyle(10, Colors.white),
                      ),
                    ),
                    decoration: BoxDecoration(
                      color: IGColors.primary(),
                      borderRadius: BorderRadius.circular(iconSize! / 2),
                    ),
                  ),
                )
            ],
          )
        : buttonView!;
  }
}
