/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class LogoImage extends StatelessWidget {
  final double height;
  final double width;
  final bool lightOnly;
  final bool darkOnly;
  final Function()? onTap;
  final bool vibrate;
  final Color? defaultColor;

  LogoImage(
      {this.height = 85,
      this.width = 50,
      this.lightOnly = false,
      this.darkOnly = false,
      this.vibrate = false,
      this.onTap,
      this.defaultColor});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap != null
          ? () async {
              await HapticFeedback.vibrate();
              onTap!();
            }
          : null,
      child: Container(
        height: height + 6.0,
        width: width + 6.0,
        child: webConfigurator.internalConfigurator.assetPathLight != null ||
                webConfigurator.internalConfigurator.assetPathDark != null
            ? Image.asset(
                webConfigurator.currentBrightness == Brightness.dark || darkOnly
                    ? lightOnly && !darkOnly
                        ? webConfigurator.internalConfigurator.assetPathLight ??
                            webConfigurator.internalConfigurator.assetPathDark!
                        : webConfigurator.internalConfigurator.assetPathDark ??
                            webConfigurator.internalConfigurator.assetPathLight!
                    : webConfigurator.internalConfigurator.assetPathLight ??
                        webConfigurator.internalConfigurator.assetPathDark!,
                height: height,
                width: width,
                fit: BoxFit.contain,
              )
            : Container(),
      ),
    );
  }
}
