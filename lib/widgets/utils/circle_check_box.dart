/// IGLU WEB FLUTTER
///
/// Copyright © 2021 - 2021 IGLU. All rights reserved.
/// Copyright © 2021 - 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class CircleCheckBox extends StatelessWidget {
  final double size;
  final bool? value;
  final Function(bool)? onValueChanged;
  final double padding;
  final Color? color;
  final Color? disabledColor;

  CircleCheckBox(
      {this.size = 20,
      this.value,
      this.onValueChanged,
      this.padding = 1,
      this.color,
      this.disabledColor});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onValueChanged == null
          ? null
          : () {
              onValueChanged!(!value!);
            },
      child: Container(
        width: size,
        height: size,
        padding: EdgeInsets.all(size / 8.0),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(size),
          border: Border.all(
              color: onValueChanged == null
                  ? disabledColor ?? IGColors.primary()
                  : color ?? IGColors.primary(),
              width: 2.0),
        ),
        child: value!
            ? Container(
                decoration: BoxDecoration(
                  color: onValueChanged == null
                      ? disabledColor ?? IGColors.primary()
                      : color ?? IGColors.primary(),
                  borderRadius: BorderRadius.circular(size),
                ),
              )
            : null,
      ),
    );
  }
}
