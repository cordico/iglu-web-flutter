/// IGLU WEB FLUTTER
///
/// Copyright © 2021 - 2021 IGLU. All rights reserved.
/// Copyright © 2021 - 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class TestDataBoxWidget extends StatelessWidget {
  TestDataBoxWidget();

  @override
  Widget build(BuildContext context) {
    return isDebugging
        ? FittedBox(
            child: Container(
              height: 15,
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(4),
                      bottomRight: Radius.circular(4)),
                  color: IGColors.orange()),
              child: Text(
                "TEST DATA",
                style: IGTextStyles.normalStyle(9, Colors.white),
              ),
            ),
          )
        : Container(
            height: 15,
            color: Colors.transparent,
          );
  }
}
