/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/foundation.dart';
import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:file_picker/file_picker.dart';
import "package:intl/intl.dart" as intl;

class DetailView extends StatefulWidget {
  final List<Widget>? actions;
  final bool showEditAction;
  final String? title;
  final Widget? header;
  final bool showHeader;
  final bool showLoading;
  final Widget? loadingWidget;
  final List<DetailViewCellRule> Function(BoxConstraints) cellRules;
  final Function()? editActionTap;
  final EdgeInsetsGeometry cardMargin;
  final Color? shadowColor;
  final double? elevation;
  final ShapeBorder? shape;
  final bool borderOnForeground;
  final Color? cardColor;
  final BorderRadius? borderRadius;
  final bool showDivider;
  final bool showBorder;
  DetailView(
      {this.actions,
      this.showEditAction = true,
      this.showLoading = false,
      this.loadingWidget,
      this.header,
      this.title = "Detail",
      this.editActionTap,
      this.cardMargin = EdgeInsets.zero,
      required this.cellRules,
      this.showHeader = true,
      this.shadowColor,
      this.elevation,
      this.borderOnForeground = true,
      this.showDivider = false,
      this.showBorder = false,
      this.borderRadius,
      this.cardColor,
      this.shape});
  @override
  _DetailViewState createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
        decoration: widget.showBorder
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius),
                border: Border.all(
                    color: widget.shadowColor ?? IGColors.separator()))
            : null,
        child: Card(
            color: widget.cardColor ?? IGColors.secondaryBackground(),
            borderOnForeground: widget.borderOnForeground,
            shadowColor: widget.shadowColor,
            elevation: widget.showBorder ? 0 : widget.elevation ?? 1.5,
            shape: widget.shape ??
                RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(kButtonPrimaryCornerRadius)),
            margin: widget.cardMargin,
            child: ClipRRect(
              borderRadius: widget.borderRadius ??
                  BorderRadius.circular(kButtonPrimaryCornerRadius),
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 0,
                      width: constraints.maxWidth,
                      child: widget.showLoading
                          ? widget.loadingWidget == null
                              ? LinearProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      webConfigurator.currentBrightness ==
                                              Brightness.light
                                          ? IGColors.primary()
                                          : Colors.white),
                                  backgroundColor:
                                      webConfigurator.currentBrightness ==
                                              Brightness.light
                                          ? IGColors.primary(opacity: 0.5)
                                          : Colors.white,
                                  minHeight: 3,
                                )
                              : widget.loadingWidget!
                          : Container()),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                          widget.showHeader
                              ? Container(
                                  height: 80,
                                  child: Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(20, 0, 20, 0),
                                      child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Expanded(child: _title()),
                                            Container(width: 30),
                                            _actions(),
                                          ])))
                              : Container(),
                        ] +
                        [
                          widget.showDivider
                              ? Divider(
                                  thickness: 1,
                                  height: 1,
                                  color: IGColors.separator())
                              : Container()
                        ] +
                        widget
                            .cellRules(constraints)
                            .map((e) => e.widget(context: context))
                            .toList(),
                  ),
                ],
              ),
            )),
      );
    });
  }

  _title() {
    return widget.header ??
        AutoSizeText(widget.title ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: IGTextStyles.boldStyle(
              28,
              IGColors.text(),
            ));
  }

  _actions() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
                  widget.showEditAction
                      ? IGButtonViewDefaults
                          .secondaryOnLargeLargeBackgroundOtherwise(
                              context: context,
                              style: IGButtonViewStyle(
                                  text: "Edit",
                                  automaticallyResize: false,
                                  showLoadingDuringAction: false,
                                  icon: Icons.edit_rounded,
                                  onTapPositioned: (position) async {
                                    if (widget.editActionTap != null) {
                                      await widget.editActionTap!();
                                    }
                                  }))
                      : Container(),
                ] +
                (!widget.showEditAction &&
                        widget.actions != null &&
                        widget.actions!.isNotEmpty
                    ? widget.actions!
                    : [Container()]),
          ),
        ).fixNestedDoubleScrollbar(),
      ],
    );
  }
}

//TODO: REPLACE WITH COLUMN LAYOUT AND REMOVE EVERY
//MARK: CELL RULES
class DetailViewCellRule<T> {
  //FUNCTIONS
  Future<void> Function()? onDelete;
  Function(dynamic)? onTap;
  Function(RelativeRect)? onTapPositioned;
  Function()? onTapPlaceholder;
  Future<bool> Function(dynamic)? onUpload;

  //BUTTON TYPE
  IconData? icon;
  BoxBorder? border;
  List<BoxShadow>? boxShadow;
  BorderRadiusGeometry? customBorderRadius;
  bool? showAnimatedArrow;
  bool? showIconOnLeft;
  bool? showIconOnRight;
  double iconSize;
  EdgeInsetsGeometry? buttonInternalPadding;
  bool showLoadingDuringAction;
  bool forceLoading;
  EdgeInsetsGeometry? externalPadding;
  bool automaticallyResize;

  //STARS TYPE
  int? stars;

  //TABBAR TYPE
  List<DetailViewCellTabItem>? tabItems = [];
  int? selectedIndex;
  Function(int)? onTapTabBarItem;

  //ICON TYPE
  Icon? iconIconType;

  //IMAGE TYPE
  bool showLoading;
  double? placeholderCornerRadius;
  PlatformFile? fileImage;
  String? imageUrl;
  Widget? placeholderWidget;
  bool showFullScreen;
  bool isFullScreen;
  BoxFit? fit;
  String? heroTag;

  //ALL TYPES
  GlobalKey? containerKey;
  TextStyle? textStyle;
  double? borderRadius;
  EdgeInsetsGeometry contentPadding;
  EdgeInsetsGeometry internalPadding;
  double height;
  double width;
  Color? backgroundColor;
  Color? tintColor;
  Color? hoverColor;
  String? content;
  MainAxisAlignment alignment;
  CrossAxisAlignment crossAxisAlignment;
  bool isScrollable;
  bool forceNoScroll;
  BoxConstraints? constraints;
  List<DetailViewCellRule>? subElements;
  String? key;
  DetailViewCellRuleType type;

  //RICHTEXT TYPE
  TextSpan? textSpan;

  //TEXT TYPE
  TextAlign? textAlign;

  //LINK TYPE
  String? tooltipMessage;
  String? urlToOpen;
  Color? backgroundHoverColor;
  TextStyle? customTextStyle;
  bool showSelection;

  //DIVIDER TYPE
  double thickness;

  //CUSTOM TYPE
  Widget? customWidget;
  bool forceOnlyCustom;

  //TEXTFORMFIELD TYPE
  FormTextFormField? textFormField;

  //CURRENCY TYPE
  double? price;
  intl.NumberFormat? numberFormat;
  String currency;

  //BOOL TYPE
  bool? valueBool;
  bool isBoolEnabled;
  Function(bool)? onChanged;

  //MOBILE
  bool reverseSubElementsOrderOnMobile;
  bool reverseSubElementsOrderOnTablet;
  bool transformHorizontalInVerticalOnMobile;
  bool transformHorizontalInVerticalOnTablet;

  //INKWELL
  DetailViewCellRule? childContainer;

  int? flex;
  BoxConstraints Function(BoxConstraints)? elaborateConstraints;
  MainAxisSize mainAxisSize;

  BoxConstraints? outSideConstraints;

  bool onlyWidget;

  bool absorbing;

  DetailViewCellRule(
      {this.key,
      this.containerKey,
      required this.type,
      this.customWidget,
      this.alignment = MainAxisAlignment.start,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      this.backgroundColor,
      this.tintColor,
      this.hoverColor,
      this.content,
      this.stars,
      this.textStyle,
      this.borderRadius,
      this.customBorderRadius,
      this.icon,
      this.showAnimatedArrow,
      this.showIconOnLeft,
      this.iconSize = 24,
      this.buttonInternalPadding,
      this.externalPadding,
      this.showIconOnRight,
      this.backgroundHoverColor,
      this.contentPadding = EdgeInsets.zero,
      this.internalPadding = EdgeInsets.zero,
      this.height = 20,
      this.customTextStyle,
      this.width = 0,
      this.onDelete,
      this.onTap,
      this.onTapPlaceholder,
      this.childContainer,
      this.onUpload,
      this.border,
      this.subElements,
      this.fileImage,
      this.iconIconType,
      this.imageUrl,
      this.boxShadow,
      this.constraints,
      this.placeholderWidget,
      this.selectedIndex,
      this.onTapTabBarItem,
      this.tabItems = const [],
      this.textAlign,
      this.placeholderCornerRadius,
      this.showLoading = false,
      this.isScrollable = false,
      this.forceNoScroll = false,
      this.tooltipMessage,
      this.urlToOpen,
      this.thickness = 1,
      this.textFormField,
      this.isFullScreen = false,
      this.absorbing = false,
      this.showFullScreen = false,
      this.numberFormat,
      this.currency = "€",
      this.price,
      this.valueBool,
      this.fit,
      this.forceOnlyCustom = false,
      this.isBoolEnabled = false,
      this.reverseSubElementsOrderOnMobile = false,
      this.reverseSubElementsOrderOnTablet = false,
      this.transformHorizontalInVerticalOnMobile = false,
      this.transformHorizontalInVerticalOnTablet = false,
      this.showSelection = false,
      this.flex,
      this.elaborateConstraints,
      this.mainAxisSize = MainAxisSize.max,
      this.textSpan,
      this.heroTag,
      this.onChanged,
      this.outSideConstraints,
      this.onTapPositioned,
      this.showLoadingDuringAction = false,
      this.forceLoading = false,
      this.onlyWidget = false,
      this.automaticallyResize = true}) {
    this.key = this.key ?? StringUtils.getRandString(10);
  }

  Widget widget(
      {required BuildContext context,
      BoxConstraints? constraintsCalculated,
      bool isSubElement = false}) {
    if (constraintsCalculated != null && elaborateConstraints != null) {
      constraints = elaborateConstraints!(constraintsCalculated);
    }
    Widget child = Container();
    Widget subChild = Container();
    var mountSubChild = false;
    var hideConstraints = false;
    var isTansformHorizontalInVerticalOnMobile =
        (type == DetailViewCellRuleType.multiple_horizontal &&
            transformHorizontalInVerticalOnMobile &&
            SizeDetector.isSmallScreen(context,
                width: outSideConstraints?.maxWidth));
    var isTansformHorizontalInVerticalOnTablet =
        (type == DetailViewCellRuleType.multiple_horizontal &&
            transformHorizontalInVerticalOnTablet &&
            SizeDetector.isMediumScreen(context,
                width: outSideConstraints?.maxWidth));

    /*if (type == DetailViewCellRuleType.button) {
      subChild = IGButtonView(
          options: IGButtonViewStyle.initialValues().copyWith(
              style: IGButtonViewStyle(
                  automaticallyResize: automaticallyResize,
                  showLoadingDuringAction: showLoadingDuringAction,
                  forceLoading: forceLoading,
                  onTapPositioned: onTapPositioned,
                  boxShadow: boxShadow,
                  border: border,
                  customBorderRadius: customBorderRadius,
                  externalPadding: externalPadding,
                  backgroundColor: backgroundColor,
                  hoverColor: backgroundColor != null
                      ? backgroundColor.withOpacity(0.8)
                      : null,
                  textColor: tintColor,
                  text: content,
                  textStyle: textStyle,
                  icon: icon,
                  iconSize: iconSize,
                  internalPadding: buttonInternalPadding,
                  showAnimatedArrow: showAnimatedArrow,
                  showIconOnLeft: showIconOnLeft,
                  showIconOnRight: showIconOnRight,
                  borderRadius: borderRadius)));
      mountSubChild = true;
    } else*/
    if (type == DetailViewCellRuleType.stars) {
      List<Widget> starWidgets = [];
      for (var i = 0; i < stars!; i++) {
        starWidgets
            .add(Icon(Icons.star_rounded, color: tintColor, size: height));
      }
      child = Container(
        constraints: constraints ?? null,
        width: constraints != null ? constraints!.minWidth : null,
        child: Row(
          mainAxisSize: mainAxisSize,
          mainAxisAlignment: alignment,
          children: starWidgets,
        ),
      );
    } else if (type == DetailViewCellRuleType.text) {
      Widget _c = SelectableText(content ?? "",
          toolbarOptions: ToolbarOptions(copy: true, selectAll: true),
          style: textStyle,
          textAlign: textAlign);
      if (!showSelection) {
        _c = Text(content ?? "", style: textStyle, textAlign: textAlign);
      }
      hideConstraints = true;
      subChild = constraints != null ? Expanded(child: _c) : _c;
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.richtext) {
      Widget _c = SelectableText.rich(textSpan!,
          toolbarOptions: ToolbarOptions(copy: true, selectAll: true),
          style: textStyle,
          textAlign: textAlign);
      if (!showSelection) {
        _c = RichText(text: textSpan!, textAlign: textAlign!);
      }
      hideConstraints = true;
      subChild = constraints != null ? Expanded(child: _c) : _c;
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.currency) {
      var fo = numberFormat ??
          intl.NumberFormat.compactCurrency(symbol: currency, decimalDigits: 2);
      Widget _c = SelectableText("${price != null ? fo.format(price) : ""}",
          toolbarOptions: ToolbarOptions(copy: true, selectAll: true),
          style: textStyle,
          textAlign: textAlign);
      if (!showSelection) {
        _c = Text("${price != null ? fo.format(price) : ""}",
            style: textStyle, textAlign: textAlign);
      }
      hideConstraints = true;
      subChild = constraints != null ? Expanded(child: _c) : _c;
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.text_link) {
      var _c = FittedBox(
        child: Link(
          uri: Uri.parse(urlToOpen ?? ""),
          target: LinkTarget.blank,
          builder: (BuildContext context, FollowLink? followLink) =>
              IGButtonViewDefaults.defaultTextHoverButton(
                  context: context,
                  style: IGButtonViewStyle(
                      text: content ?? "",
                      tooltipMessage: tooltipMessage,
                      externalPadding: EdgeInsets.zero,
                      internalPadding: EdgeInsets.zero,
                      height: 20,
                      textStyle: customTextStyle,
                      onTapPositioned: (_) async {
                        if (onTap != null) {
                          onTap!(null);
                        } else if (urlToOpen != null) {
                          followLink!();
                        }
                      })),
        ),
      );
      subChild = _c;
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.text_tag) {
      subChild = Flexible(
        fit: FlexFit.loose,
        child: FittedBox(
          child: IGButtonViewDefaults.primaryTagBackgroundButton(
              context: context,
              style: IGButtonViewStyle(text: content ?? ""),
              tagColor: backgroundColor),
        ),
      );
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.bool) {
      subChild = Container(
          height: isBoolEnabled ? null : 10,
          child: AbsorbPointer(
              absorbing: !isBoolEnabled,
              child: Switch(
                value: valueBool!,
                onChanged: onChanged,
                inactiveTrackColor: isBoolEnabled || !valueBool!
                    ? null
                    : (tintColor != null
                        ? tintColor!.withOpacity(0.5)
                        : IGColors.primary(opacity: 0.5)),
                inactiveThumbColor: isBoolEnabled || !valueBool!
                    ? null
                    : (tintColor ?? IGColors.primary()),
                activeColor: tintColor ?? IGColors.primary(),
              )));
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.separator) {
      child = Divider(
          height: height,
          thickness: thickness,
          color: IGColors.separator(),
          endIndent: (internalPadding as EdgeInsets).left,
          indent: (internalPadding as EdgeInsets).right);
    } else if (type == DetailViewCellRuleType.space) {
      child = Container(height: height, width: width);
    } else if (type == DetailViewCellRuleType.spacer) {
      child = Spacer();
    } else if (type == DetailViewCellRuleType.inkwell) {
      child = InkWell(
        onTap: onTap != null
            ? () {
                onTap!(null);
              }
            : null,
        child: childContainer!.widget(context: context),
      );
    } else if (type == DetailViewCellRuleType.multiple_vertical ||
        isTansformHorizontalInVerticalOnMobile ||
        isTansformHorizontalInVerticalOnTablet) {
      var _c = Column(
          mainAxisSize: mainAxisSize,
          mainAxisAlignment: alignment,
          crossAxisAlignment: crossAxisAlignment,
          children: (reverseSubElementsOrderOnMobile &&
                  (SizeDetector.isSmallScreen(context,
                          width: outSideConstraints?.maxWidth) ||
                      (reverseSubElementsOrderOnTablet &&
                          SizeDetector.isMediumScreen(context,
                              width: outSideConstraints?.maxWidth))))
              ? subElements!.reversed
                  .map((e) => e.widget(context: context, isSubElement: true))
                  .toList()
              : subElements!
                  .map((e) => e.widget(context: context, isSubElement: true))
                  .toList());
      child = isScrollable ? SingleChildScrollView(child: _c) : _c;
    } else if (type == DetailViewCellRuleType.multiple_horizontal) {
      var _c = LayoutBuilder(builder: (ctx, con) {
        var constraintsCalculated =
            !con.hasInfiniteWidth || !con.hasInfiniteHeight ? con : null;
        return Row(
            mainAxisSize: mainAxisSize,
            mainAxisAlignment: alignment,
            crossAxisAlignment: crossAxisAlignment,
            children: (reverseSubElementsOrderOnMobile &&
                    (SizeDetector.isSmallScreen(context,
                            width: outSideConstraints?.maxWidth) ||
                        (reverseSubElementsOrderOnTablet &&
                            SizeDetector.isMediumScreen(context,
                                width: outSideConstraints?.maxWidth))))
                ? subElements!.reversed
                    .map((e) => e.widget(
                        context: context,
                        constraintsCalculated: constraintsCalculated,
                        isSubElement: true))
                    .toList()
                : subElements!
                    .map((e) => e.widget(
                        context: context,
                        constraintsCalculated: constraintsCalculated,
                        isSubElement: true))
                    .toList());
      });
      child = (isScrollable || SizeDetector.isMini(context)) && !forceNoScroll
          ? SingleChildScrollView(scrollDirection: Axis.horizontal, child: _c)
              .fixNestedDoubleScrollbar()
          : _c;
    } else if (type == DetailViewCellRuleType.tabbar && tabItems!.isNotEmpty) {
      List<Widget> widgets = [];
      tabItems!.asMap().forEach((index, value) {
        widgets.add(IGButtonViewDefaults.onlyBottomBorderButtonPrimary(
            context: context,
            isSelected: selectedIndex == index,
            style: IGButtonViewStyle(
                onTapPositioned: (position) {
                  if (onTapTabBarItem != null) {
                    onTapTabBarItem!(index);
                  }
                },
                text: value.title,
                icon: value.icon)));
      });
      child = Container(
        child: Row(
            mainAxisSize: mainAxisSize,
            mainAxisAlignment: alignment,
            crossAxisAlignment: crossAxisAlignment,
            children: widgets),
      );
    } else if (type == DetailViewCellRuleType.image) {
      subChild = ImageContainerView(
        heroTag: heroTag,
        fileImage: fileImage,
        imageUrl: imageUrl,
        fit: fit,
        absorbing: absorbing,
        constraints: constraints,
        placeholderCornerRadius: placeholderCornerRadius,
        borderRadius: borderRadius,
        showLoading: showLoading,
        internalPadding: internalPadding,
        placeholderWidget: placeholderWidget,
        onTap: onTap,
        onDelete: onDelete as Future<bool> Function()?,
        onTapPlaceholder: onTapPlaceholder,
        onUpload: onUpload,
        backgroundColor: backgroundColor,
        isFullScreen: isFullScreen,
        showFullScreen: showFullScreen,
      );
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.icon) {
      subChild = iconIconType ?? Container();
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.textformfield) {
      subChild = textFormField ?? Container();
      mountSubChild = true;
    } else if (type == DetailViewCellRuleType.custom && customWidget != null) {
      subChild = customWidget ?? Container();
      child = customWidget ?? Container();
      mountSubChild = !forceOnlyCustom;
      if (onlyWidget) {
        return customWidget ?? Container();
      }
    }
    var finalWidget = Container(
        key: containerKey,
        constraints: hideConstraints ? null : constraints,
        child: mountSubChild
            ? isSubElement
                ? subChild
                : Container(
                    child: Row(
                      mainAxisSize: mainAxisSize,
                      mainAxisAlignment: alignment,
                      children: [subChild],
                    ),
                  )
            : child,
        padding: contentPadding == EdgeInsets.zero ? null : contentPadding);
    if (flex != null) {
      return Flexible(key: containerKey, flex: flex!, child: finalWidget);
    }
    return finalWidget;
  }

  @override
  bool operator ==(dynamic o) => o is DetailViewCellRule && o.key == key;

  @override
  int get hashCode => key.hashCode;
}

extension DetailViewCellRuleArray on List<DetailViewCellRule> {
  DetailViewCellRule? hasKey(String key) {
    var result;
    forEach((element) {
      if (element.key == key) {
        result = element;
      }
    });
    return result;
  }
}

class DetailViewCellTabItem {
  String title;
  String? exportTitle;
  IconData? icon;
  DetailViewCellTabItem({required this.title, this.exportTitle, this.icon});
}

enum DetailViewCellRuleType {
  //button,
  text,
  richtext,
  stars,
  separator,
  space,
  multiple_vertical,
  multiple_horizontal,
  spacer,
  text_tag,
  text_link,
  image,
  icon,
  tabbar,
  textformfield,
  currency,
  bool,
  inkwell,
  custom
}

class DetailViewCellRulePreBuildInfo {
  final String key;
  final String? value;
  final TextStyle? keyStyle;
  final TextStyle? valueStyle;

  ///IS TAG VALUES ONLY
  final bool isTag;
  final double borderRadius;
  final Color? backgroundColor;
  final EdgeInsetsGeometry internalPadding;

  ///IS STARS VALUES ONLY
  final bool isStars;
  final double? stars;

  final Color? primaryColor;

  final bool isLink;
  final bool isMail;
  final bool isCopy;
  final String? tooltipMessage;
  final String? urlToOpen;

  final bool showSelection;

  final Function? onTap;

  final Map<ScreenSizeType, int> flexForScreenSize;

  int? flex({required BuildContext context}) {
    if (SizeDetector.isLargeScreen(context)) {
      return flexForScreenSize[ScreenSizeType.L];
    } else if (SizeDetector.isMediumScreen(context)) {
      return flexForScreenSize[ScreenSizeType.M];
    } else if (SizeDetector.isSmallScreen(context)) {
      return flexForScreenSize[ScreenSizeType.S];
    }
    return 1;
  }

  DetailViewCellRulePreBuildInfo(
      {required this.key,
      this.value,
      this.keyStyle,
      this.valueStyle,
      this.backgroundColor,
      this.borderRadius = 2,
      this.isTag = false,
      this.internalPadding = const EdgeInsets.all(5),
      this.isStars = false,
      this.stars,
      this.primaryColor,
      this.isLink = false,
      this.isMail = false,
      this.isCopy = false,
      this.tooltipMessage,
      this.onTap,
      this.showSelection = true,
      this.urlToOpen,
      this.flexForScreenSize = const {
        ScreenSizeType.L: 5,
        ScreenSizeType.M: 5,
        ScreenSizeType.S: 5
      }});
}

extension DetailViewCellRulePreBuild on DetailViewCellRule {
  static List<DetailViewCellRule> formSheetSectionTitle(
      {required BuildContext context,
      required BoxConstraints boxConstraints,
      required String title,
      Color? titleColor,
      required IconData icon,
      TextStyle? textStyle,
      bool showSeparator = true,
      Color? iconColor}) {
    return [
      DetailViewCellRule(type: DetailViewCellRuleType.space, height: 20),
      DetailViewCellRule(
          type: DetailViewCellRuleType.multiple_horizontal,
          alignment: MainAxisAlignment.start,
          contentPadding: EdgeInsets.symmetric(horizontal: 20),
          forceNoScroll: true,
          subElements: [
            DetailViewCellRule(
                type: DetailViewCellRuleType.icon,
                contentPadding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                iconIconType:
                    Icon(icon, size: 30, color: iconColor ?? IGColors.text())),
            DetailViewCellRule(
                type: DetailViewCellRuleType.text,
                constraints:
                    BoxConstraints(maxWidth: boxConstraints.maxWidth - 80),
                content: title,
                textStyle: textStyle ??
                    IGTextStyles.boldStyle(22, titleColor ?? IGColors.text()))
          ]),
      if (showSeparator)
        DetailViewCellRule(
            type: DetailViewCellRuleType.separator,
            internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0))
    ];
  }

  static FormTextFormField yesOrNoTextFormFieldItemsProportionally(
      {required BuildContext context,
      required bool initialValue,
      required String labelText,
      required IGFormContainerState formContainerState,
      required Function(bool, bool) onChanged,
      TextInputAction textInputAction = TextInputAction.next}) {
    return FormTextFormField(
      keyboardType: TextInputType.text,
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      onChanged: (value, isReal) {
        var e = YesOrNoEnum.values
            .firstWhereOrNull((e) => describeEnum(e).titleCase == value);
        if (e == YesOrNoEnum.yes) {
          onChanged(true, isReal);
        } else {
          onChanged(false, isReal);
        }
      },
      readOnly: true,
      formContainerState: formContainerState,
      showMenuOptions: true,
      initialValue:
          initialValue ? YesOrNoEnum.yes.asString() : YesOrNoEnum.no.asString(),
      labelText: labelText,
      textInputAction: textInputAction,
      formFieldValidator: IGFormFieldValidator(
          isOptional: true,
          inArray:
              YesOrNoEnum.values.map((e) => describeEnum(e).titleCase).toList(),
          type: IGFormFieldValidatorType.array),
      menuOptions:
          YesOrNoEnum.values.map((e) => describeEnum(e).titleCase).toList(),
    );
  }

  static FormTextFormField enumTextFormFieldItemsProportionally(
      {required BuildContext context,
      required String? initialValue,
      required String labelText,
      required List<Enum> values,
      required IGFormContainerState formContainerState,
      required Function(Enum?, bool) onChanged,
      bool isOptional = true,
      String Function(Enum)? transform,
      TextInputAction textInputAction = TextInputAction.next}) {
    String Function(Enum) getValue = (en) {
      if (transform != null) {
        return transform(en);
      }
      return describeEnum(en).titleCase;
    };

    return FormTextFormField(
      keyboardType: TextInputType.text,
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      onChanged: (value, isReal) {
        onChanged(values.firstWhereOrNull((e) => getValue(e) == value), isReal);
      },
      readOnly: true,
      formContainerState: formContainerState,
      showMenuOptions: true,
      initialValue: initialValue,
      labelText: labelText,
      textInputAction: textInputAction,
      formFieldValidator: IGFormFieldValidator(
          isOptional: isOptional,
          inArray: values.map((e) => getValue(e)).toList(),
          type: IGFormFieldValidatorType.array),
      menuOptions: values.map((e) => getValue(e)).toList(),
    );
  }

  static List<DetailViewCellRule> jsonTextField(
      {required BuildContext context,
      required IGFormContainerState? formContainerState,
      required String json,
      required Function(RelativeRect) beautifyHandler,
      required Function(String?, bool) onChanged,
      Function(String)? onFieldSubmitted,
      TextInputAction? textInputAction,
      String labelText = "Schema"}) {
    return [
      DetailViewCellRule<dynamic>(
          type: DetailViewCellRuleType.multiple_horizontal,
          forceNoScroll: true,
          alignment: MainAxisAlignment.end,
          contentPadding: const EdgeInsets.symmetric(horizontal: 20),
          subElements: [
            DetailViewCellRule<dynamic>(
              type: DetailViewCellRuleType.custom,
              customWidget: IGButtonViewDefaults.primaryTextHoverButton(
                  context: context,
                  style: IGButtonViewStyle(
                      text: "Beautify",
                      externalPadding: EdgeInsets.zero,
                      internalPadding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                      iconColor: IGColors.primary(),
                      textColor: IGColors.primary(),
                      hoverColor: IGColors.primary().withOpacity(0.15),
                      hoverIconColor: IGColors.primary(),
                      hoverTextColor: IGColors.primary(),
                      automaticallyResize: false,
                      onTapPositioned: (RelativeRect position) async {
                        beautifyHandler(position);
                      })),
            ),
          ]),
      DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
          context: context,
          firstContentPadding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
          first: FormTextFormField(
              formContainerState: formContainerState,
              keyboardType: TextInputType.multiline,
              maxLines: 100000,
              style: IGTextStyles.normalStyle(15, IGColors.text())
                  .copyWith(letterSpacing: 1.25),
              formFieldValidator: IGFormFieldValidator(
                type: IGFormFieldValidatorType.json,
              ),
              initialValue: json,
              labelText: labelText,
              textInputAction: textInputAction ?? TextInputAction.next,
              onChanged: onChanged,
              onFieldSubmitted: (value) {
                if (onFieldSubmitted != null) {
                  onFieldSubmitted(value);
                } else {
                  FocusScope.of(context).nextFocus();
                }
              }))
    ];
  }

  static DetailViewCellRule textFormFieldItemsProportionally(
      {required BuildContext context,
      required FormTextFormField first,
      EdgeInsetsGeometry firstContentPadding = const EdgeInsets.all(20),
      EdgeInsetsGeometry secondContentPadding = const EdgeInsets.all(20),
      bool forceHalf = false,
      FormTextFormField? second,
      int firstFlex = 5,
      int secondFlex = 5,
      int spaceFlex = 5,
      Widget? customSecond}) {
    var showHalf =
        second == null && forceHalf && !SizeDetector.isSmallScreen(context);
    return DetailViewCellRule(
        type: DetailViewCellRuleType.multiple_horizontal,
        alignment:
            showHalf ? MainAxisAlignment.start : MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        transformHorizontalInVerticalOnMobile: true,
        subElements: [
          DetailViewCellRule(
              type: DetailViewCellRuleType.textformfield,
              contentPadding: firstContentPadding,
              flex: SizeDetector.isSmallScreen(context)
                  ? null
                  : second == null && !forceHalf
                      ? firstFlex * 2
                      : firstFlex,
              textFormField: first),
          if (showHalf)
            DetailViewCellRule(
                type: DetailViewCellRuleType.space, flex: spaceFlex),
          if (second != null)
            DetailViewCellRule(
                type: DetailViewCellRuleType.textformfield,
                contentPadding: secondContentPadding,
                flex: SizeDetector.isSmallScreen(context) ? null : secondFlex,
                textFormField: second),
          if (customSecond != null)
            DetailViewCellRule(
                type: DetailViewCellRuleType.custom,
                contentPadding: secondContentPadding,
                flex: SizeDetector.isSmallScreen(context) ? null : secondFlex,
                forceOnlyCustom: true,
                customWidget: customSecond),
        ]);
  }

  static DetailViewCellRule genericInfoRow(
      {required BuildContext context,
      required DetailViewCellRulePreBuildInfo first,
      DetailViewCellRulePreBuildInfo? second}) {
    DetailViewCellRule Function(DetailViewCellRulePreBuildInfo, bool)
        calculateElements = (info, isFirst) {
      var tagWidget = DetailViewCellRule(
          type: DetailViewCellRuleType.multiple_horizontal,
          alignment: SizeDetector.isSmallScreen(context)
              ? MainAxisAlignment.start
              : isFirst
                  ? MainAxisAlignment.start
                  : MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          flex: SizeDetector.isSmallScreen(context)
              ? null
              : second == null
                  ? info.flex(context: context)! * 2
                  : info.flex(context: context),
          subElements: [
            DetailViewCellRule(
                type: DetailViewCellRuleType.richtext,
                showSelection: info.showSelection,
                textSpan: TextSpan(children: [
                  TextSpan(
                      text: info.key,
                      style: info.keyStyle ??
                          IGTextStyles.boldStyle(16, IGColors.text()))
                ])),
            DetailViewCellRule(
                type: DetailViewCellRuleType.text_tag,
                showSelection: false,
                content: info.value,
                borderRadius: info.borderRadius,
                internalPadding: info.internalPadding,
                backgroundColor: info.backgroundColor ?? IGColors.primary(),
                textStyle: info.valueStyle ??
                    IGTextStyles.normalStyle(
                        15, IGColors.text(forceDark: true))),
          ]);
      var starsWidget = DetailViewCellRule(
          type: DetailViewCellRuleType.multiple_horizontal,
          alignment: SizeDetector.isSmallScreen(context)
              ? MainAxisAlignment.start
              : isFirst
                  ? MainAxisAlignment.start
                  : MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          flex: SizeDetector.isSmallScreen(context)
              ? null
              : second == null
                  ? info.flex(context: context)! * 2
                  : info.flex(context: context),
          subElements: [
            DetailViewCellRule(
                type: DetailViewCellRuleType.richtext,
                showSelection: info.showSelection,
                textSpan: TextSpan(children: [
                  TextSpan(
                      text: info.key,
                      style: info.keyStyle ??
                          IGTextStyles.boldStyle(16, IGColors.text()))
                ])),
            DetailViewCellRule(
                type: DetailViewCellRuleType.stars,
                showSelection: false,
                stars: info.stars as int? ?? 0,
                tintColor: info.primaryColor ?? IGColors.primary(),
                height: 18,
                textStyle: info.valueStyle ??
                    IGTextStyles.normalStyle(15, IGColors.text())),
          ]);
      var urlToOpen = info.urlToOpen != null
          ? info.urlToOpen
          : info.onTap == null &&
                  !info.isCopy &&
                  info.value != null &&
                  info.value!.isNotEmpty
              ? (info.isMail ? "mailto:${info.value}" : info.value)
              : null;
      var linkWidget = DetailViewCellRule(
          type: DetailViewCellRuleType.multiple_horizontal,
          alignment: SizeDetector.isSmallScreen(context)
              ? MainAxisAlignment.start
              : isFirst
                  ? MainAxisAlignment.start
                  : MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          flex: SizeDetector.isSmallScreen(context)
              ? null
              : second == null
                  ? info.flex(context: context)! * 2
                  : info.flex(context: context),
          subElements: [
            DetailViewCellRule(
                type: DetailViewCellRuleType.richtext,
                showSelection: info.showSelection,
                textSpan: TextSpan(children: [
                  TextSpan(
                      text: info.key,
                      style: info.keyStyle ??
                          IGTextStyles.boldStyle(16, IGColors.text()))
                ])),
            DetailViewCellRule(
                type: DetailViewCellRuleType.text_link,
                content: info.value ?? "",
                flex: 8,
                hoverColor: IGColors.primary(opacity: 0.8),
                tintColor: IGColors.primary(),
                showSelection: false,
                tooltipMessage: info.value == null || info.value!.isEmpty
                    ? null
                    : info.tooltipMessage != null
                        ? info.tooltipMessage
                        : info.isMail
                            ? "Send an email to ${info.value}"
                            : info.isCopy
                                ? "Copy ${info.value}"
                                : "Open ${info.value}",
                urlToOpen: urlToOpen,
                onTap: urlToOpen != null
                    ? null
                    : (_) {
                        if (info.onTap != null) {
                          info.onTap!();
                        } else if (info.isCopy) {
                          if (info.value!.isNotEmpty) {
                            webConfigurator.showFlush(
                                context: context,
                                icon: Icon(Icons.copy_rounded,
                                    size: 24,
                                    color: IGColors.text(isOpposite: true)),
                                content: "Copied ${info.value ?? ""}");
                            Clipboard.setData(ClipboardData(text: info.value));
                          }
                        }
                      },
                internalPadding: EdgeInsets.all(5),
                textStyle: IGTextStyles.normalStyle(15, IGColors.text())),
          ]);
      var generalWidget = DetailViewCellRule(
          type: DetailViewCellRuleType.richtext,
          showSelection: true,
          flex: SizeDetector.isSmallScreen(context)
              ? null
              : second == null
                  ? info.flex(context: context)! * 2
                  : info.flex(context: context),
          textSpan: TextSpan(children: [
            TextSpan(
                text: info.key,
                style: info.keyStyle ??
                    IGTextStyles.boldStyle(16, IGColors.text())),
            TextSpan(
                text: info.value ?? "",
                style: info.valueStyle ??
                    IGTextStyles.custom(
                        color: IGColors.text(),
                        fontSize: 15,
                        fontWeight: FontWeight.w400)),
          ]));
      return info.isLink
          ? linkWidget
          : info.isStars
              ? starsWidget
              : info.isTag
                  ? tagWidget
                  : generalWidget;
    };

    var subElements = [calculateElements(first, true)];
    if (second != null) {
      subElements.add(spaceHeight30());
      subElements.add(calculateElements(second, false));
    }
    return DetailViewCellRule(
        type: DetailViewCellRuleType.multiple_horizontal,
        alignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: SizeDetector.isSmallScreen(context)
            ? CrossAxisAlignment.start
            : CrossAxisAlignment.center,
        contentPadding: EdgeInsets.symmetric(horizontal: 20),
        transformHorizontalInVerticalOnMobile: true,
        mainAxisSize: MainAxisSize.min,
        subElements: subElements);
  }

  static List<DetailViewCellRule> generalLoading() {
    return [
      DetailViewCellRulePreBuild.spaceHeightCustom(50),
      DetailViewCellRule<dynamic>(
        type: DetailViewCellRuleType.multiple_horizontal,
        forceNoScroll: true,
        alignment: MainAxisAlignment.center,
        subElements: [
          DetailViewCellRule<dynamic>(
            type: DetailViewCellRuleType.custom,
            customWidget: WidgetsDefaults.defaultCirculaProgressIndicator(),
          ),
        ],
      ),
      DetailViewCellRulePreBuild.spaceHeightCustom(50),
    ];
  }

  static List<DetailViewCellRule> generalEmptyState({
    required BuildContext context,
    required BoxConstraints? constraints,
    double spaceHeight = 75,
    bool returnItems = true,
    String value = "No items registered",
  }) {
    return [
      DetailViewCellRulePreBuild.spaceHeightCustom(50),
      DetailViewCellRule<dynamic>(
        type: DetailViewCellRuleType.multiple_horizontal,
        forceNoScroll: true,
        alignment: MainAxisAlignment.center,
        subElements: [
          DetailViewCellRule<dynamic>(
            type: DetailViewCellRuleType.custom,
            customWidget:
                Text(value, style: IGTextStyles.boldStyle(24, IGColors.text())),
          ),
        ],
      ),
      DetailViewCellRulePreBuild.spaceHeightCustom(50),
    ];
  }

  //MARK: BUTTONS SECTION
  static DetailViewCellRule generalSaveDeleteButtonRow(
      {required BuildContext context,
      required bool isEdit,
      bool isDeleteLoading = false,
      bool isLoading = false,
      Function? deleteAction,
      Function? saveAction,
      bool showSave = true,
      bool showDelete = true,
      Color? deleteBackgroundColor,
      Color? saveBackgroundColor,
      String deleteText = "Delete",
      String saveText = "Save"}) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.multiple_horizontal,
        forceNoScroll: true,
        alignment: isEdit && showDelete
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.end,
        subElements: [
          if (isEdit && showDelete)
            DetailViewCellRule(
              type: DetailViewCellRuleType.custom,
              contentPadding: EdgeInsets.symmetric(horizontal: 20),
              customWidget: IGButtonViewDefaults.largeButtonIconBackground(
                  context: context,
                  style: IGButtonViewStyle(
                      icon: Icons.delete_rounded,
                      tooltipMessage: deleteText,
                      onTapPositioned: (position) async {
                        if (deleteAction != null) {
                          await deleteAction();
                        }
                      })),
            ),
          DetailViewCellRule(
            type: DetailViewCellRuleType.custom,
            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            customWidget: IGButtonViewDefaults.primaryDefaultButton(
                context: context,
                style: IGButtonViewStyle(
                    text: saveText,
                    icon: Icons.check_rounded,
                    forceCircleDuringLoading: true,
                    forceLoading: isLoading,
                    automaticallyResize: false,
                    onTapPositioned: (position) async {
                      if (saveAction != null) {
                        await saveAction();
                      }
                    })),
          ),
        ]);
  }

  //MARK: SEPARATOR SECTION
  static DetailViewCellRule separator20({double height = 20}) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.separator,
        height: height,
        internalPadding: EdgeInsets.only(left: 20, right: 20));
  }

  static DetailViewCellRule separator0({double height = 20}) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.separator,
        height: height,
        internalPadding: EdgeInsets.only(left: 0, right: 0));
  }

  static DetailViewCellRule separatorMargin(
      {double height = 20, left: 20, right: 0}) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.separator,
        height: height,
        internalPadding: EdgeInsets.only(left: left, right: right));
  }

  //MARK: SPACE SECTION
  static DetailViewCellRule spaceHeight10() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, height: 10);
  }

  static DetailViewCellRule spaceHeight20() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, height: 20);
  }

  static DetailViewCellRule spaceHeight30() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, height: 30);
  }

  static DetailViewCellRule spaceHeightCustom(double height) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.space, height: height);
  }

  static DetailViewCellRule spaceWidth10() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, width: 10);
  }

  static DetailViewCellRule spaceWidth20() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, width: 20);
  }

  static DetailViewCellRule spaceWidth30() {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, width: 30);
  }

  static DetailViewCellRule spaceWidthCustom(double width) {
    return DetailViewCellRule(type: DetailViewCellRuleType.space, width: width);
  }
}

extension DetailViewConstraints on DetailView {
  static BoxConstraints boxConstraintsHalf(
      {required BoxConstraints constraints,
      required BuildContext context,
      int minusValue = 60,
      bool forceHalf = false,
      bool columnOnlySmall = true}) {
    return BoxConstraints(
        maxWidth:
            SizeDetector.isSmallScreen(context, width: constraints.maxWidth) ||
                    (!columnOnlySmall &&
                            SizeDetector.isMediumScreen(context,
                                width: constraints.maxWidth)) &&
                        !forceHalf
                ? constraints.maxWidth
                : (constraints.maxWidth - minusValue) / 2.0);
  }
}

enum YesOrNoEnum {
  yes,
  no,
}

extension YesOrNoEnumString on YesOrNoEnum {
  String asString() {
    switch (this) {
      case YesOrNoEnum.yes:
        return 'Yes';
      case YesOrNoEnum.no:
        return 'No';
    }
  }
}
