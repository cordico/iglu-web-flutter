/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class BreadCrumbItem {
  String? title;
  String? tooltip;
  Function(RelativeRect)? onTapPositioned;
  BreadCrumbItem({this.title, this.tooltip, this.onTapPositioned});
}

class BreadCrumbWidget extends StatelessWidget {
  BreadCrumbWidget({this.items, this.isVisibleOnMobile = false});
  final List<BreadCrumbItem>? items;
  final bool isVisibleOnMobile;

  @override
  Widget build(BuildContext context) {
    return (SizeDetector.isSmallScreen(context) && !isVisibleOnMobile) ||
            items!.isEmpty
        ? Container()
        : SingleChildScrollView(
                child: Row(children: children(context)),
                scrollDirection: Axis.horizontal)
            .fixNestedDoubleScrollbar();
  }

  Widget separator(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(6, 0, 6, 0),
      child: Text("/", style: IGTextStyles.normalStyle(20, IGColors.text())),
    );
  }

  List<Widget> children(BuildContext context) {
    List<Widget> temp = [];
    items!.asMap().forEach((index, element) {
      temp.add(
        IGButtonViewDefaults.primaryTextHoverButton(
            context: context,
            style: IGButtonViewStyle(
                internalPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                externalPadding: EdgeInsets.zero,
                text: element.title,
                hoverTextColor: resetValueField,
                textColor: IGColors.text(),
                hoverColor: IGHoverType.normal.hoverColor(),
                textOverflow: TextOverflow.ellipsis,
                maxLines: 1,
                onTapPositioned: element.onTapPositioned != null
                    ? (p) async {
                        element.onTapPositioned!(p);
                      }
                    : null)),
      );
      if (index != items!.length - 1) {
        temp.add(separator(context));
      }
    });
    return temp;
  }
}
