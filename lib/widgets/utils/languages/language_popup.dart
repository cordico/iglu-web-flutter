/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class LanguagePopup {
  static DetailViewCellRule languageSupportedPicker(
      {required BuildContext context,
      required String selectedLanguage,
      required Function(String?) completionHandler}) {
    return DetailViewCellRule(
        type: DetailViewCellRuleType.custom,
        alignment: MainAxisAlignment.start,
        customWidget: IGButtonViewDefaults.expandedLargeButton(
            context: context,
            style: IGButtonViewStyle(
                text:
                    "Choose (${webConfigurator.supportedLanguage.firstWhereOrNull((element) => element.key == selectedLanguage)?.name(locale: "en")})",
                onTapPositioned: (position) async {
                  await showMenuCustom(
                      items: _calculateAllLanguagePopupMenuButton(
                          context: context,
                          selectedLanguage: selectedLanguage,
                          completionHandler: completionHandler),
                      color: IGColors.secondaryBackground(),
                      position: position,
                      context: context,
                      elevation: 1.5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)));
                })));
  }

  static _calculateAllLanguagePopupMenuButton(
      {required BuildContext context,
      required String? selectedLanguage,
      required Function(String?) completionHandler}) {
    var finalTemp = List<PopupMenuCustomItem>.from([], growable: true);
    finalTemp.add(PopupMenuCustomItem(
        child: IGButtonRow(
          title: "Supported Languages",
          leadingIcon: Icons.flag_rounded,
        ),
        enabled: false));
    finalTemp.add(PopupMenuCustomItem(
        height: 1, child: Divider(thickness: 1, height: 1), enabled: false));
    webConfigurator.supportedLanguage.asMap().forEach((index, element) {
      finalTemp.add(PopupMenuCustomItem(
        enabled: false,
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          var complete = () {
            Navigator.of(context).pop();
            if (selectedLanguage != element.key) {
              selectedLanguage = element.key;
              _set(() {});
              completionHandler(selectedLanguage);
            }
          };
          return IGButtonRow(
              title: "${element.name(locale: "en")}",
              height: 36,
              trailing: (states) {
                return CircleCheckBox(
                  value: selectedLanguage == element.key,
                );
              },
              onTap: () {
                complete();
              });
        }),
      ));
      if (index != webConfigurator.supportedLanguage.length - 1)
        finalTemp.add(PopupMenuCustomItem(
            height: 1,
            child: Divider(
              thickness: 1,
              height: 1,
              endIndent: 20,
              indent: 20,
            ),
            enabled: false));
    });
    return finalTemp;
  }
}
