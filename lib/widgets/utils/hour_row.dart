/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class HourRow extends StatefulWidget {
  final String? openText;
  final String? closedText;
  final List<dynamic> range;
  final Function(int, int)? onHoursChanged;
  final bool? isLast;
  final bool isEditMode;
  final bool? isClosed;
  final int? index;
  final Function? onAddTap;
  final Function(int?)? onRemoveTap;

  HourRow({
    required this.range,
    this.onHoursChanged,
    this.index,
    this.isLast,
    this.isEditMode = true,
    this.isClosed,
    this.onAddTap,
    this.onRemoveTap,
    this.openText,
    this.closedText,
  });
  @override
  HourRowState createState() => HourRowState();
}

class HourRowState extends State<HourRow> {
  var isOpenSelected = false;
  var isClosedSelected = false;

  @override
  Widget build(BuildContext context) {
    int openValue = 0;
    int closeValue = 0;
    if (widget.range.isNotEmpty) {
      openValue = widget.range[0];
      closeValue = widget.range[1];
    }
    return AbsorbPointer(
      absorbing: !widget.isEditMode,
      child: Container(
        padding: const EdgeInsets.only(bottom: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            if (widget.isClosed!)
              IGView(
                  height: 54,
                  constraints: BoxConstraints(minWidth: 114),
                  margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(kButtonPrimaryCornerRadius),
                      border: Border.all(
                          color: isClosedSelected
                              ? IGColors.primary()
                              : IGColors.separator())),
                  child: (states, context) {
                    return Row(
                      children: [
                        Icon(Icons.access_time_rounded),
                        Container(width: 8),
                        Text(
                          "Closed",
                          style: IGTextStyles.normalStyle(14, IGColors.text()),
                        ),
                      ],
                    );
                  }),
            if (openValue == 0 &&
                (closeValue == 1440 || closeValue == 1439) &&
                !widget.isEditMode)
              IGView(
                  height: 54,
                  constraints: BoxConstraints(minWidth: 114),
                  margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(kButtonPrimaryCornerRadius),
                      border: Border.all(
                          color: isClosedSelected
                              ? IGColors.primary()
                              : IGColors.separator())),
                  child: (states, context) {
                    return Row(
                      children: [
                        Icon(Icons.access_time_rounded),
                        Container(width: 8),
                        Text(
                          "Open 24h",
                          style: IGTextStyles.normalStyle(14, IGColors.text()),
                        ),
                      ],
                    );
                  }),
            if ((!widget.isClosed! &&
                !(!widget.isEditMode &&
                    openValue == 0 &&
                    (closeValue == 1440 || closeValue == 1439))))
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IGView(
                      onTap: () async {
                        setState(() {
                          isOpenSelected = true;
                        });
                        TimeOfDay _initialTime = TimeOfDay(
                          hour: (widget.range[0] as int).getHours(),
                          minute: (widget.range[0] as int).getMinutes(),
                        );
                        TimeOfDay? picked = await showTimePicker(
                            context: context,
                            initialTime: _initialTime,
                            initialEntryMode:
                                SizeDetector.isLargeScreen(context)
                                    ? TimePickerEntryMode.input
                                    : TimePickerEntryMode.dial,
                            builder: (BuildContext context, Widget? child) {
                              return MediaQuery(
                                  data: MediaQuery.of(context)
                                      .copyWith(alwaysUse24HourFormat: true),
                                  child: FormTextFormFieldStyle.datePickerTheme(
                                      context: context, child: child));
                            });
                        FocusScope.of(context).requestFocus(FocusNode());
                        if (picked != null) {
                          var intTime = (picked.hour * 60) + picked.minute;
                          openValue = intTime;
                          widget.onHoursChanged!(openValue, closeValue);
                        }
                        setState(() {
                          isOpenSelected = false;
                        });
                      },
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(kButtonPrimaryCornerRadius),
                          border: Border.all(
                              color: isOpenSelected
                                  ? IGColors.primary()
                                  : IGColors.separator())),
                      child: (states, context) {
                        return Row(
                          children: [
                            Icon(Icons.access_time_rounded),
                            Container(width: 8),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "Opening",
                                  style: IGTextStyles.semiboldStyle(
                                      14,
                                      isOpenSelected
                                          ? IGColors.primary()
                                          : IGColors.textSecondary()),
                                ),
                                Text(widget.openText!,
                                    style: IGTextStyles.normalStyle(
                                        14, IGColors.text())),
                              ],
                            )
                          ],
                        );
                      }),
                  Container(
                    width: 12,
                  ),
                  IGView(
                      onTap: () async {
                        setState(() {
                          isClosedSelected = true;
                        });
                        TimeOfDay _initialTime = TimeOfDay(
                          hour: (widget.range[1] as int).getHours(),
                          minute: (widget.range[1] as int).getMinutes(),
                        );
                        TimeOfDay? picked = await showTimePicker(
                          context: context,
                          initialTime: _initialTime,
                          initialEntryMode: SizeDetector.isLargeScreen(context)
                              ? TimePickerEntryMode.input
                              : TimePickerEntryMode.dial,
                          builder: (BuildContext context, Widget? child) {
                            return MediaQuery(
                                data: MediaQuery.of(context)
                                    .copyWith(alwaysUse24HourFormat: true),
                                child: FormTextFormFieldStyle.datePickerTheme(
                                    context: context, child: child));
                          },
                        );
                        FocusScope.of(context).requestFocus(FocusNode());
                        if (picked != null) {
                          var intTime = (picked.hour * 60) + picked.minute;
                          widget.range[1] = intTime;
                          closeValue = intTime;
                          widget.onHoursChanged!(openValue, closeValue);
                        }
                        setState(() {
                          isClosedSelected = false;
                        });
                      },
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.circular(kButtonPrimaryCornerRadius),
                          border: Border.all(
                              color: isClosedSelected
                                  ? IGColors.primary()
                                  : IGColors.separator())),
                      child: (states, context) {
                        return Row(
                          children: [
                            Icon(Icons.access_time_rounded),
                            Container(width: 8),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  "Closure",
                                  style: IGTextStyles.semiboldStyle(
                                      14,
                                      isClosedSelected
                                          ? IGColors.primary()
                                          : IGColors.textSecondary()),
                                ),
                                Text(
                                  widget.closedText!,
                                  style: IGTextStyles.normalStyle(
                                      14, IGColors.text()),
                                )
                              ],
                            )
                          ],
                        );
                      }),
                  Container(width: 8),
                ],
              ),
            if (widget.isEditMode &&
                widget.isLast! &&
                !(openValue == 0 && closeValue == 1440 || closeValue == 1439))
              IGButtonViewDefaults.mediumButtonIconOnlyHover(
                  context: context,
                  style: IGButtonViewStyle(
                      height: 54,
                      width: 54,
                      iconSize: 22,
                      icon: Icons.add_rounded,
                      backgroundColor: IGColors.primary(),
                      iconColor: Colors.white,
                      tooltipMessage: "Add",
                      onTapPositioned: (position) async {
                        widget.onAddTap!();
                      })),
            if (widget.isEditMode &&
                widget.isLast! &&
                !(openValue == 0 && closeValue == 1440 || closeValue == 1439))
              Container(width: 8),
            if (widget.isEditMode &&
                (!widget.isClosed! &&
                    !(!widget.isEditMode &&
                        openValue == 0 &&
                        (closeValue == 1440 || closeValue == 1439))))
              IGButtonViewDefaults.mediumButtonIconOnlyHover(
                  context: context,
                  style: IGButtonViewStyle(
                      height: 54,
                      width: 54,
                      iconSize: 22,
                      icon: Icons.close_rounded,
                      iconColor: IGColors.text(),
                      backgroundColor: IGColors.separator(),
                      tooltipMessage: "Delete",
                      onTapPositioned: (position) async {
                        widget.onRemoveTap!(widget.index);
                      })),
          ],
        ),
      ),
    );
  }
}
