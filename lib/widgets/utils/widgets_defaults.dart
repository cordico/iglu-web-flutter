/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class WidgetsDefaults {
  static Tooltip defaultTooltip(
      {required BuildContext context,
      Key? key,
      required String message,
      double? height,
      EdgeInsetsGeometry? padding,
      EdgeInsetsGeometry? margin,
      double? verticalOffset,
      bool? preferBelow,
      bool? excludeFromSemantics,
      Decoration? decoration,
      TextStyle? textStyle,
      Duration? waitDuration,
      Duration? showDuration,
      required Widget child}) {
    return Tooltip(
      key: key,
      message: message,
      height: height ?? 25,
      padding: padding ?? EdgeInsets.all(10),
      margin: margin,
      verticalOffset: verticalOffset,
      preferBelow: preferBelow,
      excludeFromSemantics: excludeFromSemantics,
      child: child,
      decoration: decoration ??
          BoxDecoration(
              color: IGColors.secondaryBackground(isOpposite: true),
              borderRadius:
                  BorderRadius.all(Radius.circular(kButtonPrimaryCornerRadius)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10,
                )
              ]),
      showDuration: showDuration,
      textStyle: textStyle ??
          IGTextStyles.lightStyle(12, IGColors.text(isOpposite: true)),
      waitDuration: waitDuration,
    );
  }

  static SnackBar defaultSnackBar(
      {required BuildContext context,
      required String content,
      Icon? icon,
      IGButtonView? action,
      Duration? duration}) {
    return SnackBar(
        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      if (icon != null) icon,
                      if (icon != null) Container(width: 8),
                      Flexible(
                        child: Text(content,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: IGTextStyles.normalStyle(
                                14, IGColors.text(isOpposite: true))),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            if (action != null) Container(width: 8),
            action ?? Container()
          ],
        ),
        duration: duration ?? Duration(seconds: 1, milliseconds: 200),
        margin: SizeDetector.isSmallScreen(context)
            ? EdgeInsets.fromLTRB(
                20, MediaQuery.of(context).size.height - 80, 20, 20)
            : EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width / 4.0,
                MediaQuery.of(context).size.height - 80,
                MediaQuery.of(context).size.width / 4.0,
                20),
        behavior: SnackBarBehavior.floating,
        backgroundColor: IGColors.secondaryBackground(isOpposite: true),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius)));
  }

  static CircularProgressIndicator defaultCirculaProgressIndicator(
      {double strokeWidth = 2.0, double? value, Color? color}) {
    return CircularProgressIndicator(
        value: value,
        strokeWidth: strokeWidth,
        valueColor: AlwaysStoppedAnimation<Color>(color != null
            ? color
            : webConfigurator.currentBrightness == Brightness.dark
                ? Colors.white
                : IGColors.primary()));
  }
}
