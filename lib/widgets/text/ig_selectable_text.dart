/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

class IGSelectableText extends StatelessWidget {
  final String? data;
  final Key? key;
  final FocusNode? focusNode;
  final TextStyle? style;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final double? textScaleFactor;
  final bool showCursor;
  final bool autofocus;
  final ToolbarOptions? toolbarOptions;
  final int? minLines;
  final int? maxLines;
  final double cursorWidth;
  final double? cursorHeight;
  final Radius? cursorRadius;
  final Color? cursorColor;
  final DragStartBehavior dragStartBehavior;
  final bool enableInteractiveSelection;
  final TextSelectionControls? selectionControls;
  final Function()? onTap;
  final ScrollPhysics? scrollPhysics;
  final TextHeightBehavior? textHeightBehavior;
  final TextWidthBasis? textWidthBasis;
  final Function(TextSelection, SelectionChangedCause?)? onSelectionChanged;
  final Color? selectionColor;
  final Color? selectionHandleColor;
  IGSelectableText(
      {this.autofocus = false,
      this.cursorColor,
      this.cursorHeight,
      this.cursorRadius,
      this.cursorWidth = 2.0,
      this.data,
      this.dragStartBehavior = DragStartBehavior.start,
      this.enableInteractiveSelection = true,
      this.focusNode,
      this.key,
      this.maxLines,
      this.minLines,
      this.onSelectionChanged,
      this.onTap,
      this.scrollPhysics,
      this.selectionControls,
      this.showCursor = false,
      this.strutStyle,
      this.style,
      this.textAlign,
      this.textDirection,
      this.textHeightBehavior,
      this.textScaleFactor,
      this.textWidthBasis,
      this.toolbarOptions,
      this.selectionColor,
      this.selectionHandleColor});

  @override
  Widget build(BuildContext context) {
    return TextSelectionTheme(
        data: TextSelectionTheme.of(context).copyWith(
            selectionColor: selectionColor ?? Colors.blue.shade100,
            cursorColor: cursorColor,
            selectionHandleColor: selectionHandleColor),
        child: SelectableText(
          data!,
          autofocus: autofocus,
          cursorColor: cursorColor,
          cursorHeight: cursorHeight,
          cursorRadius: cursorRadius,
          cursorWidth: cursorWidth,
          dragStartBehavior: dragStartBehavior,
          enableInteractiveSelection: enableInteractiveSelection,
          focusNode: focusNode,
          key: key,
          maxLines: maxLines,
          minLines: minLines,
          onSelectionChanged: onSelectionChanged,
          onTap: onTap,
          scrollPhysics: scrollPhysics,
          selectionControls: selectionControls,
          showCursor: showCursor,
          strutStyle: strutStyle,
          style: style,
          textAlign: textAlign,
          textDirection: textDirection,
          textHeightBehavior: textHeightBehavior,
          textScaleFactor: textScaleFactor,
          textWidthBasis: textWidthBasis,
          toolbarOptions: toolbarOptions,
        ));
  }
}
