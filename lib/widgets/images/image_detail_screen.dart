/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import 'package:flutter/services.dart';

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:file_picker/file_picker.dart';

class ImageDetailsScreen extends StatefulWidget {
  final List<dynamic>? elements;
  final ImageRenderMethodForWeb? imageRenderMethodForWeb;
  final BoxFit? fit;
  final List<String?>? heroTag;
  final int? initialPosition;
  final Map<String, String>? httpHeaders;
  ImageDetailsScreen(
      {this.elements,
      this.imageRenderMethodForWeb,
      this.httpHeaders,
      this.fit,
      this.initialPosition,
      this.heroTag});
  @override
  _ImageDetailsScreenState createState() => _ImageDetailsScreenState();
}

class _ImageDetailsScreenState extends State<ImageDetailsScreen> {
  PageController controller = PageController();
  int? currentPosition = 0;
  @override
  void initState() {
    super.initState();
    if (widget.initialPosition != null) {
      currentPosition = widget.initialPosition;
      if (currentPosition! < 0) {
        currentPosition = 0;
      }
      controller = PageController(initialPage: widget.initialPosition!);
    }
  }

  Widget elaborateElement({required int index, BoxConstraints? constraints}) {
    var element = widget.elements![index];
    if (element is String) {
      return OptimizedCacheImage(
          imageUrl: element,
          httpHeaders: widget.httpHeaders,
          imageRenderMethodForWeb: widget.imageRenderMethodForWeb != null
              ? widget.imageRenderMethodForWeb
              : element.contains("api.")
                  ? ImageRenderMethodForWeb.HttpGet
                  : ImageRenderMethodForWeb.HtmlImage,
          fit: widget.fit ?? BoxFit.contain,
          progressIndicatorBuilder: (context, url, downloadProgress) => Center(
                child: Container(
                  constraints: BoxConstraints(
                      maxHeight: constraints != null
                          ? constraints.maxHeight / 3.0
                          : 30,
                      maxWidth: constraints != null
                          ? constraints.maxHeight / 3.0
                          : 30),
                  child: Container(
                      width: constraints != null
                          ? constraints.maxHeight / 3.0
                          : 30,
                      height: constraints != null
                          ? constraints.maxHeight / 3.0
                          : 30,
                      child: WidgetsDefaults.defaultCirculaProgressIndicator()),
                ),
              ));
    } else if (element is PlatformFile) {
      return Image.memory(element.bytes!, fit: widget.fit ?? BoxFit.contain);
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: IGKeyboardListener(
        disableEscape: false,
        onKey: (key) {
          if (key.isKeyPressed(LogicalKeyboardKey.arrowLeft)) {
            _goBack();
          } else if (key.isKeyPressed(LogicalKeyboardKey.arrowRight)) {
            _goForward();
          } else if (key.isKeyPressed(LogicalKeyboardKey.escape)) {
            Navigator.of(context).pop();
          }
        },
        child: Scaffold(
          backgroundColor: Colors.black.withOpacity(0.9),
          appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              toolbarHeight: 76,
              actions: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: IGButtonViewDefaults.largeButtonIconBackground(
                      context: context,
                      style: IGButtonViewStyle(
                          icon: Icons.close_rounded,
                          tooltipMessage: "Close",
                          backgroundColorSelected: resetValueField,
                          tintColorSelected: resetValueField,
                          onTapPositioned: (posistion) {
                            Navigator.of(context).pop();
                          })),
                ),
              ],
              leading: Container()),
          body: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              color: Colors.transparent,
              child: SafeArea(
                child: Container(
                  color: Colors.transparent,
                  child: Center(
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        PageView.builder(
                            controller: controller,
                            itemCount: widget.heroTag!.length,
                            pageSnapping: true,
                            onPageChanged: (value) {
                              setState(() {
                                currentPosition = value;
                              });
                            },
                            itemBuilder: (context, index) {
                              return InteractiveViewer(
                                panEnabled: true,
                                boundaryMargin: EdgeInsets.all(0),
                                child: LayoutBuilder(
                                    builder: (context, constraints) {
                                  return Container(
                                    height: constraints.maxHeight / 2.0,
                                    width: constraints.maxWidth,
                                    child: Stack(
                                      alignment: Alignment.centerLeft,
                                      children: [
                                        Center(
                                          child: Container(
                                            color: Colors.transparent,
                                            constraints: SizeDetector
                                                    .isSmallScreen(context)
                                                ? constraints
                                                : BoxConstraints(
                                                    maxHeight:
                                                        constraints.maxHeight /
                                                            2.0,
                                                    maxWidth:
                                                        (constraints.maxWidth -
                                                                200) /
                                                            2.0),
                                            child: Hero(
                                                tag: widget.heroTag![index]!,
                                                child: elaborateElement(
                                                    index: index,
                                                    constraints: constraints)),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //),
                                  );
                                }),
                              );
                            }),
                        showPreviousArrow
                            ? Positioned(
                                left: 0,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: IGButtonViewDefaults
                                      .largeButtonIconBackground(
                                          context: context,
                                          style: IGButtonViewStyle(
                                              icon: Icons
                                                  .arrow_back_ios_rounded,
                                              width:
                                                  SizeDetector.isMobile(context)
                                                      ? 30
                                                      : 100,
                                              height:
                                                  SizeDetector.isMobile(context)
                                                      ? 30
                                                      : 100,
                                              iconSize:
                                                  SizeDetector.isMobile(context)
                                                      ? 20
                                                      : 80,
                                              iconColor:
                                                  SizeDetector.isMobile(context)
                                                      ? null
                                                      : Colors.white,
                                              tooltipMessage: "Back",
                                              backgroundColor:
                                                  SizeDetector.isMobile(
                                                          context)
                                                      ? null
                                                      : resetValueField,
                                              backgroundColorSelected:
                                                  resetValueField,
                                              tintColorSelected:
                                                  resetValueField,
                                              onTapPositioned: (posistion) {
                                                _goBack();
                                              })),
                                ),
                              )
                            : Container(
                                width: 100,
                                height: 100,
                              ),
                        showNextArrow
                            ? Positioned(
                                right: 0,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                  child: IGButtonViewDefaults
                                      .largeButtonIconBackground(
                                          context: context,
                                          style: IGButtonViewStyle(
                                              icon: Icons
                                                  .arrow_forward_ios_rounded,
                                              tooltipMessage: "Next",
                                              width:
                                                  SizeDetector.isMobile(context)
                                                      ? 30
                                                      : 100,
                                              height:
                                                  SizeDetector.isMobile(context)
                                                      ? 30
                                                      : 100,
                                              iconSize:
                                                  SizeDetector.isMobile(context)
                                                      ? 20
                                                      : 80,
                                              iconColor:
                                                  SizeDetector.isMobile(context)
                                                      ? null
                                                      : Colors.white,
                                              backgroundColor:
                                                  SizeDetector.isMobile(
                                                          context)
                                                      ? null
                                                      : resetValueField,
                                              backgroundColorSelected:
                                                  resetValueField,
                                              tintColorSelected:
                                                  resetValueField,
                                              onTapPositioned: (posistion) {
                                                _goForward();
                                              })),
                                ),
                              )
                            : Container(
                                width: 100,
                                height: 100,
                              ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _goForward() {
    controller.nextPage(
        curve: Curves.easeInOut, duration: Duration(milliseconds: 300));
  }

  _goBack() {
    controller.previousPage(
        curve: Curves.easeInOut, duration: Duration(milliseconds: 300));
  }

  bool get showPreviousArrow {
    return ((widget.elements != null && currentPosition != 0));
  }

  bool get showNextArrow {
    return widget.elements != null &&
        currentPosition != widget.elements!.length - 1;
  }
}
