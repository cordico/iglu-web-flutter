/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'dart:typed_data';

import "package:flutter/material.dart";
import "package:flutter/gestures.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:file_picker/file_picker.dart';

class ImageContainerView extends StatefulWidget {
  final PlatformFile? fileImage;
  final String? imageUrl;
  final Color? defaultColor;
  final Color? loadingColor;
  final Color? hoverColor;
  final Color? backgroundHoverColor;
  final Future<bool> Function()? onDelete;
  final Function(PlatformFile)? onTap;
  final Function()? onTapPlaceholder;
  final Future<bool> Function(PlatformFile)? onUpload;
  final double? borderRadius;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? internalPadding;
  final BoxConstraints? constraints;
  final Widget? placeholderWidget;
  final double? placeholderCornerRadius;
  final bool showLoading;
  final bool? showFullScreen;
  final bool isFullScreen;
  final BoxFit? fit;
  final bool sendHttpHeaders;
  final Map<String, String>? httpHeaders;
  final ImageRenderMethodForWeb? imageRenderMethodForWeb;
  final bool absorbing;
  final String? heroTag;
  final List<ActionMenuButtonOption> actions;
  final List<ActionMenuButtonOption> pickImageActions;
  final bool showBorder;
  final bool showDefaultProgressIndicator;
  final List<dynamic>? galleryElements;
  final List<String>? galleryHeroTags;
  final int galleryInitialPosition;
  final IGLayoutItemTypeImagePickFilesOptions? imagePickFilesOptions;
  ImageContainerView(
      {this.fileImage,
      this.imageUrl,
      this.defaultColor,
      this.onDelete,
      this.loadingColor,
      this.showFullScreen,
      this.onTap,
      this.onTapPlaceholder,
      this.onUpload,
      this.backgroundColor,
      this.borderRadius,
      this.internalPadding,
      this.constraints,
      this.hoverColor,
      this.heroTag,
      this.backgroundHoverColor,
      this.placeholderCornerRadius,
      this.placeholderWidget,
      this.showLoading = false,
      this.isFullScreen = false,
      this.sendHttpHeaders = true,
      this.httpHeaders,
      this.absorbing = false,
      this.showBorder = true,
      this.showDefaultProgressIndicator = true,
      this.fit,
      this.imageRenderMethodForWeb,
      this.galleryElements,
      this.galleryHeroTags,
      this.galleryInitialPosition = 0,
      this.imagePickFilesOptions,
      this.actions = const [],
      this.pickImageActions = const []});
  @override
  _ImageContainerViewState createState() => _ImageContainerViewState();
}

class _ImageContainerViewState extends State<ImageContainerView>
    with SingleTickerProviderStateMixin {
  bool isMenuVisible = false;
  bool showLoadingInternal = false;
  PlatformFile? selectedImage;
  PlatformFile? deletedImage;
  String? deletedUrl;
  String? selectedUrl;
  ValueObserve<bool> isError = ValueObserve();
  final GlobalKey moreContainerKey = GlobalKey();

  String? heroTag = "";
  @override
  void initState() {
    super.initState();
    selectedUrl = widget.imageUrl?.trim();
    if (selectedUrl?.isEmpty == true) {
      selectedUrl = null;
    }
    selectedImage = widget.fileImage;
    isError.value = false;
    isError.addListener(() {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        _setStateMounted(() {});
      });
    });
    if (widget.heroTag != null) {
      heroTag = widget.heroTag;
    } else {
      heroTag = Uuid().v4();
    }
  }

  @override
  Widget build(BuildContext context) {
    return IGView(
        constraints: widget.constraints,
        enabled: !widget.absorbing,
        onTap: widget.absorbing ||
                isError.value! ||
                showLoadingInternal ||
                (widget.isFullScreen && widget.onTap == null) ||
                ((!widget.showFullScreen! ||
                        (widget.showFullScreen! &&
                            selectedImage == null &&
                            selectedUrl == null)) &&
                    widget.onDelete == null &&
                    widget.onUpload == null &&
                    widget.onTapPlaceholder == null)
            ? null
            : () async {
                if ((selectedUrl != null) &&
                    widget.showFullScreen! &&
                    !widget.isFullScreen) {
                  await Navigator.of(context).push(
                    PageTransition(
                        type: PageTransitionType.fade,
                        alignment: Alignment.center,
                        duration: Duration(milliseconds: 500),
                        child: ImageDetailsScreen(
                            initialPosition: widget.galleryInitialPosition,
                            elements: widget.galleryElements != null
                                ? widget.galleryElements
                                : [
                                    selectedUrl,
                                  ],
                            heroTag: widget.galleryHeroTags != null
                                ? widget.galleryHeroTags
                                : [heroTag])),
                  );
                } else if (selectedImage != null) {
                  if (widget.onTap != null) {
                    if (selectedImage != null) widget.onTap!(selectedImage!);
                  } else if (widget.showFullScreen! && !widget.isFullScreen) {
                    await Navigator.of(context).push(
                      PageTransition(
                          type: PageTransitionType.fade,
                          alignment: Alignment.center,
                          duration: Duration(milliseconds: 500),
                          child: ImageDetailsScreen(
                              initialPosition: widget.galleryInitialPosition,
                              elements: widget.galleryElements != null
                                  ? widget.galleryElements
                                  : [
                                      selectedImage,
                                    ],
                              heroTag: widget.galleryHeroTags != null
                                  ? widget.galleryHeroTags
                                  : [heroTag])),
                    );
                  }
                } else {
                  await _pickImage(
                      position: RelativeRect.fromLTRB(
                          moreContainerKey.globalPaintBounds!.left +
                              moreContainerKey.globalPaintBounds!.width -
                              10,
                          moreContainerKey.globalPaintBounds!.top + 40,
                          moreContainerKey.globalPaintBounds!.right,
                          moreContainerKey.globalPaintBounds!.bottom));
                }
              },
        onLongPress: () async {
          await _showActionsMenu(
              position: RelativeRect.fromLTRB(
                  moreContainerKey.globalPaintBounds!.left +
                      moreContainerKey.globalPaintBounds!.width -
                      10,
                  moreContainerKey.globalPaintBounds!.top + 40,
                  moreContainerKey.globalPaintBounds!.right,
                  moreContainerKey.globalPaintBounds!.bottom));
        },
        radius: widget.borderRadius ?? kButtonPrimaryCornerRadius,
        child: (states, context) {
          return LayoutBuilder(builder: (context, constraints) {
            return Listener(
              onPointerSignal: (signal) async {
                if (signal.buttons == kSecondaryMouseButton) {
                  await _showActionsMenu(
                      position: RelativeRect.fromLTRB(
                          moreContainerKey.globalPaintBounds!.left,
                          moreContainerKey.globalPaintBounds!.top +
                              moreContainerKey.globalPaintBounds!.height,
                          moreContainerKey.globalPaintBounds!.right,
                          moreContainerKey.globalPaintBounds!.bottom));
                }
              },
              child: Hero(
                key: moreContainerKey,
                tag: heroTag!,
                child: Stack(
                  alignment: Alignment.topRight,
                  children: [
                    selectedUrl != null || selectedImage != null
                        ? Container(
                            constraints: widget.constraints ?? null,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  widget.borderRadius ??
                                      kButtonPrimaryCornerRadius),
                              border: widget.isFullScreen
                                  ? null
                                  : widget.showBorder
                                      ? Border.all(
                                          color: IGColors.separator(),
                                          width: 0.8)
                                      : null,
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(
                                  widget.borderRadius ??
                                      kButtonPrimaryCornerRadius),
                              child: Container(
                                  constraints: widget.constraints ?? null,
                                  padding: widget.internalPadding,
                                  decoration: BoxDecoration(
                                      color: widget.backgroundColor,
                                      borderRadius: BorderRadius.circular(widget.borderRadius ??
                                          kButtonPrimaryCornerRadius)),
                                  child: selectedImage != null &&
                                          selectedImage!.bytes != null
                                      ? Image.memory(
                                          selectedImage!.bytes ?? Uint8List(0),
                                          height: widget.constraints != null
                                              ? widget.constraints!.maxHeight
                                              : null,
                                          width: widget.constraints != null
                                              ? widget.constraints!.maxWidth
                                              : null,
                                          fit: widget.fit ?? BoxFit.cover,
                                        )
                                      : selectedUrl != null
                                          ? OptimizedCacheImage(
                                              imageUrl: selectedUrl!,
                                              height: widget.constraints != null
                                                  ? widget
                                                      .constraints!.maxHeight
                                                  : null,
                                              width: widget.constraints != null
                                                  ? widget.constraints!.maxWidth
                                                  : null,
                                              httpHeaders: widget.sendHttpHeaders
                                                  ? widget.httpHeaders
                                                  : null,
                                              imageRenderMethodForWeb: widget.imageRenderMethodForWeb !=
                                                      null
                                                  ? widget
                                                      .imageRenderMethodForWeb!
                                                  : selectedUrl!
                                                          .contains("api.")
                                                      ? ImageRenderMethodForWeb
                                                          .HttpGet
                                                      : ImageRenderMethodForWeb
                                                          .HtmlImage,
                                              fit: widget.fit ??
                                                  (SizeDetector.isSmallScreen(context)
                                                      ? BoxFit.contain
                                                      : BoxFit.cover),
                                              progressIndicatorBuilder: widget.showDefaultProgressIndicator
                                                  ? (context, url, downloadProgress) => _circularProgressIndicator(constraints: constraints)
                                                  : null,
                                              errorWidget: (context, url, error) {
                                                if (isError.value == false) {
                                                  isError.value = true;
                                                }
                                                logger(error);
                                                if (error is String &&
                                                    error.toLowerCase().contains(
                                                        "failed to load network image")) {
                                                  selectedImage = null;
                                                  selectedUrl = null;
                                                }
                                                return widget
                                                        .placeholderWidget ??
                                                    _placeholder(
                                                        states: states);
                                              })
                                          : Container()),
                            ),
                          )
                        : widget.placeholderWidget ??
                            _placeholder(states: states),
                    (states.contains(MaterialState.hovered) || isMenuVisible) &&
                            ((selectedUrl != null || selectedImage != null) &&
                                (widget.onDelete != null ||
                                    widget.onUpload != null ||
                                    widget.actions.isNotEmpty))
                        ? Positioned(
                            top: 0,
                            right: 0,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                              child: IGButtonViewDefaults
                                  .mediumButtonAnimatedArrow(
                                      style: IGButtonViewStyle(
                                          onTapPositioned: (position) async {
                                            _setStateMounted(() {
                                              isMenuVisible = true;
                                            });
                                            _showActionsMenu(
                                                position: position);
                                          },
                                          tooltipMessage: "Actions"),
                                      context: context),
                            ),
                          )
                        : Container(),
                    widget.showLoading || showLoadingInternal
                        ? _circularProgressIndicator(constraints: constraints)
                        : Container()
                  ],
                ),
              ),
            );
          });
        });
  }

  @override
  void didUpdateWidget(ImageContainerView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      _setStateMounted(() {
        selectedUrl = widget.imageUrl?.trim();
        if (selectedUrl?.isEmpty == true) {
          selectedUrl = null;
        }
        selectedImage = widget.fileImage;
      });
    }
  }

  //INTERNALLY
  _circularProgressIndicator({BoxConstraints? constraints}) {
    return Center(
      child: Container(
        constraints: BoxConstraints(
            maxHeight:
                widget.constraints != null ? constraints!.maxHeight / 3.0 : 30,
            maxWidth:
                widget.constraints != null ? constraints!.maxHeight / 3.0 : 30),
        child: Container(
            width:
                widget.constraints != null ? constraints!.maxHeight / 3.0 : 30,
            height:
                widget.constraints != null ? constraints!.maxHeight / 3.0 : 30,
            child: WidgetsDefaults.defaultCirculaProgressIndicator()),
      ),
    );
  }

  _setStateMounted(void Function() fn) {
    if (mounted) {
      setState(fn);
    }
  }

  Future _pickImage({required RelativeRect position}) async {
    if (widget.onUpload == null || showLoadingInternal) {
      return;
    }

    Function defaultPickAction = () async {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          type: widget.imagePickFilesOptions?.type ?? FileType.image,
          allowMultiple: widget.imagePickFilesOptions?.allowMultiple ?? false,
          allowCompression:
              widget.imagePickFilesOptions?.allowCompression ?? true,
          allowedExtensions: widget.imagePickFilesOptions?.allowedExtensions,
          dialogTitle: widget.imagePickFilesOptions?.dialogTitle,
          onFileLoading: widget.imagePickFilesOptions?.onFileLoading,
          withData: widget.imagePickFilesOptions?.withData ?? true,
          withReadStream:
              widget.imagePickFilesOptions?.withReadStream ?? false);

      if (result != null && result.files.isNotEmpty) {
        selectedImage = result.files.first;
        _setStateMounted(() {});
        if (widget.onUpload != null) {
          _setStateMounted(() {
            showLoadingInternal = true;
          });
          var keep = false;
          if (selectedImage != null) {
            await widget.onUpload!(selectedImage!);
          }
          if (!keep) {
            selectedImage = null;
          }
          _setStateMounted(() {
            showLoadingInternal = false;
          });
        }
      }
    };

    if (widget.pickImageActions.isNotEmpty) {
      await showActionsMenu(
          context: context,
          position: RelativeRect.fromLTRB(position.left, position.top - 30,
              position.right, position.bottom),
          actions: widget.pickImageActions.map((e) {
                return ActionMenuButtonOption(
                    title: e.title,
                    icon: e.icon,
                    iconColor: e.iconColor,
                    textColor: e.textColor,
                    onPressed: () async {
                      _setStateMounted(() {
                        isMenuVisible = false;
                        showLoadingInternal = true;
                      });
                      Navigator.of(context).pop();
                      await e.onPressed!();
                      _setStateMounted(() {
                        showLoadingInternal = false;
                      });
                    });
              }).toList() +
              [
                ActionMenuButtonOption(
                    title: "Select from library",
                    icon: Icons.upload_file,
                    onPressed: () async {
                      _setStateMounted(() {
                        isMenuVisible = false;
                      });
                      Navigator.of(context).pop();
                      await defaultPickAction();
                    }),
              ]);
    } else {
      await defaultPickAction();
    }
  }

  Widget _placeholder({required Set<MaterialState> states}) {
    return Container(
        constraints: BoxConstraints(
            maxWidth: widget.constraints != null
                ? widget.constraints!.maxWidth - 4
                : double.infinity,
            maxHeight: widget.constraints != null
                ? widget.constraints!.maxHeight
                : double.infinity),
        margin: EdgeInsets.all(2),
        child: DottedLine(
          color: states.contains(MaterialState.hovered)
              ? (widget.hoverColor ?? IGColors.primary())
              : IGColors.separator(),
          corner: DottedLineCorner.all(widget.placeholderCornerRadius ?? 8.0),
          dottedLength: 12,
          strokeWidth: 3.0,
          space: 7.0,
          child: Center(
            child: Icon(Icons.photo_size_select_actual_rounded,
                color: states.contains(MaterialState.hovered)
                    ? widget.onUpload != null
                        ? (widget.hoverColor ?? IGColors.primary())
                        : null
                    : (widget.defaultColor ?? IGColors.separator()),
                size: (widget.constraints?.maxHeight ?? 250) / 2.5),
          ),
        ));
  }

  _showActionsMenu({required RelativeRect position}) async {
    if (((selectedUrl != null || selectedImage != null) &&
        (widget.onDelete != null ||
            widget.onUpload != null ||
            widget.actions.isNotEmpty))) {
      await showActionsMenu(
          context: context,
          position: RelativeRect.fromLTRB(position.left, position.top - 30,
              position.right, position.bottom),
          actions: widget.actions.map((e) {
                return ActionMenuButtonOption(
                    title: e.title,
                    icon: e.icon,
                    iconColor: e.iconColor,
                    textColor: e.textColor,
                    onPressed: () async {
                      _setStateMounted(() {
                        isMenuVisible = false;
                        showLoadingInternal = true;
                      });
                      Navigator.of(context).pop();
                      await e.onPressed!();
                      _setStateMounted(() {
                        showLoadingInternal = false;
                      });
                    });
              }).toList() +
              [
                if (widget.onUpload != null)
                  ActionMenuButtonOption(
                      title: "Change image",
                      icon: Icons.upload_file,
                      onPressed: () async {
                        _setStateMounted(() {
                          isMenuVisible = false;
                        });
                        Navigator.of(context).pop();
                        await _pickImage(position: position);
                      }),
                if (widget.onDelete != null)
                  ActionMenuButtonOption(
                      title: "Delete image",
                      icon: Icons.delete_rounded,
                      iconColor: IGColors.red(),
                      onPressed: () async {
                        _setStateMounted(() {
                          isMenuVisible = false;
                        });
                        Navigator.of(context).pop();
                        await AlertView.showSimpleDialog(context,
                            title: "Delete image",
                            message:
                                "Are you sure you want to delete this image?",
                            actionTitle: "Delete", okAction: () async {
                          if (widget.onDelete != null) {
                            _setStateMounted(() {
                              showLoadingInternal = true;
                            });
                            var result = await widget.onDelete!();
                            if (result) {
                              deletedImage = selectedImage;
                              deletedUrl = selectedUrl;
                              selectedImage = null;
                              selectedUrl = null;
                            }
                            _setStateMounted(() {
                              showLoadingInternal = false;
                            });
                          } else {
                            deletedImage = selectedImage;
                            deletedUrl = selectedUrl;
                            selectedImage = null;
                            selectedUrl = null;
                            _setStateMounted(() {});
                          }
                          Navigator.of(context).pop();
                        });
                      }),
              ]);
      _setStateMounted(() {
        isMenuVisible = false;
      });
    }
  }
}
