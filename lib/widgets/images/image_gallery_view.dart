/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import 'package:flutter/services.dart';
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:file_picker/file_picker.dart';

class ImageGalleryView extends StatefulWidget {
  final List<Attach> elements;
  final Widget? title;
  final ImageRenderMethodForWeb? imageRenderMethodForWeb;
  final Future<bool> Function(PlatformFile, int)? onUpload;
  final Future<bool> Function(int)? onDelete;
  final List<ActionMenuButtonOption> Function(int)? actions;
  ImageGalleryView(
      {required this.elements,
      this.imageRenderMethodForWeb,
      this.title,
      this.onDelete,
      this.onUpload,
      this.actions});
  @override
  _ImageGalleryViewState createState() => _ImageGalleryViewState();
}

class _ImageGalleryViewState extends State<ImageGalleryView> {
  late List<Attach> elements;

  List<String> elementsString = [];
  List<String> heroTags = [];

  @override
  void initState() {
    super.initState();
    elements = widget.elements;
    elements.forEach((element) {
      heroTags.add(Uuid().v4());
      elementsString.add(element.url ?? "");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: IGKeyboardListener(
        disableEscape: false,
        onKey: (key) {
          if (key.isKeyPressed(LogicalKeyboardKey.escape)) {
            Navigator.of(context).pop();
          }
        },
        child: Scaffold(
          backgroundColor: IGColors.separator(opacity: 0.9),
          appBar: AppBar(
              backgroundColor: IGColors.primaryBackground(),
              elevation: 1,
              toolbarHeight: 76,
              centerTitle: true,
              title: widget.title,
              shadowColor: IGColors.separator(),
              actions: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: IGButtonViewDefaults.largeButtonIconBackground(
                      context: context,
                      style: IGButtonViewStyle(
                          icon: Icons.close_rounded,
                          tooltipMessage: "Close",
                          backgroundColorSelected: resetValueField,
                          tintColorSelected: resetValueField,
                          onTapPositioned: (posistion) {
                            Navigator.of(context).pop();
                          })),
                ),
              ],
              leading: Container()),
          body: LayoutBuilder(builder: (context, constraints) {
            var numberOfElementInARow = (constraints.maxWidth / 300).floor();
            if (numberOfElementInARow > 4) {
              numberOfElementInARow = 4;
            }
            var maxWidth = (constraints.maxWidth / numberOfElementInARow);
            return Scrollbar(
              child: CustomScrollView(slivers: [
                SliverGrid(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        maxCrossAxisExtent: maxWidth),
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        var attach = elements[index];
                        return Container(
                          height: maxWidth,
                          width: maxWidth,
                          child: ImageContainerView(
                            borderRadius: 0.0,
                            constraints: BoxConstraints(
                                maxHeight: maxWidth, maxWidth: maxWidth),
                            fit: BoxFit.cover,
                            isFullScreen: false,
                            showFullScreen: true,
                            heroTag: heroTags[index],
                            galleryElements: elementsString,
                            galleryHeroTags: heroTags,
                            galleryInitialPosition: index,
                            actions: widget.actions!(index),
                            onUpload: widget.onUpload != null
                                ? (filePickerCross) async {
                                    return await widget.onUpload!(
                                        filePickerCross, index);
                                  }
                                : null,
                            onDelete: widget.onDelete != null
                                ? () async {
                                    var result = await widget.onDelete!(index);
                                    if (result) {
                                      elements.removeAt(index);
                                      if (elements.isEmpty) {
                                        Navigator.of(context).pop();
                                      } else {
                                        setState(() {});
                                      }
                                    }
                                    return result;
                                  }
                                : null,
                            imageUrl: attach.url,
                          ),
                        );
                      },
                      childCount: elements.length,
                    )),
                SliverToBoxAdapter(
                  child: Container(height: 15),
                )
              ]),
            );
          }),
        ),
      ),
    );
  }
}
