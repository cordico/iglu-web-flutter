/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class AlertView {
  static Size textSize(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  static Future<dynamic> showSimpleDialog(BuildContext context,
      {String? title,
      String? message,
      Function? okAction,
      Function? cancelAction,
      Function? onClose,
      String? actionTitle,
      String? cancelTitle,
      TextStyle? titleStyle,
      TextStyle? messageStyle,
      Color? cancelActionPrimaryColor,
      Color? okActionPrimaryColor,
      bool dismissable = true}) async {
    return await Navigator.of(context).push(
      PageTransition(
          type: PageTransitionType.fade,
          child: LayoutBuilder(builder: (_, constraints) {
            var width = (constraints.maxWidth / 1.3);
            var titleWidth = (AlertView.textSize(
                    title ?? "Attention",
                    IGTextStyles.normalStyle(
                      28,
                      IGColors.text(),
                    )).width) +
                100 +
                constraints.maxWidth;
            return FormSheet(
                internalMargin: EdgeInsets.zero,
                alwaysDismiss: dismissable,
                horizontalMargin: width.abs() > titleWidth.abs()
                    ? width.abs()
                    : titleWidth.abs(),
                title: title ?? "Attention",
                onClose: onClose,
                cellRules: (constraints, state) {
                  return [
                    DetailViewCellRulePreBuild.spaceHeight20(),
                    DetailViewCellRule(
                        type: DetailViewCellRuleType.text,
                        textAlign: TextAlign.start,
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        constraints:
                            BoxConstraints(maxWidth: constraints.maxWidth - 40),
                        content: message ?? "",
                        textStyle:
                            IGTextStyles.normalStyle(16, IGColors.text())),
                    DetailViewCellRulePreBuild.spaceHeight20(),
                    DetailViewCellRule(
                        type: DetailViewCellRuleType.multiple_horizontal,
                        alignment: MainAxisAlignment.end,
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        subElements: [
                          if (cancelAction != null)
                            DetailViewCellRule(
                                type: DetailViewCellRuleType.custom,
                                contentPadding:
                                    EdgeInsets.fromLTRB(0, 0, 20, 0),
                                customWidget:
                                    IGButtonViewDefaults.secondaryButton(
                                        context: context,
                                        style: IGButtonViewStyle(
                                          text: cancelTitle ?? "Cancel",
                                          backgroundColorSelected:
                                              resetValueField,
                                          tintColorSelected: resetValueField,
                                          onTapPositioned: (d) async {
                                            await cancelAction();
                                          },
                                        ))),
                          DetailViewCellRule(
                              type: DetailViewCellRuleType.custom,
                              customWidget:
                                  IGButtonViewDefaults.primaryDefaultButton(
                                      context: context,
                                      style: IGButtonViewStyle(
                                        text: actionTitle ?? "OK",
                                        forceCircleDuringLoading: true,
                                        circleBorderRadius: 20,
                                        onTapPositioned: (d) async {
                                          if (okAction != null) {
                                            await okAction();
                                          } else {
                                            Navigator.of(context).pop();
                                          }
                                        },
                                      ))),
                        ]),
                    DetailViewCellRulePreBuild.spaceHeight20(),
                  ];
                });
          })),
    );
  }

  static Future<dynamic> showInformationDialog(BuildContext context,
      {String? title,
      Function? okAction,
      TextSpan? textSpan,
      bool dismissable = true}) async {
    return await Navigator.of(context).push(
      PageTransition(
          type: PageTransitionType.fade,
          child: LayoutBuilder(builder: (_, constraints) {
            var width = (constraints.maxWidth / 1.3);
            var titleWidth = (AlertView.textSize(
                    title ?? "Attention",
                    IGTextStyles.normalStyle(
                      28,
                      IGColors.text(),
                    )).width) +
                100 +
                constraints.maxWidth;
            return FormSheet(
                internalMargin: EdgeInsets.zero,
                alwaysDismiss: dismissable,
                horizontalMargin: width.abs() > titleWidth.abs()
                    ? width.abs()
                    : titleWidth.abs(),
                title: title ?? "Attention",
                onClose: () {},
                cellRules: (constraints, state) {
                  return [
                    DetailViewCellRulePreBuild.spaceHeight20(),
                    DetailViewCellRule(
                        type: DetailViewCellRuleType.richtext,
                        textSpan: textSpan,
                        textAlign: TextAlign.left,
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        constraints: BoxConstraints(
                            maxWidth: constraints.maxWidth - 40)),
                    DetailViewCellRulePreBuild.spaceHeight20(),
                  ];
                });
          })),
    );
  }

  static Future<dynamic> showAlertInputField(
    BuildContext context, {
    String? title,
    String? message,
    String? okTitle,
    Function? okAction,
    Function? cancelAction,
    String? cancelTitle,
    TextStyle? titleStyle,
    TextStyle? messageStyle,
    Color? cancelActionPrimaryColor,
    Color? okActionPrimaryColor,
    Widget Function()? customTextField,
  }) async {
    final GlobalKey<FormState> formKey = GlobalKey();
    return await Navigator.of(context).push(
      PageTransition(
          type: PageTransitionType.fade,
          child: LayoutBuilder(builder: (_, constraints) {
            var width = (constraints.maxWidth / 1.3);
            var titleWidth = (AlertView.textSize(
                    title ?? "Attention",
                    IGTextStyles.normalStyle(
                      28,
                      IGColors.text(),
                    )).width) +
                100 +
                constraints.maxWidth;
            return FormSheet(
                formKey: formKey,
                internalMargin: EdgeInsets.zero,
                alwaysDismiss: false,
                horizontalMargin: width.abs() > titleWidth.abs()
                    ? width.abs()
                    : titleWidth.abs(),
                title: title ?? "Attention",
                onClose: () {},
                cellRules: (constraints, state) {
                  return [
                    DetailViewCellRulePreBuild.spaceHeight20(),
                    DetailViewCellRule(
                        type: DetailViewCellRuleType.text,
                        textAlign: TextAlign.start,
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        constraints:
                            BoxConstraints(maxWidth: constraints.maxWidth - 40),
                        content: message ?? "",
                        textStyle:
                            IGTextStyles.normalStyle(16, IGColors.text())),
                    DetailViewCellRulePreBuild.spaceHeightCustom(5),
                    DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                        context: context,
                        first: customTextField!() as FormTextFormField),
                    DetailViewCellRulePreBuild.spaceHeight20(),
                    DetailViewCellRule(
                        type: DetailViewCellRuleType.multiple_horizontal,
                        alignment: MainAxisAlignment.end,
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        subElements: [
                          if (cancelAction != null)
                            DetailViewCellRule(
                                type: DetailViewCellRuleType.custom,
                                contentPadding:
                                    EdgeInsets.symmetric(horizontal: 20),
                                customWidget:
                                    IGButtonViewDefaults.secondaryButton(
                                        context: context,
                                        style: IGButtonViewStyle(
                                          text: cancelTitle ?? "Cancel",
                                          backgroundColorSelected:
                                              resetValueField,
                                          tintColorSelected: resetValueField,
                                          onTapPositioned: (d) async {
                                            await cancelAction();
                                          },
                                        ))),
                          DetailViewCellRule(
                              type: DetailViewCellRuleType.custom,
                              customWidget:
                                  IGButtonViewDefaults.primaryDefaultButton(
                                      context: context,
                                      style: IGButtonViewStyle(
                                        text: okTitle ?? "OK",
                                        forceCircleDuringLoading: true,
                                        circleBorderRadius: 20,
                                        onTapPositioned: (d) async {
                                          if (okAction != null) {
                                            if (formKey.currentState!
                                                .validate()) {
                                              okAction();
                                            }
                                          } else {
                                            Navigator.of(context).pop();
                                          }
                                        },
                                      ))),
                        ]),
                    DetailViewCellRulePreBuild.spaceHeight20(),
                  ];
                });
          })),
    );
  }

  static Future showSimpleErrorDialog(
      {required BuildContext context,
      String title = "Attention",
      String? message}) {
    return AlertView.showSimpleDialog(context,
        title: title,
        message: message,
        okActionPrimaryColor: IGColors.primary(), okAction: () {
      Navigator.of(context).pop();
    });
  }
}
