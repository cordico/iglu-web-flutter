/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:provider/provider.dart';

class HeaderBar extends StatefulWidget implements PreferredSizeWidget {
  final List<BreadCrumbItem> Function()? generateBreadCrumbItems;
  final Function? contextStateRefresh;
  HeaderBar({this.generateBreadCrumbItems, this.contextStateRefresh});
  @override
  _HeaderBarState createState() => _HeaderBarState();

  @override
  Size get preferredSize => Size.fromHeight(55);
}

class _HeaderBarState extends State<HeaderBar> {
  List<BreadCrumbItem> _items = [];
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      if (widget.generateBreadCrumbItems != null) {
        List<BreadCrumbItem> items = widget.generateBreadCrumbItems!();
        setItems(items);
      }
    });
  }

  setItems(List<BreadCrumbItem> items, {bool noSetState = false}) {
    if (mounted) {
      if (!noSetState) {
        setState(() {
          _items = items;
        });
      } else {
        _items = items;
      }
      if (items.isNotEmpty) {
        var last = items.last;
        if (webConfigurator.rootState != null) {
          webConfigurator.rootState!.changeTitle(
              "${last.title ?? ""} | ${webConfigurator.internalConfigurator.appName}");
        }
      }
    }
  }

  @override
  void didUpdateWidget(covariant HeaderBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      if (widget.generateBreadCrumbItems != null) {
        List<BreadCrumbItem> items = widget.generateBreadCrumbItems!();
        setItems(items, noSetState: true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HeaderBarChangeNotifier>(builder: (context, state, child) {
      return LayoutBuilder(
        builder: (context, constraints) {
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              height: 55,
              padding: EdgeInsets.fromLTRB(0, 0, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                      if (SizeDetector.isSmallScreen(context))
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: LogoImage(
                              height: 27,
                              width: 27,
                              lightOnly: webConfigurator.currentBrightness ==
                                  Brightness.light,
                              darkOnly: webConfigurator.currentBrightness ==
                                  Brightness.dark,
                              onTap: () async {
                                _openDrawer();
                              }),
                        ),
                      if (SizeDetector.isSmallScreen(context))
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: IGButtonViewDefaults.largeButtonIconOnlyHover(
                              context: context,
                              style: IGButtonViewStyle(
                                  icon: Icons.menu_rounded,
                                  circleBorderRadius: 20,
                                  tooltipMessage: "Menu",
                                  onTapPositioned: (position) async {
                                    await _openDrawer();
                                  })),
                        ),
                      Expanded(child: BreadCrumbWidget(items: _items)),
                      Container(width: 50)
                    ] +
                    (webConfigurator.internalConfigurator
                            .headerBarConfigurations?.customWidgets ??
                        <Widget>[]) +
                    [
                      if (webConfigurator.internalConfigurator
                              .headerBarConfigurations?.showHeaderFeedback ==
                          true)
                        IGButtonViewDefaults.largeButtonIconBackground(
                            context: context,
                            style: IGButtonViewStyle(
                                icon: Icons.feedback_rounded,
                                iconSize: 20,
                                tooltipMessage: "Feedback",
                                externalPadding: EdgeInsets.fromLTRB(
                                    15,
                                    0,
                                    webConfigurator
                                                .internalConfigurator
                                                .headerBarConfigurations
                                                ?.showHeaderNotification ==
                                            true
                                        ? 15
                                        : 0,
                                    0),
                                onTapPositioned: (position) async {
                                  await Navigator.of(context).push(
                                    PageTransition(
                                        type: PageTransitionType.fade,
                                        child: FeedbackFormSheet()),
                                  );
                                })),
                      if (webConfigurator
                              .internalConfigurator
                              .headerBarConfigurations
                              ?.showHeaderNotification ==
                          true)
                        Consumer<LocalNotificationsChangeNotifier>(
                            builder: (context, state, child) {
                          return BadgeIconWidget(
                            /*badgeCounter: (webConfigurator.internalConfigurator
                                        .loggedInUser?.shouldVerifyEmail ??
                                    false)
                                ? 1 + state.value!.length
                                : state.value!.length,*/
                            badgeCounter: state.value!.length,
                            buttonView:
                                IGButtonViewDefaults.largeButtonIconBackground(
                                    context: context,
                                    style: IGButtonViewStyle(
                                        externalPadding: EdgeInsets.fromLTRB(
                                            webConfigurator
                                                        .internalConfigurator
                                                        .headerBarConfigurations
                                                        ?.showHeaderFeedback ==
                                                    true
                                                ? 15
                                                : 0,
                                            0,
                                            0,
                                            0),
                                        icon: Icons.notifications_rounded,
                                        tooltipMessage: "Notifications",
                                        onTapPositioned: (position) async {
                                          await _showMenuNotification(
                                              position: position,
                                              constraints: constraints,
                                              state: state);
                                        })),
                            iconSize: 24,
                          );
                        }),
                      _returnCircularAvatar(
                          context: context, constraints: constraints)
                    ],
              ),
              decoration: BoxDecoration(
                  color: IGColors.primaryBackground(),
                  border: BorderDirectional(
                      bottom: BorderSide(
                          color: isDebugging
                              ? IGColors.orange()
                              : IGColors.separator()))),
            ),
          );
        },
      );
    });
  }

  _openDrawer() async {
    await Navigator.of(context).push(PageTransition(
        type: PageTransitionType.leftToRight, child: AppDrawer()));
  }

  Widget _returnCircularAvatar(
      {required BuildContext context, required BoxConstraints constraints}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
      child: IGButtonViewDefaults.largeButtonAnimatedArrow(
          context: context,
          style: IGButtonViewStyle(
              tooltipMessage: "Account",
              onTapPositioned: (position) async {
                await _showMenu(position: position, constraints: constraints);
              })),
    );
  }

  bool showSendNewConfirmationEmailNotification = false;
  _showMenuNotification(
      {required RelativeRect position,
      required BoxConstraints constraints,
      required LocalNotificationsChangeNotifier state}) async {
    var items = <PopupMenuCustomItem>[
      PopupMenuCustomItem(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
            child: Text("Notifications",
                style:
                    IGTextStyles.custom(fontSize: 22, color: IGColors.text())),
          ),
          enabled: false),
      /*if (webConfigurator
              .internalConfigurator.loggedInUser?.shouldVerifyEmail ==
          true)
        PopupMenuCustomItem(
            child: StatefulBuilder(builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                        "Email not verified yet. Check your Inbox to verify it",
                        textAlign: TextAlign.left,
                        style: IGTextStyles.custom(
                            fontSize: 14, color: IGColors.textSecondary())),
                    Container(height: 4),
                    Row(
                      children: [
                        IGButtonViewDefaults.defaultTextHoverButton(
                            context: context,
                            style: IGButtonViewStyle(
                                text: "Send again",
                                textStyle: IGTextStyles.custom(fontSize: 14),
                                onTapPositioned: (position) async {
                                  await _sendEmailConfirmationAgain(
                                      state: state);
                                })),
                        if (showSendNewConfirmationEmailNotification)
                          Container(
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              width: 14,
                              height: 14,
                              child: WidgetsDefaults
                                  .defaultCirculaProgressIndicator(
                                      strokeWidth: 1.0))
                      ],
                    ),
                  ],
                ),
              );
            }),
            enabled: false),
      if (!(webConfigurator
                  .internalConfigurator.loggedInUser?.shouldVerifyEmail ??
              false) &&
          state.value!.isEmpty)
        PopupMenuCustomItem(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
              child: Text("You have no notification at the moment",
                  style: IGTextStyles.custom(
                      fontSize: 14, color: IGColors.text())),
            ),
            enabled: false),*/
    ];
    if (state.value!.isNotEmpty) {
      state.value!.asMap().forEach((index, e) {
        items.add(PopupMenuCustomItem(
            child: IGButtonRow(
                title: e.title ?? "",
                subTitle: e.message ?? "",
                onTap: e.onTap,
                trailing: (states) {
                  return Icon(Icons.arrow_forward_ios_rounded,
                      size: 16,
                      color:
                          webConfigurator.currentBrightness == Brightness.light
                              ? IGColors.primary()
                              : IGColors.text());
                }),
            enabled: false));
        if (index != state.value!.length - 1) {
          items.add(
            PopupMenuCustomItem(
                height: 1,
                child: Divider(
                    thickness: 1,
                    height: 1,
                    endIndent: 20,
                    indent: 20,
                    color: IGColors.separator()),
                enabled: false),
          );
        }
      });
    }
    await showMenuCustom(
        color: IGColors.secondaryBackground(),
        items: items,
        position: position,
        context: context,
        elevation: 1.5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius)));
  }

  /*_sendEmailConfirmationAgain(
      {required Function(void Function()) state}) async {
    //TODO: HANDLE SEND NEW CONFIRMATION EMAIL
    
    state(() {
      showSendNewConfirmationEmailNotification = true;
    });
    var map = await Shared.queryManager.confirmEmailProcessUser();
    state(() {
      showSendNewConfirmationEmailNotification = false;
    });
    if (map["error"] == null) {
      Navigator.of(context).pop();
      webConfigurator.showFlush(
          context: context,
          content: "Email correctly sent",
          icon: Icon(Icons.check_rounded,
              size: 24, color: IGColors.text(isOpposite: true)));
    } else {
      await AlertView.showSimpleErrorDialog(
          context: context, message: map["error"]);
    }
  }*/

  _showMenu(
      {required RelativeRect position,
      required BoxConstraints constraints}) async {
    String? appearance = "auto";
    if (webConfigurator.hivePreferencesBox.containsKey("appearance")) {
      appearance = webConfigurator.hivePreferencesBox.get("appearance");
    }
    await showMenuCustom(
        color: IGColors.secondaryBackground(),
        items: <PopupMenuEntry>[
          PopupMenuCustomItem(
              child: IGButtonRow(
                height: null,
                title: webConfigurator
                        .internalConfigurator.loggedInUser?.fullName ??
                    webConfigurator
                        .internalConfigurator.loggedInUser?.identifier ??
                    "Account",
                subTitle:
                    webConfigurator.internalConfigurator.loggedInUser?.email ??
                        null,
                leadingIcon: Icons.person,
              ),
              enabled: false),
          if (webConfigurator.internalConfigurator.headerBarConfigurations
                  ?.showChangePassword ==
              true)
            PopupMenuCustomItem(
                height: 1,
                child: Divider(
                    thickness: 1,
                    height: 1,
                    endIndent: 20,
                    indent: 20,
                    color: IGColors.separator()),
                enabled: false),
          if (webConfigurator.internalConfigurator.headerBarConfigurations
                  ?.showChangePassword ==
              true)
            PopupMenuCustomItem(
                child: IGButtonRow(
                  title: "Change Password",
                  leadingIcon: Icons.vpn_key_rounded,
                  onTap: () async {
                    Navigator.of(context).pop();
                    _changePassword(constraints: constraints);
                  },
                ),
                enabled: false),
          PopupMenuCustomItem(
              height: 1,
              child:
                  Divider(thickness: 1, height: 1, color: IGColors.separator()),
              enabled: false),
          PopupMenuCustomItem(
              child: IGButtonRow(
                title: "Appearance",
                leadingIcon: Icons.nights_stay_rounded,
              ),
              enabled: false),
          PopupMenuCustomItem(
              child: IGButtonRow(
                title: "Light",
                leftMarginEmptyIcon: 48.0,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                onTap: appearance != "light"
                    ? () async {
                        await _changeBrightness(appearance: "light");
                        Navigator.of(context).pop();
                        _showMenu(position: position, constraints: constraints);
                      }
                    : null,
                trailing: (states) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: CircleCheckBox(
                      value: appearance == "light",
                    ),
                  );
                },
              ),
              enabled: false),
          PopupMenuCustomItem(
              child: IGButtonRow(
                  title: "Dark",
                  leftMarginEmptyIcon: 48.0,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  onTap: appearance != "dark"
                      ? () async {
                          await _changeBrightness(appearance: "dark");
                          Navigator.of(context).pop();
                          _showMenu(
                              position: position, constraints: constraints);
                        }
                      : null,
                  trailing: (states) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: CircleCheckBox(
                        value: appearance == "dark",
                      ),
                    );
                  }),
              enabled: false),
          PopupMenuCustomItem(
              child: IGButtonRow(
                  title: "Automatic",
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  leftMarginEmptyIcon: 48.0,
                  onTap: appearance != "auto"
                      ? () async {
                          await _changeBrightness(appearance: "auto");
                          Navigator.of(context).pop();
                          _showMenu(
                              position: position, constraints: constraints);
                        }
                      : null,
                  trailing: (states) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                      child: CircleCheckBox(
                        value: appearance == "auto",
                      ),
                    );
                  }),
              enabled: false),
          PopupMenuCustomItem(
              height: 1,
              child:
                  Divider(thickness: 1, height: 1, color: IGColors.separator()),
              enabled: false),
          PopupMenuCustomItem(
              child: IGButtonRow(
                onTap: () async {
                  _handleLogout();
                },
                title: "Logout",
                leadingIcon: Icons.exit_to_app_rounded,
              ),
              enabled: false)
        ],
        position: position,
        context: context,
        elevation: 1.5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius)));
  }

  _changeBrightness({required String appearance}) async {
    if (appearance == "auto") {
      webConfigurator.currentBrightness =
          MediaQuery.of(context).platformBrightness;
    } else {
      webConfigurator.currentBrightness =
          appearance == "dark" ? Brightness.dark : Brightness.light;
    }
    webConfigurator.hivePreferencesBox.put("appearance", appearance);
    if (webConfigurator.kingState != null) {
      webConfigurator.kingState!.refresh();
    }
    if (webConfigurator.rootState != null) {
      webConfigurator.rootState!.refresh();
    }
    if (widget.contextStateRefresh != null) {
      widget.contextStateRefresh!();
    }
    return;
  }

  _changePassword({required BoxConstraints constraints}) async {
    Navigator.of(context).push(
      PageTransition(
          type: PageTransitionType.fade, child: ChangePasswordFormSheet()),
    );
  }

  _handleLogout() async {
    if (webConfigurator
            .internalConfigurator.handlersConfigurations?.logoutHandler !=
        null) {
      var r = await webConfigurator
              .internalConfigurator.handlersConfigurations?.logoutHandler!() ??
          false;
      if (r) {
        webConfigurator.sessionHandler.handleLogout(context: context);
      } else {
        await AlertView.showSimpleErrorDialog(
            context: context, message: "Could not log out at the moment");
      }
    }
  }
}
