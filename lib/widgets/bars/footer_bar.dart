/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class FooterBar extends StatefulWidget {
  FooterBar();
  @override
  _FooterBarState createState() => _FooterBarState();
}

class _FooterBarState extends State<FooterBar> {
  bool highlightIGLU = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (SizeDetector.isSmallScreen(context)) {
      return Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(height: 10),
            Text(
              "${webConfigurator.internalConfigurator.appName} v${webConfigurator.internalConfigurator.version}",
              style: IGTextStyles.normalStyle(13, IGColors.text()),
            ),
            Container(height: 5),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Copyright © ${webConfigurator.internalConfigurator.year} ",
                    style: IGTextStyles.normalStyle(13, IGColors.text()),
                  ),
                  Link(
                    uri: Uri.parse(
                        webConfigurator.internalConfigurator.companyURL!),
                    target: LinkTarget.blank,
                    builder: (BuildContext context, FollowLink? followLink) =>
                        IGButtonViewDefaults.primaryTextHoverButton(
                            context: context,
                            style: IGButtonViewStyle(
                                text:
                                    "${webConfigurator.internalConfigurator.companyName}",
                                tooltipMessage:
                                    "${webConfigurator.internalConfigurator.companyName}",
                                onTapPositioned: (position) {
                                  followLink!();
                                })),
                  ),
                ]),
            Container(height: 5),
            Text(
              "All rights reserved",
              style: IGTextStyles.normalStyle(13, IGColors.text()),
            ),
            if (webConfigurator.internalConfigurator.footerBarConfigurations
                    ?.showCreatorsInsideTheFooter ==
                true)
              Container(height: 5),
            if (webConfigurator.internalConfigurator.footerBarConfigurations
                    ?.showCreatorsInsideTheFooter ==
                true)
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Crafted by  ",
                      style: IGTextStyles.normalStyle(13, IGColors.text()),
                    ),
                    Image.asset(
                      webConfigurator.currentBrightness == Brightness.light
                          ? "assets/images/iglu_logo.png"
                          : "assets/images/logo_main_dark.png",
                      package: "iglu_web_flutter",
                      width: 35,
                      height: 35,
                    ),
                    Link(
                      uri: Uri.parse("https://www.iglu.dev"),
                      target: LinkTarget.blank,
                      builder: (BuildContext context, FollowLink? followLink) =>
                          IGButtonViewDefaults.primaryTextHoverButton(
                              context: context,
                              style: IGButtonViewStyle(
                                  text: " IGLU",
                                  tooltipMessage: "IGLU Agency",
                                  onTapPositioned: (position) {
                                    followLink!();
                                  })),
                    )
                  ],
                ),
              ),
            Container(height: 5 + MediaQuery.of(context).viewPadding.bottom)
          ],
        ),
        decoration: BoxDecoration(
            color: IGColors.primaryBackground(),
            border: BorderDirectional(
                top: BorderSide(color: IGColors.separator()))),
      );
    } else {
      return Container(
        height: 55,
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: CustomScrollView(
          scrollDirection: Axis.horizontal,
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    children: [
                      Text(
                        "${webConfigurator.internalConfigurator.appName} v${webConfigurator.internalConfigurator.version} | Copyright © ${webConfigurator.internalConfigurator.year} ",
                        style: IGTextStyles.normalStyle(13, IGColors.text()),
                      ),
                      Link(
                        uri: Uri.parse(
                            webConfigurator.internalConfigurator.companyURL!),
                        target: LinkTarget.blank,
                        builder: (BuildContext context,
                                FollowLink? followLink) =>
                            IGButtonViewDefaults.primaryTextHoverButton(
                                context: context,
                                style: IGButtonViewStyle(
                                    text:
                                        "${webConfigurator.internalConfigurator.companyName}",
                                    tooltipMessage:
                                        "${webConfigurator.internalConfigurator.companyName}",
                                    onTapPositioned: (position) {
                                      followLink!();
                                    })),
                      ),
                      Text(
                        ". All rights reserved",
                        style: IGTextStyles.normalStyle(13, IGColors.text()),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (webConfigurator.internalConfigurator.footerBarConfigurations
                    ?.showCreatorsInsideTheFooter ==
                true)
              SliverFillRemaining(
                fillOverscroll: false,
                hasScrollBody: false,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          children: [
                            Text(
                              "Crafted by  ",
                              style:
                                  IGTextStyles.normalStyle(13, IGColors.text()),
                            ),
                            Image.asset(
                              webConfigurator.currentBrightness ==
                                      Brightness.light
                                  ? "assets/images/iglu_logo.png"
                                  : "assets/images/logo_main_dark.png",
                              package: "iglu_web_flutter",
                              width: 35,
                              height: 35,
                            ),
                            Link(
                              uri: Uri.parse("https://www.iglu.dev"),
                              target: LinkTarget.blank,
                              builder: (BuildContext context,
                                      FollowLink? followLink) =>
                                  IGButtonViewDefaults.primaryTextHoverButton(
                                      context: context,
                                      style: IGButtonViewStyle(
                                          text: " IGLU",
                                          tooltipMessage: "IGLU Agency",
                                          onTapPositioned: (position) {
                                            followLink!();
                                          })),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
          ],
        ).fixNestedDoubleScrollbar(),
        decoration: BoxDecoration(
            color: IGColors.primaryBackground(),
            border: BorderDirectional(
                top: BorderSide(color: IGColors.separator()))),
      );
    }
  }
}
