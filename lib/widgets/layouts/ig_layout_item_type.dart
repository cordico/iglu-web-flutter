/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:file_picker/file_picker.dart';
import 'package:markdown/markdown.dart' as md;

enum IGLayoutItemType {
  text,
  tag,
  link,
  none,
  stars,
  image,
  header,
  bool,
  wrapTag,
  custom,
  separator,
  space,
  markdown
}

class IGLayoutItemTypeMarkdownOptions {
  final MarkdownStyleSheet? styleSheet;
  final MarkdownStyleSheetBaseTheme? styleSheetTheme;
  final SyntaxHighlighter? syntaxHighlighter;
  final void Function(String, String?, String)? onTapLink;
  final void Function()? onTapText;
  final String? imageDirectory;
  final List<md.BlockSyntax>? blockSyntaxes;
  final List<md.InlineSyntax>? inlineSyntaxes;
  final md.ExtensionSet? extensionSet;
  final Widget Function(Uri, String?, String?)? imageBuilder;
  final Widget Function(bool)? checkboxBuilder;
  final Widget Function(int, BulletStyle)? bulletBuilder;
  final Map<String, MarkdownElementBuilder> builders;
  final MarkdownListItemCrossAxisAlignment listItemCrossAxisAlignment;
  final bool shrinkWrap;
  final bool fitContent;
  final bool softLineBreak;
  final EdgeInsets? padding;
  final ScrollController? controller;
  final ScrollPhysics? physics;
  final Map<String, MarkdownPaddingBuilder>? paddingBuilders;
  IGLayoutItemTypeMarkdownOptions(
      {this.styleSheet,
      this.styleSheetTheme,
      this.syntaxHighlighter,
      this.onTapLink,
      this.onTapText,
      this.imageDirectory,
      this.blockSyntaxes,
      this.inlineSyntaxes,
      this.extensionSet,
      this.imageBuilder,
      this.checkboxBuilder,
      this.bulletBuilder,
      this.builders = const <String, MarkdownElementBuilder>{},
      this.listItemCrossAxisAlignment =
          MarkdownListItemCrossAxisAlignment.baseline,
      this.shrinkWrap = true,
      this.fitContent = true,
      this.softLineBreak = false,
      this.padding = const EdgeInsets.all(16.0),
      this.controller,
      this.physics,
      this.paddingBuilders});
}

class IGLayoutItemTypeHeaderOptions {
  final String? title;
  final Color? titleColor;
  final IconData? icon;
  final TextStyle? textStyle;
  final bool showSeparator;
  final Color? iconColor;
  IGLayoutItemTypeHeaderOptions(
      {this.title,
      this.titleColor,
      this.icon,
      this.iconColor,
      this.showSeparator = true,
      this.textStyle});
}

class IGLayoutItemTypeImagePickFilesOptions {
  final String? dialogTitle;
  final FileType type;
  final List<String>? allowedExtensions;
  final dynamic Function(FilePickerStatus)? onFileLoading;
  final bool allowCompression;
  final bool allowMultiple;
  final bool withData;
  final bool withReadStream;
  IGLayoutItemTypeImagePickFilesOptions({
    this.dialogTitle,
    this.type = FileType.any,
    this.allowedExtensions,
    this.onFileLoading,
    this.allowCompression = true,
    this.allowMultiple = false,
    this.withData = true,
    this.withReadStream = false,
  });
}

class IGLayoutItemTypeImageOptions {
  final PlatformFile? fileImage;
  final String? imageUrl;
  final Color? defaultColor;
  final Color? loadingColor;
  final Color? hoverColor;
  final Color? backgroundHoverColor;
  final Future<bool> Function()? onDelete;
  final Function(PlatformFile)? onTap;
  final Function()? onTapPlaceholder;
  final Future<bool> Function(PlatformFile)? onUpload;
  final double? borderRadius;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? internalPadding;
  final BoxConstraints? constraints;
  final Widget? placeholderWidget;
  final double? placeholderCornerRadius;
  final bool showLoading;
  final bool? showFullScreen;
  final bool isFullScreen;
  final BoxFit fit;
  final bool sendHttpHeaders;
  final Map<String, String>? httpHeaders;
  final ImageRenderMethodForWeb? imageRenderMethodForWeb;
  final bool absorbing;
  final String? heroTag;
  final List<ActionMenuButtonOption> actions;
  final List<ActionMenuButtonOption> pickImageActions;
  final bool showBorder;
  final bool showDefaultProgressIndicator;
  final IGLayoutItemTypeImagePickFilesOptions? imagePickFilesOptions;
  IGLayoutItemTypeImageOptions(
      {this.fileImage,
      this.imageUrl,
      this.defaultColor,
      this.onDelete,
      this.loadingColor,
      this.showFullScreen,
      this.onTap,
      this.onTapPlaceholder,
      this.onUpload,
      this.backgroundColor,
      this.borderRadius,
      this.internalPadding,
      this.constraints,
      this.hoverColor,
      this.heroTag,
      this.backgroundHoverColor,
      this.placeholderCornerRadius,
      this.placeholderWidget,
      this.showLoading = false,
      this.isFullScreen = false,
      this.sendHttpHeaders = true,
      this.absorbing = false,
      this.showBorder = true,
      this.httpHeaders,
      this.showDefaultProgressIndicator = true,
      this.fit = BoxFit.cover,
      this.imageRenderMethodForWeb,
      this.imagePickFilesOptions,
      this.actions = const [],
      this.pickImageActions = const []});
}
