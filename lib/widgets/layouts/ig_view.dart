/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGView extends StatefulWidget {
  final Key? key;
  final Widget? Function(Set<MaterialState>, BuildContext)? child;
  final Widget? childNoState;
  final MaterialStateProperty<Color?>? color;
  final Function()? onTap;
  final Function(BuildContext, RelativeRect)? onTapPositioned;
  final Function()? onLongPress;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final double? radius;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  final BoxBorder? border;
  final double? height;
  final double? width;
  final BoxConstraints? constraints;
  final BoxDecoration? decoration;
  final bool enabled;
  final bool selected;
  final bool bouncingEnabled;
  final bool vibrate;
  final MouseCursor? mouseCursor;
  IGView(
      {this.key,
      this.child,
      this.childNoState,
      this.onTap,
      this.onTapPositioned,
      this.onLongPress,
      this.margin,
      this.padding,
      this.radius,
      this.borderRadius,
      this.boxShadow,
      this.border,
      this.height,
      this.width,
      this.constraints,
      this.decoration,
      this.color,
      this.vibrate = false,
      this.enabled = true,
      this.selected = false,
      this.bouncingEnabled = false,
      this.mouseCursor});
  @override
  IGViewState createState() => IGViewState();
}

class IGViewState extends State<IGView> {
  MaterialStateProperty<Color?>? color;

  final Set<MaterialState> _states = <MaterialState>{};

  //bool get _selected => _states.contains(MaterialState.selected);
  bool get _hovered => _states.contains(MaterialState.hovered);
  bool get _focused => _states.contains(MaterialState.focused);
  bool get _pressed => _states.contains(MaterialState.pressed);
  bool get _disabled => _states.contains(MaterialState.disabled);

  void _updateState(MaterialState state, bool value) {
    value ? _states.add(state) : _states.remove(state);
  }

  void _handleHighlightChanged(bool value) {
    if (_pressed != value) {
      setState(() {
        _updateState(MaterialState.pressed, value);
      });
    }
  }

  void _handleHoveredChanged(bool value) {
    if (_hovered != value) {
      setState(() {
        _updateState(MaterialState.hovered, value);
      });
    }
  }

  void _handleFocusedChanged(bool value) {
    if (_focused != value) {
      setState(() {
        _updateState(MaterialState.focused, value);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.color == null) {
      color = MaterialStateProperty.resolveWith<Color?>(
          (Set<MaterialState> states) {
        if (states.contains(MaterialState.hovered) ||
            states.contains(MaterialState.pressed)) {
          return IGHoverType.normal.hoverColor();
        }
        return null;
      });
    } else {
      color = widget.color;
    }
    _updateState(MaterialState.disabled, !widget.enabled);
    _updateState(MaterialState.selected, widget.selected);
  }

  @override
  void didUpdateWidget(IGView oldWidget) {
    super.didUpdateWidget(oldWidget);
    _updateState(MaterialState.disabled, !widget.enabled);
    // If the button is disabled while a press gesture is currently ongoing,
    // InkWell makes a call to handleHighlightChanged. This causes an exception
    // because it calls setState in the middle of a build. To preempt this, we
    // manually update pressed to false when this situation occurs.
    if (_disabled && _pressed) {
      _handleHighlightChanged(false);
    }
    if (oldWidget.selected != widget.selected) {
      _updateState(MaterialState.selected, widget.selected);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.bouncingEnabled) {
      return AbsorbPointer(absorbing: _disabled, child: _child(context));
    } else {
      return AbsorbPointer(
          absorbing: _disabled, child: IGBouncingView(child: _child(context)));
    }
  }

  _borderRadius() {
    return widget.border != null && !widget.border!.isUniform
        ? null
        : widget.borderRadius ??
            BorderRadius.circular(
              widget.radius ?? kButtonPrimaryCornerRadius,
            );
  }

  _subchild() {
    return widget.childNoState != null
        ? widget.childNoState
        : widget.padding != null
            ? Padding(
                child: widget.child!(_states, context),
                padding: widget.padding!)
            : widget.child!(_states, context);
  }

  Widget _child(BuildContext context) {
    return Container(
        key: widget.key,
        constraints: widget.constraints,
        height: widget.height,
        width: widget.width,
        margin: widget.margin,
        decoration: widget.decoration ??
            BoxDecoration(
                color: _states.contains(MaterialState.pressed) ||
                        _states.contains(MaterialState.hovered)
                    ? color!.resolve(Set.from([]))
                    : color!.resolve(_states),
                borderRadius: _borderRadius(),
                boxShadow: widget.boxShadow,
                border: widget.border),
        child: Material(
          borderRadius: _borderRadius(),
          type: MaterialType.transparency,
          child: InkWell(
              mouseCursor: widget.mouseCursor,
              onHighlightChanged: _handleHighlightChanged,
              onHover: _handleHoveredChanged,
              onFocusChange: _handleFocusedChanged,
              splashColor: Colors.transparent,
              highlightColor: color!.resolve(Set.from([MaterialState.pressed])),
              hoverColor: color!.resolve(Set.from([MaterialState.hovered])),
              borderRadius: _borderRadius(),
              onTap: widget.onTap != null || widget.onTapPositioned != null
                  ? () async {
                      if (widget.vibrate) {
                        await HapticFeedback.vibrate();
                      }
                      if (widget.onTapPositioned != null) {
                        var position = RelativeRect.fromLTRB(
                            context.globalPaintBounds!.left,
                            context.globalPaintBounds!.top +
                                context.globalPaintBounds!.height,
                            context.globalPaintBounds!.right,
                            context.globalPaintBounds!.bottom);

                        await widget.onTapPositioned!(context, position);
                      } else {
                        widget.onTap!();
                      }
                    }
                  : null,
              onLongPress: widget.onLongPress,
              child: _subchild()),
        ));
  }
}
