/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGLayout {
  static DetailViewCellRule columnLayout(
      {required BuildContext context,
      required BoxConstraints outSideConstraints,
      required List<IGLayoutItem> items}) {
    List<Widget> finalItems = [];
    List<Widget> allItems = [];

    bool isLarge =
        SizeDetector.isLargeScreen(context, width: outSideConstraints.maxWidth);
    bool isMedium = SizeDetector.isMediumScreen(context,
        width: outSideConstraints.maxWidth);
    bool isMobile =
        SizeDetector.isMobile(context, width: outSideConstraints.maxWidth);

    List<Widget> currentLeft = [];
    List<Widget> currentRight = [];
    items.asMap().forEach((index, value) {
      var isLastLine = ((items.length / 2) - 1).round() == finalItems.length;
      var isLeft = index % 2 == 0;
      var isLast = index == items.length - 1;
      var isNone = value.type == IGLayoutItemType.none;
      var isSpace = value.type == IGLayoutItemType.space;
      var isCustom = value.type == IGLayoutItemType.custom ||
          value.type == IGLayoutItemType.separator ||
          value.type == IGLayoutItemType.space ||
          value.type == IGLayoutItemType.header;
      //CALCULATE
      IGLayoutItem? nextItem;
      if (index + 1 <= items.length - 1) {
        nextItem = items[index + 1];
      }
      IGLayoutItem? previousItem;
      if (index - 1 > 0) {
        previousItem = items[index - 1];
      }
      var isAlone = (isLeft && isLast) ||
          (!isLeft && isLast) ||
          (isLeft &&
              nextItem != null &&
              (nextItem.type == IGLayoutItemType.none ||
                  (nextItem.hideOnMobile && isMobile))) ||
          (!isLeft &&
              previousItem != null &&
              (previousItem.type == IGLayoutItemType.none ||
                  (previousItem.hideOnMobile && isMobile)));
      var val = Padding(
          padding: value.outsidePadding,
          child: value.elaborate(
              context: context,
              constraints: outSideConstraints,
              isLeft: isLeft,
              isAlone: isAlone));
      var space = Container(
          height:
              isMobile ? value.intraItemHeight / 2.0 : value.intraItemHeight);
      var miniSpace = Container(height: value.intraItemHeight / 2.0);
      var separator = Divider(
          thickness: 1,
          height: 1,
          endIndent: 0,
          indent: value.mobileSeparatorLineIndent,
          color: IGColors.separator());
      if (!isNone) {
        value.logging("-----1");
        if (isLeft) {
          currentLeft.add(val);
          value.logging("-----2");
          if (!isLast && !isLastLine && !isSpace) {
            value.logging("-----3");
            currentLeft.add(space);
          }
        } else {
          value.logging("-----4");
          currentRight.add(val);
          if (!isLast && !isLastLine && !isSpace) {
            currentRight.add(space);
            value.logging("-----5");
          }
        }
      }
      if (!isNone) {
        value.logging("-----6");
        allItems.add(val);
      }
      if (!isLast) {
        if (isMobile &&
            !isNone &&
            !isCustom &&
            value.showAutomaticallySeparatorLineOnMobile) {
          if (!isSpace) {
            value.logging("-----7");
            allItems.add(miniSpace);
          }
          value.logging("-----8");
          allItems.add(separator);
        }
        if (!isNone && !isSpace) {
          value.logging("-----9");
          allItems.add(space);
        }
      }

      if (!isLeft || (isLeft && isLast)) {
        value.logging("-----10");
        finalItems.add(IGLayoutItem.generateRow(
            List.from(currentLeft), List.from(currentRight)));
        currentLeft.clear();
        currentRight.clear();
      }
    });
    return DetailViewCellRule(
      type: DetailViewCellRuleType.custom,
      forceOnlyCustom: true,
      customWidget: isLarge || isMedium
          ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: finalItems,
              ),
            )
          : Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: allItems,
              ),
            ),
    );
  }

  static DetailViewCellRule loading(
      {required BuildContext context,
      required BoxConstraints constraints,
      double spaceHeight = 75}) {
    return IGLayout.columnLayout(
        context: context,
        outSideConstraints: constraints,
        items: [
          IGLayoutItem(type: IGLayoutItemType.space, spaceHeight: 75),
          IGLayoutItem(type: IGLayoutItemType.none),
          IGLayoutItem(
              type: IGLayoutItemType.text,
              textAlign: TextAlign.center,
              showAutomaticallySeparatorLineOnMobile: false,
              valueStyle:
                  IGTextStyles.custom(color: IGColors.text(), fontSize: 30),
              value: "Loading..."),
          IGLayoutItem(type: IGLayoutItemType.none),
          IGLayoutItem(type: IGLayoutItemType.space, spaceHeight: 75),
        ]);
  }

  static dynamic emptyState(
      {required BuildContext context,
      required BoxConstraints? constraints,
      double spaceHeight = 75,
      bool returnItems = false,
      String value = "No items registered"}) {
    var items = [
      IGLayoutItem(type: IGLayoutItemType.space, spaceHeight: spaceHeight),
      IGLayoutItem(type: IGLayoutItemType.none),
      IGLayoutItem(
          type: IGLayoutItemType.text,
          textAlign: TextAlign.center,
          showAutomaticallySeparatorLineOnMobile: false,
          valueStyle: IGTextStyles.custom(color: IGColors.text(), fontSize: 20),
          value: value),
      IGLayoutItem(type: IGLayoutItemType.none),
      IGLayoutItem(type: IGLayoutItemType.space, spaceHeight: spaceHeight),
    ];
    if (returnItems) {
      return items;
    }
    return IGLayout.columnLayout(
        context: context, outSideConstraints: constraints!, items: items);
  }

  static Widget rowTwoChildsAdaptable(
      {required BuildContext context,
      required BoxConstraints constraints,
      required List<Widget> children,
      bool columnOnlySmall = true}) {
    var isColumn =
        SizeDetector.isSmallScreen(context, width: constraints.maxWidth) ||
            SizeDetector.isMediumScreen(context, width: constraints.maxWidth);
    if (columnOnlySmall) {
      isColumn =
          SizeDetector.isSmallScreen(context, width: constraints.maxWidth);
    }
    List<Widget> _children = [];
    children.asMap().forEach((index, e) {
      _children.add(e);
      if (index != children.length - 1)
        _children.add(Container(
            width: isColumn ? null : 20, height: isColumn ? 20 : null));
    });

    return isColumn
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: _children)
        : Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: _children);
  }
}
