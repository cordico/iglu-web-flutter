/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGViewStyle {
  IGViewStyle(
      {this.color,
      this.iconColor,
      this.subtitleColor,
      this.titleColor,
      this.iconBackgrundColor,
      this.subTitleStyle,
      this.titleStyle});
  MaterialStateProperty<Color>? color;
  MaterialStateProperty<Color>? titleColor;
  MaterialStateProperty<Color>? subtitleColor;
  MaterialStateProperty<TextStyle>? titleStyle;
  MaterialStateProperty<TextStyle>? subTitleStyle;
  MaterialStateProperty<Color>? iconColor;
  MaterialStateProperty<Color>? iconBackgrundColor;

  IGViewStyle copyWith({required IGViewStyle style}) {
    return IGViewStyle(
        color: style.color ?? this.color,
        iconBackgrundColor: style.iconBackgrundColor ?? this.iconBackgrundColor,
        iconColor: style.iconColor ?? this.iconColor,
        subTitleStyle: style.subTitleStyle ?? this.subTitleStyle,
        subtitleColor: style.subtitleColor ?? this.subtitleColor,
        titleColor: style.titleColor ?? this.titleColor,
        titleStyle: style.titleStyle ?? this.titleStyle);
  }
}

extension IGViewStyleExtension on IGViewStyle {
  static MaterialStateProperty<Color> opacityBackgroundColorBasedOnColor(
      {required Color color}) {
    return MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
      if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed) ||
          states.contains(MaterialState.selected)) {
        return color;
      }
      return color.withOpacity(
          webConfigurator.currentBrightness == Brightness.light ? 0.20 : 0.7);
    });
  }

  static MaterialStateProperty<Color> get primaryOpacityBackgroundColor {
    return MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
      if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed) ||
          states.contains(MaterialState.selected)) {
        return IGColors.primary();
      }
      return IGColors.primary(useBrightnessOpacity: true);
    });
  }

  static MaterialStateProperty<Color> get primaryOpacityTextColor {
    return MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
      if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed) ||
          states.contains(MaterialState.selected)) {
        return Colors.white;
      }
      return IGColors.text();
    });
  }

  static IGViewStyle gray() {
    return IGViewStyle(color:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed)) {
        return IGHoverType.normal.hoverColor();
      } else if (states.contains(MaterialState.selected)) {
        return IGHoverType.normal.hoverColor();
      }
      return Colors.transparent;
    }), iconColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return IGColors.text();
    }), titleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return IGColors.text();
    }), subtitleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return IGColors.textSecondary();
    }), iconBackgrundColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return Colors.transparent;
    }));
  }

  static IGViewStyle primary() {
    return IGViewStyle(color:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed)) {
        return IGHoverType.normal.hoverColor();
      } else if (states.contains(MaterialState.selected)) {
        return IGColors.primary(useBrightnessOpacity: true);
      }
      return Colors.transparent;
    }), iconColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return IGColors.primary(forceWhiteInDarkMode: true);
      }
      return IGColors.text();
    }), titleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return IGColors.primary(forceWhiteInDarkMode: true);
      }
      return IGColors.text();
    }), subtitleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return IGColors.text();
      }
      return IGColors.textSecondary();
    }), iconBackgrundColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return Colors.transparent;
    }));
  }

  static IGViewStyle sidebar() {
    return IGViewStyle(color:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected)) {
        return IGColors.primary(useBrightnessOpacity: true);
      } else if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed)) {
        return IGHoverType.normal.hoverColor();
      }
      return Colors.transparent;
    }), iconColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected) &&
          webConfigurator.currentBrightness == Brightness.light) {
        return IGColors.primary();
      }
      return IGColors.text();
    }), titleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.selected) &&
          webConfigurator.currentBrightness == Brightness.light) {
        return IGColors.primary();
      }
      return IGColors.text();
    }), subtitleColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return IGColors.textSecondary();
    }), iconBackgrundColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      return Colors.transparent;
    }));
  }
}
