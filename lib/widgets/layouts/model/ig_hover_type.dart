/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

enum IGHoverType { normal }

extension IGHoverTypeExtension on IGHoverType {
  Color hoverColor() {
    Color color = Colors.transparent;
    if (this == IGHoverType.normal) {
      return webConfigurator.currentBrightness == Brightness.dark
          ? Colors.white.withOpacity(0.04)
          : Colors.black.withOpacity(0.04);
    }
    return color;
  }
}
