/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGLayoutItem {
  IGLayoutItemType type;

  //custom
  Widget? custom;

  //label
  String? label;
  bool isLabelOnTheLeft; //LEFT OR RIGHT
  Container? labelWidget;
  bool showLabelNearValueInMobile;

  TextAlign textAlign;
  TextStyle? labelStyle;
  TextStyle? valueStyle;

  //options
  IGLayoutItemTypeImageOptions? imageOptions;
  IGLayoutItemTypeHeaderOptions? headerOptions;

  //markdown
  IGLayoutItemTypeMarkdownOptions? markdownOptions;

  //all
  dynamic value;

  //stars, tag, bool
  Color? tagColor;

  //LINK
  bool isMail;
  bool isCopy;
  String? tooltipMessage;
  String? urlToOpen;

  //SPACE AND SEPARATOR
  double spaceHeight;
  double spaceWidth;
  EdgeInsets? separatorIndent;

  bool showAutomaticallySeparatorLineOnMobile;
  double mobileSeparatorLineIndent;

  bool hideOnMobile;

  Function(dynamic value)? onTap;

  EdgeInsets outsidePadding;
  double intraItemHeight;

  bool logEnabled;

  bool selectableText;

  List<Widget> actions;

  String key;

  IGLayoutItem({
    required this.type,
    this.label,
    this.value,
    this.isCopy = false,
    this.isMail = false,
    this.tagColor,
    this.showLabelNearValueInMobile = false,
    this.onTap,
    this.markdownOptions,
    this.custom,
    this.textAlign = TextAlign.start,
    this.isLabelOnTheLeft = true,
    this.imageOptions,
    this.tooltipMessage,
    this.labelWidget,
    this.labelStyle,
    this.valueStyle,
    this.spaceHeight = 0,
    this.spaceWidth = 0,
    this.showAutomaticallySeparatorLineOnMobile = true,
    this.mobileSeparatorLineIndent = 20,
    this.intraItemHeight = 30,
    this.hideOnMobile = false,
    this.separatorIndent = EdgeInsets.zero,
    this.outsidePadding = const EdgeInsets.fromLTRB(20, 0, 20, 0),
    this.urlToOpen,
    this.headerOptions,
    this.actions = const [],
    this.logEnabled = false,
    this.selectableText = true,
    this.key = "",
  });

  static Row Function(List<Widget>, List<Widget>) generateRow =
      (List<Widget> leftItems, List<Widget> rightItems) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (leftItems.isNotEmpty)
          Flexible(
            flex: rightItems.isNotEmpty ? 5 : 1,
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: leftItems),
            ),
          ),
        if (rightItems.isNotEmpty) Container(width: 20),
        if (rightItems.isNotEmpty)
          Flexible(
            flex: 5,
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: rightItems),
            ),
          )
      ],
    );
  };

  logging(dynamic value) {
    if (logEnabled) {
      logger(value, tag: "[IGLayoutItem]");
    }
  }

  Widget _labelRule(
      {required BuildContext context,
      required BoxConstraints constraints,
      bool isLeft = true,
      bool isAlone = false}) {
    var isSmall =
        SizeDetector.isSmallScreen(context, width: constraints.maxWidth);
    var isMobile = SizeDetector.isMobile(context, width: constraints.maxWidth);
    var width = isMobile
        ? constraints.maxWidth - 50
        : isSmall
            ? (constraints.maxWidth - 50) / 2.0
            : ((constraints.maxWidth - 80) / 4.0);
    var child = Container(
      //color: Colors.yellow,
      width: labelWidget != null ? null : width,
      child: labelWidget != null
          ? labelWidget
          : Text(this.label ?? "",
              overflow: TextOverflow.ellipsis,
              maxLines: 20,
              textAlign: textAlign,
              style: labelStyle ??
                  IGTextStyles.boldStyle(16, IGColors.textSecondary())),
    );
    return label != null || labelWidget != null
        ? Padding(
            padding: EdgeInsets.fromLTRB(!isLabelOnTheLeft ? 10 : 0, 0,
                isLabelOnTheLeft ? 10 : 0, isMobile ? 3 : 0),
            child: child,
          )
        : Container();
  }

  Widget? elaborate(
      {required BuildContext context,
      required BoxConstraints constraints,
      bool isLeft = true,
      bool isAlone = false}) {
    var isMobile = SizeDetector.isMobile(context, width: constraints.maxWidth);
    var label = _labelRule(
        constraints: constraints,
        context: context,
        isLeft: isLeft,
        isAlone: isAlone);
    if (type == IGLayoutItemType.custom) {
      return custom;
    } else if (type == IGLayoutItemType.space) {
      return Container(height: spaceHeight, width: spaceWidth);
    } else if (type == IGLayoutItemType.separator) {
      return Divider(
          height: spaceHeight,
          thickness: 1.0,
          color: IGColors.separator(),
          endIndent: separatorIndent == null ? 20 : separatorIndent!.left,
          indent: separatorIndent == null ? 20 : separatorIndent!.right);
    }
    if (type == IGLayoutItemType.text) {
      var text = Container(
          child: Text(value ?? "",
              textAlign: textAlign,
              style:
                  valueStyle ?? IGTextStyles.custom(color: IGColors.text())));
      if (selectableText) {
        text = Container(
            child: SelectableText(value ?? "",
                textAlign: textAlign,
                style:
                    valueStyle ?? IGTextStyles.custom(color: IGColors.text())));
      }
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    text,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Expanded(child: text),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.markdown) {
      var text = Container(
          child: IGLayoutItemWidgetHelper.markdownBody(
              data: value ?? "",
              selectable: selectableText,
              markdownOptions: markdownOptions));

      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    text,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Expanded(child: text),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.header) {
      return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                    if (headerOptions!.icon != null)
                      Icon(headerOptions!.icon,
                          size: 30,
                          color:
                              headerOptions!.iconColor ?? IGColors.primary()),
                    if (headerOptions!.icon != null) Container(width: 10),
                    Expanded(
                        child: Text(headerOptions!.title!,
                            style: headerOptions!.textStyle ??
                                IGTextStyles.boldStyle(
                                    22,
                                    headerOptions!.titleColor ??
                                        IGColors.text())))
                  ] +
                  actions,
            ),
            if (headerOptions!.showSeparator) Container(height: 10),
            if (headerOptions!.showSeparator)
              Divider(
                  thickness: 1,
                  height: 1,
                  color: IGColors.separator(),
                  endIndent:
                      separatorIndent == null ? 20 : separatorIndent!.left,
                  indent:
                      separatorIndent == null ? 20 : separatorIndent!.right),
          ]);
    } else if (type == IGLayoutItemType.image) {
      BoxConstraints? _boxConstraints = imageOptions!.constraints;
      var margin = (isAlone ? 42 : 52);
      var containerWidth = isAlone
          ? (constraints.maxWidth - margin)
          : ((constraints.maxWidth / 2.0) - margin);
      if (_boxConstraints == null) {
        _boxConstraints = BoxConstraints(
            maxWidth: containerWidth, maxHeight: containerWidth / 2.0);
      } else {
        var newMaxHeight = _boxConstraints.maxHeight;
        if (newMaxHeight == double.infinity) {
          newMaxHeight = containerWidth / 2.0;
        }
        var newMaxWidth = _boxConstraints.maxWidth;
        if (newMaxWidth == double.infinity) {
          newMaxWidth = containerWidth;
        }

        _boxConstraints =
            BoxConstraints(maxWidth: newMaxWidth, maxHeight: newMaxHeight);
      }
      var text = ImageContainerView(
        absorbing: imageOptions!.absorbing,
        actions: imageOptions!.actions,
        backgroundColor: imageOptions!.backgroundColor,
        backgroundHoverColor: imageOptions!.backgroundHoverColor,
        borderRadius: imageOptions!.borderRadius,
        constraints: _boxConstraints,
        defaultColor: imageOptions!.defaultColor,
        fileImage: imageOptions!.fileImage,
        fit: imageOptions!.fit,
        heroTag: imageOptions!.heroTag,
        hoverColor: imageOptions!.hoverColor,
        imageRenderMethodForWeb: imageOptions!.imageRenderMethodForWeb,
        imageUrl: imageOptions!.imageUrl,
        internalPadding: imageOptions!.internalPadding,
        isFullScreen: imageOptions!.isFullScreen,
        loadingColor: imageOptions!.loadingColor,
        onDelete: imageOptions!.onDelete,
        onTap: imageOptions!.onTap,
        onTapPlaceholder: imageOptions!.onTapPlaceholder,
        onUpload: imageOptions!.onUpload,
        placeholderCornerRadius: imageOptions!.placeholderCornerRadius,
        placeholderWidget: imageOptions!.placeholderWidget,
        sendHttpHeaders: imageOptions!.sendHttpHeaders,
        httpHeaders: imageOptions!.httpHeaders,
        showBorder: imageOptions!.showBorder,
        showDefaultProgressIndicator:
            imageOptions!.showDefaultProgressIndicator,
        showFullScreen: imageOptions!.showFullScreen,
        showLoading: imageOptions!.showLoading,
        imagePickFilesOptions: imageOptions!.imagePickFilesOptions,
        pickImageActions: imageOptions!.pickImageActions,
      );
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    text,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Flexible(child: text),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.bool) {
      var text = Container(
          child: Row(
        children: [
          Icon(
              value == true ? Icons.check_circle_rounded : Icons.cancel_rounded,
              size: 24,
              color: value == true
                  ? IGColors.primary(forceWhiteInDarkMode: true)
                  : IGColors.textSecondary()),
          Container(width: 5),
          Expanded(
            child: Text(value == true ? "Yes" : "No",
                textAlign: textAlign,
                style: valueStyle ??
                    IGTextStyles.custom(color: IGColors.text(), height: 1.3)),
          )
        ],
      ));
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    text,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Expanded(child: text),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.stars) {
      List<Widget> starWidgets = [];
      for (var i = 0; i < value; i++) {
        starWidgets.add(Icon(Icons.star_rounded,
            color: tagColor ?? IGColors.primary(forceWhiteInDarkMode: true),
            size: 27));
      }
      var stars = Container(
        child: Row(
          mainAxisAlignment: labelWidget != null
              ? MainAxisAlignment.center
              : MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: starWidgets,
        ),
      );
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    stars,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Expanded(child: stars),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.wrapTag) {
      var button = Wrap(
          alignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.start,
          spacing: 8,
          runSpacing: 8,
          children: (value as List)
              .map((e) => Container(
                  child: IGButtonViewDefaults.primaryTagBackgroundButton(
                      context: context,
                      tagColor: tagColor ??
                          IGColors.primary(forceWhiteInDarkMode: true),
                      style: IGButtonViewStyle(
                          text: e,
                          onTapPositioned: onTap != null
                              ? (_) {
                                  onTap!(e);
                                }
                              : null,
                          internalPadding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                          showExpandedVersion: true))))
              .toList());
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    button,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Flexible(child: button),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.tag) {
      var button = Container(
          //color: Colors.green,
          child: IGButtonViewDefaults.primaryTagBackgroundButton(
              context: context,
              tagColor:
                  tagColor ?? IGColors.primary(forceWhiteInDarkMode: true),
              style: IGButtonViewStyle(
                  text: value,
                  onTapPositioned: onTap != null
                      ? (_) {
                          onTap!(value);
                        }
                      : null,
                  internalPadding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  showExpandedVersion: true)));
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    button,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Flexible(child: button),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    } else if (type == IGLayoutItemType.link) {
      var urlToOpen = this.urlToOpen != null
          ? this.urlToOpen
          : this.onTap == null &&
                  !this.isCopy &&
                  this.value != null &&
                  this.value.isNotEmpty
              ? (this.isMail ? "mailto:${this.value}" : this.value)
              : null;
      var tooltipMessage = this.value == null || this.value.isEmpty
          ? null
          : this.tooltipMessage != null
              ? this.tooltipMessage
              : this.isMail
                  ? "Send an email to ${this.value}"
                  : this.isCopy
                      ? "Copy ${this.value}"
                      : "Open ${this.value}";
      var onTap = urlToOpen != null
          ? null
          : (_) {
              if (this.onTap != null) {
                this.onTap!(null);
              } else if (this.isCopy) {
                if (this.value.isNotEmpty) {
                  webConfigurator.showFlush(
                      context: context,
                      icon: Icon(Icons.copy_rounded,
                          size: 24, color: IGColors.text(isOpposite: true)),
                      content: "Copied ${this.value ?? ""}");
                  Clipboard.setData(ClipboardData(text: this.value));
                }
              }
            };
      var button = (FollowLink? followLink) {
        return Container(
            //color: Colors.green,
            child: IGButtonViewDefaults.defaultTextHoverButton(
                context: context,
                style: IGButtonViewStyle(
                    text: value,
                    showExpandedVersion: true,
                    tooltipMessage: tooltipMessage,
                    externalPadding: EdgeInsets.zero,
                    internalPadding: EdgeInsets.zero,
                    height: 20,
                    onTapPositioned: (_) async {
                      if (onTap != null) {
                        onTap(null);
                      } else if (urlToOpen != null) {
                        followLink!();
                      }
                    })));
      };
      Widget link;
      if (urlToOpen != null) {
        link = Link(
            uri: Uri.parse(urlToOpen),
            target: LinkTarget.blank,
            builder: (BuildContext context, FollowLink? followLink) =>
                button(followLink));
      } else {
        link = button(null);
      }
      return Container(
        child: isMobile && !showLabelNearValueInMobile
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    link,
                    if (!isLabelOnTheLeft) label
                  ])
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: labelWidget != null
                    ? CrossAxisAlignment.center
                    : CrossAxisAlignment.start,
                children: [
                    if (isLabelOnTheLeft) label,
                    Flexible(child: link),
                    if (!isLabelOnTheLeft) label
                  ]),
      );
    }
    return Container();
  }

  static IGLayoutItem get none {
    return IGLayoutItem(type: IGLayoutItemType.none);
  }
}

extension IGLayoutItemWidgetHelper on IGLayoutItem {
  static MarkdownBody markdownBody(
      {required String data,
      bool selectable = false,
      IGLayoutItemTypeMarkdownOptions? markdownOptions}) {
    return MarkdownBody(
      data: data,
      selectable: selectable,
      onTapLink: markdownOptions?.onTapLink ??
          (String text, String? href, String title) {
            if (href != null) {
              webConfigurator.openUrl(url: href);
            }
          },
      extensionSet: markdownOptions?.extensionSet,
      styleSheet: markdownOptions?.styleSheet ??
          MarkdownStyleSheet(
            a: IGTextStyles.body1(IGColors.primary()),
            p: IGTextStyles.body1(IGColors.text()),
            code: IGTextStyles.body1(IGColors.text()),
            h1: IGTextStyles.headline1(IGColors.text()),
            h2: IGTextStyles.headline2(IGColors.text()),
            h3: IGTextStyles.headline3(IGColors.text()),
            h4: IGTextStyles.headline4(IGColors.text()),
            h5: IGTextStyles.headline5(IGColors.text()),
            h6: IGTextStyles.headline6(IGColors.text()),
            em: IGTextStyles.body1(IGColors.text()),
            strong: IGTextStyles.body1(IGColors.text())
                .copyWith(fontWeight: FontWeight.bold),
            del: IGTextStyles.body1(IGColors.text()),
            blockquote: IGTextStyles.body1(IGColors.text()),
            img: IGTextStyles.body1(IGColors.primary()),
            checkbox: IGTextStyles.body1(IGColors.text()),
            listBullet: IGTextStyles.body1(IGColors.text()),
            tableBody: IGTextStyles.body1(IGColors.text()),
            tableHead: IGTextStyles.body1(IGColors.text()),
          ),
      blockSyntaxes: markdownOptions?.blockSyntaxes,
      builders: markdownOptions?.builders ?? {},
      bulletBuilder: markdownOptions?.bulletBuilder,
      checkboxBuilder: markdownOptions?.checkboxBuilder,
      fitContent: markdownOptions?.fitContent ?? true,
      imageBuilder: markdownOptions?.imageBuilder,
      imageDirectory: markdownOptions?.imageDirectory,
      inlineSyntaxes: markdownOptions?.inlineSyntaxes,
      listItemCrossAxisAlignment: markdownOptions?.listItemCrossAxisAlignment ??
          MarkdownListItemCrossAxisAlignment.baseline,
      onTapText: markdownOptions?.onTapText,
      shrinkWrap: markdownOptions?.shrinkWrap ?? true,
      softLineBreak: markdownOptions?.softLineBreak ?? false,
      styleSheetTheme: markdownOptions?.styleSheetTheme,
      syntaxHighlighter: markdownOptions?.syntaxHighlighter,
    );
  }

  static Markdown markdown(
      {required String data,
      bool selectable = false,
      IGLayoutItemTypeMarkdownOptions? markdownOptions}) {
    return Markdown(
      data: data,
      selectable: selectable,
      onTapLink: markdownOptions?.onTapLink ??
          (String text, String? href, String title) {
            if (href != null) {
              webConfigurator.openUrl(url: href);
            }
          },
      extensionSet: markdownOptions?.extensionSet,
      styleSheet: markdownOptions?.styleSheet ??
          MarkdownStyleSheet(
            a: IGTextStyles.body1(IGColors.primary()),
            p: IGTextStyles.body1(IGColors.text()),
            code: IGTextStyles.body1(IGColors.text()),
            h1: IGTextStyles.headline1(IGColors.text()),
            h2: IGTextStyles.headline2(IGColors.text()),
            h3: IGTextStyles.headline3(IGColors.text()),
            h4: IGTextStyles.headline4(IGColors.text()),
            h5: IGTextStyles.headline5(IGColors.text()),
            h6: IGTextStyles.headline6(IGColors.text()),
            em: IGTextStyles.body1(IGColors.text()),
            strong: IGTextStyles.body1(IGColors.text())
                .copyWith(fontWeight: FontWeight.bold),
            del: IGTextStyles.body1(IGColors.text()),
            blockquote: IGTextStyles.body1(IGColors.text()),
            img: IGTextStyles.body1(IGColors.primary()),
            checkbox: IGTextStyles.body1(IGColors.text()),
            listBullet: IGTextStyles.body1(IGColors.text()),
            tableBody: IGTextStyles.body1(IGColors.text()),
            tableHead: IGTextStyles.body1(IGColors.text()),
          ),
      blockSyntaxes: markdownOptions?.blockSyntaxes,
      builders: markdownOptions?.builders ?? {},
      bulletBuilder: markdownOptions?.bulletBuilder,
      checkboxBuilder: markdownOptions?.checkboxBuilder,
      imageBuilder: markdownOptions?.imageBuilder,
      imageDirectory: markdownOptions?.imageDirectory,
      inlineSyntaxes: markdownOptions?.inlineSyntaxes,
      listItemCrossAxisAlignment: markdownOptions?.listItemCrossAxisAlignment ??
          MarkdownListItemCrossAxisAlignment.baseline,
      onTapText: markdownOptions?.onTapText,
      shrinkWrap: markdownOptions?.shrinkWrap ?? true,
      softLineBreak: markdownOptions?.softLineBreak ?? false,
      styleSheetTheme: markdownOptions?.styleSheetTheme,
      syntaxHighlighter: markdownOptions?.syntaxHighlighter,
      padding: markdownOptions?.padding ?? EdgeInsets.all(16.0),
      physics: markdownOptions?.physics,
      controller: markdownOptions?.controller,
      paddingBuilders: markdownOptions?.paddingBuilders ?? {},
    );
  }
}
