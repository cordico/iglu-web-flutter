/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';

class _IGTreeNodeKey extends ValueKey {
  _IGTreeNodeKey(dynamic value) : super(value);
}

class KeyProvider {
  int _nextIndex = 0;
  final Set<Key> _keys = <Key>{};

  Key key(Key? originalKey) {
    if (originalKey == null) {
      return _IGTreeNodeKey(_nextIndex++);
    }
    if (_keys.contains(originalKey)) {
      throw ArgumentError('There should not be nodes with the same kays. '
          'Duplicate value found: $originalKey.');
    }
    _keys.add(originalKey);
    return originalKey;
  }
}
