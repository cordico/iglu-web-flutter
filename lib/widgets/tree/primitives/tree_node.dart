/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';

class IGTreeNode {
  final List<IGTreeNode>? children;
  final Widget content;
  final Key? key;

  IGTreeNode({this.key, this.children, Widget? content})
      : content = content ?? Container(width: 0, height: 0);
}
