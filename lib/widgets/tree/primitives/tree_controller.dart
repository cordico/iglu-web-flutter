/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/foundation.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGTreeController {
  bool allNodesExpanded;
  IGTreeViewState? treeViewState;
  final Map<Key, bool> _expanded = <Key, bool>{};

  IGTreeController({this.allNodesExpanded = true, this.treeViewState});

  bool isNodeExpanded(Key key) {
    return _expanded[key] ?? allNodesExpanded;
  }

  void toggleNodeExpanded(Key key) {
    _expanded[key] = !isNodeExpanded(key);
  }

  void expandAll() {
    allNodesExpanded = true;
    _expanded.clear();
    if (treeViewState != null) {
      treeViewState?.refresh();
    }
  }

  void collapseAll() {
    allNodesExpanded = false;
    _expanded.clear();
    if (treeViewState != null) {
      treeViewState?.refresh();
    }
  }

  void expandNode(Key key) {
    _expanded[key] = true;
    if (treeViewState != null) {
      treeViewState?.refresh();
    }
  }

  void collapseNode(Key key) {
    _expanded[key] = false;
    if (treeViewState != null) {
      treeViewState?.refresh();
    }
  }
}
