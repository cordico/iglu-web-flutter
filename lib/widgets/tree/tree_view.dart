/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGTreeView extends StatefulWidget {
  final List<IGTreeNode> nodes;
  final double? indent;
  final double? iconSize;
  final bool isSliver;
  final IGTreeController? treeController;

  IGTreeView(
      {Key? key,
      required List<IGTreeNode> nodes,
      this.indent = 16,
      this.iconSize,
      this.isSliver = false,
      this.treeController})
      : nodes = copyTreeNodes(nodes),
        super(key: key);

  @override
  IGTreeViewState createState() => IGTreeViewState();
}

class IGTreeViewState extends State<IGTreeView> {
  IGTreeController? _controller;

  @override
  void initState() {
    _controller = widget.treeController ?? IGTreeController();
    _controller?.treeViewState = this;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return buildNodes(
        widget.nodes, widget.indent, _controller!, widget.iconSize,
        isSliver: widget.isSliver);
  }

  refresh() {
    setState(() {});
  }
}

extension IGTreeViewExtension on IGTreeView {
  static List<IGTreeNode> toTreeNodes(dynamic parsedJson) {
    if (parsedJson is Map<String, dynamic>) {
      return parsedJson.keys
          .map((k) => IGTreeNode(
              content: Expanded(
                child: Text('$k',
                    maxLines: null,
                    style: IGTextStyles.normalStyle(16, IGColors.text())),
              ),
              children: toTreeNodes(parsedJson[k])))
          .toList();
    }
    if (parsedJson is List<dynamic>) {
      return parsedJson
          .asMap()
          .map((i, element) => MapEntry(
              i,
              IGTreeNode(
                  content: Expanded(
                    child: Text('[$i]',
                        maxLines: null,
                        style: IGTextStyles.normalStyle(16, IGColors.text())),
                  ),
                  children: toTreeNodes(element))))
          .values
          .toList();
    }
    return [
      IGTreeNode(
        content: Expanded(
          child: SelectableText(parsedJson.toString(),
              maxLines: null,
              style: IGTextStyles.normalStyle(16, IGColors.text())),
        ),
      )
    ];
  }
}
