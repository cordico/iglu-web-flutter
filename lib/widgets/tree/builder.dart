/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';

import 'node_widget.dart';
import 'primitives/tree_controller.dart';
import 'primitives/tree_node.dart';

Widget buildNodes(List<IGTreeNode> nodes, double? indent,
    IGTreeController state, double? iconSize,
    {bool isSliver = false}) {
  if (!isSliver) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: nodes.length,
      itemBuilder: (context, index) {
        var node = nodes[index];
        return IGTreeNodeWidget(
          treeNode: node,
          indent: indent,
          state: state,
          iconSize: iconSize,
        );
      },
    );
  } else {
    return SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      var node = nodes[index];
      return IGTreeNodeWidget(
        treeNode: node,
        indent: indent,
        state: state,
        iconSize: iconSize,
      );
    }, childCount: nodes.length));
  }
}
