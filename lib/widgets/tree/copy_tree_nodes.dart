/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'primitives/key_provider.dart';
import 'primitives/tree_node.dart';

List<IGTreeNode> copyTreeNodes(List<IGTreeNode>? nodes) {
  return _copyNodesRecursively(nodes, KeyProvider())!;
}

List<IGTreeNode>? _copyNodesRecursively(
    List<IGTreeNode>? nodes, KeyProvider keyProvider) {
  if (nodes == null) {
    return null;
  }
  return List.unmodifiable(nodes.map((n) {
    return IGTreeNode(
      key: keyProvider.key(n.key),
      content: n.content,
      children: _copyNodesRecursively(n.children, keyProvider),
    );
  }));
}
