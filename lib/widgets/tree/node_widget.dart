/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/widgets/buttons/ig_button_view_style.dart';

import 'builder.dart';
import 'primitives/tree_controller.dart';
import 'primitives/tree_node.dart';

class IGTreeNodeWidget extends StatefulWidget {
  final IGTreeNode treeNode;
  final double? indent;
  final double? iconSize;
  final IGTreeController state;

  const IGTreeNodeWidget(
      {Key? key,
      required this.treeNode,
      this.indent,
      required this.state,
      this.iconSize})
      : super(key: key);

  @override
  _IGTreeNodeWidgetState createState() => _IGTreeNodeWidgetState();
}

class _IGTreeNodeWidgetState extends State<IGTreeNodeWidget> {
  bool get _isLeaf {
    return widget.treeNode.children == null ||
        widget.treeNode.children!.isEmpty;
  }

  bool get _isExpanded {
    return widget.state.isNodeExpanded(widget.treeNode.key!);
  }

  @override
  Widget build(BuildContext context) {
    var icon = _isLeaf
        ? null
        : _isExpanded
            ? Icons.expand_more
            : Icons.chevron_right;

    var onIconPressed = _isLeaf
        ? null
        : () => setState(
            () => widget.state.toggleNodeExpanded(widget.treeNode.key!));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            IGButtonViewDefaults.mediumButtonIconOnlyHover(
                context: context,
                style: IGButtonViewStyle(
                    icon: icon,
                    iconSize: widget.iconSize ?? 24.0,
                    circleBorderRadius: 20,
                    /*tooltipMessage: _isLeaf
                        ? null
                        : _isExpanded
                            ? "Close"
                            : "Expand",*/
                    onTapPositioned: icon != null
                        ? (position) async {
                            if (onIconPressed != null) {
                              onIconPressed();
                            }
                          }
                        : null)),
            widget.treeNode.content,
          ],
        ),
        if (_isExpanded && !_isLeaf)
          Padding(
            padding: EdgeInsets.only(left: widget.indent!),
            child: buildNodes(widget.treeNode.children!, widget.indent,
                widget.state, widget.iconSize),
          )
      ],
    );
  }
}
