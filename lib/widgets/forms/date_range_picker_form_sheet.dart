/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class DateRangePickerFormSheet extends StatefulWidget {
  final String? title;
  final String? message;
  final IconData? icon;
  final String startDisplayDateFormat;
  final DateTime? startInitialDate;
  final DateTime? startFirstDate;
  final DateTime? startLastDate;
  final bool Function(DateTime)? startSelectableDayPredicate;
  final String endDisplayDateFormat;
  final DateTime? endInitialDate;
  final DateTime? endFirstDate;
  final DateTime? endLastDate;
  final bool Function(DateTime)? endSelectableDayPredicate;
  final int maximumRange;
  final bool showFutureRange;
  DateRangePickerFormSheet(
      {this.title,
      this.message,
      this.icon,
      this.endFirstDate,
      this.endInitialDate,
      this.endLastDate,
      this.endSelectableDayPredicate,
      this.endDisplayDateFormat = "dd MMMM yyyy",
      this.startFirstDate,
      this.startInitialDate,
      this.startLastDate,
      this.startSelectableDayPredicate,
      this.maximumRange = 90,
      this.showFutureRange = false,
      this.startDisplayDateFormat = "dd MMMM yyyy"});
  @override
  _DateRangePickerFormSheetState createState() =>
      _DateRangePickerFormSheetState();
}

class _DateRangePickerFormSheetState extends State<DateRangePickerFormSheet> {
  final formSheetKey = GlobalKey<FormState>();
  DateTime? start;
  DateTime? end;

  @override
  void initState() {
    super.initState();
    start = widget.startInitialDate ?? DateTime.now();
    end = widget.endInitialDate ?? DateTime.now();

    if (start!.isAfter(end!)) {
      start = end;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      return FormSheet(
          formKey: formSheetKey,
          internalMargin: EdgeInsets.zero,
          horizontalMargin: constraints.maxWidth / 1.5,
          title: widget.title ?? "Select Interval",
          alignment: Alignment.topCenter,
          alwaysDismiss: false,
          showAlertBeforeDismiss: false,
          externalBackgroundColor: Colors.transparent,
          externalMargin: EdgeInsets.fromLTRB(20, 100, 20, 20),
          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)],
          onClose: () {},
          mainAxisAlignment: MainAxisAlignment.start,
          cellRules: (constraints, state) {
            return DetailViewCellRulePreBuild.formSheetSectionTitle(
                    context: context,
                    boxConstraints: constraints,
                    title: widget.message ?? "Select a date or date range:",
                    showSeparator: false,
                    textStyle: IGTextStyles.semiboldStyle(16, IGColors.text()),
                    iconColor: IGColors.primary(forceWhiteInDarkMode: true),
                    icon: widget.icon ?? Icons.date_range_rounded) +
                [
                  DetailViewCellRulePreBuild.spaceHeight20(),
                  DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                      context: context,
                      firstContentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      first: FormTextFormField(
                          keyboardType: TextInputType.datetime,
                          formContainerState: state,
                          style: IGTextStyles.normalStyle(15, IGColors.text()),
                          onChanged: (value, isReal) {
                            setState(() {
                              start = DateExtension.formatDBDate(value ?? "");
                              end = start;
                            });
                          },
                          hideBorder: true,
                          showBackgroundStyle: true,
                          backgroundMargin: EdgeInsets.zero,
                          displayDateFormat: widget.startDisplayDateFormat,
                          selectableDayPredicate:
                              widget.startSelectableDayPredicate,
                          showDatePicker: true,
                          readOnly: true,
                          initialDate: widget.startInitialDate,
                          firstDate: widget.startFirstDate,
                          lastDate: widget.startLastDate,
                          initialValue: start != null
                              ? DateExtension.formatDate(
                                  date: start!,
                                  format: widget.startDisplayDateFormat)
                              : "",
                          labelText: "Start date",
                          textInputAction: TextInputAction.next)),
                  DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                      context: context,
                      firstContentPadding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                      first: FormTextFormField(
                          keyboardType: TextInputType.datetime,
                          style: IGTextStyles.normalStyle(15, IGColors.text()),
                          onChanged: (value, isReal) {
                            setState(() {
                              end = DateExtension.formatDBDate(value ?? "");
                            });
                          },
                          formContainerState: state,
                          backgroundMargin: EdgeInsets.zero,
                          hideBorder: true,
                          showBackgroundStyle: true,
                          displayDateFormat: widget.endDisplayDateFormat,
                          selectableDayPredicate:
                              widget.endSelectableDayPredicate,
                          showDatePicker: true,
                          readOnly: true,
                          initialDate: start ?? widget.endInitialDate,
                          firstDate: start ?? widget.endFirstDate,
                          lastDate: start!.add(Duration(
                              days: widget.maximumRange)), //widget.endLastDate,

                          initialValue: end != null
                              ? DateExtension.formatDate(
                                  date: end!,
                                  format: widget.endDisplayDateFormat)
                              : "",
                          labelText: "End date",
                          textInputAction: TextInputAction.next)),
                  DetailViewCellRulePreBuild.generalSaveDeleteButtonRow(
                      context: context,
                      saveText: "Apply",
                      isEdit: false,
                      showDelete: false,
                      saveAction: () {
                        _saveAction();
                      }),
                  DetailViewCellRulePreBuild.spaceHeight20(),
                ] +
                DetailViewCellRulePreBuild.formSheetSectionTitle(
                    context: context,
                    boxConstraints: constraints,
                    title: "Or select a predefined period:",
                    showSeparator: false,
                    textStyle: IGTextStyles.semiboldStyle(16, IGColors.text()),
                    iconColor: IGColors.primary(forceWhiteInDarkMode: true),
                    icon: widget.icon ?? Icons.access_time_outlined) +
                [
                  DetailViewCellRulePreBuild.spaceHeight20(),
                  DetailViewCellRule(
                      type: DetailViewCellRuleType.custom,
                      forceOnlyCustom: true,
                      customWidget: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        alignment: WrapAlignment.start,
                        runAlignment: WrapAlignment.start,
                        runSpacing: 8,
                        spacing: 8,
                        children: [
                          _timeIntervalWidget("Last 90 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 90)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Last 60 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 60)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Last 30 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 30)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Last 28 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 28)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Last 14 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 14)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Last 7 days", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 14)),
                                end: DateTime.now()));
                          }),
                          _timeIntervalWidget("Yesterday", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start:
                                    DateTime.now().subtract(Duration(days: 1)),
                                end: DateTime.now()
                                    .subtract(Duration(days: 1))));
                          }),
                          _timeIntervalWidget("Today", onTap: () {
                            Navigator.of(context).pop(DateTimeRange(
                                start: DateTime.now(), end: DateTime.now()));
                          }),
                          if (widget.showFutureRange)
                            _timeIntervalWidget("Tomorrow", onTap: () {
                              Navigator.of(context).pop(DateTimeRange(
                                  end: DateTime.now().add(Duration(days: 1)),
                                  start:
                                      DateTime.now().add(Duration(days: 1))));
                            }),
                          if (widget.showFutureRange)
                            _timeIntervalWidget("Next 7 days", onTap: () {
                              Navigator.of(context).pop(DateTimeRange(
                                  end: DateTime.now().add(Duration(days: 7)),
                                  start: DateTime.now()));
                            }),
                          if (widget.showFutureRange)
                            _timeIntervalWidget("Next 14 days", onTap: () {
                              Navigator.of(context).pop(DateTimeRange(
                                  end: DateTime.now().add(Duration(days: 14)),
                                  start: DateTime.now()));
                            }),
                          if (widget.showFutureRange)
                            _timeIntervalWidget("Next 28 days", onTap: () {
                              Navigator.of(context).pop(DateTimeRange(
                                  end: DateTime.now().add(Duration(days: 28)),
                                  start: DateTime.now()));
                            })
                        ],
                      )),
                  DetailViewCellRulePreBuild.spaceHeight20(),
                ];
          });
    });
  }

  Widget _timeIntervalWidget(String title, {Function()? onTap}) {
    return IGView(
      onTap: onTap,
      padding: EdgeInsets.all(10),
      border: Border.all(color: IGColors.separator()),
      borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius),
      child: (state, context) {
        return Text(
          title,
          style: IGTextStyles.normalStyle(15, IGColors.text()),
        );
      },
    );
  }

  _saveAction() {
    FocusScope.of(context).unfocus();
    if (formSheetKey.currentState!.validate()) {
      Navigator.of(context).pop(DateTimeRange(start: start!, end: end!));
    }
  }
}
