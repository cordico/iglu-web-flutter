/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:tuple/tuple.dart';

class FeedbackFormSheet extends StatefulWidget {
  FeedbackFormSheet();
  @override
  _FeedbackFormSheetState createState() => _FeedbackFormSheetState();
}

class _FeedbackFormSheetState extends State<FeedbackFormSheet> {
  final formSheetKey = GlobalKey<FormState>();
  var isLoading = false;

  var feedback = "";

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      return FormSheet(
          formKey: formSheetKey,
          internalMargin: EdgeInsets.zero,
          horizontalMargin: constraints.maxWidth / 1.5,
          title: "Technical support",
          absorbing: isLoading,
          onClose: () {},
          cellRules: (constraints, state) {
            return DetailViewCellRulePreBuild.formSheetSectionTitle(
                    context: context,
                    boxConstraints: constraints,
                    title:
                        "To report technical problems and / or suggest improvements:",
                    showSeparator: false,
                    textStyle: IGTextStyles.semiboldStyle(16, IGColors.text()),
                    iconColor: IGColors.primary(forceWhiteInDarkMode: true),
                    icon: Icons.feedback_rounded) +
                [
                  DetailViewCellRulePreBuild.spaceHeight20(),
                  DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                      context: context,
                      first: FormTextFormField(
                          maxLines: 20,
                          keyboardType: TextInputType.multiline,
                          style: IGTextStyles.normalStyle(15, IGColors.text()),
                          formFieldValidator: IGFormFieldValidator(
                              type: IGFormFieldValidatorType.range,
                              min: 15,
                              max: 4096),
                          initialValue: "",
                          labelText: "Write your message here",
                          textInputAction: TextInputAction.done,
                          onChanged: (value, isReal) {
                            feedback = value ?? "";
                          },
                          onFieldSubmitted: (value) async {
                            await _saveAction();
                          })),
                  DetailViewCellRulePreBuild.generalSaveDeleteButtonRow(
                      context: context,
                      saveText: "Send Message",
                      isEdit: false,
                      showDelete: false,
                      isLoading: isLoading,
                      saveAction: () async {
                        await _saveAction();
                      }),
                  DetailViewCellRulePreBuild.spaceHeight20(),
                ];
          });
    });
  }

  _saveAction() async {
    FocusScope.of(context).unfocus();
    if (formSheetKey.currentState!.validate()) {
      setState(() {
        isLoading = true;
      });
      Tuple2<String?, bool> result = Tuple2(null, false);
      if (webConfigurator.internalConfigurator.handlersConfigurations
              ?.sendFeedbackHandler !=
          null) {
        result = await webConfigurator.internalConfigurator
            .handlersConfigurations!.sendFeedbackHandler!(feedback);
      }
      setState(() {
        isLoading = false;
      });
      if (result.item2) {
        Navigator.of(context).pop();
        webConfigurator.showFlush(
            context: context,
            content: "Feedback sent successfully",
            icon: Icon(Icons.check_rounded,
                size: 24, color: IGColors.text(isOpposite: true)));
      } else {
        await AlertView.showSimpleErrorDialog(
            context: context, message: result.item1);
      }
    }
  }
}
