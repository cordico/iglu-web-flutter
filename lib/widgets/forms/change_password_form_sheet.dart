/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class ChangePasswordFormSheet extends StatefulWidget {
  ChangePasswordFormSheet();
  @override
  _ChangePasswordFormSheetState createState() =>
      _ChangePasswordFormSheetState();
}

class _ChangePasswordFormSheetState extends State<ChangePasswordFormSheet> {
  final formSheetKey = GlobalKey<FormState>();
  var isLoading = false;

  var oldPassword = "";
  var newPassword = "";
  var confirmPassword = "";

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      return FormSheet(
          formKey: formSheetKey,
          internalMargin: EdgeInsets.zero,
          horizontalMargin: constraints.maxWidth / 1.5,
          title: "Change Password",
          absorbing: isLoading,
          onClose: () {},
          cellRules: (constraints, state) {
            return [
              DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                  context: context,
                  first: FormTextFormField(
                      keyboardType: TextInputType.visiblePassword,
                      style: IGTextStyles.normalStyle(15, IGColors.text()),
                      formFieldValidator: IGFormFieldValidator(
                          type: IGFormFieldValidatorType.range, min: 5),
                      initialValue: oldPassword,
                      labelText: "Old password",
                      autofillHints: [AutofillHints.password],
                      textInputAction: TextInputAction.next,
                      onChanged: (value, isReal) {
                        oldPassword = value ?? "";
                      },
                      obscureText: true,
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).nextFocus();
                      })),
              DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                  context: context,
                  first: FormTextFormField(
                    keyboardType: TextInputType.visiblePassword,
                    style: IGTextStyles.normalStyle(15, IGColors.text()),
                    formFieldValidator: IGFormFieldValidator(
                        type: IGFormFieldValidatorType.range, min: 5),
                    initialValue: newPassword,
                    textInputAction: TextInputAction.next,
                    autofillHints: [AutofillHints.newPassword],
                    onChanged: (value, isReal) {
                      newPassword = value ?? "";
                    },
                    onFieldSubmitted: (value) {
                      FocusScope.of(context).nextFocus();
                    },
                    obscureText: true,
                    labelText: "New password",
                  )),
              DetailViewCellRulePreBuild.textFormFieldItemsProportionally(
                  context: context,
                  first: FormTextFormField(
                    keyboardType: TextInputType.visiblePassword,
                    style: IGTextStyles.normalStyle(15, IGColors.text()),
                    formFieldValidator: IGFormFieldValidator(
                        type: IGFormFieldValidatorType.range, min: 5),
                    initialValue: confirmPassword,
                    autofillHints: [AutofillHints.newPassword],
                    textInputAction: TextInputAction.done,
                    onChanged: (value, isReal) {
                      confirmPassword = value ?? "";
                    },
                    onFieldSubmitted: (value) async {
                      await _saveAction();
                    },
                    obscureText: true,
                    labelText: "Repeat the password",
                  )),
              DetailViewCellRulePreBuild.generalSaveDeleteButtonRow(
                  context: context,
                  isEdit: false,
                  showDelete: false,
                  isLoading: isLoading,
                  saveAction: () async {
                    await _saveAction();
                  }),
              DetailViewCellRulePreBuild.spaceHeight20(),
            ];
          });
    });
  }

  _saveAction() async {
    FocusScope.of(context).unfocus();
    if (formSheetKey.currentState!.validate()) {
      setState(() {
        isLoading = true;
      });
      Map<String, dynamic> result = {};
      //TODO: HANDLE CHANGE PASSWORD
      /*
      result = await Shared.queryManager.updatePasswordUser(
          confirmPassword: confirmPassword,
          oldPassword: oldPassword,
          password: newPassword);
          */
      setState(() {
        isLoading = false;
      });
      if (result["data"] != null) {
        Navigator.of(context).pop();
        webConfigurator.showFlush(
            context: context,
            content: "Password changed successfully",
            icon: Icon(Icons.check_rounded,
                size: 24, color: IGColors.text(isOpposite: true)));
      } else {
        await AlertView.showSimpleErrorDialog(
            context: context, message: result["error"]);
      }
    }
  }
}
