/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
import 'dart:math';
import "package:flutter/material.dart";
import 'package:flutter/services.dart';

import "package:iglu_web_flutter/iglu_web_flutter.dart";

class FormSheet extends StatefulWidget {
  final GlobalKey<FormState>? formKey;
  final Function? onClose;
  final double externalBorderRadius;
  final EdgeInsetsGeometry externalMargin;
  final EdgeInsetsGeometry internalMargin;
  final Color? backgroundColor;
  final Color? externalBackgroundColor;
  final List<BoxShadow>? boxShadow;
  final double verticalMargin;
  final double horizontalMargin;
  final AlignmentGeometry alignment;
  final Widget? header;
  final String? title;
  final bool showHeader;
  final List<DetailViewCellRule> Function(BoxConstraints, IGFormContainerState)?
      cellRules;
  final List<Widget> Function(BoxConstraints, IGFormContainerState)? widgets;
  final bool absorbing;
  final bool alwaysDismiss;
  final bool useCustomDimensionOnMobile;
  final bool showAlertBeforeDismiss;
  final BoxConstraints preferredConstraints;
  final EdgeInsets padding;
  final MainAxisAlignment? mainAxisAlignment;
  final CrossAxisAlignment? crossAxisAlignment;
  final bool isFullScreen;
  final bool showCloseButton;
  FormSheet(
      {this.formKey,
      this.onClose,
      this.absorbing = false,
      this.externalBorderRadius = 8.0,
      this.backgroundColor,
      this.externalBackgroundColor,
      this.boxShadow,
      this.horizontalMargin = 400,
      this.verticalMargin = 250,
      this.showHeader = true,
      this.alwaysDismiss = true,
      this.useCustomDimensionOnMobile = false,
      this.showAlertBeforeDismiss = false,
      this.isFullScreen = false,
      this.showCloseButton = true,
      this.header,
      this.title,
      this.cellRules,
      this.widgets,
      this.mainAxisAlignment,
      this.crossAxisAlignment,
      this.padding = EdgeInsets.zero,
      this.preferredConstraints = const BoxConstraints(),
      this.alignment = Alignment.center,
      this.externalMargin = EdgeInsets.zero,
      this.internalMargin = const EdgeInsets.all(20)});
  @override
  FormSheetState createState() => FormSheetState();
}

class FormSheetState extends State<FormSheet> {
  final GlobalKey containerKey = GlobalKey();
  var isSomethingChanged = false;

  calculateWidth(BoxConstraints constraints) {
    var width = (constraints.maxWidth - widget.horizontalMargin).abs();
    var minimum = 414.0;
    var smallWidth = constraints.maxWidth -
        (widget.externalMargin != EdgeInsets.zero
            ? widget.externalMargin.horizontal
            : widget.isFullScreen
                ? 0
                : 40);

    var titleWidth = (AlertView.textSize(
            widget.title ?? "Attention",
            IGTextStyles.normalStyle(
              28,
              IGColors.text(),
            )).width) +
        100;
    if (width < titleWidth) {
      width = titleWidth;
    }
    if (width < minimum) {
      width = minimum;
    }
    if (width > smallWidth) {
      width = smallWidth;
    }
    if (SizeDetector.isMobile(context)) {
      return smallWidth;
    }
    return width;
  }

  close({bool force = false}) async {
    FocusScope.of(context).unfocus();
    if (force || widget.alwaysDismiss) {
      if (widget.showAlertBeforeDismiss && isSomethingChanged) {
        await AlertView.showSimpleDialog(context,
            dismissable: false,
            title: "Close",
            actionTitle: "Close",
            message:
                "Are you sure you want to cancel this operation? Any changes made will be lost. This action is irreversible.",
            okActionPrimaryColor: IGColors.primary(), okAction: () {
          Navigator.of(context).pop();
          if (widget.onClose != null) {
            widget.onClose!();
          }
          Navigator.of(context).pop();
        });
      } else {
        if (widget.onClose != null) {
          widget.onClose!();
        }
        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizeDetector.isTooSmall(context)
        ? TooSmallController()
        : AbsorbPointer(
            absorbing: widget.absorbing,
            child: GestureDetector(
              onTap: () {
                close();
              },
              child: Material(
                color: widget.externalBackgroundColor ?? Colors.black26,
                child: IGKeyboardListener(
                  disableEscape: false,
                  onKey: (key) {
                    if (key.isKeyPressed(LogicalKeyboardKey.escape)) {
                      close(force: true);
                    }
                  },
                  child: LayoutBuilder(builder: (_, constraints) {
                    return Container(
                        color: widget.backgroundColor,
                        width: constraints.maxWidth,
                        height: constraints.maxHeight,
                        child: Align(
                          alignment: widget.alignment,
                          child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).unfocus();
                            },
                            child: Form(
                              onChanged: () {
                                isSomethingChanged = true;
                              },
                              key: widget.formKey,
                              child: Container(
                                key: containerKey,
                                margin: constraints.maxHeight <= 600
                                    ? EdgeInsets.all(
                                        widget.isFullScreen ? 0 : 20)
                                    : widget.externalMargin,
                                constraints: BoxConstraints(
                                    maxWidth:
                                        widget.preferredConstraints.maxWidth,
                                    maxHeight: min(
                                        constraints.maxHeight <= 600
                                            ? constraints.maxHeight -
                                                (widget.isFullScreen ? 0 : 40)
                                            : SizeDetector.isSmallScreen(
                                                    context)
                                                ? constraints.maxHeight -
                                                    (widget.externalMargin !=
                                                            EdgeInsets.zero
                                                        ? widget.externalMargin
                                                            .horizontal
                                                        : (widget.isFullScreen
                                                            ? 0
                                                            : 40))
                                                : constraints.maxHeight -
                                                    widget.verticalMargin,
                                        widget.preferredConstraints.maxHeight)),
                                width: calculateWidth(constraints),
                                child: LayoutBuilder(builder: (_, constraints) {
                                  return ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                          widget.externalBorderRadius),
                                      child: Scrollbar(
                                        child: SingleChildScrollView(
                                          child: IGFormContainer(
                                              containerKey: containerKey,
                                              builder: (state) {
                                                return Padding(
                                                  padding: widget.padding,
                                                  child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      mainAxisAlignment: widget
                                                              .mainAxisAlignment ??
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment: widget
                                                              .crossAxisAlignment ??
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                            if (widget
                                                                .showHeader)
                                                              Container(
                                                                height: 60,
                                                                padding:
                                                                    EdgeInsets
                                                                        .fromLTRB(
                                                                            20,
                                                                            0,
                                                                            20,
                                                                            0),
                                                                child: Row(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Expanded(
                                                                        child: widget.header ??
                                                                            AutoSizeText(widget.title ?? "",
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                                style: IGTextStyles.normalStyle(
                                                                                  28,
                                                                                  IGColors.text(),
                                                                                )),
                                                                      ),
                                                                      Container(
                                                                          width:
                                                                              30),
                                                                      if (widget
                                                                          .showCloseButton)
                                                                        IGButtonViewDefaults.largeButtonIconBackground(
                                                                            context: context,
                                                                            style: IGButtonViewStyle(
                                                                                tooltipMessage: "Close",
                                                                                backgroundColorSelected: resetValueField,
                                                                                tintColorSelected: resetValueField,
                                                                                icon: Icons.close_rounded,
                                                                                onTapPositioned: (position) async {
                                                                                  close(force: true);
                                                                                }))
                                                                    ]),
                                                              ),
                                                            if (widget
                                                                .showHeader)
                                                              DetailViewCellRulePreBuild
                                                                      .separator0(
                                                                          height:
                                                                              1)
                                                                  .widget(
                                                                      context:
                                                                          context),
                                                          ] +
                                                          (widget.cellRules !=
                                                                  null
                                                              ? widget
                                                                  .cellRules!(
                                                                      constraints,
                                                                      state)
                                                                  .map((e) => e.widget(
                                                                      context:
                                                                          context))
                                                                  .toList()
                                                              : widget.widgets !=
                                                                      null
                                                                  ? widget.widgets!(
                                                                      constraints,
                                                                      state)
                                                                  : <Widget>[])),
                                                );
                                              }),
                                        ),
                                      ));
                                }),
                                decoration: BoxDecoration(
                                  color: widget.backgroundColor ??
                                      IGColors.secondaryBackground(),
                                  borderRadius: BorderRadius.circular(
                                      widget.externalBorderRadius),
                                  boxShadow: widget.boxShadow ??
                                      [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 5)
                                      ],
                                ),
                              ),
                            ),
                          ),
                        ));
                  }),
                ),
              ),
            ),
          );
  }
}
