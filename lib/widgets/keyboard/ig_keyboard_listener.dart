/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class IGKeyboardListener extends StatefulWidget {
  final Widget? child;
  final bool disableEscape;
  final Function(RawKeyEvent)? onKey;
  final FocusNode? focusNode;
  IGKeyboardListener(
      {this.child, this.disableEscape = true, this.onKey, this.focusNode});
  @override
  _IGKeyboardListenerState createState() => _IGKeyboardListenerState();
}

class _IGKeyboardListenerState extends State<IGKeyboardListener> {
  FocusNode? _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.focusNode != null) {
      _focusNode = widget.focusNode;
    }
  }

  void _handleKeyEvent(RawKeyEvent event) {
    if (event.isKeyPressed(LogicalKeyboardKey.escape) && widget.disableEscape) {
    } else {
      if (widget.onKey != null) {
        widget.onKey!(event);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      focusNode: _focusNode!,
      onKey: _handleKeyEvent,
      autofocus: true,
      child: widget.child!,
    );
  }
}
