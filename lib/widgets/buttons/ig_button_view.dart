/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:math";
import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class IGButtonView extends StatefulWidget {
  final IGButtonViewStyle? options;
  IGButtonView({this.options});
  @override
  _IGButtonViewState createState() => _IGButtonViewState();
}

class _IGButtonViewState extends State<IGButtonView>
    with SingleTickerProviderStateMixin {
  bool expanded = true;
  AnimationController? controller;
  Animation<double>? _animation;
  bool isLoading = false;
  bool isSelected = false;
  bool isDisabled = false;

  @override
  void initState() {
    super.initState();
    if (widget.options!.showAnimatedArrow) {
      controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 300),
      );
      _animation = Tween<double>(begin: 0, end: 1).animate(controller!)
        ..addListener(() {
          if (mounted) {
            setState(() {});
          }
        });
    }
  }

  @override
  void dispose() {
    _animation?.removeListener(() {});
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    isDisabled =
        widget.options!.absorbing || widget.options!.onTapPositioned == null;
    return !isDisabled &&
            (!isLoading && !widget.options!.forceLoading) &&
            (widget.options!.tooltipMessage != null ||
                (showCircle() && widget.options!.text != null))
        ? WidgetsDefaults.defaultTooltip(
            context: context,
            message: widget.options!.tooltipMessage ?? widget.options!.text,
            child: _child())
        : _child();
  }

  Widget _child() {
    return IGView(
        selected: widget.options!.forceSelected || isSelected,
        enabled: !(isDisabled || isLoading || widget.options!.forceLoading),
        onTap: isDisabled || isLoading || widget.options!.forceLoading
            ? null
            : () async {
                changeState();
                if (widget.options!.onTapPositioned != null) {
                  var position = RelativeRect.fromLTRB(
                      context.globalPaintBounds!.left,
                      context.globalPaintBounds!.top +
                          context.globalPaintBounds!.height,
                      context.globalPaintBounds!.right,
                      context.globalPaintBounds!.bottom);
                  await widget.options!.onTapPositioned(position);
                }
                changeState();
              },
        margin: widget.options!.externalPadding,
        height: widget.options!.showExpandedVersion
            ? null
            : showCircle()
                ? widget.options!.circleIconSize
                : widget.options!.height,
        width: widget.options!.showExpandedVersion
            ? null
            : showCircle()
                ? widget.options!.circleIconSize
                : widget.options!.width,
        padding:
            showCircle() ? EdgeInsets.zero : widget.options!.internalPadding,
        boxShadow: widget.options!.boxShadow,
        border: widget.options!.border,
        color: _color(),
        borderRadius: _borderRadius() as BorderRadius?,
        constraints: widget.options!.showExpandedVersion
            ? BoxConstraints(
                maxWidth: widget.options!.width ?? double.infinity,
                minHeight: widget.options!.height ?? 0)
            : null,
        child: (states, context) {
          return ((isLoading || widget.options!.forceLoading) &&
                  widget.options!.showLoadingDuringAction)
              ? Center(
                  child: Container(
                      width: _loadingSize(states: states),
                      height: _loadingSize(states: states),
                      child: WidgetsDefaults.defaultCirculaProgressIndicator(
                          color: widget.options!.textColor ?? Colors.white)))
              : Row(
                  mainAxisSize: widget.options!.showExpandedVersion
                      ? MainAxisSize.min
                      : MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (widget.options!.showIconOnLeft &&
                        widget.options!.icon != null)
                      Icon(widget.options!.icon,
                          size: _iconSize(states: states),
                          color: _iconColor(states: states)),
                    if (widget.options!.showIconOnLeft &&
                        widget.options!.icon != null &&
                        hideTextAutomatically(states: states))
                      Container(width: widget.options!.internalSpacing),
                    if (hideTextAutomatically(states: states))
                      widget.options!.showExpandedVersion
                          ? Flexible(child: _text(states: states))
                          : _text(states: states),
                    if (widget.options!.showIconOnRight &&
                        widget.options!.icon != null &&
                        hideTextAutomatically(states: states))
                      Container(width: widget.options!.internalSpacing),
                    if (widget.options!.showIconOnRight &&
                        widget.options!.icon != null)
                      Icon(widget.options!.icon,
                          size: _iconSize(states: states),
                          color: _iconColor(states: states)),
                    if (widget.options!.showAnimatedArrow)
                      Transform.rotate(
                        angle: pi * _animation!.value,
                        child: Icon(Icons.arrow_drop_down_rounded,
                            size: _iconSize(states: states),
                            color: _iconColor(states: states)),
                      ),
                  ],
                );
        });
  }

  Widget _text({Set<MaterialState>? states}) {
    return Text(widget.options!.text,
        style: _textStyle(states: states),
        overflow: widget.options!.showExpandedVersion
            ? widget.options!.textOverflow
            : TextOverflow.ellipsis,
        maxLines:
            widget.options!.showExpandedVersion ? widget.options!.maxLines : 1);
  }

  changeState() {
    if (mounted) {
      setState(() {
        isSelected = !isSelected;
        if (widget.options!.showAnimatedArrow) {
          expanded ? controller!.forward() : controller!.reverse();
          expanded = !expanded;
        }
        if (widget.options!.showLoadingDuringAction &&
            widget.options!.isInternalLoadingValid) {
          isLoading = !isLoading;
        }
      });
    }
  }

  double? _loadingSize({Set<MaterialState>? states}) {
    return widget.options!.height != null ? widget.options!.height / 2.0 : 20;
  }

  TextStyle? _textStyle({Set<MaterialState>? states}) {
    return widget.options!.textStyle != null
        ? widget.options!.textStyle.copyWith(color: _textColor(states: states!))
        : IGTextStyles.custom(color: _textColor(states: states!));
  }

  Color? _textColor({required Set<MaterialState> states}) {
    return states.contains(MaterialState.hovered) &&
            widget.options!.hoverTextColor != null
        ? widget.options!.hoverTextColor
        : states.contains(MaterialState.selected) &&
                widget.options!.tintColorSelected != null
            ? widget.options!.tintColorSelected
            : widget.options!.textColor ?? Colors.white;
  }

  double _iconSize({Set<MaterialState>? states}) {
    return widget.options!.iconSize ?? (showCircle() ? 22 : 27);
  }

  Color? _iconColor({required Set<MaterialState> states}) {
    return states.contains(MaterialState.hovered) &&
            (widget.options!.hoverTextColor != null ||
                widget.options!.hoverIconColor != null)
        ? widget.options!.hoverTextColor ?? widget.options!.hoverIconColor
        : states.contains(MaterialState.selected) &&
                widget.options!.tintColorSelected != null
            ? widget.options!.tintColorSelected
            : widget.options!.iconColor ?? Colors.white;
  }

  MaterialStateProperty<Color?> _color() {
    return MaterialStateProperty.resolveWith<Color?>(
        (Set<MaterialState> states) {
      if (states.contains(MaterialState.disabled) &&
          !isLoading &&
          !widget.options!.forceLoading) {
        return (widget.options!.disabledColor ?? IGColors.separator());
      } else if ((states.contains(MaterialState.selected)) &&
          widget.options!.backgroundColorSelected != null &&
          !isLoading &&
          !widget.options!.forceLoading) {
        return widget.options!.backgroundColorSelected;
      } else if (states.contains(MaterialState.hovered) ||
          states.contains(MaterialState.pressed)) {
        return widget.options!.hoverColor ?? IGHoverType.normal.hoverColor();
      }
      return widget.options!.backgroundColor;
    });
  }

  BorderRadiusGeometry? _borderRadius() {
    return widget.options!.border != null && !widget.options!.border.isUniform
        ? null
        : widget.options!.forceCircleDuringLoading &&
                (isLoading || widget.options!.forceLoading)
            ? BorderRadius.circular((widget.options!.height != null
                ? (widget.options!.height / 2.0) * 2
                : 20))
            : showCircle() || widget.options!.height == widget.options!.width
                ? BorderRadius.circular(widget.options!.circleBorderRadius)
                : (widget.options!.customBorderRadius ??
                    BorderRadius.circular(widget.options!.borderRadius ??
                        kButtonPrimaryCornerRadius));
  }

  bool hideTextAutomatically({Set<MaterialState>? states}) {
    return (widget.options!.text != null &&
        (!SizeDetector.isSmallScreen(context) ||
            !widget.options!.automaticallyResize));
  }

  bool showCircle() {
    var res = ((isLoading ||
                widget.options!.forceLoading ||
                (SizeDetector.isSmallScreen(context))) &&
            widget.options!.automaticallyResize) ||
        ((isLoading || widget.options!.forceLoading) &&
            widget.options!.showLoadingDuringAction &&
            widget.options!.forceCircleDuringLoading);
    return res;
  }
}
