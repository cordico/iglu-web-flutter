/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class IGButtonRow extends StatelessWidget {
  IGButtonRow(
      {this.title,
      this.titleWidget,
      this.trailingIcon,
      this.leadingIcon,
      this.onTap,
      this.subTitle,
      this.trailing,
      this.leading,
      this.leftMarginEmptyIcon = 0.0,
      this.height = 52.0,
      this.constraints,
      this.radius,
      this.titleAlign = TextAlign.start,
      this.margin = const EdgeInsets.all(8),
      this.padding = const EdgeInsets.all(8),
      this.maxLines = 2,
      this.mouseCursor,
      this.igViewStyle,
      this.columMainAxisAlignment = MainAxisAlignment.start,
      this.columnCrossAxisAlignment = CrossAxisAlignment.start,
      this.mainAxisAlignment = MainAxisAlignment.start,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      this.outsideMainAxisSize = MainAxisSize.max,
      this.insideMainAxisSize = MainAxisSize.max,
      this.borderRadius,
      this.boxShadow,
      this.decoration,
      this.enabled = true,
      this.selected = false,
      this.bouncingEnabled = false,
      this.vibrate = false,
      this.isFittedWidth = false,
      this.onLongPress,
      this.width});
  final IconData? trailingIcon;
  final IconData? leadingIcon;
  final String? title;
  final TextAlign titleAlign;
  final String? subTitle;
  final Widget Function(Set<MaterialState>)? titleWidget;
  final Widget Function(Set<MaterialState>)? leading;
  final Widget Function(Set<MaterialState>)? trailing;
  final double leftMarginEmptyIcon;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment columMainAxisAlignment;
  final CrossAxisAlignment columnCrossAxisAlignment;
  final int maxLines;
  final IGViewStyle? igViewStyle;
  final Function()? onTap;
  final Function()? onLongPress;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final double? radius;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  final double? height;
  final double? width;
  final BoxConstraints? constraints;
  final BoxDecoration? decoration;
  final bool enabled;
  final bool selected;
  final bool vibrate;
  final bool bouncingEnabled;
  final MouseCursor? mouseCursor;
  final MainAxisSize outsideMainAxisSize;
  final MainAxisSize insideMainAxisSize;
  final bool isFittedWidth;

  @override
  Widget build(BuildContext context) {
    return IGView(
        height: height,
        color: igViewStyle?.color,
        constraints: constraints,
        radius: radius,
        onTap: onTap,
        margin: margin,
        borderRadius: borderRadius,
        bouncingEnabled: bouncingEnabled,
        boxShadow: boxShadow,
        decoration: decoration,
        enabled: enabled,
        key: key,
        mouseCursor: mouseCursor,
        onLongPress: onLongPress,
        padding: padding,
        selected: selected,
        vibrate: vibrate,
        width: width,
        child: (states, context) {
          var widget = Row(
            mainAxisSize: outsideMainAxisSize,
            children: [
              Expanded(
                child: Row(
                  mainAxisSize: insideMainAxisSize,
                  crossAxisAlignment: crossAxisAlignment,
                  mainAxisAlignment: mainAxisAlignment,
                  children: [
                    if (leadingIcon != null || leading != null)
                      leading != null
                          ? leading!(states)
                          : IGButtonViewDefaults.largeButtonIconBackground(
                              context: context,
                              style: IGButtonViewStyle(
                                  iconColor:
                                      igViewStyle?.iconColor?.resolve(states),
                                  disabledColor: igViewStyle?.iconBackgrundColor
                                      ?.resolve(states),
                                  onTapPositioned: null,
                                  icon: leadingIcon)),
                    if ((leadingIcon != null || leading != null) &&
                            title != null ||
                        titleWidget != null)
                      Container(width: 12),
                    if (leadingIcon == null && leading == null)
                      Container(width: leftMarginEmptyIcon),
                    if (title != null || titleWidget != null)
                      Expanded(
                        child: Container(
                          child: subTitle != null && titleWidget == null
                              ? Column(
                                  crossAxisAlignment: columnCrossAxisAlignment,
                                  mainAxisAlignment: columMainAxisAlignment,
                                  children: [
                                    Text(title!,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: titleAlign,
                                        maxLines: 1,
                                        style: igViewStyle?.titleStyle
                                                ?.resolve(states) ??
                                            IGTextStyles.custom(
                                                fontWeight: FontWeight.w500,
                                                color: igViewStyle?.titleColor
                                                        ?.resolve(states) ??
                                                    IGColors.text())),
                                    if (subTitle != null)
                                      Text(subTitle!,
                                          maxLines: maxLines,
                                          overflow: TextOverflow.ellipsis,
                                          style: igViewStyle?.subTitleStyle
                                                  ?.resolve(states) ??
                                              IGTextStyles.custom(
                                                  fontSize: 14,
                                                  color: igViewStyle
                                                          ?.subtitleColor
                                                          ?.resolve(states) ??
                                                      IGColors.textSecondary()))
                                  ],
                                )
                              : titleWidget != null
                                  ? titleWidget!(states)
                                  : Text(title!,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: titleAlign,
                                      style: igViewStyle?.titleStyle
                                              ?.resolve(states) ??
                                          IGTextStyles.custom(
                                              fontWeight: FontWeight.w500,
                                              color: igViewStyle?.titleColor
                                                      ?.resolve(states) ??
                                                  IGColors.text())),
                        ),
                      ),
                    if (trailing != null || trailingIcon != null)
                      Container(width: 10),
                    if (trailing != null || trailingIcon != null)
                      trailing != null
                          ? trailing!(states)
                          : IGButtonViewDefaults.largeButtonIconBackground(
                              context: context,
                              style: IGButtonViewStyle(
                                  iconColor:
                                      igViewStyle?.iconColor?.resolve(states),
                                  disabledColor: igViewStyle?.iconBackgrundColor
                                      ?.resolve(states),
                                  onTapPositioned: null,
                                  icon: trailingIcon)),
                  ],
                ),
              ),
            ],
          );
          if (isFittedWidth) {
            return IntrinsicWidth(child: widget);
          } else {
            return widget;
          }
        });
  }
}

extension IGButtonRowDefaults on IGButtonRow {
  static Widget generalSmallContextWidget(
      {required BuildContext context,
      required String text,
      bool forceSelected = false,
      String? subText,
      Function()? onTap}) {
    return IGButtonRow(
        constraints: BoxConstraints(minHeight: 40),
        height: null,
        onTap: forceSelected ? null : onTap,
        selected: forceSelected,
        igViewStyle: IGViewStyleExtension.primary(),
        margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
        title: text,
        subTitle: subText ?? null,
        maxLines: 1,
        trailing: (states) {
          var isTapped = states.contains(MaterialState.selected);
          return Icon(Icons.arrow_forward_ios_rounded,
              size: 16,
              color: isTapped &&
                      webConfigurator.currentBrightness == Brightness.light
                  ? IGColors.primary()
                  : IGColors.text());
        });
  }
}
