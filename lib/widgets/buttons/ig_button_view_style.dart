/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

ResetValueField resetValueField = ResetValueField();

class ResetValueField {}

class IGButtonViewStyle {
  final dynamic
      onTapPositioned; //final Function(RelativeRect position) onTapPositioned;
  final dynamic text; //final String text;
  final dynamic textStyle; //final TextStyle textStyle;
  final dynamic showAnimatedArrow; //final bool showAnimatedArrow;
  final dynamic showIconOnLeft; //final bool showIconOnLeft;
  final dynamic showIconOnRight; //final bool showIconOnRight;
  final dynamic icon; //final IconData icon;
  final dynamic backgroundColor; //final Color backgroundColor;
  final dynamic border; //final BoxBorder border;
  final dynamic
      customBorderRadius; //final BorderRadiusGeometry customBorderRadius;
  final dynamic boxShadow; //final List<BoxShadow> boxShadow;
  final dynamic hoverColor; //final Color hoverColor;
  final dynamic hoverTextColor; //final Color hoverTextColor;
  final dynamic hoverIconColor; //final Color hoverIconColor;
  final dynamic textColor; //final Color textColor;
  final dynamic iconColor; //final Color iconColor;
  final dynamic disabledColor; //final Color disabledColor;
  final dynamic iconSize; //final double iconSize;
  final dynamic borderRadius; //final double borderRadius;
  final dynamic internalPadding; //final EdgeInsetsGeometry internalPadding;
  final dynamic externalPadding; //final EdgeInsetsGeometry externalPadding;
  final dynamic height; //final double height;
  final dynamic width; //final double width;
  final dynamic internalSpacing; //final double internalSpacing;
  final dynamic absorbing; //final bool absorbing;
  final dynamic isInternalLoadingValid; //final bool isInternalLoadingValid;
  final dynamic showLoadingDuringAction; //final bool showLoadingDuringAction;
  final dynamic forceCircleDuringLoading; //final bool forceCircleDuringLoading;
  final dynamic forceLoading; //final bool forceLoading;
  final dynamic tintColorSelected; //final Color tintColorSelected;
  final dynamic backgroundColorSelected; //final Color backgroundColorSelected;
  final dynamic tooltipMessage; //final String tooltipMessage;
  final dynamic automaticallyResize; //final bool automaticallyResize;
  final dynamic forceSelected; //final bool forceSelected;
  final dynamic circleIconSize; //final double circleIconSize;
  final dynamic circleBorderRadius; //final double circleBorderRadius;
  final dynamic showExpandedVersion; //final bool showExpandedVersion;
  final dynamic textOverflow; //final TextOverflow textOverflow;
  final dynamic maxLines; //final int maxLines;
  IGButtonViewStyle(
      {this.onTapPositioned,
      this.text,
      this.boxShadow,
      this.textStyle,
      this.showIconOnLeft,
      this.showIconOnRight,
      this.icon,
      this.backgroundColor,
      this.border,
      this.hoverColor,
      this.hoverTextColor,
      this.hoverIconColor,
      this.textColor,
      this.iconColor,
      this.disabledColor,
      this.iconSize,
      this.borderRadius,
      this.customBorderRadius,
      this.showAnimatedArrow,
      this.externalPadding,
      this.internalPadding,
      this.internalSpacing,
      this.height,
      this.absorbing,
      this.width,
      this.tintColorSelected,
      this.backgroundColorSelected,
      this.tooltipMessage,
      this.automaticallyResize,
      this.forceLoading,
      this.forceSelected,
      this.forceCircleDuringLoading,
      this.showLoadingDuringAction,
      this.isInternalLoadingValid,
      this.circleIconSize,
      this.circleBorderRadius,
      this.showExpandedVersion,
      this.maxLines,
      this.textOverflow}) {
    assert(
        _fieldChecker<Function(RelativeRect position)>(this.onTapPositioned));
    assert(_fieldChecker<String>(this.text));
    assert(_fieldChecker<TextStyle>(this.textStyle));
    assert(_fieldChecker<bool>(this.showAnimatedArrow));
    assert(_fieldChecker<bool>(this.showIconOnLeft));
    assert(_fieldChecker<bool>(this.showIconOnRight));
    assert(_fieldChecker<IconData>(this.icon));
    assert(_fieldChecker<Color>(this.backgroundColor));
    assert(_fieldChecker<BoxBorder>(this.border));
    assert(_fieldChecker<BorderRadiusGeometry>(this.customBorderRadius));
    assert(_fieldChecker<List<BoxShadow>>(this.boxShadow));
    assert(_fieldChecker<Color>(this.hoverColor));
    assert(_fieldChecker<Color>(this.hoverTextColor));
    assert(_fieldChecker<Color>(this.hoverIconColor));
    assert(_fieldChecker<Color>(this.textColor));
    assert(_fieldChecker<Color>(this.iconColor));
    assert(_fieldChecker<Color>(this.disabledColor));
    assert(_fieldChecker<double>(this.iconSize));
    assert(_fieldChecker<double>(this.borderRadius));
    assert(_fieldChecker<EdgeInsetsGeometry>(this.internalPadding));
    assert(_fieldChecker<EdgeInsetsGeometry>(this.externalPadding));
    assert(_fieldChecker<double>(this.height));
    assert(_fieldChecker<double>(this.width));
    assert(_fieldChecker<double>(this.internalSpacing));
    assert(_fieldChecker<bool>(this.absorbing));
    assert(_fieldChecker<bool>(this.showLoadingDuringAction));
    assert(_fieldChecker<bool>(this.isInternalLoadingValid));
    assert(_fieldChecker<bool>(this.forceCircleDuringLoading));
    assert(_fieldChecker<bool>(this.forceLoading));
    assert(_fieldChecker<Color>(this.tintColorSelected));
    assert(_fieldChecker<Color>(this.backgroundColorSelected));
    assert(_fieldChecker<String>(this.tooltipMessage));
    assert(_fieldChecker<bool>(this.automaticallyResize));
    assert(_fieldChecker<bool>(this.forceSelected));
    assert(_fieldChecker<double>(this.circleIconSize));
    assert(_fieldChecker<double>(this.circleBorderRadius));
    assert(_fieldChecker<bool>(this.showExpandedVersion));
    assert(_fieldChecker<TextOverflow>(this.textOverflow));
    assert(_fieldChecker<int>(this.maxLines));
  }

  bool _fieldChecker<T>(value) {
    return value == null || value is T || value is ResetValueField;
  }

  static IGButtonViewStyle initialValues() {
    return IGButtonViewStyle(
        showIconOnLeft: true,
        showIconOnRight: false,
        showAnimatedArrow: false,
        externalPadding: EdgeInsets.zero,
        internalPadding: EdgeInsets.zero,
        internalSpacing: 8.0,
        height: 36.0,
        absorbing: false,
        automaticallyResize: true,
        forceLoading: false,
        forceSelected: false,
        forceCircleDuringLoading: true,
        showLoadingDuringAction: false,
        isInternalLoadingValid: true,
        showExpandedVersion: false,
        circleIconSize: 36,
        circleBorderRadius: 20);
  }

  IGButtonViewStyle copyWith({required IGButtonViewStyle style}) {
    return IGButtonViewStyle(
      text: style.text,
      boxShadow: style.boxShadow is ResetValueField
          ? null
          : style.boxShadow ?? this.boxShadow,
      textStyle: style.textStyle is ResetValueField
          ? null
          : style.textStyle ?? this.textStyle,
      showIconOnLeft: style.showIconOnLeft is ResetValueField
          ? null
          : style.showIconOnLeft ?? this.showIconOnLeft,
      showIconOnRight: style.showIconOnRight is ResetValueField
          ? null
          : style.showIconOnRight ?? this.showIconOnRight,
      icon: style.icon is ResetValueField ? null : style.icon ?? this.icon,
      backgroundColor: style.backgroundColor is ResetValueField
          ? null
          : style.backgroundColor ?? this.backgroundColor,
      disabledColor: style.disabledColor is ResetValueField
          ? null
          : style.disabledColor ?? this.disabledColor,
      border:
          style.border is ResetValueField ? null : style.border ?? this.border,
      hoverColor: style.hoverColor is ResetValueField
          ? null
          : style.hoverColor ?? this.hoverColor,
      hoverTextColor: style.hoverTextColor is ResetValueField
          ? null
          : style.hoverTextColor ?? this.hoverTextColor,
      hoverIconColor: style.hoverIconColor is ResetValueField
          ? null
          : style.hoverIconColor ?? this.hoverIconColor,
      textColor: style.textColor is ResetValueField
          ? null
          : style.textColor ?? this.textColor,
      iconColor: style.iconColor is ResetValueField
          ? null
          : style.iconColor ?? this.iconColor,
      iconSize: style.iconSize is ResetValueField
          ? null
          : style.iconSize ?? this.iconSize,
      borderRadius: style.borderRadius is ResetValueField
          ? null
          : style.borderRadius ?? this.borderRadius,
      customBorderRadius: style.customBorderRadius is ResetValueField
          ? null
          : style.customBorderRadius ?? this.customBorderRadius,
      showAnimatedArrow: style.showAnimatedArrow is ResetValueField
          ? null
          : style.showAnimatedArrow ?? this.showAnimatedArrow,
      externalPadding: style.externalPadding is ResetValueField
          ? null
          : style.externalPadding ?? this.externalPadding,
      internalPadding: style.internalPadding is ResetValueField
          ? null
          : style.internalPadding ?? this.internalPadding,
      internalSpacing: style.internalSpacing is ResetValueField
          ? null
          : style.internalSpacing ?? this.internalSpacing,
      height:
          style.height is ResetValueField ? null : style.height ?? this.height,
      absorbing: style.absorbing is ResetValueField
          ? null
          : style.absorbing ?? this.absorbing,
      width: style.width is ResetValueField ? null : style.width ?? this.width,
      tintColorSelected: style.tintColorSelected is ResetValueField
          ? null
          : style.tintColorSelected ?? this.tintColorSelected,
      backgroundColorSelected: style.backgroundColorSelected is ResetValueField
          ? null
          : style.backgroundColorSelected ?? this.backgroundColorSelected,
      tooltipMessage:
          style.tooltipMessage is ResetValueField ? null : style.tooltipMessage,
      automaticallyResize: style.automaticallyResize is ResetValueField
          ? null
          : style.automaticallyResize ?? this.automaticallyResize,
      forceLoading: style.forceLoading is ResetValueField
          ? null
          : style.forceLoading ?? this.forceLoading,
      forceSelected: style.forceSelected is ResetValueField
          ? null
          : style.forceSelected ?? this.forceSelected,
      forceCircleDuringLoading:
          style.forceCircleDuringLoading is ResetValueField
              ? null
              : style.forceCircleDuringLoading ?? this.forceCircleDuringLoading,
      showLoadingDuringAction: style.showLoadingDuringAction is ResetValueField
          ? null
          : style.showLoadingDuringAction ?? this.showLoadingDuringAction,
      isInternalLoadingValid: style.isInternalLoadingValid is ResetValueField
          ? null
          : style.isInternalLoadingValid ?? this.isInternalLoadingValid,
      onTapPositioned: style.onTapPositioned is ResetValueField
          ? null
          : style.onTapPositioned ?? this.onTapPositioned,
      circleIconSize: style.circleIconSize is ResetValueField
          ? null
          : style.circleIconSize ?? this.circleIconSize,
      circleBorderRadius: style.circleBorderRadius is ResetValueField
          ? null
          : style.circleBorderRadius ?? this.circleBorderRadius,
      showExpandedVersion: style.showExpandedVersion is ResetValueField
          ? null
          : style.showExpandedVersion ?? this.showExpandedVersion,
      textOverflow: style.textOverflow is ResetValueField
          ? null
          : style.textOverflow ?? this.textOverflow,
      maxLines: style.maxLines is ResetValueField
          ? null
          : style.maxLines ?? this.maxLines,
    );
  }
}

extension IGButtonViewDefaults on IGButtonView {
  static double _lButtonSize = 36;
  static double _m1ButtonSize = 30;
  static double _mButtonSize = 26;

  static double _lIconSize = 22;
  static double _m1IconSize = 24;
  static double _mIconSize = 18;

  static int _defaultDarkenAmount = 5;

  static IGButtonView largeButtonIconBackground(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _lButtonSize,
      height: _lButtonSize,
      iconSize: _lIconSize,
      automaticallyResize: false,
      showAnimatedArrow: false,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: IGColors.separator(),
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  static IGButtonView semiMediumButtonIconBackground(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _m1ButtonSize,
      height: _m1ButtonSize,
      iconSize: _m1IconSize,
      automaticallyResize: false,
      showAnimatedArrow: false,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: IGColors.separator(),
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  static IGButtonView largeButtonAnimatedArrow(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _lButtonSize,
      height: _lButtonSize,
      iconSize: _lButtonSize,
      automaticallyResize: false,
      showAnimatedArrow: true,
      showIconOnLeft: false,
      showIconOnRight: false,
      icon: Icons.arrow_drop_down_rounded,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: IGColors.separator(),
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  static IGButtonView mediumButtonAnimatedArrow(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _mButtonSize,
      height: _mButtonSize,
      iconSize: _mButtonSize,
      automaticallyResize: false,
      showAnimatedArrow: true,
      showIconOnLeft: false,
      showIconOnRight: false,
      icon: Icons.arrow_drop_down_rounded,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: IGColors.separator(),
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  //ICON BUTTON ONLY HOVER TYPE----
  static IGButtonView largeButtonIconOnlyHover(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _lButtonSize,
      height: _lButtonSize,
      iconSize: _lIconSize,
      automaticallyResize: false,
      showAnimatedArrow: false,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: Colors.transparent,
      disabledColor: Colors.transparent,
      circleBorderRadius: kButtonPrimaryCornerRadius,
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  static IGButtonView mediumButtonIconOnlyHover(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _mButtonSize,
      height: _mButtonSize,
      iconSize: _mIconSize,
      automaticallyResize: false,
      showAnimatedArrow: false,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      disabledColor: Colors.transparent,
      backgroundColor: Colors.transparent,
      circleBorderRadius: kButtonPrimaryCornerRadius,
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  static IGButtonView semiMediumButtonIconOnlyHover(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      width: _m1ButtonSize,
      height: _m1ButtonSize,
      iconSize: _m1IconSize,
      automaticallyResize: false,
      showAnimatedArrow: false,
      tintColorSelected: IGColors.primary(forceWhiteInDarkMode: true),
      backgroundColorSelected: IGColors.primary(useBrightnessOpacity: true),
      backgroundColor: Colors.transparent,
      disabledColor: Colors.transparent,
      circleBorderRadius: kButtonPrimaryCornerRadius,
      hoverColor: IGHoverType.normal.hoverColor(),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
    ).copyWith(style: style)));
  }

  //PRIMARY BUTTON TYPE----
  static IGButtonView primaryDefaultButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      automaticallyResize: false,
      showAnimatedArrow: false,
      showLoadingDuringAction: true,
      forceCircleDuringLoading: false,
      height: 40,
      internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      circleBorderRadius: kButtonPrimaryCornerRadius,
      textColor: IGColors.text(forceDark: true),
      backgroundColor: IGColors.primary(),
      hoverColor: IGHoverType.normal.hoverColor(),
    ).copyWith(style: style)));
  }

  //SECONDARY BUTTON TYPE----
  static IGButtonView secondaryButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      automaticallyResize: false,
      showAnimatedArrow: false,
      showLoadingDuringAction: false,
      forceCircleDuringLoading: false,
      height: 40,
      internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      circleBorderRadius: kButtonPrimaryCornerRadius,
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
      backgroundColor: IGColors.separator(),
      hoverColor: IGHoverType.normal.hoverColor(),
    ).copyWith(style: style)));
  }

  //TEXT HOVER COLOR NO BACKGROUND TYPE----
  static IGButtonView primaryTextHoverButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      automaticallyResize: false,
      showAnimatedArrow: false,
      showLoadingDuringAction: false,
      forceCircleDuringLoading: false,
      textColor: IGColors.primary(),
      backgroundColor: Colors.transparent,
      disabledColor: Colors.transparent,
      hoverColor: Colors.transparent,
      hoverTextColor: IGColors.primary().darken(_defaultDarkenAmount),
    ).copyWith(style: style)));
  }

  static IGButtonView defaultTextHoverButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      automaticallyResize: false,
      showAnimatedArrow: false,
      showLoadingDuringAction: false,
      forceCircleDuringLoading: false,
      textColor: IGColors.text(),
      backgroundColor: Colors.transparent,
      disabledColor: Colors.transparent,
      hoverColor: Colors.transparent,
      hoverTextColor: IGColors.textSecondary().darken(_defaultDarkenAmount),
    ).copyWith(style: style)));
  }

  //TEXT HOVER COLOR NO BACKGROUND TYPE----
  static IGButtonView primaryTextHoverBackgroundButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
      height: 30,
      automaticallyResize: false,
      showAnimatedArrow: false,
      showLoadingDuringAction: false,
      forceCircleDuringLoading: false,
      internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      textColor: IGColors.text(),
      iconColor: IGColors.text(),
      backgroundColor: Colors.transparent,
      disabledColor: Colors.transparent,
      hoverIconColor: IGColors.primary(forceWhiteInDarkMode: true)
          .darken(_defaultDarkenAmount),
      hoverColor: IGHoverType.normal.hoverColor(),
      hoverTextColor: IGColors.primary(forceWhiteInDarkMode: true)
          .darken(_defaultDarkenAmount),
    ).copyWith(style: style)));
  }

  //BUTTON TAG BACKGROUND TYPE----
  static IGButtonView primaryTagBackgroundButton(
      {required BuildContext context,
      required IGButtonViewStyle style,
      Color? tagColor}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
                    height: 30,
                    automaticallyResize: false,
                    showAnimatedArrow: false,
                    showLoadingDuringAction: false,
                    forceCircleDuringLoading: false,
                    internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    textColor: IGColors.text(),
                    iconColor: IGColors.text(),
                    backgroundColor:
                        (tagColor ?? IGColors.primary()).withOpacity(0.1),
                    disabledColor:
                        (tagColor ?? IGColors.primary()).withOpacity(0.1),
                    hoverColor: IGHoverType.normal.hoverColor(),
                    border: Border.all(color: tagColor ?? IGColors.primary()))
                .copyWith(style: style)));
  }

  //EXPANDED BUTTON (SELECT COLUMNS OR LANGUAGES FOR EXAMPLE)
  static IGButtonView expandedLargeButton(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues().copyWith(
            style: IGButtonViewStyle(
                automaticallyResize: false,
                showLoadingDuringAction: false,
                textColor: IGColors.text(),
                showIconOnLeft: false,
                showIconOnRight: true,
                icon: Icons.unfold_more_rounded,
                iconSize: 16,
                iconColor: IGColors.text(),
                internalPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                backgroundColor:
                    webConfigurator.currentBrightness == Brightness.dark
                        ? IGColors.tertiaryBackground()
                        : Colors.white,
                border:
                    Border.all(color: IGColors.text(opacity: 0.12), width: 1.0),
                boxShadow: [
          BoxShadow(blurRadius: 4, color: IGColors.text(opacity: 0.12))
        ]).copyWith(style: style)));
  }

  //ONLY BOTTOM BORDER BUTTON (TAB BARS FOR EXAMPLE)
  static IGButtonView onlyBottomBorderButtonPrimary(
      {required BuildContext context,
      required IGButtonViewStyle style,
      required bool isSelected}) {
    return IGButtonView(
        options: IGButtonViewStyle.initialValues()
            .copyWith(
                style: IGButtonViewStyle(
                    automaticallyResize: false,
                    backgroundColor: Colors.transparent,
                    hoverColor: IGHoverType.normal.hoverColor(),
                    textColor: isSelected
                        ? IGColors.primary(forceWhiteInDarkMode: true)
                        : IGColors.textSecondary(),
                    showAnimatedArrow: false,
                    showIconOnLeft: false,
                    showIconOnRight: false,
                    internalPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    externalPadding: EdgeInsets.zero,
                    borderRadius: 0.0,
                    textStyle: IGTextStyles.normalStyle(
                        14,
                        isSelected
                            ? IGColors.primary(forceWhiteInDarkMode: true)
                            : IGColors.separator()),
                    border: isSelected
                        ? Border(
                            bottom: BorderSide(
                                width: 2,
                                color: IGColors.primary(
                                    forceWhiteInDarkMode: true)))
                        : null))
            .copyWith(style: style));
  }

  //CUSTOM BUTTON BASED ON SIZE
  static IGButtonView secondaryOnLargeSemiMediumHoverOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.secondaryButton(context: context, style: style);
  }

  static IGButtonView secondaryOnLargeSemiMediumBackgroundOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.semiMediumButtonIconBackground(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.secondaryButton(context: context, style: style);
  }

  static IGButtonView secondaryOnLargeLargeBackgroundOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.largeButtonIconBackground(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.secondaryButton(context: context, style: style);
  }

  static IGButtonView primaryOnLargeSemiMediumHoverOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.primaryDefaultButton(
            context: context, style: style);
  }

  static IGButtonView primaryOnLargeSemiMediumBackgroundOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.semiMediumButtonIconBackground(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.primaryDefaultButton(
            context: context, style: style);
  }

  static IGButtonView primaryOnLargeLargeBackgroundOtherwise(
      {required BuildContext context, required IGButtonViewStyle style}) {
    return SizeDetector.isSmallScreen(context)
        ? IGButtonViewDefaults.largeButtonIconBackground(
            context: context,
            style: style.copyWith(
                style:
                    IGButtonViewStyle(text: null, tooltipMessage: style.text)))
        : IGButtonViewDefaults.primaryDefaultButton(
            context: context, style: style);
  }
}
