/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class IGFormContainer extends StatefulWidget {
  final Widget? Function(IGFormContainerState)? builder;
  final GlobalKey? containerKey;
  IGFormContainer({this.builder, this.containerKey});
  @override
  IGFormContainerState createState() => IGFormContainerState();
}

class IGFormContainerState extends State<IGFormContainer>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  Widget? _overlay;

  Size? get containerSize {
    try {
      if (widget.containerKey != null) {
        return widget.containerKey!.globalPaintBounds!.size;
      }
    } catch (e) {
      return null;
    }
    return null;
  }

  refresh() {
    setState(() {});
  }

  addOverlay({Widget? overlay}) {
    setState(() {
      _overlay = overlay;
    });
  }

  removeOverlay() {
    setState(() {
      _overlay = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        widget.builder!(this)!,
        if (_overlay != null)
          AnimatedContainer(
              curve: Curves.bounceIn,
              duration: Duration(milliseconds: 300),
              child: _overlay ?? Container())
      ],
    );
  }
}
