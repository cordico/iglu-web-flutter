/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'dart:convert';

JsonDecoder decoder = const JsonDecoder();
JsonEncoder encoder = const JsonEncoder.withIndent('  ');

String prettyPrintJson(dynamic input) {
  final prettyString = encoder.convert(input);
  return prettyString;
}
