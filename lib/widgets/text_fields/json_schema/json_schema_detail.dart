/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class JsonSchemaDetail {
  final JsonSchemaModel jsonSchema;
  final Map<String, dynamic> data;
  IGLayoutItem? Function(String, dynamic)? customLayoutItem;
  JsonSchemaDetail(
      {required this.data, required this.jsonSchema, this.customLayoutItem});

  List<IGLayoutItem> get calculateLayoutItems {
    return _calculateLayoutItems(
        properties: jsonSchema.properties ?? {},
        data: data,
        required: jsonSchema.propertyOrder ?? jsonSchema.required,
        isRoot: true);
  }

  List<IGLayoutItem> _calculateLayoutItems(
      {required Map<String, JsonSchemaModelProperty> properties,
      required Map<String, dynamic> data,
      List<dynamic>? required,
      bool isRoot = false,
      bool isInsideArray = false}) {
    var temp = <IGLayoutItem>[];
    var mainItemTemp = <IGLayoutItem>[];
    properties.forEach((key, value) {
      try {
        dynamic cli;
        if (customLayoutItem != null) {
          cli = customLayoutItem!(key, data[key]);
        }
        if ((value.type ?? "") == "object") {
          //MARK: OBJECT TYPE
          if (data[key] != null) {
            temp.add(
              IGLayoutItem(
                  key: key,
                  type: IGLayoutItemType.header,
                  headerOptions: IGLayoutItemTypeHeaderOptions(
                    title: key
                        .toUpperCase()
                        .replaceAll("_", " ")
                        .replaceAll("-", " "),
                  )),
            );
            var calcs = _calculateLayoutItems(
                properties: value.properties ?? {},
                data: data[key],
                required: value.propertyOrder ?? value.required);
            _sortArray(calcs, required: value.propertyOrder ?? value.required);
            temp.addAll(calcs);
          }
        } else if ((value.type ?? "") == "array") {
          //MARK: ARRAY TYPE
          if (value.items?["type"] != null) {
            //MARK: PRIMITIVE TYPE
            var i = cli ??
                IGLayoutItem(
                    key: key,
                    type: IGLayoutItemType.wrapTag,
                    label: (value.title ?? key)
                        .toUpperCase()
                        .replaceAll("_", " ")
                        .replaceAll("-", " "),
                    value: ((data[key] as List?) ?? []));
            if (isRoot) {
              mainItemTemp.add(i);
            } else {
              temp.add(i);
            }
          } else if (value.items?["anyOf"] != null) {
            var property =
                JsonSchemaModelProperty.fromJson(value.items?["anyOf"][0]);
            var l = data[key] as List?;
            if (l != null && l.isNotEmpty) {
              var type = property.type?.toLowerCase();
              if (["string", "boolean", "integer", "number"].contains(type)) {
                //MARK: PRIMITIVE TYPE
                var i = cli ??
                    IGLayoutItem(
                        key: key,
                        type: IGLayoutItemType.wrapTag,
                        label: key
                            .toUpperCase()
                            .replaceAll("_", " ")
                            .replaceAll("-", " "),
                        value: ((data[key] as List?) ?? []));
                if (isRoot) {
                  mainItemTemp.add(i);
                } else {
                  temp.add(i);
                }
              } else {
                //MARK: COMPOSITE TYPE
                l.asMap().forEach((index, element) {
                  var calcs = _calculateLayoutItems(
                      properties: property.properties ?? {},
                      data: element,
                      isInsideArray: true,
                      required: property.propertyOrder ?? property.required);
                  _sortArray(calcs,
                      required: property.propertyOrder ?? property.required);
                  if (calcs.isNotEmpty) {
                    temp.add(
                      IGLayoutItem(
                          key: "$index-$key",
                          type: IGLayoutItemType.header,
                          headerOptions: IGLayoutItemTypeHeaderOptions(
                            title: (property.title ?? key)
                                    .toUpperCase()
                                    .replaceAll("_", " ")
                                    .replaceAll("-", " ") +
                                " - #${index + 1}",
                          )),
                    );
                    temp.addAll(calcs);
                  }
                });
              }
            }
          }
        } else {
          //MARK: PRIMITIVE TYPE
          var i = cli ??
              IGLayoutItem(
                  key: key,
                  type: value.type?.toLowerCase() == "boolean"
                      ? IGLayoutItemType.bool
                      : (value.type?.toLowerCase() == "integer" ||
                              value.type?.toLowerCase() == "number")
                          ? IGLayoutItemType.text
                          : IGLayoutItemType.text,
                  label: (value.title ?? key)
                      .toUpperCase()
                      .replaceAll("_", " ")
                      .replaceAll("-", " "),
                  value: "${data[key] ?? "-"}");
          if (isRoot) {
            mainItemTemp.add(i);
          } else {
            temp.add(i);
          }
        }
      } catch (e) {
        logger(e);
      }
    });
    if (required != null && required.isNotEmpty) {
      if (isRoot) {
        _sortArray(mainItemTemp, required: required);
        temp = mainItemTemp + temp;
      } else {
        _sortArray(temp, required: required);
      }
    }
    var newTemp = <IGLayoutItem>[];
    temp.forEach((element) {
      newTemp.add(element);
      if (isRoot && element.type != IGLayoutItemType.none) {
        //ADD NONE ONLY IF IS THE MAIN ROOT AND NOT INSIDE THE RECURSION
        newTemp.add(IGLayoutItem.none);
      }
    });
    return newTemp;
  }

  _sortArray(List<IGLayoutItem> array, {List<dynamic>? required}) {
    //SORT ONLY NOT NONE ITEM
    array.removeWhere((element) => element.type == IGLayoutItemType.none);
    array.sort((item1, item2) {
      var indx1 = (required ?? []).indexOf(item1.key);
      var indx2 = (required ?? []).indexOf(item2.key);
      if (indx1 == -1) {
        return 1;
      }
      if (indx2 == -1) {
        return -1;
      }
      return indx1.compareTo(indx2);
    });
  }
}
