/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

class JsonSchemaModel {
  String? id;
  String? type;
  String? title;
  String? schema;
  List<dynamic>? examples;
  List<dynamic>? required;
  String? description;
  bool? additionalProperties;
  Map<String, JsonSchemaModelProperty>? properties;
  List<dynamic>? propertyOrder;

  JsonSchemaModel();

  JsonSchemaModel.fromJson(Map<String, dynamic> json) {
    this.id = json['\$id'];
    this.type = json['type'];
    this.title = json['title'];
    this.schema = json['\$schema'];
    this.examples = json['examples'];
    this.required = json['required'];
    this.description = json['description'];
    this.additionalProperties = json['additionalProperties'];
    this.propertyOrder = json['propertyOrder'];
    if (properties == null) {
      properties = {};
    }
    (json["properties"] as Map<String, dynamic>?)?.forEach((key, value) {
      properties![key] = JsonSchemaModelProperty.fromJson(value);
    });
  }

  static JsonSchemaModel fromJSON(Map<String, dynamic> json) {
    return JsonSchemaModel.fromJson(json);
  }
}

class JsonSchemaModelProperty {
  String? id;
  String? type;
  String? title;
  Map<String, dynamic>? items;
  List<dynamic>? examples;
  List<dynamic>? required;
  Map<String, JsonSchemaModelProperty>? properties;
  String? description;
  bool? additionalProperties;
  List<dynamic>? propertyOrder;

  JsonSchemaModelProperty();

  JsonSchemaModelProperty.fromJson(Map<String, dynamic> json) {
    this.id = json['\$id'];
    this.type = json['type'];
    this.title = json['title'];
    this.items = json['items'];
    this.examples = json['examples'];
    this.required = json['required'];
    this.description = json['description'];
    this.additionalProperties = json['additionalProperties'];
    this.propertyOrder = json['propertyOrder'];
    if (properties == null) {
      properties = {};
    }
    (json["properties"] as Map<String, dynamic>?)?.forEach((key, value) {
      properties![key] = JsonSchemaModelProperty.fromJson(value);
    });
  }

  static JsonSchemaModelProperty fromJSON(Map<String, dynamic> json) {
    return JsonSchemaModelProperty.fromJson(json);
  }
}
