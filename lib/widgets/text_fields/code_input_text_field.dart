/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

enum CodeInputTextFieldStyle { border, outline, circle }

class CodeInputTextField extends StatefulWidget {
  final Key? key;
  final int codeLength;
  final CodeInputTextFieldStyle style;
  final Function(String)? onChanged;
  final TextEditingController? textEditingController;

  CodeInputTextField(
      {this.codeLength = 9,
      this.onChanged,
      this.key,
      this.textEditingController,
      this.style = CodeInputTextFieldStyle.border});
  @override
  CodeInputTextFieldState createState() => CodeInputTextFieldState();
}

class CodeInputTextFieldState extends State<CodeInputTextField>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  late Animation<Color?> _animation;
  late Animation<double> _animation2;
  late AnimationController _animationController;

  TextEditingController? textEditingController = TextEditingController();
  FocusNode focusNode = FocusNode();

  String code = '';
  bool canPaste = false;
  bool isLoading = false;
  bool isCodeValid = false;

  setAnimationController() {
    _animationController = new AnimationController(
        duration: const Duration(milliseconds: 400), vsync: this);
    final Animation curve = new CurvedAnimation(
        parent: _animationController, curve: Curves.easeInOut);
    _animation = ColorTween(
      begin: IGColors.primary().withOpacity(0.4),
      end: IGColors.primary().withOpacity(0.9),
    ).animate(curve as Animation<double>);
    _animation2 = new Tween<double>(
      begin: 0.8,
      end: 1,
    ).animate(curve);

    _animationController.addListener(() {
      if (mounted) setState(() {});
    });

    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _animationController.forward();
      }
    });

    focusNode.addListener(() {
      if (!focusNode.hasFocus) {
        _animationController.reset();
        _animationController.stop();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.textEditingController == null) {
      textEditingController = TextEditingController();
    } else {
      textEditingController = widget.textEditingController;
    }
    setAnimationController();
  }

  setNewValue(String val) {
    setState(() {
      code = val;
      isCodeValid = val.length == widget.codeLength;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      key: widget.key,
      color: IGColors.secondaryBackground(),
      child: LayoutBuilder(builder: (context, constraints) {
        var w = widthStep(constraints);
        return Container(
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              child: Stack(
                children: [
                  Opacity(
                    opacity: 0.01,
                    child: Align(
                      child: Container(
                        height: 40,
                        alignment: Alignment.center,
                        constraints: BoxConstraints(
                            maxWidth:
                                (((widget.codeLength * w) + 100.toDouble()) /
                                        2.0) -
                                    60),
                        child: TextField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          focusNode: focusNode,
                          controller: textEditingController,
                          enableInteractiveSelection: false,
                          toolbarOptions: ToolbarOptions(paste: true),
                          showCursor: false,
                          autofocus: true,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  top: 0, bottom: 0, left: 0, right: 0),
                              labelStyle: IGTextStyles.custom(
                                  color: IGColors.secondaryBackground(),
                                  fontSize: 36)),
                          textAlign: TextAlign.center,
                          autofillHints: [],
                          maxLength: widget.codeLength,
                          onChanged: (val) {
                            setState(() {
                              code = val;
                              isCodeValid = val.length == widget.codeLength;
                            });
                            if (widget.onChanged != null) {
                              widget.onChanged!(val);
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                  Align(
                    child: Container(
                      color: Colors.transparent,
                      alignment: Alignment.center,
                      constraints: BoxConstraints(
                          maxWidth: ((widget.codeLength * w) + 100.toDouble())),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ...List<int>.generate(widget.codeLength, (i) => i)
                              .map((index) {
                            String text;
                            if (index < code.length) {
                              text = code[index];
                            } else {
                              text = '';
                            }
                            return returnText(text, index, constraints);
                          }).toList(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  double widthStep(BoxConstraints constraints) {
    var possible = widget.style == CodeInputTextFieldStyle.circle ? 60 : 50;
    if ((possible * widget.codeLength) + 100 > constraints.maxWidth) {
      return ((constraints.maxWidth - 100) / widget.codeLength);
    }
    return possible.toDouble();
  }

  double heightStep(BoxConstraints constraints) {
    var w = widthStep(constraints);
    return widget.style == CodeInputTextFieldStyle.circle ? w : 65;
  }

  double heightCursor(BoxConstraints constraints) {
    var w = widthStep(constraints);
    return widget.style == CodeInputTextFieldStyle.circle ? w / 2.0 : 35;
  }

  Widget returnText(String value, int index, BoxConstraints constraints) {
    var w = widthStep(constraints);
    return InkWell(
      onTap: () {
        focusNode.requestFocus();
        setState(() {});
        _animationController.forward();
      },
      mouseCursor: SystemMouseCursors.text,
      borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: widget.style == CodeInputTextFieldStyle.outline
                ? Border(bottom: BorderSide(color: IGColors.text()))
                : Border.all(color: IGColors.separator()),
            borderRadius: widget.style == CodeInputTextFieldStyle.outline
                ? null
                : widget.style == CodeInputTextFieldStyle.circle
                    ? BorderRadius.circular(w / 2.0)
                    : BorderRadius.circular(kButtonPrimaryCornerRadius)),
        width: w,
        height: heightStep(constraints),
        child: value.isEmpty && focusNode.hasFocus && (index == code.length)
            ? ScaleTransition(
                scale: _animation2,
                child: Container(
                    width: 2,
                    height: heightCursor(constraints),
                    color: _animation.value))
            : Text(
                value,
                style:
                    IGTextStyles.mediumStyle(w > 36 ? 36 : w, IGColors.text()),
              ),
      ),
    );
  }
}
