/// IGLU MOBILE LIBRARY DART
///
/// Copyright © 2021 - 2021 IGLU. All rights reserved.
/// Copyright © 2021 - 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class SearchField extends StatefulWidget {
  final bool autoFocus;
  final String? hint;
  final Function(String)? onChanged;
  final Function(String)? onSubmitted;
  final double? height;
  final double? width;
  final double? radius;
  final Color? backgroundColor;
  final bool hasShadow;
  final double contentHeight;
  final EdgeInsetsGeometry? margin;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final bool showClose;

  SearchField({
    this.autoFocus = false,
    this.onChanged,
    this.onSubmitted,
    this.hint,
    this.height,
    this.width,
    this.radius,
    this.backgroundColor,
    this.hasShadow = true,
    this.contentHeight = 0,
    this.margin,
    this.focusNode,
    this.controller,
    this.showClose = false,
  });
  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  TextEditingController? _controller = TextEditingController();
  FocusNode? _focusNode;
  CrossFadeState searchIconState = CrossFadeState.showFirst;

  @override
  void initState() {
    super.initState();
    if (widget.controller != null) {
      _controller = widget.controller;
    }
    if (widget.focusNode != null) {
      _focusNode = widget.focusNode;
    } else {
      _focusNode = FocusNode();
    }
    _focusNode!.addListener(() {
      if (_focusNode == null || _focusNode!.hasFocus) {
        final val = TextSelection.collapsed(offset: _controller!.text.length);
        _controller!.selection = val;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: widget.margin,
        constraints: BoxConstraints(
            maxWidth: widget.width ?? double.infinity,
            maxHeight: widget.height ?? double.infinity),
        decoration: BoxDecoration(
          color: widget.backgroundColor != null
              ? widget.backgroundColor
              : IGColors.tertiaryBackground(),
          borderRadius: BorderRadius.circular(widget.radius != null
              ? widget.radius!
              : kButtonPrimaryCornerRadius),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AnimatedCrossFade(
                firstCurve: Curves.bounceIn,
                secondCurve: Curves.bounceOut,
                duration: Duration(milliseconds: 300),
                secondChild: Container(),
                firstChild: Icon(
                  Icons.search_rounded,
                  size: 20,
                  color: IGColors.text(),
                ),
                crossFadeState: searchIconState,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: TextField(
                    enableSuggestions: false,
                    autofillHints: [],
                    keyboardType: TextInputType.text,
                    focusNode: _focusNode,
                    controller: _controller,
                    keyboardAppearance: webConfigurator.currentBrightness,
                    style: IGTextStyles.normalStyle(14, IGColors.text()),
                    autofocus: widget.autoFocus,
                    cursorColor: IGColors.text(),
                    onChanged: widget.onChanged,
                    onSubmitted: widget.onSubmitted,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding:
                          EdgeInsets.only(bottom: (widget.height! / 2.0) - 10),
                      hintText: widget.hint,
                      hintStyle: IGTextStyles.normalStyle(
                          14, IGColors.textSecondary()),
                    ),
                  ),
                ),
              ),
              if (widget.showClose)
                Opacity(
                  opacity: _controller?.text.isEmpty == true ? 0 : 1,
                  child: InkWell(
                    mouseCursor: SystemMouseCursors.click,
                    onTap: () {
                      _controller?.clear();
                      _focusNode?.unfocus();
                      setState(() {});
                      if (widget.onChanged != null) {
                        widget.onChanged!("");
                      }
                    },
                    child: Icon(Icons.clear_rounded,
                        size: 20, color: IGColors.textSecondary()),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
