/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'dart:async';
import 'package:flutter/foundation.dart';
import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import "package:intl/intl.dart";

enum SuggestionBoxStyle { list, wrap }

class FormTextFormFieldMenuOption {
  String key;
  String value;
  String? displayValue;
  FormTextFormFieldMenuOption(
      {this.key = "", this.value = "", this.displayValue});

  @override
  String toString() {
    return "$key-$value";
  }

  @override
  bool operator ==(dynamic o) =>
      o is FormTextFormFieldMenuOption && o.key == key && o.value == value;

  @override
  int get hashCode => key.hashCode + value.hashCode;
}

class FormTextFormField extends StatefulWidget {
  final IGFormFieldValidator? formFieldValidator;
  final Function(String)? onFieldSubmitted;
  final Function(String?, bool)? onChanged;
  final Function(Size?)? onChangedSize;
  final Function()? onEditingComplete;
  final Function(TextEditingController?)? onUnFocus;
  final Function()? onTap;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final String? labelText;
  final TextStyle? labelStyle;
  final Color? primaryColor;
  final EdgeInsetsGeometry? contentPadding;
  final bool? isDense;
  final InputDecoration? decoration;
  final bool? obscureText;
  final bool? autofocus;
  final String? initialValue;
  final List<TextInputFormatter>? inputFormatters;
  final TextStyle? style;
  final int? maxLines;
  final int? minLines;
  final bool readOnly;
  final bool? enabled;
  final bool alwaysRemoveFocus;
  final Iterable<String>? autofillHints;

  final bool hideBorder;
  final bool showBackgroundStyle;
  final BoxDecoration? backgroundDecoration;
  final EdgeInsets? backgroundMargin;

  final TextEditingController? textEditingController;

  final IGFormContainerState? formContainerState;

  final bool showDatePicker;
  final DateTime? initialDate;
  final DateTime? firstDate;
  final DateTime? lastDate;
  final bool Function(DateTime)? selectableDayPredicate;
  final String displayDateFormat;

  final bool showMenuOptions;
  final List<String>? menuOptions;
  final SuggestionBoxStyle suggestionBoxStyle;
  final Widget Function(FormTextFormFieldMenuOption, FormTextFormFieldState?)?
      suggestionBoxWidgetAtIndex;
  final List<FormTextFormFieldMenuOption>? fieldMenuOptions;
  final Future<List<FormTextFormFieldMenuOption>> Function()?
      generateMenuOption;
  final double suggestionsBoxVerticalOffset;
  final WrapAlignment suggestionBoxWrapAlignment;
  final bool filterOptionsWhileTyping;
  final bool hideOnResizing;
  final bool isMultipleSelection;
  final bool allowsNonOptionsValue;
  final String Function(String)? suggestionBoxNoResultPlaceholder;

  //HELPER
  final String? helperText;
  final TextStyle? helperTextStyle;

  final int helperMaxLines;
  final int errorMaxLines;

  //RESET UTILITY
  final bool reset;
  final bool resetGeneratedOptions;

  //
  final bool isInteractionDisabled;
  final bool vibrate;

  //MARKDOWN
  /// List of action the component can handle
  final List<MarkdownType> markdownActions;

  FormTextFormField(
      {this.keyboardType,
      this.textInputAction,
      this.onChanged,
      this.onChangedSize,
      this.labelText,
      this.labelStyle,
      this.onFieldSubmitted,
      this.primaryColor,
      this.contentPadding,
      this.isDense,
      this.decoration,
      this.obscureText,
      this.onEditingComplete,
      this.onTap,
      this.autofocus,
      this.initialValue,
      this.inputFormatters,
      this.style,
      this.autofillHints,
      this.maxLines,
      this.minLines,
      this.readOnly = false,
      this.enabled,
      this.suggestionBoxNoResultPlaceholder,
      this.textEditingController,
      this.formContainerState,
      this.generateMenuOption,
      this.selectableDayPredicate,
      this.suggestionBoxWrapAlignment = WrapAlignment.start,
      this.suggestionsBoxVerticalOffset = 0.0,
      this.helperMaxLines = 5,
      this.errorMaxLines = 5,
      this.filterOptionsWhileTyping = true,
      this.hideBorder = false,
      this.resetGeneratedOptions = false,
      this.hideOnResizing = false,
      this.alwaysRemoveFocus = false,
      this.showDatePicker = false,
      this.isMultipleSelection = false,
      this.allowsNonOptionsValue = false,
      this.initialDate,
      this.firstDate,
      this.lastDate,
      this.suggestionBoxWidgetAtIndex,
      this.suggestionBoxStyle = SuggestionBoxStyle.list,
      this.displayDateFormat = "dd/MM/yyyy",
      this.backgroundMargin,
      this.menuOptions,
      this.fieldMenuOptions,
      this.showMenuOptions = false,
      this.vibrate = false,
      this.helperText,
      this.helperTextStyle,
      this.reset = false,
      this.showBackgroundStyle = false,
      this.backgroundDecoration,
      this.isInteractionDisabled = false,
      this.formFieldValidator,
      this.markdownActions = const [],
      this.onUnFocus});
  @override
  FormTextFormFieldState createState() => FormTextFormFieldState();
}

class FormTextFormFieldState extends State<FormTextFormField>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  late FocusNode focusNode;
  final GlobalKey containerKey = GlobalKey();
  String currentValue = "";
  var textEditingController = TextEditingController();
  bool isFocused = false;
  SuggestionsBox? suggestionsBox;
  final LayerLink layerLink = LayerLink();
  List<FormTextFormFieldMenuOption>? originalMenuOptions = [];
  List<FormTextFormFieldMenuOption>? filteredMenuOptions = [];
  FormTextFormFieldMenuOption initMenuOption = FormTextFormFieldMenuOption();
  IGFormFieldValidator? formFieldValidator;

  @override
  void didChangeMetrics() {
    if (suggestionsBox != null &&
        focusNode.hasFocus &&
        suggestionsBox!.isOpened) {
      if (widget.hideOnResizing) {
        focusNode.unfocus();
      }
      suggestionsBox!.onChangeMetrics();
    }
  }

  @override
  void didUpdateWidget(FormTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      formFieldValidator = widget.formFieldValidator;
    }
    if (oldWidget != widget && widget.initialValue != oldWidget.initialValue) {
      currentValue = widget.initialValue ?? "";
      initMenuOption = FormTextFormFieldMenuOption(
          key: widget.initialValue ?? "", value: widget.initialValue ?? "");
      if (widget.textEditingController == null) {
        textEditingController = TextEditingController(text: currentValue);
      } else {
        textEditingController =
            widget.textEditingController ?? TextEditingController();
        currentValue = widget.initialValue ?? textEditingController.text;
      }
    }
    if (widget.resetGeneratedOptions) {
      logger("RESET GENERATED OPTIONS");
      originalMenuOptions!.clear();
      filteredMenuOptions!.clear();
      if (suggestionsBox != null) {
        suggestionsBox!.calculateGenerateMenuOptionAlreadyDone = false;
      }
      textEditingController =
          widget.textEditingController ?? TextEditingController();
      currentValue = widget.initialValue ?? textEditingController.text;
      initMenuOption = FormTextFormFieldMenuOption(
          key: widget.initialValue ?? "", value: widget.initialValue ?? "");
    }
    if (oldWidget != widget &&
        (!listEquals(widget.menuOptions, oldWidget.menuOptions) ||
            !listEquals(widget.fieldMenuOptions, oldWidget.fieldMenuOptions))) {
      if (widget.generateMenuOption == null) {
        originalMenuOptions = widget.fieldMenuOptions != null
            ? widget.fieldMenuOptions
            : widget.menuOptions == null
                ? []
                : widget.menuOptions!
                    .map((e) => FormTextFormFieldMenuOption(key: e, value: e))
                    .toList();
        filteredMenuOptions = originalMenuOptions;
      }
    }
    moveTextControllerCursorToLast();
  }

  _initLocal() {
    formFieldValidator = widget.formFieldValidator;
    originalMenuOptions = widget.fieldMenuOptions != null
        ? widget.fieldMenuOptions
        : widget.menuOptions == null
            ? []
            : widget.menuOptions!
                .map((e) => FormTextFormFieldMenuOption(key: e, value: e))
                .toList();
    filteredMenuOptions = originalMenuOptions;
    currentValue = widget.initialValue ?? "";
    initMenuOption = FormTextFormFieldMenuOption(
        key: widget.initialValue ?? "", value: widget.initialValue ?? "");
    if (widget.textEditingController == null) {
      textEditingController = TextEditingController(text: currentValue);
    } else {
      textEditingController =
          widget.textEditingController ?? TextEditingController();
      currentValue = widget.initialValue ?? textEditingController.text;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    _initLocal();
    if (currentValue.isNotEmpty == true && widget.onChanged != null) {
      if (widget.showDatePicker) {
        DateFormat dateFormat = DateFormat("yyyy-MM-ddTHH:mm:ss.SSS");
        var d = dateFormat.format(widget.initialDate ?? DateTime.now());
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          if (widget.onChanged != null) {
            widget.onChanged!(d.isNotEmpty == true ? d : null, false);
          }
          if (widget.onChangedSize != null) {
            widget.onChangedSize!(context.size);
          }
        });
      } else {
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          widget.onChanged!(
              currentValue.isNotEmpty ? currentValue : null, false);
          if (widget.onChangedSize != null) {
            widget.onChangedSize!(context.size);
          }
        });
      }
    }
    focusNode = FocusNode();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        moveTextControllerCursorToLast();
        if (widget.alwaysRemoveFocus || widget.isInteractionDisabled) {
          focusNode.unfocus();
        }
      }

      if (focusNode.hasFocus &&
          (widget.showMenuOptions || widget.showDatePicker)) {
        if (!widget.isMultipleSelection) {
          textEditingController.text = "";
        }
        suggestionsBox!.resize();
        suggestionsBox!.open();
      } else if (!focusNode.hasFocus &&
          (widget.showMenuOptions || widget.showDatePicker)) {
        _resetOptionsVariables();
        suggestionsBox!.close();
      }

      if (!widget.alwaysRemoveFocus &&
          !focusNode.hasFocus &&
          widget.onUnFocus != null) {
        widget.onUnFocus!(textEditingController);
      }
      setState(() {});
    });

    if (widget.showMenuOptions || widget.showDatePicker) {
      suggestionsBox = SuggestionsBox(
          context: context,
          state: this,
          customWidget: widget.showDatePicker ? datePicker : null);
    }
  }

  moveTextControllerCursorToLast() {
    if (textEditingController is MoneyMaskedTextController ||
        widget.markdownActions.isNotEmpty) {
    } else {
      final val = TextSelection.fromPosition(
          TextPosition(offset: textEditingController.text.length));
      textEditingController.selection = val;
    }
  }

  @override
  void dispose() {
    if (suggestionsBox != null) {
      suggestionsBox!.close();
      suggestionsBox!.widgetMounted = false;
    }
    WidgetsBinding.instance!.removeObserver(this);
    focusNode.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  refresh(void Function() fn) {
    setState(fn);
  }

  _resetOptionsVariables() {
    textEditingController.text = initMenuOption.value;
    filteredMenuOptions = originalMenuOptions;
  }

  _reset() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (widget.textEditingController == null) {
        textEditingController = TextEditingController();
      } else {
        textEditingController =
            widget.textEditingController ?? TextEditingController();
      }
      if (widget.initialValue != null) {
        textEditingController.text = widget.initialValue ?? "";
      }
      widget.onChanged!(
          widget.initialValue?.isNotEmpty == true ? widget.initialValue : null,
          false);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.reset) {
      _reset();
    }
    return CompositedTransformTarget(
      link: layerLink,
      child: InkWell(
        key: containerKey,
        canRequestFocus: false,
        onTap: widget.isInteractionDisabled || widget.markdownActions.isNotEmpty
            ? null
            : () async {
                await tapAction();
              },
        focusColor: Colors.transparent,
        hoverColor: Colors.transparent,
        mouseCursor: widget.isInteractionDisabled
            ? SystemMouseCursors.forbidden
            : widget.showDatePicker
                ? SystemMouseCursors.text
                : SystemMouseCursors.text,
        child: AbsorbPointer(
            absorbing: widget.isInteractionDisabled,
            child: widget.showBackgroundStyle
                ? Container(
                    margin: widget.backgroundMargin ??
                        EdgeInsets.fromLTRB(0, 10, 0, 0),
                    padding: EdgeInsets.all(10),
                    decoration: widget.backgroundDecoration ??
                        BoxDecoration(
                            color: IGHoverType.normal.hoverColor(),
                            border: Border.all(
                                color: hasFocus
                                    ? widget.primaryColor ?? IGColors.primary()
                                    : Colors.transparent,
                                width: 2.0),
                            borderRadius: hasFocus && suggestionsBox != null
                                ? BorderRadius.only(
                                    topLeft: Radius.circular(
                                        kButtonPrimaryCornerRadius),
                                    topRight: Radius.circular(
                                        kButtonPrimaryCornerRadius))
                                : BorderRadius.circular(
                                    kButtonPrimaryCornerRadius)),
                    child: textFormField)
                : textFormField),
      ),
    );
  }

  _onTapMarkdownOption(MarkdownType type) async {
    focusNode.requestFocus();
    await Future.delayed(Duration(milliseconds: 10), () {});

    var baseOffset = textEditingController.selection.baseOffset;
    var extentOffset = textEditingController.selection.extentOffset;

    var basePosition = baseOffset > extentOffset ? extentOffset : baseOffset;
    var extentPosition = extentOffset < baseOffset ? baseOffset : extentOffset;

    var noTextSelected = (basePosition - extentPosition) == 0;
    final result = FormatMarkdown.convertToMarkdown(
      type,
      textEditingController.text,
      basePosition,
      extentPosition,
    );

    textEditingController.value = textEditingController.value.copyWith(
        text: result.data,
        selection:
            TextSelection.collapsed(offset: basePosition + result.cursorIndex));
    widget.onChanged!(textEditingController.value.text, true);

    if (noTextSelected) {
      textEditingController.selection = TextSelection.collapsed(
          offset:
              textEditingController.selection.end - result.replaceCursorIndex);
    }
  }

  tapAction() async {
    if (widget.vibrate) {
      await HapticFeedback.vibrate();
    }
    if (widget.onTap != null) {
      widget.onTap!();
    } else {
      focusNode.requestFocus();
    }
  }

  Widget get textFormField => FocusTrapArea(
        focusNode: focusNode,
        child: MouseRegion(
          cursor: widget.isInteractionDisabled
              ? SystemMouseCursors.forbidden
              : SystemMouseCursors.basic,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (widget.markdownActions.isNotEmpty)
                Container(
                  height: 52,
                  decoration: BoxDecoration(
                    color: IGColors.tertiaryBackground(),
                    borderRadius:
                        BorderRadius.circular(kButtonPrimaryCornerRadius),
                  ),
                  child: Scrollbar(
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children:
                          widget.markdownActions.asMap().entries.map((type) {
                        return IGView(
                            onTap: () => _onTapMarkdownOption(type.value),
                            margin: EdgeInsets.fromLTRB(
                                4,
                                4,
                                type.key == widget.markdownActions.length - 1
                                    ? 4
                                    : 0,
                                4),
                            padding: EdgeInsets.all(8),
                            child: (state, context) {
                              if (type.value.icon is Widget) {
                                return type.value.icon;
                              }
                              return Icon(type.value.icon);
                            });
                      }).toList(),
                    ),
                  ),
                ),
              MouseRegion(
                cursor: widget.isInteractionDisabled
                    ? SystemMouseCursors.forbidden
                    : widget.showDatePicker
                        ? SystemMouseCursors.text
                        : SystemMouseCursors.text,
                child: TextFormField(
                    keyboardAppearance: webConfigurator.currentBrightness,
                    focusNode: focusNode,
                    controller: textEditingController,
                    autofocus: widget.autofocus ?? false,
                    inputFormatters: widget.inputFormatters,
                    style: widget.style,
                    keyboardType: widget.keyboardType ?? TextInputType.text,
                    validator: formFieldValidator?.validate,
                    autofillHints: widget.autofillHints,
                    textInputAction:
                        widget.textInputAction ?? TextInputAction.done,
                    onFieldSubmitted: (s) async {
                      currentValue = s;
                      if (widget.showMenuOptions &&
                          filteredMenuOptions?.isNotEmpty == true &&
                          !widget.isMultipleSelection) {
                        suggestionsBox!.optionCompleteAction(
                            filteredMenuOptions![0].key,
                            display: filteredMenuOptions![0].value);
                      }
                      if (widget.onFieldSubmitted != null) {
                        await widget.onFieldSubmitted!(s);
                      }
                    },
                    onEditingComplete: widget.onEditingComplete,
                    readOnly: widget.readOnly,
                    enabled: widget.enabled ?? true,
                    obscureText: widget.obscureText ?? false,
                    onTap: () async {
                      await tapAction();
                    },
                    cursorColor: widget.primaryColor ?? IGColors.primary(),
                    maxLines: widget.maxLines ?? 1,
                    minLines: widget.minLines ?? 1,
                    decoration: widget.decoration ??
                        InputDecoration(
                            hintText: widget.showMenuOptions &&
                                    !widget.isMultipleSelection
                                ? initMenuOption.value
                                : null,
                            helperText: suggestionsBox?.isOpened == true
                                ? null
                                : widget.helperText != null
                                    ? widget.helperText! + ""
                                    : null,
                            helperStyle: widget.helperTextStyle,
                            helperMaxLines: widget.helperMaxLines,
                            errorMaxLines: widget.errorMaxLines,
                            errorStyle:
                                IGTextStyles.custom(color: IGColors.red()),
                            focusColor:
                                widget.primaryColor ?? IGColors.primary(),
                            border: widget.hideBorder
                                ? InputBorder.none
                                : defaultBorder,
                            disabledBorder: widget.hideBorder
                                ? InputBorder.none
                                : defaultBorder,
                            enabledBorder: widget.hideBorder
                                ? InputBorder.none
                                : isFocused
                                    ? focusedBorder
                                    : defaultBorder,
                            errorBorder: widget.hideBorder
                                ? InputBorder.none
                                : errorBorder,
                            focusedErrorBorder: widget.hideBorder
                                ? InputBorder.none
                                : errorBorder,
                            focusedBorder: widget.hideBorder
                                ? InputBorder.none
                                : focusedBorder,
                            contentPadding: widget.contentPadding ??
                                EdgeInsets.only(
                                    left: 0, right: 30, top: 8, bottom: 8),
                            isDense: widget.isDense ?? true,
                            labelStyle: widget.labelStyle ??
                                IGTextStyles.custom(
                                    fontSize: 20,
                                    color: hasFocus
                                        ? widget.primaryColor ??
                                            IGColors.primary()
                                        : IGColors.textSecondary()),
                            labelText: labelTextWithRequirements,
                            suffixIcon: showUnfoldSuffixIcon
                                ? widget.showDatePicker && hasFocus
                                    ? InkWell(
                                        mouseCursor: SystemMouseCursors.click,
                                        onTap: () {
                                          if (widget.onChanged != null) {
                                            widget.onChanged!(null, true);
                                          }
                                          suggestionsBox!.close();
                                          textEditingController.clear();
                                          Timer(Duration(milliseconds: 100),
                                              () {
                                            focusNode.unfocus();
                                          });
                                        },
                                        child: Icon(Icons.close_outlined,
                                            color: IGColors.textSecondary()),
                                      )
                                    : Icon(Icons.unfold_more,
                                        color: hasFocus
                                            ? widget.primaryColor ??
                                                IGColors.primary()
                                            : IGColors.textSecondary())
                                : null,
                            suffixIconConstraints: showUnfoldSuffixIcon
                                ? BoxConstraints(maxHeight: 20)
                                : null),
                    onChanged: (s) {
                      currentValue = s;
                      if (widget.onChanged != null &&
                          !widget.showMenuOptions &&
                          !widget.showDatePicker) {
                        widget.onChanged!(
                            s.isNotEmpty == true ? s : null, true);
                      } else if (suggestionsBox != null &&
                          suggestionsBox!.isOpened) {
                        if (widget.filterOptionsWhileTyping) {
                          if (s.isEmpty) {
                            filteredMenuOptions = originalMenuOptions;
                          } else {
                            filteredMenuOptions = originalMenuOptions!
                                .where((element) => element.value
                                    .toLowerCase()
                                    .contains(suggestionsBox!.currentTextToUse
                                        .toLowerCase()))
                                .toList();
                          }
                        }
                        if (widget.allowsNonOptionsValue) {
                          var v = s.isNotEmpty == true ? s : "";
                          initMenuOption =
                              FormTextFormFieldMenuOption(key: v, value: v);
                          widget.onChanged!(v, true);
                        }
                        if (widget.isMultipleSelection) {
                          suggestionsBox!.calculateOptionsArray();
                        }
                        if (suggestionsBox != null) {
                          suggestionsBox!.rebuild();
                        }
                      }
                      if (widget.onChangedSize != null) {
                        widget.onChangedSize!(context.size);
                      }
                    }),
              ),
            ],
          ),
        ),
      );

  String? get labelTextWithRequirements {
    if (formFieldValidator?.isOptional == false &&
        formFieldValidator?.showRequirementText == true) {
      if (widget.labelText != null && widget.labelText!.isNotEmpty) {
        return widget.labelText! + "*";
      }
    }
    return widget.labelText;
  }

  bool get hasFocus => isFocused || focusNode.hasFocus;

  InputBorder get focusedBorder => UnderlineInputBorder(
        borderSide: BorderSide(color: IGColors.primary()),
      );
  InputBorder get defaultBorder => UnderlineInputBorder(
        borderSide: BorderSide(color: IGColors.separator()),
      );
  InputBorder get errorBorder => UnderlineInputBorder(
        borderSide: BorderSide(color: IGColors.red()),
      );

  bool get showUnfoldSuffixIcon =>
      widget.showDatePicker || widget.showMenuOptions;

  Widget get datePicker => Scrollbar(
        child: SingleChildScrollView(
          child: FormTextFormFieldStyle.datePickerTheme(
            context: context,
            child: CalendarDatePicker(
                initialDate: widget.initialDate ?? DateTime.now(),
                selectableDayPredicate: widget.selectableDayPredicate,
                firstDate: widget.firstDate != null
                    ? widget.firstDate!
                    : widget.initialDate != null &&
                            widget.initialDate!.isBefore(DateTime.now())
                        ? widget.initialDate!
                        : DateTime.now(),
                lastDate: widget.lastDate != null
                    ? widget.lastDate!
                    : DateTime.utc(DateTime.now().year + 1, 12, 31),
                onDateChanged: (picked) {
                  setState(() {
                    DateFormat dateFormat =
                        DateFormat("yyyy-MM-ddTHH:mm:ss.SSS");
                    var d = dateFormat.format(picked.toUtc());
                    if (widget.onChanged != null) {
                      widget.onChanged!(d.isNotEmpty == true ? d : null, true);
                    }
                    currentValue = DateExtension.formatDate(
                        stringDate: d, format: widget.displayDateFormat);
                    textEditingController.text = DateExtension.formatDate(
                        stringDate: d, format: widget.displayDateFormat);
                  });
                }),
          ),
        ),
      );
}
