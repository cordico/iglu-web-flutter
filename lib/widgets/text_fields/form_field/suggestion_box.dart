/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:flutter/material.dart';

class SuggestionsBox {
  static const int waitMetricsTimeoutMillis = 1000;
  static const double minOverlaySpace = 53.0;
  static const double maxHFinal = 400;

  final BuildContext? context;
  final FormTextFormFieldState? state;
  AxisDirection desiredDirection;
  bool autoFlipDirection;

  AxisDirection? direction;
  bool isOpened = false;
  bool widgetMounted = true;
  bool isMenuOptionsLoading = false;

  double maxHeight = 400.0;
  double textBoxWidth = 100.0;
  double textBoxHeight = 100.0;
  double? directionUpOffset;

  BoxConstraints? internalConstraints;

  Widget? customWidget;

  List<String> currentOptions = [];

  var calculateGenerateMenuOptionAlreadyDone = false;
  void Function(void Function())? internalStatefulBuilder;

  SuggestionsBox(
      {this.context,
      this.desiredDirection = AxisDirection.down,
      this.autoFlipDirection = true,
      this.customWidget,
      this.state});

  //MARK: VISIBILITY

  void open() async {
    if (this.isOpened) return;
    assert(state != null || state!.widget.formContainerState != null);
    _calculateGenerateMenuOption();
    if (state!.widget.isMultipleSelection) {
      currentOptions = state!.textEditingController.text.split(" - ");
    }
    if (state!.widget.formContainerState!.containerSize != null) {
      var size = state!.widget.formContainerState!.containerSize!;
      internalConstraints =
          BoxConstraints(maxHeight: size.height, maxWidth: size.width);
    }
    resize();
    this.isOpened = true;
  }

  void close() {
    if (!this.isOpened) return;
    assert(state != null || state!.widget.formContainerState != null);
    state!.widget.formContainerState!.removeOverlay();
    internalStatefulBuilder = null;
    this.isOpened = false;
  }

  void toggle() {
    if (this.isOpened) {
      this.close();
    } else {
      this.open();
    }
  }

  rebuild({bool force = false}) {
    if (internalStatefulBuilder != null && !force) {
      internalStatefulBuilder!(() {});
    } else {
      state!.widget.formContainerState!.addOverlay(overlay: _overlay());
    }
  }

  //MARK: METRICS

  MediaQuery? _findRootMediaQuery() {
    MediaQuery? rootMediaQuery;
    context!.visitAncestorElements((element) {
      if (element.widget is MediaQuery) {
        rootMediaQuery = element.widget as MediaQuery;
      }
      return true;
    });

    return rootMediaQuery;
  }

  /// Delays until the keyboard has toggled or the orientation has fully changed
  Future<bool> _waitChangeMetrics() async {
    if (widgetMounted) {
      // initial viewInsets which are before the keyboard is toggled
      EdgeInsets initial = MediaQuery.of(context!).viewInsets;
      // initial MediaQuery for orientation change
      MediaQuery? initialRootMediaQuery = _findRootMediaQuery();

      int timer = 0;
      // viewInsets or MediaQuery have changed once keyboard has toggled or orientation has changed
      while (widgetMounted && timer < waitMetricsTimeoutMillis) {
        await Future.delayed(const Duration(milliseconds: 170));
        timer += 170;

        if (widgetMounted &&
            (MediaQuery.of(context!).viewInsets != initial ||
                _findRootMediaQuery() != initialRootMediaQuery)) {
          return true;
        }
      }
    }

    return false;
  }

  void resize() {
    // check to see if widget is still mounted
    // user may have closed the widget with the keyboard still open
    if (widgetMounted) {
      _adjustMaxHeightAndOrientation();
      rebuild(force: true);
    }
  }

  // See if there's enough room in the desired direction for the overlay to display
  // correctly. If not, try the opposite direction if things look more roomy there
  void _adjustMaxHeightAndOrientation() {
    RenderBox box = context!.findRenderObject() as RenderBox;
    textBoxWidth = box.size.width;
    textBoxHeight = box.size.height;

    // top of text box
    double textBoxAbsY = box.localToGlobal(Offset.zero).dy;

    // height of window
    double windowHeight = internalConstraints != null
        ? internalConstraints!.maxHeight
        : MediaQuery.of(context!).size.height;

    textBoxAbsY = textBoxAbsY -
        (state!.widget.formContainerState!.widget.containerKey
                ?.globalPaintBounds?.top ??
            0);

    // we need to find the root MediaQuery for the unsafe area height
    // we cannot use BuildContext.ancestorWidgetOfExactType because
    // widgets like SafeArea creates a new MediaQuery with the padding removed
    MediaQuery rootMediaQuery = _findRootMediaQuery()!;

    // height of keyboard
    double keyboardHeight = rootMediaQuery.data.viewInsets.bottom;

    double maxHDesired = _calculateMaxHeight(
        desiredDirection,
        box,
        state!.widget,
        windowHeight,
        rootMediaQuery,
        keyboardHeight,
        textBoxAbsY);
    // if there's enough room in the desired direction, update the direction and the max height
    if (maxHDesired >= minOverlaySpace || !autoFlipDirection) {
      direction = desiredDirection;
      maxHeight = maxHDesired;
    } else {
      // There's not enough room in the desired direction so see how much room is in the opposite direction
      AxisDirection flipped = flipAxisDirection(desiredDirection);
      double maxHFlipped = _calculateMaxHeight(flipped, box, state!.widget,
          windowHeight, rootMediaQuery, keyboardHeight, textBoxAbsY);
      // if there's more room in this opposite direction, update the direction and maxHeight
      if (maxHFlipped > maxHDesired) {
        direction = flipped;
        maxHeight = maxHFlipped;
      }
    }

    if (maxHeight < 0) maxHeight = 0;
    if (maxHeight > maxHFinal) maxHeight = maxHFinal;
  }

  double _calculateMaxHeight(
      AxisDirection direction,
      RenderBox box,
      FormTextFormField widget,
      double windowHeight,
      MediaQuery rootMediaQuery,
      double keyboardHeight,
      double textBoxAbsY) {
    return direction == AxisDirection.down
        ? _calculateMaxHeightDown(box, widget, windowHeight, rootMediaQuery,
            keyboardHeight, textBoxAbsY)
        : _calculateMaxHeightUp(box, widget, windowHeight, rootMediaQuery,
            keyboardHeight, textBoxAbsY);
  }

  double _calculateMaxHeightDown(
      RenderBox box,
      FormTextFormField widget,
      double windowHeight,
      MediaQuery rootMediaQuery,
      double keyboardHeight,
      double textBoxAbsY) {
    // unsafe area, ie: iPhone X 'home button'
    // keyboardHeight includes unsafeAreaHeight, if keyboard is showing, set to 0
    double unsafeAreaHeight =
        keyboardHeight == 0 ? rootMediaQuery.data.padding.bottom : 0;
    return windowHeight -
        keyboardHeight -
        unsafeAreaHeight -
        textBoxHeight -
        textBoxAbsY -
        2 * widget.suggestionsBoxVerticalOffset;
  }

  double _calculateMaxHeightUp(
      RenderBox box,
      FormTextFormField widget,
      double windowHeight,
      MediaQuery rootMediaQuery,
      double keyboardHeight,
      double textBoxAbsY) {
    // recalculate keyboard absolute y value
    double keyboardAbsY = windowHeight - keyboardHeight;

    directionUpOffset = textBoxAbsY > keyboardAbsY
        ? keyboardAbsY - textBoxAbsY - widget.suggestionsBoxVerticalOffset
        : -widget.suggestionsBoxVerticalOffset;

    // unsafe area, ie: iPhone X notch
    double unsafeAreaHeight = rootMediaQuery.data.padding.top;

    return textBoxAbsY > keyboardAbsY
        ? keyboardAbsY -
            unsafeAreaHeight -
            2 * widget.suggestionsBoxVerticalOffset
        : textBoxAbsY -
            unsafeAreaHeight -
            2 * widget.suggestionsBoxVerticalOffset;
  }

  Future<void> onChangeMetrics() async {
    if (await _waitChangeMetrics()) {
      resize();
    }
  }

  //MARK: OPTIONS

  String get currentTextToUse {
    if (state?.textEditingController == null ||
        state?.textEditingController.text == null) {
      return "";
    }
    if (state!.widget.isMultipleSelection) {
      return state!.textEditingController.text.split((" - ")).last;
    } else {
      return state!.textEditingController.text;
    }
  }

  calculateOptionsArray() {
    currentOptions = state!.textEditingController.text.split(" - ");
  }

  setText(String newValue) {
    state!.initMenuOption.value = newValue;
    state!.currentValue = newValue;
    if (state!.widget.isMultipleSelection) {
      calculateOptionsArray();
      currentOptions.removeWhere((element) => element.isEmpty);
      if (currentOptions.contains(newValue)) {
        currentOptions.remove(newValue);
      } else {
        currentOptions.add(newValue);
      }
      state!.textEditingController.text = currentOptions.join(" - ");
    } else {
      state!.textEditingController.text = newValue;
    }
    state!.filteredMenuOptions = state!.originalMenuOptions;
  }

  optionCompleteAction(String save, {String? display}) {
    setText(display ?? save);
    if (state!.widget.onChanged != null) {
      if (state!.widget.onChanged != null) {
        state!.widget.onChanged!(save.isNotEmpty == true ? save : null, true);
      }
    }
    if (!state!.widget.isMultipleSelection) {
      state!.focusNode.unfocus();
      close();
    }
  }

  _calculateGenerateMenuOption() async {
    if (state!.widget.generateMenuOption != null &&
        state!.originalMenuOptions!.isEmpty &&
        !isMenuOptionsLoading &&
        !calculateGenerateMenuOptionAlreadyDone) {
      isMenuOptionsLoading = true;
      calculateGenerateMenuOptionAlreadyDone = true;
      rebuild();
      state!.originalMenuOptions = await state!.widget.generateMenuOption!();
      isMenuOptionsLoading = false;
      state!.filteredMenuOptions = state!.originalMenuOptions;
      state!.refresh(() {
        if (state!.widget.formFieldValidator != null) {
          state!.formFieldValidator = state!.widget.formFieldValidator!
              .copyWith(
                  type: IGFormFieldValidatorType.array,
                  inArray:
                      state!.filteredMenuOptions!.map((e) => e.value).toList());
        } else {
          state!.formFieldValidator = IGFormFieldValidator(
              type: IGFormFieldValidatorType.array,
              inArray:
                  state!.filteredMenuOptions!.map((e) => e.value).toList());
        }
      });
      rebuild(force: true);
    }
  }

  Widget _overlay() {
    double currentMaxHeight =
        direction == AxisDirection.down ? maxHeight - 20 : maxHeight - 20;
    if (currentMaxHeight > state!.filteredMenuOptions!.length * 53 &&
        !state!.widget.showDatePicker) {
      currentMaxHeight = state!.filteredMenuOptions!.length * 53.0;
    }
    if (currentMaxHeight <= 52) currentMaxHeight = 52;

    var chi = StatefulBuilder(builder: (context, stateful) {
      internalStatefulBuilder = stateful;
      return FocusTrapArea(
        focusNode: state!.focusNode,
        child: Container(
          width: textBoxWidth,
          constraints: BoxConstraints(maxHeight: currentMaxHeight),
          decoration: BoxDecoration(
              color: IGColors.secondaryBackground(),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(kButtonPrimaryCornerRadius),
                  bottomRight: Radius.circular(kButtonPrimaryCornerRadius)),
              boxShadow: [
                BoxShadow(blurRadius: 1, color: IGColors.text(opacity: 0.12))
              ]),
          child: customWidget ??
              Scrollbar(
                  child: isMenuOptionsLoading
                      ? Container(
                          child: Center(
                              child: Container(
                                  width: 20,
                                  height: 20,
                                  child: WidgetsDefaults
                                      .defaultCirculaProgressIndicator())))
                      : state!.filteredMenuOptions!.isEmpty
                          ? IGButtonRow(
                              title: state!.widget
                                          .suggestionBoxNoResultPlaceholder !=
                                      null
                                  ? state!.widget
                                          .suggestionBoxNoResultPlaceholder!(
                                      state!.textEditingController.text)
                                  : "No results",
                              height: 36,
                              titleAlign: TextAlign.center,
                            )
                          : state!.widget.suggestionBoxStyle ==
                                  SuggestionBoxStyle.wrap
                              ? SingleChildScrollView(
                                  padding: EdgeInsets.all(10),
                                  child: StatefulBuilder(
                                      builder: (context, stateful) {
                                    internalStatefulBuilder = stateful;
                                    return Wrap(
                                      alignment: state!
                                          .widget.suggestionBoxWrapAlignment,
                                      spacing: 10,
                                      runSpacing: 10,
                                      clipBehavior: Clip.hardEdge,
                                      children:
                                          state!.filteredMenuOptions!.map((e) {
                                        if (state!.widget
                                                .suggestionBoxWidgetAtIndex !=
                                            null) {
                                          return InkWell(
                                            onTap: () {
                                              optionCompleteAction(e.key,
                                                  display: e.value);
                                              stateful(() {});
                                              state!
                                                  .moveTextControllerCursorToLast();
                                            },
                                            child: state!.widget
                                                    .suggestionBoxWidgetAtIndex!(
                                                e, state),
                                          );
                                        } else {
                                          return IGView(
                                            border: Border.all(
                                                color: IGColors.separator()),
                                            padding: EdgeInsets.all(8),
                                            color:
                                                IGViewStyleExtension.sidebar()
                                                    .color,
                                            selected: state!.widget
                                                    .isMultipleSelection &&
                                                currentOptions
                                                    .contains("${e.value}"),
                                            child: (states, context) {
                                              return Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  DynamicTextHighlighting(
                                                    text: e.displayValue ??
                                                        e.value,
                                                    textAlign: TextAlign.center,
                                                    highlights: state!.widget
                                                            .isMultipleSelection
                                                        ? []
                                                        : currentTextToUse
                                                                .isNotEmpty
                                                            ? [currentTextToUse]
                                                            : [],
                                                    color: Colors.yellow,
                                                    style: IGTextStyles
                                                        .mediumStyle(14,
                                                            IGColors.text()),
                                                    caseSensitive: false,
                                                  ),
                                                ],
                                              );
                                            },
                                            width: (textBoxWidth - 41) / 3.0,
                                            onTap: () async {
                                              optionCompleteAction(e.key,
                                                  display: e.value);
                                              stateful(() {});
                                              state!
                                                  .moveTextControllerCursorToLast();
                                            },
                                          );
                                        }
                                      }).toList(),
                                    );
                                  }),
                                )
                              : ListView.separated(
                                  itemBuilder: (context, index) {
                                    var element =
                                        state!.filteredMenuOptions![index];
                                    return state!.widget
                                                .suggestionBoxWidgetAtIndex !=
                                            null
                                        ? state!.widget
                                                .suggestionBoxWidgetAtIndex!(
                                            element, state)
                                        : IGButtonRow(
                                            titleWidget: (states) {
                                              return DynamicTextHighlighting(
                                                text: element.value,
                                                highlights:
                                                    currentTextToUse.isNotEmpty
                                                        ? [currentTextToUse]
                                                        : [],
                                                color: Colors.yellow,
                                                style: IGTextStyles.mediumStyle(
                                                    14, IGColors.text()),
                                                caseSensitive: false,
                                              );
                                            },
                                            height: 36,
                                            onTap: () async {
                                              stateful(() {});
                                              optionCompleteAction(element.key,
                                                  display: element.value);
                                            },
                                          );
                                  },
                                  separatorBuilder: (context, index) {
                                    return Divider(
                                        thickness: 1,
                                        height: 1,
                                        endIndent: 0,
                                        indent: 16,
                                        color: IGColors.separator());
                                  },
                                  itemCount: state!.filteredMenuOptions!.length,
                                  shrinkWrap: true)),
        ),
      );
    });
    return CompositedTransformFollower(
      link: state!.layerLink,
      showWhenUnlinked: false,
      targetAnchor: direction == AxisDirection.down
          ? Alignment.bottomLeft
          : Alignment.topLeft,
      child: direction == AxisDirection.down
          ? chi
          : FractionalTranslation(
              translation: Offset(0.0, -1.0),
              child: chi,
            ),
    );
  }
}
