/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:flutter/material.dart';

extension FormTextFormFieldStyle on FormTextFormField {
  static Theme datePickerTheme(
      {required BuildContext context, required Widget? child}) {
    if (webConfigurator.currentBrightness == Brightness.dark) {
      return Theme(
        data: ThemeData.dark().copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          primaryColor: IGColors.primary(forceWhiteInDarkMode: true),
          colorScheme: ColorScheme.dark(
            primary: IGColors.primary(forceWhiteInDarkMode: true),
            onPrimary: IGColors.primaryBackground(),
            surface: IGColors.primaryBackground(),
            onSurface: IGColors.text(),
          ),
          buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.accent),
          dialogBackgroundColor: IGColors.primaryBackground(),
        ),
        child: child!,
      );
    } else {
      return Theme(
        data: ThemeData.light().copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          primaryColor: IGColors.primary(),
          colorScheme: ColorScheme.light(
            primary: IGColors.primary(),
            onPrimary: IGColors.primaryBackground(),
            surface: IGColors.primaryBackground(),
            onSurface: IGColors.text(),
          ),
          buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.accent),
          dialogBackgroundColor: IGColors.primaryBackground(),
        ),
        child: child!,
      );
    }
  }
}
