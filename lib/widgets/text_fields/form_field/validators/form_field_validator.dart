/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
///

import 'dart:convert';

import 'package:iglu_web_flutter/iglu_web_flutter.dart';
import 'package:string_validator/string_validator.dart';

enum IGFormFieldValidatorType {
  range,
  rangeNumeric,
  array,
  number,
  date,
  email,
  url,
  json,
  none
}

class IGFormFieldAdditionalValidator {
  String? message;
  bool Function(String?)? validate;
  IGFormFieldAdditionalValidator({this.message, this.validate});
}

class IGFormFieldValidator {
  dynamic customValueToValidate;
  dynamic Function()? calculateCustomValueToValidate;
  bool Function(dynamic)? customNullableValidation;
  String? message;
  bool isOptional;
  bool showRequirementText;
  bool allowZero;
  int min;
  int max;
  int valuePrecision;
  bool isMultipleOutput;
  String multipleOutputSeparator;
  String? urlContains;
  List<String> inArray;
  List<IGFormFieldAdditionalValidator>? additionalValidators;
  IGFormFieldValidatorType type;
  IGFormFieldValidator(
      {this.calculateCustomValueToValidate,
      this.message,
      this.isOptional = false,
      this.allowZero = false,
      this.isMultipleOutput = false,
      this.showRequirementText = true,
      this.multipleOutputSeparator = " - ",
      this.inArray = const [],
      this.max = 10000,
      this.min = 1,
      this.valuePrecision = 2,
      this.type = IGFormFieldValidatorType.none,
      this.additionalValidators,
      this.urlContains,
      this.customNullableValidation});

  IGFormFieldValidator copyWith({
    dynamic customValueToValidate,
    dynamic Function()? calculateCustomValueToValidate,
    bool Function(dynamic)? customNullableValidation,
    String? message,
    bool? isOptional,
    bool? allowZero,
    int? min,
    int? max,
    int? valuePrecision,
    bool? isMultipleOutput,
    String? multipleOutputSeparator,
    String? urlContains,
    List<String>? inArray,
    List<IGFormFieldAdditionalValidator>? additionalValidators,
    IGFormFieldValidatorType? type,
  }) {
    return IGFormFieldValidator(
      calculateCustomValueToValidate:
          calculateCustomValueToValidate ?? this.calculateCustomValueToValidate,
      message: message ?? this.message,
      isOptional: isOptional ?? this.isOptional,
      allowZero: allowZero ?? this.allowZero,
      isMultipleOutput: isMultipleOutput ?? this.isMultipleOutput,
      multipleOutputSeparator:
          multipleOutputSeparator ?? this.multipleOutputSeparator,
      inArray: inArray ?? this.inArray,
      max: max ?? this.max,
      min: min ?? this.min,
      valuePrecision: valuePrecision ?? this.valuePrecision,
      type: type ?? this.type,
      additionalValidators: additionalValidators ?? this.additionalValidators,
      urlContains: urlContains ?? this.urlContains,
      customNullableValidation:
          customNullableValidation ?? this.customNullableValidation,
    );
  }

  String? validate(String? value) {
    if (calculateCustomValueToValidate != null) {
      customValueToValidate = calculateCustomValueToValidate!();
    }
    if (isOptional && _valueIsEmpty(value)) {
      return null;
    } else {
      if (!_validateType(customValueToValidate ?? value)) {
        return message ?? _validateTypeMessage(customValueToValidate ?? value);
      }
      if (additionalValidators != null) {
        String? result;
        for (var item in additionalValidators!) {
          result = item.validate!(customValueToValidate ?? value)
              ? null
              : item.message;
        }
        return result;
      }
      return null;
    }
  }

  bool _checkCustomValidation(dynamic value) {
    return customNullableValidation == null ||
        (customNullableValidation != null && customNullableValidation!(value));
  }

  bool _valueIsEmpty(String? value) {
    if (customValueToValidate != null) {
      return customValueToValidate == null &&
          _checkCustomValidation(customValueToValidate);
    } else {
      return (value == null || value.isEmpty) &&
          _checkCustomValidation(customValueToValidate);
    }
  }

  bool _validateType(String value) {
    if (_valueIsEmpty(value)) {
      return false;
    }
    if (type == IGFormFieldValidatorType.range) {
      return isLength(value, min, max);
    } else if (type == IGFormFieldValidatorType.rangeNumeric) {
      if (isNumeric(value)) {
        var n = num.parse(value);
        if (!allowZero && n <= 0) {
          return false;
        }
        if (n < min || n > max) {
          return false;
        }
        return true;
      }
      return false;
    } else if (type == IGFormFieldValidatorType.array) {
      var result = true;
      if (isMultipleOutput) {
        value.split(multipleOutputSeparator).forEach((element) {
          if (result && element.trim().isNotEmpty) {
            result = isIn(element.trim(), inArray);
          }
        });
      } else {
        result = isIn(value, inArray);
      }
      return result;
    } else if (type == IGFormFieldValidatorType.date) {
      return isDate(value);
    } else if (type == IGFormFieldValidatorType.email) {
      return isEmail(value);
    } else if (type == IGFormFieldValidatorType.url) {
      return isURL(value, {
            "protocols": ["http", "https"],
            "require_tld": true,
            "require_protocol": true,
            "allow_underscores": false
          }) &&
          (urlContains != null ? value.contains(urlContains!) : true);
    } else if (type == IGFormFieldValidatorType.number) {
      var d = value.numberValue(precision: valuePrecision);
      if (!allowZero && d <= 0) {
        return false;
      }
    } else if (type == IGFormFieldValidatorType.json) {
      try {
        jsonDecode(value);
      } catch (e) {
        return false;
      }
    }
    return true;
  }

  String _validateTypeMessage(String? value) {
    if (type == IGFormFieldValidatorType.range) {
      return "The value must be between $min and $max characters";
    } else if (type == IGFormFieldValidatorType.rangeNumeric) {
      return "The value must be between $min and $max";
    } else if (type == IGFormFieldValidatorType.email) {
      return "The value must be a valid E-Mail (test@email.com)";
    } else if (type == IGFormFieldValidatorType.url) {
      return "The value must be a valid URL (https://www.example.com)";
    } else if (type == IGFormFieldValidatorType.date) {
      return "The value must be a valid date";
    } else if (type == IGFormFieldValidatorType.array) {
      return "Please enter a valid value from the possible choices";
    } else if (type == IGFormFieldValidatorType.number) {
      return "Please enter a valid numeric value";
    } else if (type == IGFormFieldValidatorType.json) {
      return "Please enter a valid json string";
    }
    return "Please enter a valid value";
  }
}
