/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:convert";
// ignore: avoid_web_libraries_in_flutter
import "dart:html";
import 'package:collection/collection.dart' show IterableExtension;
import "package:csv/csv.dart";
import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class ExportFormSheet<T> extends StatefulWidget {
  final List<HeaderRule<T>> headerSortRules;
  final List<T>? elements;
  final String? searchQuery;
  final String? title;
  final int selectedTabIndex;
  ExportFormSheet(
      {required this.headerSortRules,
      required this.elements,
      required this.selectedTabIndex,
      this.searchQuery,
      this.title});

  @override
  _ExportFormSheetState<T> createState() => _ExportFormSheetState<T>();
}

class _ExportFormSheetState<T> extends State<ExportFormSheet<T>> {
  List<HeaderRule<T>> selectedHeaderSortRules = [];
  final formSheetKey = GlobalKey<FormState>();
  var isLoading = false;
  var isExportEnabled = false;

  final GlobalKey personalizedContainerKey = GlobalKey();

  var originalValue = Map<String, bool>();

  @override
  void initState() {
    super.initState();
    widget.headerSortRules.forEach((element) {
      originalValue[element.headerKey] = element.isErasable;
    });
    selectedHeaderSortRules = List.from(widget.headerSortRules, growable: true);
    selectedHeaderSortRules.forEach((element) {
      element.isErasable = true;
    });
    isExportEnabled = selectedHeaderSortRules.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (_, constraints) {
      return FormSheet(
          formKey: formSheetKey,
          horizontalMargin: constraints.maxWidth / 1.5,
          title: "Export ${widget.title ?? ""}",
          absorbing: isLoading,
          alignment: Alignment.topCenter,
          externalBackgroundColor: Colors.transparent,
          externalMargin: EdgeInsets.fromLTRB(20, 75, 20, 20),
          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)],
          onClose: () {
            originalValue.forEach((key, value) {
              HeaderRule<T>? item = widget.headerSortRules
                  .firstWhereOrNull((element) => element.headerKey == key);
              if (item != null) {
                item.isErasable = value;
              }
            });
          },
          cellRules: (constraints, state) {
            return [
              DetailViewCellRulePreBuild.spaceHeight10(),
              if (_showFilterText())
                DetailViewCellRule(
                    type: DetailViewCellRuleType.multiple_horizontal,
                    alignment: MainAxisAlignment.start,
                    contentPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    subElements: [
                      DetailViewCellRule(
                          type: DetailViewCellRuleType.icon,
                          contentPadding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          iconIconType: Icon(Icons.info_rounded,
                              size: 24, color: IGColors.text())),
                      DetailViewCellRule(
                          type: DetailViewCellRuleType.text,
                          content: _generateFilteringText(),
                          textStyle:
                              IGTextStyles.normalStyle(15, IGColors.text())),
                    ]),
              if (_showFilterText()) DetailViewCellRulePreBuild.spaceHeight20(),
              DetailViewCellRule(
                  type: DetailViewCellRuleType.text,
                  content: "Columns",
                  contentPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  textStyle: IGTextStyles.semiboldStyle(15, IGColors.text())),
              DetailViewCellRule(
                type: DetailViewCellRuleType.custom,
                alignment: MainAxisAlignment.start,
                contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 2),
                customWidget: IGButtonViewDefaults.expandedLargeButton(
                    context: context,
                    style: IGButtonViewStyle(
                      text: "Customize (${selectedHeaderSortRules.length})",
                      onTapPositioned: (position) async {
                        await showMenuCustom(
                            items: _calculateAllColumsPopupMenuButton(),
                            color: IGColors.secondaryBackground(),
                            position: position,
                            context: context,
                            elevation: 1.5,
                            preferredWidth: 224,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                                    kButtonPrimaryCornerRadius)));
                      },
                    )),
              ),
              DetailViewCellRulePreBuild.spaceHeightCustom(8),
              DetailViewCellRule(
                  type: DetailViewCellRuleType.text,
                  constraints: BoxConstraints(
                      maxWidth: constraints.maxWidth - 40,
                      minWidth: constraints.maxWidth - 40),
                  content: _generateColumText(),
                  contentPadding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  textStyle: IGTextStyles.normalStyle(15, IGColors.text())),
              DetailViewCellRulePreBuild.spaceHeight20(),
              DetailViewCellRule(
                  type: DetailViewCellRuleType.multiple_horizontal,
                  alignment: MainAxisAlignment.end,
                  forceNoScroll: true,
                  subElements: [
                    DetailViewCellRule(
                      type: DetailViewCellRuleType.custom,
                      alignment: MainAxisAlignment.end,
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      customWidget: IGButtonViewDefaults.primaryDefaultButton(
                          context: context,
                          style: IGButtonViewStyle(
                              text: "Export",
                              icon: Icons.save_alt_rounded,
                              showLoadingDuringAction: true,
                              automaticallyResize: false,
                              forceCircleDuringLoading: true,
                              forceLoading: isLoading,
                              onTapPositioned: isExportEnabled
                                  ? (_) async {
                                      await _export();
                                    }
                                  : null)),
                    ),
                  ]),
              DetailViewCellRulePreBuild.spaceHeight20(),
            ];
          });
    });
  }

  _sortVisibleHeaders() {
    selectedHeaderSortRules.sort((a, b) {
      return widget.headerSortRules
          .indexOf(a)
          .compareTo(widget.headerSortRules.indexOf(b));
    });
  }

  _calculateAllColumsPopupMenuButton() {
    var finalTemp = List<PopupMenuEntry>.from([], growable: true);
    finalTemp.add(PopupMenuCustomItem(
        child: IGButtonRow(title: "Columns to export", height: 36),
        enabled: false));
    finalTemp.add(PopupMenuCustomItem(
        height: 1,
        child: Divider(
            thickness: 1,
            height: 1,
            endIndent: 0,
            indent: 0,
            color: IGColors.separator()),
        enabled: false));
    widget.headerSortRules.asMap().forEach((index, element) {
      finalTemp.add(PopupMenuCustomItem(
        enabled: false,
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          return IGButtonRow(
              height: 36,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              title: element.displayValue ?? element.headerKey.titleCase,
              onTap: !element.isErasable
                  ? null
                  : () async {
                      if (!element.isErasable) {
                        return;
                      } else {
                        if (selectedHeaderSortRules.contains(element)) {
                          selectedHeaderSortRules.remove(element);
                        } else {
                          selectedHeaderSortRules.add(element);
                        }
                        isExportEnabled = selectedHeaderSortRules.isNotEmpty;
                        _sortVisibleHeaders();
                        _set(() {});
                        setState(() {});
                      }
                    },
              trailing: (states) {
                return AbsorbPointer(
                  absorbing: true,
                  child: Checkbox(
                      hoverColor: Colors.transparent,
                      activeColor: IGColors.primary(),
                      value: selectedHeaderSortRules.contains(element),
                      onChanged: (_) {}),
                );
              });
        }),
      ));
      if (index != widget.headerSortRules.length - 1) {
        finalTemp.add(PopupMenuCustomItem(
            height: 1,
            child: Divider(
                thickness: 1,
                height: 1,
                endIndent: 8,
                indent: 8,
                color: IGColors.separator()),
            enabled: false));
      }
    });
    return finalTemp;
  }

  _export() async {
    setState(() {
      isLoading = true;
    });
    List<List<dynamic>> rows = [];
    List<dynamic> firstRow = [];
    List<List<dynamic>> others = [];
    selectedHeaderSortRules.forEach((element) {
      firstRow.add(element.displayValue);
    });
    rows.add(firstRow);
    for (var index = 0; index < widget.elements!.length; index++) {
      var element = widget.elements![index];
      others.add([]);
      for (var header in selectedHeaderSortRules) {
        dynamic v;
        if (header.headerValue != null) {
          v = header.headerValue!(element, header);
        }
        var result = "";
        if (header.cellDisplayRule!.elaborateValue != null) {
          result =
              "${await header.cellDisplayRule!.elaborateValue!(v, header.cellDisplayRule, null, header) ?? ""}";
        } else {
          result = "${v ?? ""}";
        }
        others[index].add(result);
      }
    }
    rows.addAll(others);
    String csv = const ListToCsvConverter().convert(rows);
    final bytes = utf8.encode(csv);
    final blob = Blob([bytes]);
    final url = Url.createObjectUrlFromBlob(blob);
    new AnchorElement(href: url)
      ..setAttribute("download",
          "${webConfigurator.internalConfigurator.appName!.toLowerCase().replaceAll(" ", "_")}_${widget.title!.toLowerCase().replaceAll(" ", "_")}.csv")
      ..click();
    setState(() {
      isLoading = false;
    });
    Navigator.of(context).pop();
  }

  _showFilterText() {
    return (widget.searchQuery != null && widget.searchQuery!.isNotEmpty) ||
        (widget.headerSortRules.firstWhereOrNull((element) =>
                element.isFilterApplied(widget.selectedTabIndex)) !=
            null);
  }

  _generateColumText() {
    var result = "";
    if (selectedHeaderSortRules.isNotEmpty) {
      selectedHeaderSortRules.forEach((element) {
        result += "${element.displayValue}, ";
      });
      result = result.substring(0, result.length - 2);
    }
    return result;
  }

  _generateFilteringText() {
    var result = "This export is applied";
    if (widget.searchQuery != null && widget.searchQuery!.isNotEmpty) {
      result += " the search filter ${widget.searchQuery}";
    }
    Iterable<HeaderRule<T>> headerFiltering = widget.headerSortRules
        .where((element) => element.isFilterApplied(widget.selectedTabIndex));
    if (headerFiltering.length > 0) {
      if (headerFiltering.length == 1) {
        if (widget.searchQuery != null && widget.searchQuery!.isNotEmpty) {
          result += " and the filter";
        } else {
          result += " the filter";
        }
      } else {
        if (widget.searchQuery != null && widget.searchQuery!.isNotEmpty) {
          result += " and filters";
        } else {
          result += " the filters";
        }
      }
      headerFiltering.forEach((element) {
        result += " ${element.displayValue},";
      });
      result = result.substring(0, result.length - 1);
    }
    return result;
  }
}
