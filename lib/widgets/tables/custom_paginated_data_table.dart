/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'dart:math' as math;
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart' show DragStartBehavior;
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

/// A material design data table that shows data using multiple pages.
///
/// A paginated data table shows [rowsPerPage] rows of data per page and
/// provides controls for showing other pages.
///
/// Data is read lazily from from a [DataTableSource]. The widget is presented
/// as a [Card].
///
/// See also:
///
///  * [DataTable], which is not paginated.
///  * <https://material.io/go/design-data-tables#data-tables-tables-within-cards>
class CustomPaginatedDataTable extends StatefulWidget {
  /// Creates a widget describing a paginated [DataTable] on a [Card].
  ///
  /// The [header] should give the card's header, typically a [Text] widget. It
  /// must not be null.
  ///
  /// The [columns] argument must be a list of as many [DataColumn] objects as
  /// the table is to have columns, ignoring the leading checkbox column if any.
  /// The [columns] argument must have a length greater than zero and cannot be
  /// null.
  ///
  /// If the table is sorted, the column that provides the current primary key
  /// should be specified by index in [sortColumnIndex], 0 meaning the first
  /// column in [columns], 1 being the next one, and so forth.
  ///
  /// The actual sort order can be specified using [sortAscending]; if the sort
  /// order is ascending, this should be true (the default), otherwise it should
  /// be false.
  ///
  /// The [source] must not be null. The [source] should be a long-lived
  /// [DataTableSource]. The same source should be provided each time a
  /// particular [CustomPaginatedDataTable] widget is created; avoid creating a new
  /// [DataTableSource] with each new instance of the [CustomPaginatedDataTable]
  /// widget unless the data table really is to now show entirely different
  /// data from a new source.
  ///
  /// The [rowsPerPage] and [availableRowsPerPage] must not be null (they
  /// both have defaults, though, so don't have to be specified).
  CustomPaginatedDataTable({
    Key? key,
    required this.header,
    this.actions,
    required this.columns,
    this.sortColumnIndex,
    this.sortAscending = true,
    this.onSelectAll,
    this.dataRowHeight = kMinInteractiveDimension,
    this.headingRowHeight = 56.0,
    this.horizontalMargin = 24.0,
    this.columnSpacing = 56.0,
    this.showCheckboxColumn = true,
    this.initialFirstRowIndex = 0,
    this.onPageChanged,
    this.defaultRowsPerPage = 10,
    this.rowsPerPage = 10,
    this.availableRowsPerPage = const <int>[10, 20, 50, 100],
    this.onRowsPerPageChanged,
    this.dragStartBehavior = DragStartBehavior.start,
    this.showFooter = true,
    this.showAllButtonInsideAvailableRowsPerPage = true,
    this.emptyState,
    this.showLoading = true,
    this.loadingWidget,
    this.onTapTabBarItem,
    this.selectedIndex,
    this.tabItems,
    this.elevation,
    this.cardColor,
    this.forceSmallContext = false,
    this.showSmallContext = false,
    this.showBorder = true,
    this.borderRadius = 4,
    this.showNextPage = false,
    this.totalCount,
    this.showOnlyOnePage = false,
    this.currentPaginatedPage,
    this.isDuringSearch = false,
    required this.source,
  })  : assert(columns.isNotEmpty),
        assert(sortColumnIndex == null ||
            (sortColumnIndex >= 0 && sortColumnIndex < columns.length)),
        assert(rowsPerPage > 0),
        assert(() {
          //NOTE: REMOVED
          //if (onRowsPerPageChanged != null)
          //  assert(availableRowsPerPage != null && availableRowsPerPage.contains(rowsPerPage));
          return true;
        }()),
        super(key: key);

  /// The table card's header.
  ///
  /// This is typically a [Text] widget, but can also be a [ButtonBar] with
  /// [TextButton]s. Suitable defaults are automatically provided for the font,
  /// button color, button padding, and so forth.
  ///
  /// If items in the table are selectable, then, when the selection is not
  /// empty, the header is replaced by a count of the selected items.
  final Widget? header;

  /// Icon buttons to show at the top right of the table.
  ///
  /// Typically, the exact actions included in this list will vary based on
  /// whether any rows are selected or not.
  ///
  /// These should be size 24.0 with default padding (8.0).
  final List<Widget>? actions;

  /// The configuration and labels for the columns in the table.
  final List<DataColumn> columns;

  /// The current primary sort key's column.
  ///
  /// See [DataTable.sortColumnIndex].
  final int? sortColumnIndex;

  /// Whether the column mentioned in [sortColumnIndex], if any, is sorted
  /// in ascending order.
  ///
  /// See [DataTable.sortAscending].
  final bool sortAscending;

  /// Invoked when the user selects or unselects every row, using the
  /// checkbox in the heading row.
  ///
  /// See [DataTable.onSelectAll].
  final ValueSetter<bool?>? onSelectAll;

  /// The height of each row (excluding the row that contains column headings).
  ///
  /// This value is optional and defaults to kMinInteractiveDimension if not
  /// specified.
  final double dataRowHeight;

  /// The height of the heading row.
  ///
  /// This value is optional and defaults to 56.0 if not specified.
  final double headingRowHeight;

  /// The horizontal margin between the edges of the table and the content
  /// in the first and last cells of each row.
  ///
  /// When a checkbox is displayed, it is also the margin between the checkbox
  /// the content in the first data column.
  ///
  /// This value defaults to 24.0 to adhere to the Material Design specifications.
  final double horizontalMargin;

  /// The horizontal margin between the contents of each data column.
  ///
  /// This value defaults to 56.0 to adhere to the Material Design specifications.
  final double columnSpacing;

  /// {@macro flutter.material.dataTable.showCheckboxColumn}
  final bool showCheckboxColumn;

  /// The index of the first row to display when the widget is first created.
  final int initialFirstRowIndex;

  /// Invoked when the user switches to another page.
  ///
  /// The value is the index of the first row on the currently displayed page.
  final ValueChanged<int?>? onPageChanged;

  /// The number of rows to show on each page.
  ///
  /// See also:
  ///
  ///  * [onRowsPerPageChanged]
  ///  * [defaultRowsPerPage]
  final int rowsPerPage;

  /// The default value for [rowsPerPage].
  ///
  /// Useful when initializing the field that will hold the current
  /// [rowsPerPage], when implemented [onRowsPerPageChanged].
  final int defaultRowsPerPage;

  /// The options to offer for the rowsPerPage.
  ///
  /// The current [rowsPerPage] must be a value in this list.
  ///
  /// The values in this list should be sorted in ascending order.
  final List<int> availableRowsPerPage;

  /// Invoked when the user selects a different number of rows per page.
  ///
  /// If this is null, then the value given by [rowsPerPage] will be used
  /// and no affordance will be provided to change the value.
  final ValueChanged<int>? onRowsPerPageChanged;

  /// The data source which provides data to show in each row. Must be non-null.
  ///
  /// This object should generally have a lifetime longer than the
  /// [CustomPaginatedDataTable] widget itself; it should be reused each time the
  /// [CustomPaginatedDataTable] constructor is called.
  final DataTableViewDataSource source;

  /// {@macro flutter.widgets.scrollable.dragStartBehavior}
  final DragStartBehavior dragStartBehavior;

  /// Hide or Show Footer
  final bool showFooter;

  ///Hide or Show 'All' Button inside availableRowsPage
  final bool showAllButtonInsideAvailableRowsPerPage;

  ///
  final Widget? emptyState;

  ///
  final bool showLoading;

  ///
  final Widget? loadingWidget;

  ///
  final List<DetailViewCellTabItem>? tabItems;

  ///
  final int? selectedIndex;

  ///
  final Function(int)? onTapTabBarItem;

  ///
  final double? elevation;

  ///
  final Color? cardColor;

  ///
  final bool forceSmallContext;

  ///
  final bool showSmallContext;

  ///
  final double? borderRadius;

  ///
  final bool showBorder;

  ///
  final bool showNextPage;

  ///
  final int? totalCount;

  ///
  final bool showOnlyOnePage;

  ///
  final int? currentPaginatedPage;

  ///
  final bool? isDuringSearch;

  @override
  CustomPaginatedDataTableState createState() =>
      CustomPaginatedDataTableState();
}

/// Holds the state of a [CustomPaginatedDataTable].
///
/// The table can be programmatically paged using the [pageTo] method.
class CustomPaginatedDataTableState extends State<CustomPaginatedDataTable> {
  int? _firstRowIndex;
  int? _rowCount;
  late bool _rowCountApproximate;
  int? _selectedRowCount;
  final Map<int, DataRow?> _rows = <int, DataRow?>{};

  ScrollController dataTableScrollController = ScrollController();

  bool needScroll = false;

  @override
  void initState() {
    super.initState();
    _firstRowIndex = PageStorage.of(context)?.readState(context) as int? ??
        widget.initialFirstRowIndex;
    _handleDataSourceChanged();
  }

  setNeedScroll({bool avoidSetState = false}) {
    needScroll = dataTableScrollController.hasClients
        ? dataTableScrollController.position.maxScrollExtent != 0
        : false;
    if (mounted && !avoidSetState) {
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(CustomPaginatedDataTable oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.source != widget.source) {
      _handleDataSourceChanged();
    }
  }

  void _handleDataSourceChanged({bool avoidSetState = false}) {
    _rowCount = widget.source.rowCount;
    _rowCountApproximate = widget.source.isRowCountApproximate;
    _selectedRowCount = widget.source.selectedRowCount;
    _rows.clear();
    Timer(Duration(seconds: 1), () {
      setNeedScroll(avoidSetState: avoidSetState);
    });
  }

  /// Ensures that the given row is visible.
  void pageTo(int? rowIndex,
      {bool isNext = false, bool reset = false, bool custom = false}) {
    if (widget.showOnlyOnePage) {
      if (widget.onPageChanged != null) {
        var indx = custom ? rowIndex : widget.currentPaginatedPage;
        if (!custom) {
          indx = isNext ? indx! + 1 : indx! - 1;
        }
        if (reset) {
          indx = 0;
        }
        widget.onPageChanged!(indx);
      }
    } else {
      final int? oldFirstRowIndex = _firstRowIndex;
      setState(() {
        _firstRowIndex = (rowIndex! ~/ widget.rowsPerPage) * widget.rowsPerPage;
      });
      if ((widget.onPageChanged != null) &&
          (oldFirstRowIndex != _firstRowIndex))
        widget.onPageChanged!(_firstRowIndex);
    }
  }

  //NOTE: ADDED THIS FOR SHOW ONLY FILLED ROW
  /*DataRow _getBlankRowFor(int index) {
    return DataRow.byIndex(
      index: index,
      cells: widget.columns.map<DataCell>((DataColumn column) => DataCell.empty).toList(),
    );
  }*/

  DataRow _getProgressIndicatorRowFor(int index) {
    bool haveProgressIndicator = false;
    final List<DataCell> cells =
        widget.columns.map<DataCell>((DataColumn column) {
      if (!column.numeric) {
        haveProgressIndicator = true;
        return const DataCell(CircularProgressIndicator());
      }
      return DataCell.empty;
    }).toList();
    if (!haveProgressIndicator) {
      haveProgressIndicator = true;
      cells[0] = const DataCell(CircularProgressIndicator());
    }
    return DataRow.byIndex(
      index: index,
      cells: cells,
    );
  }

  List<DataRow> _getRows(int firstRowIndex, int rowsPerPage) {
    final List<DataRow> result = <DataRow>[];
    final int nextPageFirstRowIndex = firstRowIndex + rowsPerPage;
    bool haveProgressIndicator = false;
    for (int index = firstRowIndex; index < nextPageFirstRowIndex; index += 1) {
      DataRow? row;
      if (index < _rowCount! || _rowCountApproximate) {
        row = _rows.putIfAbsent(index, () => widget.source.getRow(index));
        if (row == null && !haveProgressIndicator) {
          row ??= _getProgressIndicatorRowFor(index);
          haveProgressIndicator = true;
        }
      }
      //NOTE: ADDED THIS FOR SHOW ONLY FILLED ROW
      //row ??= _getBlankRowFor(index);
      if (row != null) {
        result.add(row);
      } else {
        break;
      }
    }
    return result;
  }

  void _handlePrevious() {
    pageTo(math.max(_firstRowIndex! - widget.rowsPerPage, 0), isNext: false);
  }

  void _handleNext() {
    pageTo(_firstRowIndex! + widget.rowsPerPage, isNext: true);
  }

  @override
  Widget build(BuildContext context) {
    _handleDataSourceChanged(avoidSetState: true);
    // HEADER
    final List<Widget> headerWidgets = [];
    if (_selectedRowCount == 0 && !widget.showCheckboxColumn) {
      if (widget.header != null) {
        headerWidgets.add(Expanded(child: widget.header!));
      }
    } else {
      assert(debugCheckHasMaterialLocalizations(context));
      final MaterialLocalizations localizations =
          MaterialLocalizations.of(context);
      headerWidgets.add(Expanded(
        child: AutoSizeText(
          _selectedRowCount == 0
              ? "No items selected"
              : localizations.selectedRowCountTitle(_selectedRowCount!),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: IGTextStyles.normalStyle(20, IGColors.text()),
        ),
      ));
    }
    if (widget.actions != null) {
      if (widget.header != null) {
        headerWidgets.add(Container(width: 30));
      }
      headerWidgets.addAll(widget.actions!);
    }

    // FOOTER
    final List<Widget> footerWidgets = <Widget>[];
    bool previousExpr = widget.currentPaginatedPage != null
        ? widget.currentPaginatedPage! <= 1
        : _firstRowIndex! <= 0;
    Color previousArrowColor =
        previousExpr ? IGColors.separator() : IGColors.text();
    Color nextArrowColor = !widget.showNextPage &&
            (!_rowCountApproximate &&
                ((widget.currentPaginatedPage ?? _firstRowIndex)! +
                        widget.rowsPerPage >=
                    _rowCount!))
        ? IGColors.separator()
        : IGColors.text();
    footerWidgets.addAll(<Widget>[
      Row(mainAxisSize: MainAxisSize.min, children: [
        Text(
          widget.totalCount != null
              ? widget.totalCount == 1
                  ? "1 result"
                  : "${widget.totalCount} results - page ${(widget.currentPaginatedPage ?? ((_firstRowIndex ?? 0) / 25)) + 1}"
              : _rowCount == 1
                  ? "1 result"
                  : "$_rowCount results - page ${(widget.currentPaginatedPage ?? ((_firstRowIndex ?? 0) / 25)) + 1}",
          style: IGTextStyles.custom(fontSize: 13, color: IGColors.text()),
        )
      ]),
      Row(mainAxisSize: MainAxisSize.min, children: [
        if ((widget.currentPaginatedPage ?? _firstRowIndex)! > 2)
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
            child: IGButtonViewDefaults.mediumButtonIconOnlyHover(
                context: context,
                style: IGButtonViewStyle(
                    icon: Icons.skip_previous_rounded,
                    iconColor: IGColors.text(),
                    tooltipMessage: "Start",
                    onTapPositioned: (position) async {
                      pageTo(0, reset: true);
                    })),
          ),
        Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
          child: IGButtonViewDefaults.mediumButtonIconOnlyHover(
              context: context,
              style: IGButtonViewStyle(
                  icon: Icons.chevron_left_rounded,
                  iconColor: previousArrowColor,
                  tooltipMessage: "Back",
                  onTapPositioned: previousExpr
                      ? null
                      : (position) async {
                          _handlePrevious();
                        })),
        ),
        IGButtonViewDefaults.mediumButtonIconOnlyHover(
            context: context,
            style: IGButtonViewStyle(
                icon: Icons.chevron_right_rounded,
                iconColor: nextArrowColor,
                tooltipMessage: "Next",
                onTapPositioned: !widget.showNextPage &&
                        (!_rowCountApproximate &&
                            ((widget.currentPaginatedPage ?? _firstRowIndex)! +
                                    widget.rowsPerPage >=
                                _rowCount!))
                    ? null
                    : (position) async {
                        _handleNext();
                      })),
      ])
    ]);
    //END FOOTER

    // CARD
    var _rows = _getRows(_firstRowIndex!, widget.rowsPerPage);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          decoration: widget.showBorder
              ? BoxDecoration(
                  borderRadius:
                      BorderRadius.circular(kButtonPrimaryCornerRadius),
                  border: Border.all(color: IGColors.separator()))
              : null,
          child: Card(
            elevation: widget.showBorder ? 0 : widget.elevation ?? 1.5,
            color: widget.cardColor ??
                (widget.forceSmallContext
                    ? IGColors.primaryBackground()
                    : IGColors.secondaryBackground()),
            margin: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                    widget.borderRadius ?? kButtonPrimaryCornerRadius)),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                  widget.borderRadius ?? kButtonPrimaryCornerRadius),
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      left: 0,
                      width: constraints.maxWidth,
                      child: widget.showLoading
                          ? widget.loadingWidget == null
                              ? LinearProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      webConfigurator.currentBrightness ==
                                              Brightness.light
                                          ? IGColors.primary()
                                          : Colors.white),
                                  backgroundColor:
                                      webConfigurator.currentBrightness ==
                                              Brightness.light
                                          ? IGColors.primary(opacity: 0.5)
                                          : Colors.white,
                                  minHeight: 3,
                                )
                              : widget.loadingWidget!
                          : Container()),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      if (headerWidgets.isNotEmpty)
                        Container(
                          height: 80,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: headerWidgets,
                            ),
                          ),
                        ),
                      if (!(widget.tabItems != null) &&
                          headerWidgets.isNotEmpty)
                        Divider(
                            height: 1,
                            thickness: 1,
                            endIndent: 0,
                            indent: 0,
                            color: IGColors.separator()),
                      if (widget.tabItems != null)
                        Container(
                          child: DetailView(
                              showEditAction: false,
                              showHeader: false,
                              showLoading: false,
                              showBorder: false,
                              elevation: 0,
                              borderRadius: BorderRadius.zero,
                              cardColor: widget.forceSmallContext
                                  ? IGColors.primaryBackground()
                                  : null,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0)),
                              cardMargin: EdgeInsets.zero,
                              cellRules: (constraints) {
                                return [
                                  DetailViewCellRule(
                                      type: DetailViewCellRuleType
                                          .multiple_horizontal,
                                      alignment: MainAxisAlignment.start,
                                      contentPadding:
                                          EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      isScrollable: true,
                                      subElements: [
                                        DetailViewCellRule(
                                            type: DetailViewCellRuleType.tabbar,
                                            tintColor: webConfigurator
                                                        .currentBrightness ==
                                                    Brightness.light
                                                ? IGColors.primary()
                                                : Colors.white,
                                            tabItems: widget.tabItems,
                                            selectedIndex: widget.selectedIndex,
                                            onTapTabBarItem:
                                                widget.onTapTabBarItem,
                                            alignment: MainAxisAlignment.start),
                                      ]),
                                  DetailViewCellRule(
                                      type: DetailViewCellRuleType.separator,
                                      height: 0)
                                ];
                              }),
                        ),
                      if (SizeDetector.isSmallScreen(context) ||
                          widget.forceSmallContext)
                        Container(height: 8),
                      if (_rows.length > 0 ||
                          (_rows.length == 0 &&
                              !widget.showLoading &&
                              !widget.isDuringSearch!))
                        (SizeDetector.isSmallScreen(context) &&
                                    widget.showSmallContext) ||
                                widget.forceSmallContext
                            ? Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: listChildren(_rows),
                              )
                            : Scrollbar(
                                isAlwaysShown: needScroll && _rows.length > 0,
                                controller: dataTableScrollController,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0,
                                      needScroll && _rows.length > 0 ? 10 : 0),
                                  child: SingleChildScrollView(
                                    controller: dataTableScrollController,
                                    scrollDirection: Axis.horizontal,
                                    dragStartBehavior: widget.dragStartBehavior,
                                    child: ConstrainedBox(
                                      constraints: BoxConstraints(
                                          minWidth: constraints.minWidth),
                                      child: DataTable(
                                        columns: widget.columns,
                                        sortColumnIndex: widget.sortColumnIndex,
                                        sortAscending: widget.sortAscending,
                                        onSelectAll: widget.onSelectAll,
                                        dataRowHeight: widget.dataRowHeight,
                                        headingRowHeight:
                                            widget.headingRowHeight,
                                        horizontalMargin:
                                            widget.horizontalMargin,
                                        columnSpacing: widget.columnSpacing,
                                        showCheckboxColumn:
                                            widget.showCheckboxColumn,
                                        rows: _rows,
                                      ),
                                    ),
                                  ),
                                ),
                              ).fixNestedDoubleScrollbar(),
                      widget.emptyState == null || _rows.length > 0
                          ? Container()
                          : Column(
                              children: [
                                if (!(SizeDetector.isSmallScreen(context) ||
                                        widget.forceSmallContext) &&
                                    (_rows.length <= 0 && !widget.showLoading))
                                  Divider(
                                      color: IGColors.separator(),
                                      endIndent: 0,
                                      indent: 0,
                                      height: 1,
                                      thickness: 1),
                                widget.emptyState!,
                              ],
                            ),
                      widget.showFooter &&
                              ((SizeDetector.isSmallScreen(context) &&
                                      widget.showSmallContext) ||
                                  widget.forceSmallContext)
                          ? Container(height: 8)
                          : Container(),
                      widget.showFooter
                          ? Divider(
                              color: IGColors.separator(),
                              endIndent: 0,
                              indent: 0,
                              height: 1,
                              thickness: 1)
                          : Container(),
                      widget.showFooter
                          ? Container(
                              height: 56.0,
                              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: footerWidgets,
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> listChildren(List<DataRow> rows) {
    List<Widget> temp = [];
    rows.asMap().forEach((index, value) {
      var row = widget.source.rows![index + _firstRowIndex!];
      temp.add(row.smallContextWidget ?? Container());
      if (index != rows.length - 1) {
        temp.add(Container(height: 8));
        temp.add(Divider(
            color: IGColors.separator(),
            endIndent: 0,
            indent: 20,
            height: 1,
            thickness: 1.0));
        temp.add(Container(height: 8));
      }
    });
    return temp;
  }
}
