/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";

class HeaderRule<T> {
  String headerKey;
  String sortKey;
  String? displayValue;
  HeaderSortRuleType type;
  int Function(String, bool, T, T)? customValidator;
  bool isDefault;
  bool isDefaultAsceding;
  bool hiddenByDefault;
  CellDisplayRule<T>? cellDisplayRule;
  bool isErasable;
  Function(HeaderRule<T>)? adjustValues;

  Future<List<PopupMenuEntry>> Function()? helpMenuItems;
  List<PopupMenuEntry>? originalHelpMenuItems;
  final GlobalKey helpMenuContainerKey = GlobalKey();

  bool isFilterApplied(int selectedTabIndex) {
    return originaFilterlItems[selectedTabIndex]?.length !=
        visibleFilterItems[selectedTabIndex]?.length;
  }

  Future<List<FilterRule<T>>> Function()? filterItems;
  String? filterKey;
  bool showFilterIcon = true;
  Map<int, List<FilterRule<T>>> originaFilterlItems = {};
  Map<int, List<FilterRule<T>>> visibleFilterItems = {};
  final GlobalKey filtersMenuContainerKey = GlobalKey();

  String Function(T)? searchValue;
  dynamic Function(T, HeaderRule<T>)? headerValue;

  Map<String, DataCell?> dataCell = {};

  HeaderRule(this.headerKey, this.sortKey, this.type,
      {this.customValidator,
      this.displayValue,
      this.cellDisplayRule,
      this.hiddenByDefault = false,
      this.isDefault = false,
      this.isDefaultAsceding = false,
      this.showFilterIcon = true,
      this.helpMenuItems,
      this.filterKey,
      this.filterItems,
      this.adjustValues,
      this.searchValue,
      this.headerValue,
      this.isErasable = true}) {
    this.headerKey = this.headerKey.trim();
    this.sortKey = this.sortKey.trim();
    this.filterKey = this.filterKey?.trim();
  }

  Future<DataCell> calculateDataCell(
      BuildContext context, dynamic element, String elementId,
      {bool force = false}) async {
    if (dataCell[elementId] != null && !force) {
      return dataCell[elementId]!;
    }
    headerKey = headerKey.trim();
    dynamic v;
    if (headerValue != null) {
      v = headerValue!(element, this);
    }
    dataCell[elementId] = DataCell(
        await cellDisplayRule!.widget(
            context: context,
            originalValue: v ?? null,
            json: element,
            headerRule: this),
        onTap: null,
        placeholder: false,
        showEditIcon: false);
    return dataCell[elementId]!;
  }

  calculateFilters({required int selectedTabIndex}) async {
    if (originaFilterlItems[selectedTabIndex] == null && filterItems != null) {
      var elements = await filterItems!();
      originaFilterlItems[selectedTabIndex] = List.from(elements);
      visibleFilterItems[selectedTabIndex] = List.from(
          elements.where((element) => !element.hiddenByDefault).toList(),
          growable: true);
    }
  }

  showFilterMenu({
    required BuildContext context,
    required RelativeRect position,
    required int selectedTabIndex,
    Function(List<FilterRule<T>>?, List<FilterRule<T>>?)? applyFilters,
  }) async {
    if (filterItems == null) return;
    await calculateFilters(selectedTabIndex: selectedTabIndex);
    await showMenuCustom(
        items: _calculateAllFilterMenuButton(
            selectedTabIndex: selectedTabIndex,
            context: context,
            applyFilters: applyFilters),
        color: IGColors.secondaryBackground(),
        position: position,
        context: context,
        elevation: 1.5,
        preferredWidth: 280,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonPrimaryCornerRadius)));
  }

  _calculateAllFilterMenuButton(
      {BuildContext? context,
      required int selectedTabIndex,
      Function(List<FilterRule<T>>?, List<FilterRule<T>>?)? applyFilters}) {
    if (originaFilterlItems[selectedTabIndex] == null) {
      return;
    }
    var setters = <StateSetter>[];
    var finalTemp = List<PopupMenuEntry>.from([], growable: true);
    finalTemp.add(PopupMenuCustomItem(
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          setters.add(_set);
          bool? allValue = visibleFilterItems[selectedTabIndex]?.length ==
              originaFilterlItems[selectedTabIndex]?.length;
          if (visibleFilterItems[selectedTabIndex]?.isEmpty == true) {
            allValue = false;
          }
          if (!allValue &&
              (visibleFilterItems[selectedTabIndex]?.length ?? 0) > 0) {
            allValue = null;
          }
          return IGButtonRow(
              title: "Filters",
              height: 36,
              trailing: (states) {
                return AbsorbPointer(
                  absorbing: false,
                  child: Checkbox(
                      mouseCursor: SystemMouseCursors.click,
                      hoverColor: Colors.transparent,
                      activeColor: IGColors.primary(),
                      tristate: true,
                      value: allValue,
                      onChanged: (value) {
                        if (allValue == true) {
                          visibleFilterItems[selectedTabIndex]?.clear();
                        } else {
                          visibleFilterItems[selectedTabIndex] = List.from(
                              originaFilterlItems[selectedTabIndex] ?? []);
                        }
                        setters.forEach((element) {
                          element(() {});
                        });
                        applyFilters!(originaFilterlItems[selectedTabIndex],
                            visibleFilterItems[selectedTabIndex]);
                      }),
                );
              });
        }),
        enabled: false));
    finalTemp.add(PopupMenuCustomItem(
        height: 1,
        child: Divider(
            thickness: 1,
            height: 1,
            endIndent: 0,
            indent: 0,
            color: IGColors.separator()),
        enabled: false));
    originaFilterlItems[selectedTabIndex]!.asMap().forEach((index, element) {
      finalTemp.add(PopupMenuCustomItem(
        enabled: false,
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          setters.add(_set);
          return IGButtonRow(
              height: 36,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              title: element.displayValue ?? "",
              onTap: () async {
                if (visibleFilterItems[selectedTabIndex]!.contains(element)) {
                  visibleFilterItems[selectedTabIndex]!.remove(element);
                } else {
                  visibleFilterItems[selectedTabIndex]!.add(element);
                }
                setters.forEach((element) {
                  element(() {});
                });
                applyFilters!(originaFilterlItems[selectedTabIndex],
                    visibleFilterItems[selectedTabIndex]);
              },
              trailing: (states) {
                return AbsorbPointer(
                  absorbing: true,
                  child: Checkbox(
                      hoverColor: Colors.transparent,
                      activeColor: IGColors.primary(),
                      value: visibleFilterItems[selectedTabIndex]!
                          .contains(element),
                      onChanged: (value) {}),
                );
              });
        }),
      ));
      if (index != originaFilterlItems[selectedTabIndex]!.length - 1) {
        finalTemp.add(PopupMenuCustomItem(
            height: 1,
            child: Divider(
                thickness: 1,
                height: 1,
                endIndent: 8,
                indent: 8,
                color: IGColors.separator()),
            enabled: false));
      }
    });
    return finalTemp;
  }

  Future<dynamic> _getRightValueFromCustomKey(T value) async {
    dynamic v;
    if (headerValue != null) {
      v = headerValue!(value, this);
    }
    if (cellDisplayRule!.elaborateValue != null) {
      v = await cellDisplayRule!.elaborateValue!(
          v, cellDisplayRule, value, this);
    }
    return v;
  }

  Future<List<T>> sort(List<T> elements, {bool ascending = true}) async {
    final computed = <dynamic, dynamic>{};
    for (final item in elements) {
      final v = await _getRightValueFromCustomKey(item);
      computed["$item"] = v;
    }
    elements.sort((a, b) {
      var aRSortingValue = computed["$a"];
      var bRSortingValue = computed["$b"];
      if (aRSortingValue != null && bRSortingValue != null) {
        if (type == HeaderSortRuleType.number) {
          if (ascending) {
            return aRSortingValue.compareTo(bRSortingValue);
          } else {
            return bRSortingValue.compareTo(aRSortingValue);
          }
        } else if (type == HeaderSortRuleType.bool) {
          var a = aRSortingValue;
          var b = bRSortingValue;
          if (ascending) {
            if (a == true) {
              return -1;
            } else if (b == true) {
              return 1;
            }
            return 0;
          } else {
            if (a == true) {
              return 1;
            } else if (b == true) {
              return -1;
            }
            return 0;
          }
        } else if (type == HeaderSortRuleType.string) {
          if (ascending) {
            return aRSortingValue.compareTo(bRSortingValue);
          } else {
            return bRSortingValue.compareTo(aRSortingValue);
          }
        } else if (type == HeaderSortRuleType.date) {
          var aRR = DateExtension.formatDBDate(aRSortingValue);
          var bRR = DateExtension.formatDBDate(bRSortingValue);
          if (ascending) {
            return aRR.compareTo(bRR);
          } else {
            return bRR.compareTo(aRR);
          }
        } else if (type == HeaderSortRuleType.custom &&
            customValidator != null) {
          return customValidator!(sortKey, ascending, a, b);
        }
      }
      if (aRSortingValue == null && bRSortingValue != null) {
        return ascending ? 1 : -1;
      }
      if (bRSortingValue == null && aRSortingValue != null) {
        return ascending ? -1 : 1;
      }
      return 0;
    });
    return elements;
  }

  @override
  bool operator ==(dynamic o) => o is HeaderRule && o.headerKey == headerKey;

  @override
  int get hashCode => headerKey.hashCode;
}

enum HeaderSortRuleType { number, string, date, custom, bool, none }
