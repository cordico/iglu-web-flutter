/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class DataCellDisplay {
  DataCellDisplay(this.dataCell, this.headerSortRule);
  final DataCell dataCell;
  final HeaderRule? headerSortRule;

  @override
  bool operator ==(dynamic o) =>
      o is DataCellDisplay && o.headerSortRule == headerSortRule;

  @override
  int get hashCode => headerSortRule.hashCode;
}

class DataTableViewRow<T> {
  DataTableViewRow(this.id, this.cells, this.element,
      {this.onSelectChanged, this.selected, this.smallContextWidget});
  String? id;
  Widget? smallContextWidget;
  T element;
  List<DataCell> cells;
  Function(int, bool?, DataTableViewRow<T>)? onSelectChanged;
  bool? selected = false;

  @override
  bool operator ==(dynamic o) => o is DataTableViewRow && o.id == id;

  @override
  int get hashCode => id.hashCode;
}

class DataTableViewDataSource<T> extends DataTableSource {
  DataTableViewDataSource({
    this.context,
    this.rows,
    this.showSelect = false,
    this.isSelectionMode = false,
  });
  final BuildContext? context;
  List<DataTableViewRow<T>>? rows;
  int _selectedCount = 0;
  final bool showSelect;
  final bool isSelectionMode;

  setRows(List<DataTableViewRow<T>> newRows, int selectedCount) {
    rows = newRows;
    _selectedCount = selectedCount;
  }

  @override
  DataRow? getRow(int index) {
    assert(index >= 0);
    if (index >= rows!.length) return null;
    final row = rows![index];
    return DataRow.byIndex(
        index: index,
        selected: this.showSelect || isSelectionMode ? row.selected! : false,
        color: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.selected) && isSelectionMode) {
            return index % 2 == 0
                ? Colors.transparent
                : IGColors.separator(
                    opacity:
                        webConfigurator.currentBrightness == Brightness.light
                            ? 0.3
                            : 0.1);
            /*return webConfigurator.currentBrightness == Brightness.light
                ? IGColors.primary(opacity: index % 2 == 0 ? 0.20 : 0.30)
                : Colors.white.withOpacity(index % 2 == 0 ? 0.20 : 0.30);*/
          }
          if (states.contains(MaterialState.selected) ||
              states.contains(MaterialState.hovered) ||
              states.contains(MaterialState.dragged) ||
              states.contains(MaterialState.pressed) ||
              states.contains(MaterialState.focused)) {
            return webConfigurator.currentBrightness == Brightness.light
                ? IGColors.primary(opacity: 0.08)
                : Colors.white.withOpacity(0.10);
          }
          return index % 2 == 0
              ? null
              : IGColors.separator(
                  opacity: webConfigurator.currentBrightness == Brightness.light
                      ? 0.3
                      : 0.1);
        }),
        onSelectChanged: row.onSelectChanged != null
            ? (value) async {
                if (row.selected != value && this.showSelect) {
                  row.selected = value;
                }
                row.onSelectChanged!(index, value, row);
              }
            : null,
        cells: row.cells);
  }

  @override
  int get rowCount => rows!.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
