/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import "package:intl/intl.dart" as intl;

class CellDisplayRule<T> {
  String headerKey;
  CellDisplayRuleType type;
  Widget Function(dynamic)? customWidget;
  TextStyle? style;
  StrutStyle? strutStyle;
  TextAlign? textAlign;
  TextDirection? textDirection;
  TextOverflow? overflow;
  int? maxLines;
  TextWidthBasis? textWidthBasis;
  BoxConstraints constraints;
  Color? tintColor;
  String currency;
  intl.NumberFormat? numberFormat;
  Future<dynamic> Function(dynamic, CellDisplayRule<T>?, T?, HeaderRule<T>?)?
      elaborateValue;
  EdgeInsetsGeometry internalPadding;
  EdgeInsetsGeometry? externalMargin;
  Color? backgroundColor;
  double? borderRadius;
  bool showCustomWidgetIfValusIsNull;
  Widget Function(dynamic, dynamic)? customWidgetIfValueIsNull;
  String dateFormat;

  //options
  IGLayoutItemTypeImageOptions? imageOptions;

  //TEXT_LINK
  String Function(dynamic)? tooltipMessage;
  Color? backgroundHoverColor;
  bool? hoverOnlyBackground;
  Color? hoverColor;
  bool showTooltip;
  Icon? leftIcon;
  bool? ignoreOnTap;
  double? radius;
  Function(dynamic)? onTap;

  bool showTooltipText;
  bool putMarginToTooltip;

  int limitHorizontalList;

  CellDisplayRule(
      {required this.headerKey,
      required this.type,
      this.customWidget,
      this.style,
      this.strutStyle,
      this.textAlign,
      this.textDirection,
      this.overflow,
      this.maxLines,
      this.tintColor,
      this.constraints = const BoxConstraints(maxWidth: 200),
      this.textWidthBasis,
      this.numberFormat,
      this.elaborateValue,
      this.backgroundColor,
      this.borderRadius,
      this.internalPadding = const EdgeInsets.all(4.0),
      this.currency = "€",
      this.externalMargin,
      this.customWidgetIfValueIsNull,
      this.backgroundHoverColor,
      this.hoverColor,
      this.hoverOnlyBackground,
      this.showTooltip = false,
      this.showTooltipText = true,
      this.tooltipMessage,
      this.leftIcon,
      this.ignoreOnTap,
      this.radius,
      this.limitHorizontalList = 3,
      this.imageOptions,
      this.showCustomWidgetIfValusIsNull = false,
      this.putMarginToTooltip = false,
      this.dateFormat = "MM/dd/yyyy",
      this.onTap});

  Future<Widget> widget(
      {required BuildContext context,
      required dynamic originalValue,
      required T json,
      required HeaderRule<T>? headerRule}) async {
    var value;
    if (elaborateValue != null) {
      value = await elaborateValue!(originalValue, this, json, headerRule);
    } else {
      value = originalValue;
    }
    if (type == CellDisplayRuleType.number) {
      return Container(
        margin: externalMargin,
        constraints: constraints,
        child: Text("${value ?? ""}",
            style: style ??
                IGTextStyles.custom(
                  color: IGColors.text(),
                ),
            strutStyle: strutStyle,
            textAlign: textAlign,
            textDirection: textDirection,
            overflow: overflow,
            maxLines: maxLines,
            textWidthBasis: textWidthBasis),
      );
    } else if (type == CellDisplayRuleType.currency) {
      var fo = numberFormat ??
          intl.NumberFormat.compactCurrency(symbol: currency, decimalDigits: 2);
      return Container(
        margin: externalMargin,
        constraints: constraints,
        child: Text("${value != null ? fo.format(value) : ""}",
            style: style ??
                IGTextStyles.custom(
                  color: IGColors.text(),
                ),
            strutStyle: strutStyle,
            textAlign: textAlign,
            textDirection: textDirection,
            overflow: overflow,
            maxLines: maxLines,
            textWidthBasis: textWidthBasis),
      );
    } else if (type == CellDisplayRuleType.stars) {
      List<Widget> starWidgets = [];
      if (value != null) {
        for (var i = 0; i < value; i++) {
          starWidgets.add(Icon(Icons.star_rounded,
              color: IGColors.primary(forceWhiteInDarkMode: true), size: 27));
        }
      }
      return Container(
        margin: externalMargin,
        constraints: constraints,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: starWidgets,
        ),
      );
    } else if (type == CellDisplayRuleType.bool) {
      return Container(
          margin: externalMargin,
          constraints: constraints,
          child: AbsorbPointer(
              absorbing: true,
              child: Checkbox(
                value: value ?? false,
                onChanged: (v) {},
                activeColor: tintColor ?? IGColors.primary(),
              )));
    } else if (type == CellDisplayRuleType.string) {
      var margin = 20.0;
      if (MediaQuery.of(context).size.width > 540) {
        margin = (MediaQuery.of(context).size.width - 500) / 2.0;
      }
      var w = Text(value ?? "",
          style: style ??
              IGTextStyles.custom(
                color: IGColors.text(),
              ),
          strutStyle: strutStyle,
          textAlign: textAlign,
          textDirection: textDirection,
          overflow: overflow,
          maxLines: maxLines,
          textWidthBasis: textWidthBasis);
      return Container(
          margin: externalMargin,
          constraints: constraints,
          child: showTooltipText
              ? WidgetsDefaults.defaultTooltip(
                  context: context,
                  margin: putMarginToTooltip
                      ? EdgeInsets.fromLTRB(margin, 0, margin, 0)
                      : null,
                  message: value ?? "",
                  child: w,
                )
              : w);
    } else if (type == CellDisplayRuleType.text_tag) {
      return Container(
        margin: externalMargin,
        constraints: constraints,
        child: FittedBox(
            child: IGButtonViewDefaults.primaryTagBackgroundButton(
                context: context,
                tagColor: backgroundColor,
                style: IGButtonViewStyle(text: value))),
      );
    } else if (type == CellDisplayRuleType.text_link) {
      return value == null || ((value is String) && value.isEmpty)
          ? showCustomWidgetIfValusIsNull
              ? customWidgetIfValueIsNull != null
                  ? customWidgetIfValueIsNull!(originalValue, json)
                  : Container()
              : Container()
          : FittedBox(
              child: IGButtonViewDefaults.primaryTextHoverBackgroundButton(
                  context: context,
                  style: IGButtonViewStyle(
                      text: value ?? "",
                      tooltipMessage: tooltipMessage != null
                          ? tooltipMessage!(value)
                          : showTooltip
                              ? "Open $value"
                              : null,
                      icon: leftIcon,
                      absorbing: ignoreOnTap ?? false,
                      onTapPositioned: (_) {
                        if (onTap != null) {
                          onTap!(originalValue);
                        }
                      })),
            );
    } else if (type == CellDisplayRuleType.date) {
      var val = "";
      if (value != null) {
        if (value is DateTime) {
          val = DateExtension.formatDate(date: value, format: dateFormat);
        } else {
          val = DateExtension.formatDate(stringDate: value, format: dateFormat);
        }
      }
      return Container(
          margin: externalMargin,
          constraints: constraints,
          child: Text(val,
              style: style ??
                  IGTextStyles.custom(
                    color: IGColors.text(),
                  ),
              strutStyle: strutStyle,
              textAlign: textAlign,
              textDirection: textDirection,
              overflow: overflow,
              maxLines: maxLines,
              textWidthBasis: textWidthBasis));
    } else if (type == CellDisplayRuleType.image) {
      return value == null || value.isEmpty
          ? Container()
          : Container(
              margin: externalMargin,
              constraints: constraints,
              child: ImageContainerView(
                absorbing: imageOptions!.absorbing,
                actions: imageOptions!.actions,
                backgroundColor: imageOptions!.backgroundColor,
                backgroundHoverColor: imageOptions!.backgroundHoverColor,
                borderRadius: imageOptions!.borderRadius,
                constraints: constraints,
                defaultColor: imageOptions!.defaultColor,
                fileImage: imageOptions!.fileImage,
                fit: imageOptions!.fit,
                heroTag: imageOptions!.heroTag,
                hoverColor: imageOptions!.hoverColor,
                imageRenderMethodForWeb: imageOptions!.imageRenderMethodForWeb,
                imageUrl: value as String,
                internalPadding: imageOptions!.internalPadding,
                isFullScreen: imageOptions!.isFullScreen,
                loadingColor: imageOptions!.loadingColor,
                onDelete: imageOptions!.onDelete,
                onTap: imageOptions!.onTap,
                onTapPlaceholder: imageOptions!.onTapPlaceholder,
                onUpload: imageOptions!.onUpload,
                placeholderCornerRadius: imageOptions!.placeholderCornerRadius,
                placeholderWidget: imageOptions!.placeholderWidget,
                sendHttpHeaders: imageOptions!.sendHttpHeaders,
                httpHeaders: imageOptions!.httpHeaders,
                showBorder: imageOptions!.showBorder,
                showDefaultProgressIndicator:
                    imageOptions!.showDefaultProgressIndicator,
                showFullScreen: imageOptions!.showFullScreen,
                showLoading: imageOptions!.showLoading,
                imagePickFilesOptions: imageOptions!.imagePickFilesOptions,
                pickImageActions: imageOptions!.pickImageActions,
              ),
            );
    } else if (type == CellDisplayRuleType.arrayWrap && value is List<String>) {
      var list = value
          .map((e) => FittedBox(
                child: IGButtonViewDefaults.primaryTagBackgroundButton(
                    context: context,
                    style: IGButtonViewStyle(
                      text: e,
                    )),
              ))
          .toList();
      if (list.length > limitHorizontalList && limitHorizontalList > 0) {
        var originalListLengh = list.length;
        list = list.sublist(0, limitHorizontalList);
        list.add(FittedBox(
          child: IGButtonViewDefaults.primaryTagBackgroundButton(
              context: context,
              style: IGButtonViewStyle(
                text: "+${originalListLengh - limitHorizontalList}",
              )),
        ));
      }
      return value.isEmpty
          ? Container()
          : FittedBox(
              child: Container(
                margin: externalMargin,
                constraints: constraints,
                child: Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.center,
                  spacing: 8,
                  clipBehavior: Clip.hardEdge,
                  children: list,
                ),
              ),
            );
    } else if (type == CellDisplayRuleType.custom && customWidget != null) {
      return Container(
          constraints: constraints,
          margin: externalMargin,
          child: customWidget!(value));
    }
    return Container();
  }

  @override
  bool operator ==(dynamic o) => o is HeaderRule && o.headerKey == headerKey;

  @override
  int get hashCode => headerKey.hashCode;
}

enum CellDisplayRuleType {
  arrayWrap,
  number,
  string,
  date,
  stars,
  custom,
  bool,
  currency,
  text_tag,
  text_link,
  image
}
