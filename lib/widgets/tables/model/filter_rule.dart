/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:iglu_web_flutter/iglu_web_flutter.dart';
import 'package:collection/collection.dart' show IterableExtension;

class FilterRule<T> {
  String? filterKey;
  List<String>? equalFilterKeys;
  String? displayValue;
  bool hiddenByDefault;
  FilterRule(
      {this.filterKey,
      this.displayValue,
      this.equalFilterKeys,
      this.hiddenByDefault = false});
}

extension FilterRuleArray<T> on List<FilterRule<T>> {
  List<T> apply(List<T> input, HeaderRule<T> header) {
    return List.from(input.where((e) {
      var result = false;
      for (var item in this) {
        dynamic v;
        if (header.headerValue != null) {
          v = header.headerValue!(e, header);
        }
        if (v != null) {
          if (v is List) {
            List arr = v;
            if (arr.isEmpty) {
              result = true;
            } else if (arr.contains(item.filterKey) ||
                (item.equalFilterKeys != null &&
                    item.equalFilterKeys!.isNotEmpty &&
                    arr.firstWhereOrNull((element) =>
                            item.equalFilterKeys!.contains(element)) !=
                        null)) {
              result = true;
              break;
            }
          } else {
            if (v == item.filterKey ||
                (item.equalFilterKeys != null &&
                    item.equalFilterKeys!.isNotEmpty &&
                    item.equalFilterKeys!.contains(v))) {
              result = true;
              break;
            }
          }
        } else {
          result = true;
        }
      }
      return result;
    }));
  }
}
