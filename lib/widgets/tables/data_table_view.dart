/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:collection/collection.dart' show IterableExtension;
import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:json_annotation/json_annotation.dart';

class DataTableView<T> extends StatefulWidget {
  final Key? key;
  final String? title;
  final Widget? header;
  final int defaultRowsPerPage;
  final bool showCheckboxColumn;

  final List<HeaderRule<T>>? headerSortRules;

  final Future Function(T, bool)? onRowClicked;

  final double horizontalMargin;
  final double columnSpacing;
  final double? elevation;
  final Color? cardColor;
  final double? cardBorderRadius;
  final bool showBorder;

  final bool automaticPopulation;

  final bool forceLoading;

  //SMALL CONTEXT WIDGET
  final Widget Function(T)? smallContextWidget;
  final bool forceSmallContext;

  //ADD
  final Widget? addWidget;
  final Future Function()? onAddButtonClicked;

  //REFRESH
  final Widget? refreshWidget;
  final bool hideRefreshButton;
  final Future<List<T>> Function(List<T>?, bool, int)? onRefreshButtonClicked;

  //COLUMN
  final bool showColumn;
  final Widget? columnWidget;

  //SEARCH
  final String? searchPlaceholder;
  final bool showSearch;
  final Widget? searchWidget;

  //EMPTY STATE
  final bool showEmptyState;
  final Widget? emptyStateWidget;

  //TABBAR
  final List<DetailViewCellTabItem>? tabItems;
  final int? selectedIndex;
  final Function(int)? onTapTabBarItem;

  //EXPORTING
  final bool enableExporting;

  //SELECTION
  final bool isSelectionMode;
  final bool allowMultipleSelection;
  final List<T>? preselectedItems;

  //SESSION
  final Function()? notifyListener;
  final Function(List<T>?, int)? updateSession;
  final Map<int, List<T>>? sessionElementsForTab;
  final SideItem<T>? sideItem;

  //DETAIL
  final T? detailElement;

  //IDVALUE
  final String? Function(T?)? idValueElaborate;

  DataTableView(
      {this.key,
      this.title,
      this.header,
      this.elevation,
      this.defaultRowsPerPage = 25,
      this.showCheckboxColumn = false,
      this.headerSortRules,
      this.onRowClicked,
      this.addWidget,
      this.onAddButtonClicked,
      this.refreshWidget,
      this.hideRefreshButton = false,
      this.onRefreshButtonClicked,
      this.showColumn = true,
      this.columnWidget,
      this.showSearch = true,
      this.forceLoading = false,
      this.searchPlaceholder,
      this.searchWidget,
      this.showEmptyState = true,
      this.emptyStateWidget,
      this.horizontalMargin = 24.0,
      this.columnSpacing = 56.0,
      this.automaticPopulation = false,
      this.onTapTabBarItem,
      this.selectedIndex,
      this.tabItems,
      this.enableExporting = true,
      this.isSelectionMode = false,
      this.allowMultipleSelection = false,
      this.cardColor,
      this.smallContextWidget,
      this.forceSmallContext = false,
      this.showBorder = true,
      this.cardBorderRadius,
      this.sessionElementsForTab,
      this.preselectedItems,
      this.sideItem,
      this.updateSession,
      this.notifyListener,
      this.detailElement,
      this.idValueElaborate}) {
    assert(!(T is JsonSerializable || T is Map),
        'T can be only Map or JsonSerializable');
  }

  @override
  DataTableViewState<T> createState() => DataTableViewState<T>();
}

class DataTableViewState<T> extends State<DataTableView<T>> {
  List<HeaderRule<T>> allHeaders = [];
  List<T> elements = [];
  Map<int, List<T>> elementsForTab = {};
  int rowsPerPage = 25;

  FocusNode _searchFocusNode = FocusNode();
  TextEditingController _searchTextEditingController = TextEditingController();

  final GlobalKey<CustomPaginatedDataTableState> _stateKey = GlobalKey();
  final GlobalKey viewColumnContainerKey = GlobalKey();

  bool isFirstLoading = true;
  bool showOnlySearchAction = false;
  bool _isRefreshing = false;

  List<DataTableViewRow<T>> selectedRows = [];
  int get selectedTabIndex => widget.sideItem?.selectedTab ?? 0;
  DataTableViewDataSource<T>? _dataTableViewDataSource;

  @override
  void initState() {
    super.initState();

    //SELECTED TAB INDEX
    if (widget.selectedIndex != null) {
      widget.sideItem!.selectedTab = widget.selectedIndex;
    }

    //SETUP ELEMENTI
    if (widget.sessionElementsForTab != null) {
      elements =
          List.from(widget.sessionElementsForTab![selectedTabIndex] ?? []);
      elementsForTab = Map.from(widget.sessionElementsForTab!);
      isFirstLoading = false;
    } else {
      elements = List.from([]);
      elementsForTab[selectedTabIndex] = List.from([]);
    }

    //ELEMENTO PRESELEZIONATO
    if (widget.preselectedItems != null) {
      try {
        widget.preselectedItems!.forEach((element) {
          if (widget.idValueElaborate != null) {
            var elementID = widget.idValueElaborate!(element);
            selectedRows.add(DataTableViewRow<T>(elementID, [], element));
          }
        });
      } catch (e) {
        logger(e);
      }
    }

    //SETUP DATA SOURCE
    if (_dataTableViewDataSource == null) {
      _dataTableViewDataSource = DataTableViewDataSource<T>(
          context: context,
          rows: [],
          isSelectionMode: widget.isSelectionMode,
          showSelect: widget.allowMultipleSelection);
    }
    rowsPerPage = widget.defaultRowsPerPage;
    _calculateAllHeaders();

    //RESTORATION
    //MANTIENI LA PAGINA SULLA SCHERMATA DI DETTAGLIO
    if (widget.sideItem!.currentPageTable != null) {
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        if (_stateKey.currentState != null) {
          _stateKey.currentState!.pageTo(widget.sideItem!.currentPageTable);
        }
      });
    }

    //MANTIENI LA RICERCA
    if ((widget.sideItem!.searchQueryTable != null &&
        widget.sideItem!.searchQueryTable!.isNotEmpty)) {
      _searchTextEditingController =
          TextEditingController(text: widget.sideItem!.searchQueryTable);
      showOnlySearchAction = widget.forceSmallContext;
    }

    if (elements.isEmpty) {
      isFirstLoading = true;
      _refreshButtonAction(force: true);
    } else {
      isFirstLoading = false;
      if (widget.detailElement != null && widget.idValueElaborate != null) {
        widget.sideItem!.cacheDataTableViewRowArray[selectedTabIndex]
            ?.forEach((cache) {
          cache.smallContextWidget = widget.smallContextWidget != null
              ? widget.smallContextWidget!(cache.element)
              : null;
          cache.selected = cache.id == widget.idValueElaborate!(null);
        });
      }
      _dataTableViewDataSource!.setRows(
          widget.sideItem!.cacheDataTableViewRowArray[selectedTabIndex] ?? [],
          selectedRows.length);
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        _applyAll(calculateCellAgain: false);
      });
    }
  }

  @override
  void didUpdateWidget(DataTableView<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      if (widget.sessionElementsForTab != null) {
        elements =
            List.from(widget.sessionElementsForTab![selectedTabIndex] ?? []);
        elementsForTab = Map.from(widget.sessionElementsForTab!);
      }
      //SELECTED TAB INDEX
      if (widget.selectedIndex != null &&
          widget.selectedIndex != oldWidget.selectedIndex) {
        _changeTab(widget.selectedIndex ?? 0);
        return;
      }
    }
  }

  Widget build(BuildContext context) {
    logger("building data_table_view");
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: CustomPaginatedDataTable(
        key: _stateKey,
        elevation: widget.elevation ?? 1.5,
        cardColor: widget.cardColor,
        borderRadius: widget.cardBorderRadius,
        header: widget.header ??
            (showOnlySearchAction
                ? null
                : widget.title == null
                    ? null
                    : AutoSizeText(
                        widget.title!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        minFontSize: 20,
                        style: IGTextStyles.boldStyle(28, IGColors.text()),
                      )),
        showCheckboxColumn: widget.showCheckboxColumn,
        rowsPerPage: rowsPerPage,
        availableRowsPerPage: [
          widget.defaultRowsPerPage,
          widget.defaultRowsPerPage * 2,
          widget.defaultRowsPerPage * 3,
          widget.defaultRowsPerPage * 4
        ],
        showBorder: widget.showBorder,
        forceSmallContext: widget.forceSmallContext,
        showSmallContext: widget.smallContextWidget != null,
        showLoading: isFirstLoading || _isRefreshing || widget.forceLoading,
        emptyState: widget.showEmptyState
            ? (widget.emptyStateWidget == null
                ? _emptyStateWidgetDefault()
                : widget.emptyStateWidget)
            : null,
        showAllButtonInsideAvailableRowsPerPage: false,
        defaultRowsPerPage: widget.defaultRowsPerPage,
        showFooter: elements.isNotEmpty,
        onRowsPerPageChanged: (val) {
          rowsPerPage = val;
          _stateKey.currentState!.pageTo(0);
          _setStateOnlyIfMounted(() {});
        },
        isDuringSearch: _searchText().isNotEmpty,
        tabItems: widget.tabItems,
        onTapTabBarItem: (index) {
          _changeTab(index);
        },
        onPageChanged: (value) {
          widget.sideItem!.currentPageTable = value;
        },
        selectedIndex: selectedTabIndex,
        horizontalMargin: widget.horizontalMargin,
        columnSpacing: widget.columnSpacing,
        columns: _calculateAllColums(),
        actions: [
          if (widget.showSearch) _searchWidget(),
          if (widget.onAddButtonClicked != null && !showOnlySearchAction)
            Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: widget.addWidget ??
                    IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                        context: context,
                        style: IGButtonViewStyle(
                            icon: Icons.add_rounded,
                            tooltipMessage: "Add",
                            onTapPositioned: (position) async {
                              await widget.onAddButtonClicked!();
                            }))),
          if (showOnlySearchAction)
            Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: IGButtonViewDefaults.semiMediumButtonIconBackground(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.close_rounded,
                        tooltipMessage: "Close",
                        backgroundColorSelected: resetValueField,
                        tintColorSelected: resetValueField,
                        onTapPositioned: (position) async {
                          showOnlySearchAction = false;
                          _searchTextEditingController.clear();
                          widget.sideItem!.searchQueryTable = "";
                          await _applyAll();
                          _setStateOnlyIfMounted(() {});
                        }))),
          if (widget.showColumn &&
              !showOnlySearchAction &&
              !widget.forceSmallContext &&
              !SizeDetector.isSmallScreen(context))
            Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.view_column_rounded,
                        tooltipMessage: "Visible Columns",
                        onTapPositioned: (position) async {
                          await showMenuCustom(
                              color: IGColors.secondaryBackground(),
                              items: _calculateAllColumsPopupMenuButton(),
                              preferredWidth: 224,
                              position: position,
                              context: context,
                              elevation: 1.5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      kButtonPrimaryCornerRadius)));
                        }))),
          if (widget.enableExporting &&
              !showOnlySearchAction &&
              elements.isNotEmpty)
            Padding(
                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.north_east_rounded,
                        tooltipMessage:
                            "Export ${widget.title}${(widget.tabItems != null) ? (" " + widget.tabItems![selectedTabIndex].exportTitle!) : ""}",
                        onTapPositioned: (position) async {
                          if (isFirstLoading ||
                              _isRefreshing ||
                              widget.forceLoading) {
                            return;
                          }
                          var title = widget.title ?? "";
                          if (widget.tabItems != null) {
                            title +=
                                " ${widget.tabItems![selectedTabIndex].exportTitle}";
                          }
                          await Navigator.of(context).push(PageTransition(
                            type: PageTransitionType.upToDown,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.bounceOut,
                            child: ExportFormSheet<T>(
                                elements: elements,
                                title: title,
                                selectedTabIndex: selectedTabIndex,
                                headerSortRules: List.of(
                                    widget.headerSortRules!,
                                    growable: true),
                                searchQuery: _searchText()),
                          ));
                        }))),
          if (widget.onRefreshButtonClicked != null &&
              !showOnlySearchAction &&
              !widget.hideRefreshButton)
            widget.refreshWidget ??
                Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                    child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                        context: context,
                        style: IGButtonViewStyle(
                            icon: Icons.replay_rounded,
                            tooltipMessage: "Reload ${widget.title}",
                            onTapPositioned: (position) async {
                              if (_isRefreshing) {
                                return;
                              }
                              await _refreshButtonAction(
                                  calculateCellAgain: true);
                            }))),
        ],
        source: _dataTableViewDataSource!,
        sortAscending: widget.sideItem!.sortAsceding,
        sortColumnIndex: widget.sideItem!.sortColumnIndex,
      ),
    );
  }

  //MARK: UTILITIES
  _updateSession() {
    if (widget.updateSession != null) {
      widget.updateSession!(elementsForTab[selectedTabIndex], selectedTabIndex);
    }
  }

  _getPageForElement() {
    if (widget.idValueElaborate != null) {
      var indx = elements.indexWhere((element) {
        return (widget.idValueElaborate!(null) ??
                webConfigurator.kingRouteDelegate.currentConfiguration
                    ?.routeInformation.location
                    ?.split('/')
                    .lastOrNull) ==
            widget.idValueElaborate!(element);
      });
      if (indx >= 0) {
        if (_stateKey.currentState != null) {
          widget.sideItem!.currentPageTable = indx;
          _stateKey.currentState!.pageTo(indx);
        }
      }
    }
  }

  _setStateOnlyIfMounted(Function() fn, {bool forceNormal = false}) {
    if (mounted) {
      if (widget.notifyListener != null && !forceNormal) {
        widget.notifyListener!();
      } else {
        setState(fn);
      }
    }
  }

  _refreshButtonAction(
      {bool force = true,
      bool noLoading = false,
      bool calculateCellAgain = false}) async {
    logger("_refreshAction");
    if (!noLoading) {
      _setStateOnlyIfMounted(() {
        if (force) {
          isFirstLoading = true;
        } else {
          _isRefreshing = true;
        }
      }, forceNormal: true);
    }
    if (widget.onRefreshButtonClicked != null) {
      List<T> temp = await widget.onRefreshButtonClicked!(
          elementsForTab[selectedTabIndex], force, selectedTabIndex);
      elementsForTab[selectedTabIndex] =
          temp; //+ temp + temp + temp + temp + temp;
      logger("_refreshAction_done_force_$force");
    }
    await _applyAll(calculateCellAgain: calculateCellAgain);
    _isRefreshing = false;
    isFirstLoading = false;
    _updateSession();
  }

  Future _applyAll({String? input, bool calculateCellAgain = false}) async {
    var query = input ?? _searchText();
    if (query.isEmpty) {
      if (elementsForTab[selectedTabIndex] != null) {
        elements = List.from(elementsForTab[selectedTabIndex]!);
      } else {
        elements = [];
      }
      await _applyFilters();
      await _sort();
    } else {
      await _applySearch(query);
      await _applyFilters();
      await _sort();
    }
    widget.sideItem!.cacheDataTableViewRowArray[selectedTabIndex] =
        await _calculateRows(calculateCellAgain: calculateCellAgain);
    _dataTableViewDataSource!.setRows(
        widget.sideItem!.cacheDataTableViewRowArray[selectedTabIndex] ?? [],
        selectedRows.length);
    _getPageForElement();
  }

  _changeTab(int index) {
    if (selectedTabIndex != index) {
      widget.sideItem!.selectedTab = index;
      widget.sideItem!.currentPageTable = null;
      elements.clear();
      _dataTableViewDataSource!.setRows([], 0);
      _stateKey.currentState!.pageTo(0);
      if (widget.onTapTabBarItem != null) {
        widget.onTapTabBarItem!(selectedTabIndex);
      }
      _refreshButtonAction(
          force: elementsForTab[selectedTabIndex] == null ||
              elementsForTab[selectedTabIndex]!.isEmpty,
          noLoading: elementsForTab[selectedTabIndex] != null &&
              elementsForTab[selectedTabIndex]!.isNotEmpty,
          calculateCellAgain: true);
    }
  }

  //MAKR: EMPTY STATE
  _emptyStateWidgetDefault() {
    bool? isDuringSearch = _searchText().isNotEmpty;
    return SizeDetector.isSmallScreen(context) ||
            SizeDetector.isMediumScreen(context) ||
            widget.forceSmallContext
        ? Column(
            children: [
              Container(
                constraints: BoxConstraints(minHeight: 240),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    isDuringSearch!
                        ? Icon(Icons.search_rounded,
                            color: IGColors.text(), size: 50)
                        : Container(),
                    Text(
                      isFirstLoading || widget.forceLoading
                          ? "Loading..."
                          : isDuringSearch ||
                                  (widget.headerSortRules!.firstWhereOrNull(
                                          (element) => element.isFilterApplied(
                                              selectedTabIndex)) !=
                                      null)
                              ? "No items found with this search"
                              : "No items registered",
                      textAlign: TextAlign.center,
                      style: IGTextStyles.custom(
                          color: IGColors.text(), fontSize: 30),
                    ),
                  ],
                ),
              ),
            ],
          )
        : Column(
            children: [
              Container(
                constraints: BoxConstraints(minHeight: 240),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          isDuringSearch!
                              ? Icon(Icons.search_rounded,
                                  color: IGColors.text(), size: 32)
                              : Container(),
                          isDuringSearch ? Container(width: 10) : Container(),
                          Text(
                            isFirstLoading || widget.forceLoading
                                ? "Loading..."
                                : isDuringSearch ||
                                        (widget.headerSortRules!
                                                .firstWhereOrNull((element) =>
                                                    element.isFilterApplied(
                                                        selectedTabIndex)) !=
                                            null)
                                    ? "No items found with this search"
                                    : "No items registered",
                            style: IGTextStyles.custom(
                                color: IGColors.text(), fontSize: 30),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
  }

  //MARK: SEARCHING
  _searchText() {
    return _searchTextEditingController.text;
  }

  Future _applySearch(String query) async {
    var copy = List.from(elementsForTab[selectedTabIndex]!);
    elements = List.from(copy.where((element) {
      var searchQuery = "";
      for (var rule in widget.sideItem!.visibleHeaders) {
        if (rule.headerValue != null || rule.searchValue != null) {
          if (rule.searchValue != null) {
            searchQuery += " ${rule.searchValue!(element)}";
          } else {
            searchQuery += " ${rule.headerValue!(element, rule) ?? ""}";
          }
        }
      }
      return searchQuery
          .trim()
          .toLowerCase()
          .contains(query.trim().toLowerCase());
    }));
  }

  Future _applyFilters() async {
    if (widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex] == null) {
      widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex] = {};
      for (var item in widget.sideItem!.visibleHeaders) {
        await item.calculateFilters(selectedTabIndex: selectedTabIndex);
        widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex]
                ?[item.filterKey ?? item.headerKey] =
            item.visibleFilterItems[selectedTabIndex];
      }
      if (widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex]
              ?.isNotEmpty ==
          true) {
        await _applyAll();
        _setStateOnlyIfMounted(() {});
      }
    }
    if (widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex] != null &&
        widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex]
                ?.isNotEmpty ==
            true) {
      widget.sideItem?.visibleFiltersForHeaderKey[selectedTabIndex]
          ?.forEach((key, value) {
        if (value != null) {
          if (value.isNotEmpty && elements.isNotEmpty == true) {
            elements = List.from(value.apply(
                elements,
                widget.sideItem!.visibleHeaders
                    .firstWhere((element) => element.headerKey == key)));
          } else {
            elements = List.from([]);
          }
        }
      });
    }
  }

  _searchWidget() {
    if (elementsForTab[selectedTabIndex] == null ||
        elementsForTab[selectedTabIndex]!.isEmpty) {
      return Container();
    }
    if (widget.searchWidget != null) {
      return widget.searchWidget;
    }
    return (SizeDetector.isSmallScreen(context) || widget.forceSmallContext) &&
            !showOnlySearchAction
        ? Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                context: context,
                style: IGButtonViewStyle(
                    icon: Icons.search_rounded,
                    tooltipMessage: "Search ${widget.title}",
                    onTapPositioned: (position) async {
                      if (mounted) {
                        setState(() {
                          showOnlySearchAction = true;
                        });
                      }
                      _searchFocusNode.requestFocus();
                    })),
          )
        : SearchField(
            width: 250,
            height: 40,
            showClose: !widget.forceSmallContext,
            focusNode: _searchFocusNode,
            controller: _searchTextEditingController,
            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
            hint: "Search ${widget.searchPlaceholder ?? widget.title}",
            onChanged: (searchString) async {
              widget.sideItem!.searchQueryTable = searchString;
              _stateKey.currentState!.pageTo(0);
              await _applyAll(input: searchString);
              _setStateOnlyIfMounted(() {});
            });
  }

  //MARK: SORTING
  Future _sort() async {
    var headerRule = widget.headerSortRules?.firstWhereOrNull(
        (element) => element.headerKey == widget.sideItem!.sortColumnName);
    if (headerRule != null) {
      elements = await headerRule.sort(elements,
          ascending: widget.sideItem!.sortAsceding);
    }
  }

  _sortVisibleHeaders() {
    widget.sideItem!.visibleHeaders.sort((a, b) {
      return allHeaders.indexOf(a).compareTo(allHeaders.indexOf(b));
    });
  }

  //MARK: CALCULATE
  Future<List<DataTableViewRow<T>>> _calculateRows(
      {bool calculateCellAgain = false}) async {
    var temp = List<DataTableViewRow<T>>.from([], growable: true);
    var list = elements;
    for (var element in list) {
      var elementID = "";
      if (widget.idValueElaborate != null) {
        elementID = widget.idValueElaborate!(element) ?? "";
      }
      if (widget.sideItem!.cacheDataTableViewRow[selectedTabIndex]
              ?[elementID] !=
          null) {
        var cache = widget
            .sideItem!.cacheDataTableViewRow[selectedTabIndex]![elementID]!;
        cache.smallContextWidget = widget.smallContextWidget != null
            ? widget.smallContextWidget!(element)
            : null;
        cache.selected = selectedRows.where((e) => e.id == elementID).isNotEmpty
            ? true
            : false;
        if (calculateCellAgain) {
          var cells = <DataCell>[];
          for (var e in widget.sideItem!.visibleHeaders) {
            var r = await e.calculateDataCell(context, element, elementID,
                force: calculateCellAgain);
            cells.add(r);
          }
          cache.cells = cells;
        }
        temp.add(cache);
      } else {
        var finalTemp = <DataCell>[];
        for (var e in widget.sideItem!.visibleHeaders) {
          var r = await e.calculateDataCell(context, element, elementID,
              force: calculateCellAgain);
          finalTemp.add(r);
        }
        if (widget.sideItem!.cacheDataTableViewRow[selectedTabIndex] == null) {
          widget.sideItem!.cacheDataTableViewRow[selectedTabIndex] = {};
        }
        widget.sideItem!.cacheDataTableViewRow[selectedTabIndex]![elementID] =
            DataTableViewRow<T>(elementID, finalTemp, element,
                smallContextWidget: widget.smallContextWidget != null
                    ? widget.smallContextWidget!(element)
                    : null,
                selected:
                    selectedRows.where((e) => e.id == elementID).isNotEmpty
                        ? true
                        : false,
                onSelectChanged: widget.onRowClicked != null
                    ? (index, value, row) async {
                        if (widget.isSelectionMode) {
                          _setStateOnlyIfMounted(() {
                            if (widget.allowMultipleSelection) {
                              if (selectedRows.contains(row)) {
                                selectedRows.remove(row);
                              } else {
                                selectedRows.add(row);
                              }
                            } else {
                              selectedRows.clear();
                              selectedRows.add(row);
                            }
                          });
                        }
                        widget.onRowClicked!(element, value ?? false);
                      }
                    : null);
        temp.add(widget
            .sideItem!.cacheDataTableViewRow[selectedTabIndex]![elementID]!);
      }
    }
    return temp;
  }

  _calculateAllHeaders({bool force = false}) {
    HeaderRule<T>? firstDefault;
    allHeaders.clear();
    if (!widget.sideItem!.settedSortValues || force) {
      widget.sideItem!.visibleHeaders.clear();
    }
    widget.headerSortRules!.forEach((element) {
      if (element.adjustValues != null) {
        element.adjustValues!(element);
      }
      if (firstDefault == null && element.isDefault) {
        firstDefault = element;
      }
      if (element.hiddenByDefault) {
        allHeaders.add(element);
      } else {
        if (!widget.sideItem!.settedSortValues || force) {
          widget.sideItem!.visibleHeaders.add(element);
        }
        allHeaders.add(element);
      }
    });
    if (!widget.sideItem!.settedSortValues || force) {
      widget.sideItem!.settedSortValues = true;
      widget.sideItem!.sortAsceding = firstDefault?.isDefaultAsceding ?? false;
      if (firstDefault != null) {
        widget.sideItem!.sortColumnIndex =
            widget.sideItem!.visibleHeaders.indexOf(firstDefault!);
      } else {
        widget.sideItem!.sortColumnIndex = 0;
      }
      widget.sideItem!.sortColumnName = firstDefault?.sortKey ?? "";
    }
  }

  _calculateAllColums() {
    var finalTemp = List<DataColumn>.from([], growable: true);
    widget.sideItem!.visibleHeaders.forEach((element) {
      finalTemp.add(DataColumn(
          label: Row(
            children: [
              if (element.filterItems != null && element.showFilterIcon)
                IGButtonViewDefaults.mediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.filter_list_rounded,
                        iconColor: element.isFilterApplied(selectedTabIndex)
                            ? IGColors.primary()
                            : IGColors.text(),
                        tooltipMessage: "Filters",
                        onTapPositioned: (position) async {
                          await element.showFilterMenu(
                              selectedTabIndex: selectedTabIndex,
                              context: context,
                              position: position,
                              applyFilters:
                                  (originalFilter, visibleFilters) async {
                                widget.sideItem!.visibleFiltersForHeaderKey[
                                        selectedTabIndex]![
                                    element.filterKey ??
                                        element.headerKey] = visibleFilters;
                                _stateKey.currentState!.pageTo(0);
                                await _applyAll();
                                _setStateOnlyIfMounted(() {});
                              });
                        })),
              if (element.filterItems != null) Container(width: 4),
              Text(element.displayValue ?? element.headerKey.titleCase,
                  style: IGTextStyles.custom(color: IGColors.text())),
              if (element.helpMenuItems != null) Container(width: 4),
              if (element.helpMenuItems != null)
                IGButtonViewDefaults.mediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.info_outline_rounded,
                        iconColor: IGColors.text(),
                        tooltipMessage: "Info",
                        onTapPositioned: (position) async {
                          if (element.originalHelpMenuItems == null) {
                            element.originalHelpMenuItems =
                                await element.helpMenuItems!();
                          }
                          await showMenuCustom(
                              items: element.originalHelpMenuItems,
                              color: IGColors.secondaryBackground(),
                              position: position,
                              context: context,
                              elevation: 1.5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      kButtonPrimaryCornerRadius)));
                        })),
            ],
          ),
          onSort: element.type == HeaderSortRuleType.none
              ? null
              : (columnIndex, ascending) async {
                  widget.sideItem!.sortAsceding = ascending;
                  widget.sideItem!.sortColumnIndex = columnIndex;
                  widget.sideItem!.sortColumnName = element.headerKey;
                  await _applyAll();
                  _setStateOnlyIfMounted(() {});
                }));
    });
    return finalTemp;
  }

  _calculateAllColumsPopupMenuButton() {
    var setters = <StateSetter>[];
    var finalTemp = List<PopupMenuEntry>.from([], growable: true);
    finalTemp.add(PopupMenuCustomItem(
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          setters.add(_set);
          bool? allValue =
              widget.sideItem!.visibleHeaders.length == allHeaders.length;
          if (widget.sideItem!.visibleHeaders.isEmpty == true) {
            allValue = false;
          }
          if (!allValue && widget.sideItem!.visibleHeaders.length > 0) {
            allValue = null;
          }
          return IGButtonRow(
              title: "Visible columns",
              height: 36,
              trailing: (states) {
                return Checkbox(
                    mouseCursor: SystemMouseCursors.click,
                    hoverColor: Colors.transparent,
                    activeColor: IGColors.primary(),
                    tristate: true,
                    value: allValue,
                    onChanged: (value) async {
                      if (allValue == true) {
                        widget.sideItem!.visibleHeaders
                            .removeWhere((element) => element.isErasable);
                      } else {
                        widget.sideItem!.visibleHeaders = List.from(allHeaders);
                      }
                      _sortVisibleHeaders();
                      var findIndex = widget.sideItem!.visibleHeaders.indexOf(
                          HeaderRule<T>(
                              widget.sideItem!.sortColumnName,
                              widget.sideItem!.sortColumnName,
                              HeaderSortRuleType.custom));
                      if (findIndex == -1) {
                        widget.sideItem!.sortColumnIndex = 0;
                        widget.sideItem!.sortColumnName =
                            widget.sideItem!.visibleHeaders.first.headerKey;
                      } else {
                        widget.sideItem!.sortColumnIndex = widget
                            .sideItem!.visibleHeaders
                            .indexOf(HeaderRule<T>(
                                widget.sideItem!.sortColumnName,
                                widget.sideItem!.sortColumnName,
                                HeaderSortRuleType.custom));
                      }
                      widget.sideItem!
                              .cacheDataTableViewRowArray[selectedTabIndex] =
                          await _calculateRows(calculateCellAgain: true);
                      setters.forEach((element) {
                        element(() {});
                      });
                      _setStateOnlyIfMounted(() {});
                    });
              });
        }),
        enabled: false));
    finalTemp.add(PopupMenuCustomItem(
        height: 1,
        child: Divider(
            thickness: 1,
            height: 1,
            endIndent: 0,
            indent: 0,
            color: IGColors.separator()),
        enabled: false));
    allHeaders.asMap().forEach((index, element) {
      finalTemp.add(PopupMenuCustomItem(
        enabled: false,
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          setters.add(_set);
          return IGButtonRow(
              height: 36,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              title: element.displayValue ?? element.headerKey.titleCase,
              onTap: !element.isErasable
                  ? null
                  : () async {
                      if (!element.isErasable) {
                        return;
                      } else {
                        if (widget.sideItem!.visibleHeaders.contains(element)) {
                          widget.sideItem!.visibleHeaders.remove(element);
                        } else {
                          widget.sideItem!.visibleHeaders.add(element);
                        }
                        _sortVisibleHeaders();
                        var findIndex = widget.sideItem!.visibleHeaders.indexOf(
                            HeaderRule<T>(
                                widget.sideItem!.sortColumnName,
                                widget.sideItem!.sortColumnName,
                                HeaderSortRuleType.custom));
                        if (findIndex == -1) {
                          widget.sideItem!.sortColumnIndex = 0;
                          widget.sideItem!.sortColumnName =
                              widget.sideItem!.visibleHeaders.first.headerKey;
                        } else {
                          widget.sideItem!.sortColumnIndex = widget
                              .sideItem!.visibleHeaders
                              .indexOf(HeaderRule<T>(
                                  widget.sideItem!.sortColumnName,
                                  widget.sideItem!.sortColumnName,
                                  HeaderSortRuleType.custom));
                        }
                        widget.sideItem!
                                .cacheDataTableViewRowArray[selectedTabIndex] =
                            await _calculateRows(calculateCellAgain: true);
                        setters.forEach((element) {
                          element(() {});
                        });
                        _setStateOnlyIfMounted(() {});
                      }
                    },
              trailing: (states) {
                return AbsorbPointer(
                  absorbing: true,
                  child: Checkbox(
                      hoverColor: Colors.transparent,
                      activeColor: IGColors.primary(),
                      value: widget.sideItem!.visibleHeaders.contains(element),
                      onChanged: element.isErasable ? (value) {} : null),
                );
              });
        }),
      ));
      if (index != allHeaders.length - 1) {
        finalTemp.add(PopupMenuCustomItem(
            height: 1,
            child: Divider(
                thickness: 1,
                height: 1,
                endIndent: 8,
                indent: 8,
                color: IGColors.separator()),
            enabled: false));
      }
    });
    return finalTemp;
  }
}
