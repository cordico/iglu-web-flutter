/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:collection/collection.dart' show IterableExtension;
import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:json_annotation/json_annotation.dart';

class DataTableViewPaginated<T> extends StatefulWidget {
  final Function fromJSON;
  //TODO: ADJUST THIS
  final dynamic call;
  final Map<String, dynamic>? customParameters;
  final Key? key;
  final String? title;
  final Widget? header;
  final int defaultRowsPerPage;
  final bool showCheckboxColumn;

  final List<HeaderRule<T>>? headerSortRules;

  final Future Function(T, bool)? onRowClicked;

  final Function(List<T>)? manipulateElementsList;

  final double horizontalMargin;
  final double columnSpacing;
  final double? elevation;
  final Color? cardColor;
  final double? cardBorderRadius;
  final bool showBorder;

  final bool automaticPopulation;

  final bool forceLoading;

  //SMALL CONTEXT WIDGET
  final Widget Function(T)? smallContextWidget;
  final bool forceSmallContext;

  //ADD
  final Widget? addWidget;
  final Future Function()? onAddButtonClicked;

  //REFRESH
  final Widget? refreshWidget;
  final bool hideRefreshButton;

  //COLUMN
  final bool showColumn;
  final Widget? columnWidget;

  //SEARCH
  final String? searchPlaceholder;
  final bool showSearch;
  final Widget? searchWidget;

  //EMPTY STATE
  final bool showEmptyState;
  final Widget? emptyStateWidget;

  //TABBAR
  final List<DetailViewCellTabItem>? tabItems;
  final int? selectedIndex;
  final Function(int)? onTapTabBarItem;

  //EXPORTING
  final bool enableExporting;

  //SELECTION
  final bool isSelectionMode;
  final bool allowMultipleSelection;
  final List<T>? preselectedItems;

  //SESSION
  final SideItem<T> sideItem;
  final Function(List<T>, int)? updateSession;
  final Map<int, List<T>>? sessionElementsForTab;

  DataTableViewPaginated({
    this.key,
    required this.fromJSON,
    required this.call,
    this.customParameters,
    this.title,
    this.header,
    this.elevation,
    this.defaultRowsPerPage = 25,
    this.showCheckboxColumn = false,
    this.headerSortRules,
    this.onRowClicked,
    this.addWidget,
    this.onAddButtonClicked,
    this.refreshWidget,
    this.hideRefreshButton = false,
    this.showColumn = true,
    this.columnWidget,
    this.showSearch = true,
    this.forceLoading = false,
    this.searchPlaceholder,
    this.searchWidget,
    this.showEmptyState = true,
    this.emptyStateWidget,
    this.horizontalMargin = 24.0,
    this.columnSpacing = 56.0,
    this.automaticPopulation = false,
    this.onTapTabBarItem,
    this.selectedIndex,
    this.tabItems,
    this.enableExporting = true,
    this.isSelectionMode = false,
    this.allowMultipleSelection = false,
    this.cardColor,
    this.smallContextWidget,
    this.forceSmallContext = false,
    this.showBorder = true,
    this.cardBorderRadius,
    this.preselectedItems,
    this.updateSession,
    this.manipulateElementsList,
    this.sessionElementsForTab,
    required this.sideItem,
  }) {
    assert(!(T is JsonSerializable || T is Map),
        'T can be only Map or JsonSerializable');
  }

  @override
  DataTableViewPaginatedState<T> createState() =>
      DataTableViewPaginatedState<T>();
}

class DataTableViewPaginatedState<T> extends State<DataTableViewPaginated<T>> {
  bool _sortAsceding = false;
  int? _sortColumnIndex;
  String _sortColumnName = "";

  List<HeaderRule<T>?> allHeaders = [];
  List<HeaderRule<T>?> visibleHeaders = [];

  late int rowsPerPage;

  FocusNode _searchFocusNode = FocusNode();
  TextEditingController _searchTextEditingController = TextEditingController();

  final GlobalKey<CustomPaginatedDataTableState> _stateKey = GlobalKey();

  bool _isLoading = true;

  Map<int, Map<String, List<FilterRule<T>>?>> visibleFiltersForHeaderKey = {};

  bool showOnlySearchAction = false;

  final GlobalKey viewColumnContainerKey = GlobalKey();

  List<DataTableViewRow<T>> selectedRows = [];

  int get selectedTabIndex => widget.sideItem.selectedTab ?? 0;

  DataTableViewDataSource<T>? _dataTableViewDataSource;

  List<DataColumn> _columns = [];

  updateSession() {
    if (widget.updateSession != null) {
      widget.updateSession!(
          widget.sideItem.paginatedQuery!.elements, selectedTabIndex);
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.sideItem.paginatedQuery == null) {
      logger("RESET PQ");
      widget.sideItem.paginatedQuery =
          PaginatedQuery(fromJSON: widget.fromJSON);
    }
    widget.sideItem.paginatedQuery!.query!.setCall(widget.call);
    if (widget.customParameters != null) {
      widget.sideItem.paginatedQuery!.query!
          .setCustomParameters(widget.customParameters!);
    }
    widget.sideItem.paginatedQuery!.keepOnlyOnePage = true;

    //SELECTED TAB INDEX
    if (widget.selectedIndex != null) {
      widget.sideItem.selectedTab = widget.selectedIndex;
    }

    //ELEMENTO PRESELEZIONATO
    if (widget.preselectedItems != null) {
      try {
        widget.preselectedItems!.forEach((element) {
          var elementID = element is JsonSerializable
              ? element.toJson()["id"]
              : element is Map
                  ? element["id"]
                  : "";
          selectedRows.add(DataTableViewRow<T>(elementID, [], element));
        });
      } catch (e) {
        logger(e);
      }
    }

    //SETUP DATA SOURCE
    if (_dataTableViewDataSource == null) {
      _dataTableViewDataSource = DataTableViewDataSource<T>(
          context: context,
          rows: [],
          isSelectionMode: widget.isSelectionMode,
          showSelect: widget.allowMultipleSelection);
    }
    rowsPerPage = widget.defaultRowsPerPage;
    _calculateAllHeaders(changeCurrentConfiguration: true);
    _columns = _calculateAllColums();

    //MANTIENI LA RICERCA
    if (widget.sideItem.paginatedQuery?.query?.q != null &&
        widget.sideItem.paginatedQuery?.query?.q.isNotEmpty == true) {
      _searchTextEditingController = TextEditingController(
          text: widget.sideItem.paginatedQuery?.query?.q ?? "");
      showOnlySearchAction = widget.forceSmallContext;
    }

    //SETUP ELEMENTI
    if (widget.sessionElementsForTab != null) {
      widget.sideItem.paginatedQuery!.elements =
          List.from(widget.sessionElementsForTab![selectedTabIndex] ?? []);
    }
    _isLoading = true;
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      _getData();
    });
  }

  @override
  void didUpdateWidget(DataTableViewPaginated<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget != widget) {
      if (widget.sessionElementsForTab != null) {
        widget.sideItem.paginatedQuery!.elements =
            List.from(widget.sessionElementsForTab![selectedTabIndex] ?? []);
        if (widget.sideItem.paginatedQuery!.elements.isNotEmpty) {
          WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            _getData();
          });
        } else {
          _setStateOnlyIfMounted(() {
            _dataTableViewDataSource!
                .setRows(_calculateRows(), selectedRows.length);
            _isLoading = false;
          });
        }
      }
    }
  }

  _setStateOnlyIfMounted(void Function() fn) {
    if (mounted) {
      setState(fn);
    }
  }

  _getData({bool isNew = false}) async {
    if (isNew || (!isNew && widget.sideItem.paginatedQuery!.elements.isEmpty)) {
      logger("_getData");
      _setStateOnlyIfMounted(() {
        _isLoading = true;
        widget.sideItem.paginatedQuery!.elements.clear();
        _dataTableViewDataSource!.setRows([], 0);
      });
      if (widget.sideItem.paginatedQuery!.page == 0) {
        widget.sideItem.paginatedQuery!.page = 1;
      }
      var st = _searchText();
      if (st != null && st.isNotEmpty) {
        widget.sideItem.paginatedQuery!.query!.search(st);
      } else {
        widget.sideItem.paginatedQuery!.query!.search("");
      }
      await widget.sideItem.paginatedQuery!
          .perform(handlePage: false, forceRefresh: true);
      if (widget.manipulateElementsList != null) {
        widget.manipulateElementsList!(
            widget.sideItem.paginatedQuery?.elements ?? []);
      }
      _columns = _calculateAllColums();
      _dataTableViewDataSource!.setRows(_calculateRows(), selectedRows.length);
      updateSession();
      logger("_getData_done");
      _isLoading = false;
      _setStateOnlyIfMounted(() {});
    } else {
      _setStateOnlyIfMounted(() {
        _dataTableViewDataSource!
            .setRows(_calculateRows(), selectedRows.length);
        _isLoading = false;
      });
    }
  }

  Widget build(BuildContext context) {
    logger("building data_table_view");
    return LayoutBuilder(builder: (context, constraints) {
      return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: CustomPaginatedDataTable(
          key: _stateKey,
          elevation: widget.elevation ?? 1.5,
          cardColor: widget.cardColor,
          borderRadius: widget.cardBorderRadius,
          showNextPage: !widget.sideItem.paginatedQuery!.isEnd,
          showOnlyOnePage: true,
          totalCount: widget.sideItem.paginatedQuery!.totalElements,
          currentPaginatedPage: widget.sideItem.paginatedQuery!.page,
          header: widget.header ??
              (showOnlySearchAction
                  ? null
                  : widget.title == null
                      ? null
                      : AutoSizeText(
                          widget.title!,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: IGTextStyles.boldStyle(28, IGColors.text()),
                        )),
          showCheckboxColumn: widget.showCheckboxColumn,
          rowsPerPage: rowsPerPage,
          availableRowsPerPage: [
            widget.defaultRowsPerPage,
            widget.defaultRowsPerPage * 2,
            widget.defaultRowsPerPage * 3,
            widget.defaultRowsPerPage * 4
          ],
          showBorder: widget.showBorder,
          forceSmallContext: widget.forceSmallContext,
          showSmallContext: widget.smallContextWidget != null,
          showLoading: _isLoading || widget.forceLoading,
          isDuringSearch: _searchText().isNotEmpty,
          emptyState: widget.showEmptyState
              ? (widget.emptyStateWidget == null
                  ? _emptyStateWidgetDefault()
                  : widget.emptyStateWidget)
              : null,
          showAllButtonInsideAvailableRowsPerPage: false,
          defaultRowsPerPage: widget.defaultRowsPerPage,
          showFooter: widget.sideItem.paginatedQuery!.elements.isNotEmpty,
          onRowsPerPageChanged: (val) {
            rowsPerPage = val;
            _stateKey.currentState!.pageTo(0, reset: true);
          },
          tabItems: widget.tabItems,
          onTapTabBarItem: (index) {
            if (selectedTabIndex != index) {
              widget.sideItem.selectedTab = index;
              widget.sideItem.currentPaginatedPageDetail = null;
              if (widget.onTapTabBarItem != null) {
                widget.onTapTabBarItem!(selectedTabIndex);
              }
              _calculateAllHeaders();
              _stateKey.currentState!.pageTo(0, reset: true);
            }
          },
          onPageChanged: (value) {
            widget.sideItem.paginatedQuery!.page = value ?? 0;
            widget.sideItem.currentPaginatedPageDetail = value;
            _getData(isNew: true);
          },
          selectedIndex: selectedTabIndex,
          horizontalMargin: widget.horizontalMargin,
          columnSpacing: widget.columnSpacing,
          columns: _columns,
          actions: [
            if (widget.showSearch) _searchWidget(constraints: constraints),
            if (widget.onAddButtonClicked != null && !showOnlySearchAction)
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: widget.addWidget ??
                      IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                          context: context,
                          style: IGButtonViewStyle(
                              icon: Icons.add_rounded,
                              tooltipMessage: "Add",
                              onTapPositioned: (position) async {
                                await widget.onAddButtonClicked!();
                              }))),
            if (showOnlySearchAction)
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: IGButtonViewDefaults.semiMediumButtonIconBackground(
                      context: context,
                      style: IGButtonViewStyle(
                          icon: Icons.close_rounded,
                          tooltipMessage: "Close",
                          backgroundColorSelected: resetValueField,
                          tintColorSelected: resetValueField,
                          onTapPositioned: (position) async {
                            showOnlySearchAction = false;
                            _searchTextEditingController.clear();
                            _stateKey.currentState!.pageTo(0, reset: true);
                          }))),
            if (widget.showColumn &&
                !showOnlySearchAction &&
                !widget.forceSmallContext &&
                !SizeDetector.isSmallScreen(context))
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                      context: context,
                      style: IGButtonViewStyle(
                          icon: Icons.view_column_rounded,
                          tooltipMessage: "Visible Columns",
                          onTapPositioned: (position) async {
                            await showMenuCustom(
                                color: IGColors.secondaryBackground(),
                                items: _calculateAllColumsPopupMenuButton(),
                                preferredWidth: 224,
                                position: position,
                                context: context,
                                elevation: 1.5,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        kButtonPrimaryCornerRadius)));
                          }))),
            if (widget.enableExporting &&
                !showOnlySearchAction &&
                widget.sideItem.paginatedQuery!.elements.isNotEmpty)
              Padding(
                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                      context: context,
                      style: IGButtonViewStyle(
                          icon: Icons.north_east_rounded,
                          tooltipMessage:
                              "Export ${widget.title}${(widget.tabItems != null) ? (" " + widget.tabItems![selectedTabIndex].exportTitle!) : ""}",
                          onTapPositioned: (position) async {
                            if (_isLoading || widget.forceLoading) {
                              return;
                            }
                            var title = widget.title ?? "";
                            if (widget.tabItems != null) {
                              title +=
                                  " ${widget.tabItems![selectedTabIndex].exportTitle}";
                            }
                            await Navigator.of(context).push(PageTransition(
                              type: PageTransitionType.upToDown,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.bounceOut,
                              child: ExportFormSheet<T>(
                                  elements:
                                      widget.sideItem.paginatedQuery!.elements,
                                  selectedTabIndex: selectedTabIndex,
                                  title: title,
                                  headerSortRules: List.of(
                                      widget.headerSortRules!,
                                      growable: true),
                                  searchQuery: _searchText()),
                            ));
                          }))),
            if (!showOnlySearchAction && !widget.hideRefreshButton)
              widget.refreshWidget ??
                  Padding(
                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                      child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                          context: context,
                          style: IGButtonViewStyle(
                              icon: Icons.replay_rounded,
                              tooltipMessage: "Reload ${widget.title}",
                              onTapPositioned: (position) async {
                                if (_isLoading) {
                                  return;
                                }
                                _getData(isNew: true);
                              }))),
          ],
          source: _dataTableViewDataSource!,
          sortAscending: _sortAsceding,
          sortColumnIndex: _sortColumnIndex,
        ),
      );
    });
  }

  //MAKR: EMPTY STATE
  _emptyStateWidgetDefault() {
    bool? isDuringSearch = _searchText().isNotEmpty;
    return SizeDetector.isSmallScreen(context) ||
            SizeDetector.isMediumScreen(context) ||
            widget.forceSmallContext
        ? Column(
            children: [
              Container(
                constraints: BoxConstraints(minHeight: 240),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    isDuringSearch!
                        ? Icon(Icons.search_rounded,
                            color: IGColors.text(), size: 50)
                        : Container(),
                    Text(
                      _isLoading || widget.forceLoading
                          ? "Loading..."
                          : isDuringSearch ||
                                  (widget.headerSortRules!.firstWhereOrNull(
                                          (element) => element.isFilterApplied(
                                              selectedTabIndex)) !=
                                      null)
                              ? "No items found with this search"
                              : "No items registered",
                      textAlign: TextAlign.center,
                      style: IGTextStyles.custom(
                          color: IGColors.text(), fontSize: 30),
                    ),
                  ],
                ),
              ),
            ],
          )
        : Column(
            children: [
              Container(
                constraints: BoxConstraints(minHeight: 240),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          isDuringSearch!
                              ? Icon(Icons.search_rounded,
                                  color: IGColors.text(), size: 32)
                              : Container(),
                          isDuringSearch ? Container(width: 10) : Container(),
                          Text(
                            _isLoading || widget.forceLoading
                                ? "Loading..."
                                : isDuringSearch ||
                                        (widget.headerSortRules!
                                                .firstWhereOrNull((element) =>
                                                    element.isFilterApplied(
                                                        selectedTabIndex)) !=
                                            null)
                                    ? "No items found with this search"
                                    : "No items registered",
                            style: IGTextStyles.custom(
                                color: IGColors.text(), fontSize: 30),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
  }

  //MARK: SEARCHING
  _searchText() {
    return _searchTextEditingController.text;
  }

  _searchWidget({required BoxConstraints constraints}) {
    if ((widget.sideItem.paginatedQuery?.elements == null ||
            widget.sideItem.paginatedQuery?.elements.isEmpty == true) &&
        _searchText().isEmpty) {
      return Container();
    }
    if (widget.searchWidget != null) {
      return widget.searchWidget;
    }
    return (SizeDetector.isSmallScreen(context) || widget.forceSmallContext) &&
            !showOnlySearchAction
        ? Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: IGButtonViewDefaults.semiMediumButtonIconOnlyHover(
                context: context,
                style: IGButtonViewStyle(
                    icon: Icons.search_rounded,
                    tooltipMessage: "Search ${widget.title}",
                    onTapPositioned: (position) async {
                      _setStateOnlyIfMounted(() {
                        showOnlySearchAction = true;
                      });
                      _searchFocusNode.requestFocus();
                    })),
          )
        : SearchField(
            width: 250,
            height: 40,
            focusNode: _searchFocusNode,
            controller: _searchTextEditingController,
            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
            hint: "Search ${widget.searchPlaceholder ?? widget.title}",
            onSubmitted: (searchString) async {
              if (!_isLoading) {
                _stateKey.currentState!.pageTo(0, reset: true);
              }
            });
  }

  //MARK: CALCULATE
  _sortVisibleHeaders() {
    visibleHeaders.sort((a, b) {
      return allHeaders.indexOf(a).compareTo(allHeaders.indexOf(b));
    });
  }

  _calculateRows() async {
    var temp = List<DataTableViewRow<T>>.from([], growable: true);
    widget.sideItem.paginatedQuery!.elements.forEach((element) {
      var temp1 = List<DataCellDisplay>.from([], growable: true);
      var json = element is JsonSerializable
          ? element.toJson()
          : element is Map
              ? element
              : {};

      visibleHeaders.forEach((rule) async {
        rule!.headerKey = rule.headerKey.trim();
        var v = json[rule.headerKey];
        var components = rule.headerKey.split(".");

        try {
          if (components.length > 1) {
            components.asMap().forEach((index, element) {
              if (index == 0) {
                v = json[element];
              } else {
                v = v is JsonSerializable ? v.toJson() : v;
                if (v != null) {
                  v = v[element];
                }
              }
            });
          }
        } catch (e) {
          logger(e);
        }
        temp1.add(DataCellDisplay(
            DataCell(
                await rule.cellDisplayRule!.widget(
                    context: context,
                    originalValue: v ?? null,
                    json: element,
                    headerRule: rule),
                onTap: null,
                placeholder: false,
                showEditIcon: false),
            rule));
      });
      var finalTemp = List<DataCell>.from([], growable: true);
      /*temp1.sort((a, b) {
        return allHeaders
            .indexOf(a.headerSortRule)
            .compareTo(allHeaders.indexOf(b.headerSortRule));
      });*/
      temp1.forEach((element) {
        finalTemp.add(element.dataCell);
      });
      var elementID = "";
      try {
        elementID = element is JsonSerializable
            ? element.toJson()["id"]
            : element is Map
                ? element["id"]
                : "";
      } catch (e) {
        logger(e);
      }
      DataTableViewRow<T> dataTableRow = DataTableViewRow<T>(
          elementID, finalTemp, element,
          smallContextWidget: widget.smallContextWidget != null
              ? widget.smallContextWidget!(element)
              : null,
          selected: selectedRows.where((e) => e.id == elementID).isNotEmpty
              ? true
              : false,
          onSelectChanged: widget.onRowClicked != null
              ? (index, value, row) async {
                  if (widget.isSelectionMode) {
                    setState(() {
                      if (widget.allowMultipleSelection) {
                        if (selectedRows.contains(row)) {
                          selectedRows.remove(row);
                        } else {
                          selectedRows.add(row);
                        }
                      } else {
                        selectedRows.clear();
                        selectedRows.add(row);
                      }
                    });
                  }
                  widget.onRowClicked!(element, value ?? false);
                }
              : null);
      temp.add(dataTableRow);
    });
    return temp;
  }

  _calculateAllHeaders({bool changeCurrentConfiguration = false}) {
    HeaderRule<T>? firstDefault;
    allHeaders.clear();
    visibleHeaders.clear();
    widget.headerSortRules!.forEach((element) {
      if (element.adjustValues != null) {
        element.adjustValues!(element);
      }
      if (firstDefault == null && element.isDefault) {
        firstDefault = element;
      }
      if (element.hiddenByDefault) {
        allHeaders.add(element);
      } else {
        visibleHeaders.add(element);
        allHeaders.add(element);
      }
    });
    if (firstDefault != null && changeCurrentConfiguration) {
      _sortAsceding = firstDefault?.isDefaultAsceding ?? false;
      _sortColumnIndex = visibleHeaders.indexOf(firstDefault);
      _sortColumnName = firstDefault?.sortKey ?? "";
    }
  }

  _calculateAllColums() {
    var finalTemp = List<DataColumn>.from([], growable: true);
    visibleHeaders.forEach((element) {
      finalTemp.add(DataColumn(
          label: Row(
            children: [
              if (element!.filterItems != null && element.showFilterIcon)
                IGButtonViewDefaults.mediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.filter_list_rounded,
                        iconColor: element.isFilterApplied(selectedTabIndex)
                            ? IGColors.primary()
                            : IGColors.text(),
                        tooltipMessage: "Filters",
                        onTapPositioned: (position) async {
                          await element.showFilterMenu(
                              selectedTabIndex: selectedTabIndex,
                              context: context,
                              position: position,
                              applyFilters: (originalFilter, visibleFilters) {
                                visibleFiltersForHeaderKey[selectedTabIndex]![
                                    element.filterKey ??
                                        element.headerKey] = visibleFilters;
                                _stateKey.currentState!.pageTo(0, reset: true);
                              });
                        })),
              if (element.filterItems != null) Container(width: 4),
              Text(element.displayValue ?? element.headerKey.titleCase,
                  style: IGTextStyles.custom(color: IGColors.text())),
              if (element.helpMenuItems != null) Container(width: 4),
              if (element.helpMenuItems != null)
                IGButtonViewDefaults.mediumButtonIconOnlyHover(
                    context: context,
                    style: IGButtonViewStyle(
                        icon: Icons.info_outline_rounded,
                        iconColor: IGColors.text(),
                        tooltipMessage: "Info",
                        onTapPositioned: (position) async {
                          if (element.originalHelpMenuItems == null) {
                            element.originalHelpMenuItems =
                                await element.helpMenuItems!();
                          }
                          await showMenuCustom(
                              items: element.originalHelpMenuItems,
                              color: IGColors.secondaryBackground(),
                              position: position,
                              context: context,
                              elevation: 1.5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      kButtonPrimaryCornerRadius)));
                        })),
            ],
          ),
          onSort: element.type == HeaderSortRuleType.none
              ? null
              : (columnIndex, ascending) {
                  _setStateOnlyIfMounted(() {
                    _sortAsceding = ascending;
                    _sortColumnIndex = columnIndex;
                    _sortColumnName = element.headerKey;
                    _getData(isNew: true);
                  });
                }));
    });
    return finalTemp;
  }

  _calculateAllColumsPopupMenuButton() {
    var finalTemp = List<PopupMenuEntry>.from([], growable: true);
    finalTemp.add(PopupMenuCustomItem(
        child: IGButtonRow(title: "Visible columns", height: 36),
        enabled: false));
    finalTemp.add(PopupMenuCustomItem(
        height: 1,
        child: Divider(
            thickness: 1,
            height: 1,
            endIndent: 0,
            indent: 0,
            color: IGColors.separator()),
        enabled: false));
    allHeaders.asMap().forEach((index, element) {
      finalTemp.add(PopupMenuCustomItem(
        enabled: false,
        child: StatefulBuilder(builder: (BuildContext c, StateSetter _set) {
          return IGButtonRow(
              height: 36,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              title: element!.displayValue ?? element.headerKey.titleCase,
              onTap: !element.isErasable
                  ? null
                  : () async {
                      if (!element.isErasable) {
                        return;
                      } else {
                        if (visibleHeaders.contains(element)) {
                          visibleHeaders.remove(element);
                        } else {
                          visibleHeaders.add(element);
                        }
                        _sortVisibleHeaders();
                        var findIndex = visibleHeaders.indexOf(HeaderRule<T>(
                            _sortColumnName,
                            _sortColumnName,
                            HeaderSortRuleType.custom));
                        if (findIndex == -1) {
                          _sortColumnIndex = 0;
                          _sortColumnName =
                              visibleHeaders.first?.headerKey ?? "";
                        } else {
                          _sortColumnIndex = visibleHeaders.indexOf(
                              HeaderRule<T>(_sortColumnName, _sortColumnName,
                                  HeaderSortRuleType.custom));
                        }
                        _set(() {});
                        _columns = _calculateAllColums();
                        _dataTableViewDataSource!
                            .setRows(_calculateRows(), selectedRows.length);
                        _setStateOnlyIfMounted(() {});
                      }
                    },
              trailing: (states) {
                return AbsorbPointer(
                  absorbing: true,
                  child: Checkbox(
                      hoverColor: Colors.transparent,
                      activeColor: IGColors.primary(),
                      value: visibleHeaders.contains(element),
                      onChanged: element.isErasable ? (value) {} : null),
                );
              });
        }),
      ));
      if (index != allHeaders.length - 1) {
        finalTemp.add(PopupMenuCustomItem(
            height: 1,
            child: Divider(
                thickness: 1,
                height: 1,
                endIndent: 8,
                indent: 8,
                color: IGColors.separator()),
            enabled: false));
      }
    });
    return finalTemp;
  }
}
