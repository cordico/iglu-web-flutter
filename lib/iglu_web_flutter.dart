/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

library iglu_web_flutter;

//MARK: CONTROLLERS----

//MARK: EXTENSIONS----AUTH----
export "./controllers/auth/forgot_password_screen.dart";
export "./controllers/auth/login_screen.dart";
export "./controllers/auth/reset_password_screen.dart";
export "./controllers/auth/verify_email_screen.dart";
export "./controllers/auth/login_code_screen.dart";

//MARK: EXTENSIONS----ROOT----

//MARK: EXTENSIONS----ROOT----NAVIGATOR----
export "./controllers/root/navigator/king_route_path.dart";
export "./controllers/root/navigator/king_route_delegate.dart";
export "./controllers/root/navigator/king_route_information_parser.dart";
export "./controllers/root/root.dart";
export "./controllers/root/king.dart";
export "./controllers/root/routes.dart";

//MARK: EXTENSIONS----SIDEBAR----
export "./controllers/sidebar/app_drawer.dart";
export "./controllers/sidebar/side_bar.dart";
export "./controllers/sidebar/side_item_manager.dart";
export "./controllers/sidebar/side_item.dart";
export "./controllers/sidebar/side_bar_style.dart";

//MARK: EXTENSIONS----UTIL----
export "./controllers/util/loading_controller.dart";
export "./controllers/util/empty_controller.dart";
export "./controllers/util/left_detail_container.dart";
export "./controllers/util/left_screen_container.dart";
export "./controllers/util/service_not_available_controller.dart";
export "./controllers/util/404.dart";
export "./controllers/util/too_small_controller.dart";

//MARK: EXTENSIONS----
export "./extensions/global_key_extension.dart";
export "./extensions/text_input_formatter_extension.dart";
export "./extensions/widget_extension.dart";
export "./extensions/number_extension.dart";

export "./extensions/array_utils.dart";
export "./extensions/coordinates_utils.dart";
export "./extensions/date_extension.dart";
export "./extensions/device_detector.dart";
export "./extensions/double_ext.dart";
export "./extensions/drop_cap_text.dart";
export "./extensions/int_ext.dart";
export "./extensions/masked_text_controller.dart";
export "./extensions/recase.dart";
export "./extensions/string_ext.dart";
export "./extensions/string_utils.dart";
export "./extensions/time_manager.dart";

//MARK: EXTENSIONS----SCROLLBEHAVIORS----
export "./extensions/scroll_behaviors/no_glowing_scroll_behavior.dart";

//MARK: EXTENSIONS----TRANSITIONS----
export "./extensions/transitions/animation_page.dart";
export "./extensions/transitions/page_transition.dart";

//MARK: HELPERS----
export "./helpers/colors/color_extension.dart";
export 'helpers/colors/color_coversion.dart';

export "./helpers/crypto_manager.dart";
export "./helpers/language.dart";
export "./helpers/session_handler.dart";
export "./helpers/colors.dart";
export "./helpers/ig_text_styles.dart";
export "./helpers/constants.dart";
export "./helpers/places_manager.dart";
export "./helpers/header_bar_change_notifier.dart";

//MARK: MODELS----

//MARK: MODELS----OBSERVER----
export "./models/observer/array_class_change_notifier.dart";
export "./models/observer/array_value_observer.dart";
export "./models/observer/map_value_observer.dart";
export "./models/observer/value_observer.dart";

//MARK: MODELS----CORE----
export "./models/core/attach.dart";
export "./models/core/config.dart";
export "./models/core/const.dart";
export "./models/core/user_schema_model.dart";

export "./models/core/ig_search/ig_paginated_query.dart";

//MARK: MODELS----NOTIFICATIONS----
export "./models/notifications/local_notification.dart";
export "./models/notifications/local_notification_change_notifier.dart";

//MARK: VIEWS----

//MARK: VIEWS----ALERTS----
export "./widgets/alerts/alert_view.dart";
export "./widgets/alerts/flush.dart";

//MARK: VIEWS----BARS----
export "./widgets/bars/footer_bar.dart";
export "./widgets/bars/header_bar.dart";

//MARK: VIEWS----BUTTONS----
export "./widgets/buttons/ig_button_row.dart";
export "./widgets/buttons/ig_button_view_style.dart";
export "./widgets/buttons/ig_button_view.dart";

//MARK: VIEWS----FORMS----
export "./widgets/forms/change_password_form_sheet.dart";
export "./widgets/forms/feedback_form_sheet.dart";
export "./widgets/forms/form_sheet.dart";
export "./widgets/forms/date_range_picker_form_sheet.dart";

//MARK: VIEWS----IMAGES----
export "./widgets/images/image_detail_screen.dart";
export "./widgets/images/image_container_view.dart";
export "./widgets/images/image_gallery_view.dart";

//MARK: VIEWS----KEYBOARD----
export "./widgets/keyboard/ig_keyboard_listener.dart";

//MARK: VIEWS----LAYOUTS----
export './widgets/layouts/ig_layout_item_type.dart';
export './widgets/layouts/ig_layout_item.dart';
export './widgets/layouts/ig_layout.dart';
export './widgets/layouts/ig_view.dart';
export './widgets/layouts/ig_bouncing_view.dart';
export './widgets/layouts/model/ig_hover_type.dart';
export "./widgets/layouts/model/ig_view_state.dart";

//MARK: VIEWS----MENU----
export "./widgets/menu/popup_menu_custom.dart";

//MARK: VIEWS----TABLES----
export "./widgets/tables/model/cell_display_rule.dart";
export "./widgets/tables/model/filter_rule.dart";
export "./widgets/tables/model/header_rule.dart";
export "./widgets/tables/model/data_cell_display.dart";
export "./widgets/tables/custom_paginated_data_table.dart";
export "./widgets/tables/data_table_view.dart";
export "./widgets/tables/data_table_view_paginated.dart";
export "./widgets/tables/export_form_sheet.dart";

//MARK: VIEWS----TEXT----
export "./widgets/text/ig_selectable_text.dart";

//MARK: VIEWS----TREE----
export "./widgets/tree/primitives/key_provider.dart";
export "./widgets/tree/primitives/tree_controller.dart";
export "./widgets/tree/primitives/tree_node.dart";
export "./widgets/tree/builder.dart";
export "./widgets/tree/copy_tree_nodes.dart";
export "./widgets/tree/node_widget.dart";
export "./widgets/tree/tree_view.dart";

//MARK: VIEWS----TEXTFIELDS----
export "./widgets/text_fields/search_field.dart";
export "./widgets/text_fields/code_input_text_field.dart";
export "./widgets/text_fields/ig_form_container.dart";
export "./widgets/text_fields/dynamic_text_highlighting.dart";
export './widgets/text_fields/form_field/form_text_form_field.dart';
export './widgets/text_fields/form_field/form_text_form_field_style.dart';
export "./widgets/text_fields/form_field/validators/form_field_validator.dart";
export './widgets/text_fields/form_field/suggestion_box.dart';

export './widgets/text_fields/json_schema/json_prettifier.dart';
export './widgets/text_fields/json_schema/json_schema_detail.dart';
export './widgets/text_fields/json_schema/json_schema.dart';

export './widgets/text_fields/markdown/format_markdown.dart';

//MARK: VIEWS----UTILS

//MARK: VIEWS----UTILS----LANGUAGES----
export "./widgets/utils/languages/language_popup.dart";

export "./widgets/utils/auto_size_text.dart";
export "./widgets/utils/badge_icon_widget.dart";
export "./widgets/utils/breadcrumb.dart";
export "./widgets/utils/circle_check_box.dart";
export "./widgets/utils/detail_view.dart";
export "./widgets/utils/dotted_line.dart";
export "./widgets/utils/hour_row.dart";
export "./widgets/utils/logo_image.dart";
export "./widgets/utils/widgets_defaults.dart";

//MARK: ------
export 'package:uuid/uuid.dart';
export 'package:linked_scroll_controller/linked_scroll_controller.dart';
export 'package:google_fonts/google_fonts.dart';
export "package:url_launcher/url_launcher.dart";
export 'package:url_launcher/link.dart';
export 'package:optimized_cached_image/optimized_cached_image.dart';
export 'package:flutter_markdown/flutter_markdown.dart';
export 'package:flutter_dotenv/flutter_dotenv.dart';
