/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';
import 'package:collection/collection.dart' show IterableExtension;

enum EnabledRoutes {
  login,
  reset_password,
  forgot_password,
  sideitem,
  verify_email,
  verify_code
}

extension EnabledRoutesString on EnabledRoutes {
  String get value {
    switch (this) {
      case EnabledRoutes.login:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/login";
      case EnabledRoutes.reset_password:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/reset";
      case EnabledRoutes.forgot_password:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/forgot";
      case EnabledRoutes.sideitem:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/";
      case EnabledRoutes.verify_email:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/verify/email";
      case EnabledRoutes.verify_code:
        return "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/verify/code";
    }
  }
}

extension EnabledRoutesValidation on EnabledRoutes {
  bool checkIfPathIsARoute(String path, String route) {
    var pathUri = Uri.tryParse("https://www.iglu.dev" + path);
    if (pathUri == null) {
      return false;
    }

    var routeComponents = route.split("/");
    routeComponents.removeWhere((element) => element.isEmpty);

    var components = pathUri.pathSegments;

    if (components.length >= routeComponents.length) {
      var result = true;
      routeComponents.forEachIndexed((index, element) {
        if (components[index] != element) {
          result = false;
        }
      });
      return result;
    }

    return false;
  }

  bool isPathValid(String? path) {
    if (path == null || path.isEmpty) {
      return false;
    }

    switch (this) {
      case EnabledRoutes.login:
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.login) &&
            checkIfPathIsARoute(path, EnabledRoutes.login.value) &&
            webConfigurator.internalConfigurator.loggedInUser == null);
      case EnabledRoutes.reset_password:
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.reset_password) &&
            checkIfPathIsARoute(path, EnabledRoutes.reset_password.value) &&
            webConfigurator.internalConfigurator.loggedInUser == null);
      case EnabledRoutes.forgot_password:
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.forgot_password) &&
            checkIfPathIsARoute(path, EnabledRoutes.forgot_password.value) &&
            webConfigurator.internalConfigurator.loggedInUser == null);
      case EnabledRoutes.sideitem:
        var sideItem = webConfigurator.sideItemManager.sideItems.hasPath(path);
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.sideitem) &&
            sideItem != null);
      case EnabledRoutes.verify_email:
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.verify_email) &&
            checkIfPathIsARoute(path, EnabledRoutes.verify_email.value) &&
            webConfigurator.internalConfigurator.loggedInUser != null);
      case EnabledRoutes.verify_code:
        return (webConfigurator.internalConfigurator.enabledRoutes
                .contains(EnabledRoutes.verify_code) &&
            checkIfPathIsARoute(path, EnabledRoutes.verify_code.value) &&
            webConfigurator.isDuringLoginCodeProcess == true &&
            webConfigurator.internalConfigurator.loggedInUser == null);
    }
  }
}

extension EnabledRoutesExtension on EnabledRoutes {
  static Object materialPage(
      {required KingRoutePath settings, Key? key, bool? maintainState}) {
    var child = EnabledRoutesExtension.widget(settings);
    return AnimationPage(
        valueKey: key, child: child as Widget?, maintainState: maintainState);
  }

  static Object widget(KingRoutePath settings) {
    if (EnabledRoutes.login.isPathValid(settings.routeInformation.location)) {
      return LoginScreen();
    } else if (EnabledRoutes.forgot_password
        .isPathValid(settings.routeInformation.location)) {
      return ForgotPasswordScreen();
    } else if (EnabledRoutes.reset_password
        .isPathValid(settings.routeInformation.location)) {
      return ResetPasswordScreen();
    } else if (EnabledRoutes.verify_email
        .isPathValid(settings.routeInformation.location)) {
      return VerifyEmailScreen();
    } else if (EnabledRoutes.verify_code
        .isPathValid(settings.routeInformation.location)) {
      return LoginCodeScreen();
    } else if (EnabledRoutes.sideitem
        .isPathValid(settings.routeInformation.location)) {
      var sideItem = webConfigurator.sideItemManager.sideItems
          .hasPath(settings.routeInformation.location ?? "")!;
      var pathSegments = settings.routeInformation.location!.split("/");
      pathSegments.removeWhere((element) => element.isEmpty);
      return Root(
          sideItem: sideItem,
          loadDetails: sideItem.hasDetail &&
                  pathSegments.length ==
                      sideItem.numberOfPathSegment! +
                          webConfigurator.internalConfigurator
                              .numberOfPathSegmentOfMainRoute
              ? true
              : false);
    } /*else if (webConfigurator.internalConfigurator.onGenerateRouteCustom !=
        null) {
      return webConfigurator.internalConfigurator
          .onGenerateRouteCustom(settings);
    }*/
    else {
      if (webConfigurator.internalConfigurator.show404Page &&
          settings.routeInformation.location != "/") {
        return Controller404();
      }
      return webConfigurator.internalConfigurator.loggedInUser == null
          ? LoginScreen()
          : Root();
    }
  }

  static Future<KingRoutePath> kingRoutePath(
      {required RouteInformation routeInformation}) async {
    logger(routeInformation.location, tag: "KingRoute");
    if (webConfigurator.internalConfigurator.showEntryPointKing &&
        webConfigurator.internalConfigurator.entryPointWidget != null) {
      logger("entry point route", tag: "KingRoute");
      var information = routeInformation;
      if (webConfigurator.internalConfigurator.clearEntryPointWidget != null) {
        information = RouteInformation(
            location: await webConfigurator.internalConfigurator
                .clearEntryPointWidget!(information.location));
      }
      return KingRoutePath(routeInformation: information);
    }
    if (webConfigurator.internalConfigurator.remoteConfig == null ||
        !webConfigurator.internalConfigurator.loadedSession) {
      logger("loading route", tag: "KingRoute");
      return KingRoutePath(routeInformation: routeInformation);
    }
    if (EnabledRoutes.login.isPathValid(routeInformation.location)) {
      logger("login route", tag: "KingRoute");
      return KingRoutePath.loginPath(routeInformation.location);
    } else if (EnabledRoutes.forgot_password
        .isPathValid(routeInformation.location)) {
      logger("forgot route", tag: "KingRoute");
      return KingRoutePath.forgotPassword;
    } else if (EnabledRoutes.reset_password
        .isPathValid(routeInformation.location)) {
      logger("reset route", tag: "KingRoute");
      return KingRoutePath.resetPasswordPath(routeInformation.location);
    } else if (EnabledRoutes.verify_email
        .isPathValid(routeInformation.location)) {
      logger("verify email route", tag: "KingRoute");
      return KingRoutePath.verifyEmail;
    } else if (EnabledRoutes.verify_code
        .isPathValid(routeInformation.location)) {
      logger("verify code route", tag: "KingRoute");
      return KingRoutePath.verifyCode;
    } else if (EnabledRoutes.sideitem.isPathValid(routeInformation.location)) {
      logger("${routeInformation.location} route", tag: "KingRoute");
      return KingRoutePath.routePath(routeInformation.location);
    } else {
      logger("UNKNOWN PATH", tag: "KingRoute");
      return KingRoutePath(
          routeInformation: webConfigurator.internalConfigurator.clearUnknownURL
              ? RouteInformation(
                  location: webConfigurator.internalConfigurator.loggedInUser ==
                              null &&
                          webConfigurator.internalConfigurator.remoteConfig !=
                              null
                      ? EnabledRoutes.login.value
                      : webConfigurator.internalConfigurator
                          .mainRouteToDisplay(forceNotEmpty: true))
              : routeInformation);
    }
  }

  static bool isUnknownPath({required RouteInformation routeInformation}) {
    logger(routeInformation.location, tag: "KingRoute");
    if (EnabledRoutes.login.isPathValid(routeInformation.location)) {
      return false;
    } else if (EnabledRoutes.forgot_password
        .isPathValid(routeInformation.location)) {
      return false;
    } else if (EnabledRoutes.reset_password
        .isPathValid(routeInformation.location)) {
      return false;
    } else if (EnabledRoutes.verify_email
        .isPathValid(routeInformation.location)) {
      return false;
    } else if (EnabledRoutes.verify_code
        .isPathValid(routeInformation.location)) {
      return false;
    } else if (EnabledRoutes.sideitem.isPathValid(routeInformation.location)) {
      return false;
    } else {
      return true;
    }
  }
}
