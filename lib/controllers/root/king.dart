/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:flutter_localizations/flutter_localizations.dart";
import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:iglu_web_flutter/helpers/localizations.dart';
import 'package:iglu_web_flutter/helpers/restoration_change_notifier.dart';
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:provider/provider.dart';

class King extends StatefulWidget {
  @override
  KingState createState() => KingState();
}

class KingState extends State<King> with WidgetsBindingObserver {
  Future<Map<String, dynamic>>? loadingConfigSession;
  @override
  void initState() {
    super.initState();
    setUrlStrategy(PathUrlStrategy());
    webConfigurator.kingState = this;
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  static configureAppereance(BuildContext context) async {
    if (!webConfigurator.initialAppereanceConfigured) {
      webConfigurator.initialAppereanceConfigured = true;
      if (webConfigurator.hivePreferencesBox.containsKey("appearance")) {
        var appearance = webConfigurator.hivePreferencesBox.get("appearance");
        webConfigurator.currentBrightness = appearance == "dark"
            ? Brightness.dark
            : appearance == "light"
                ? Brightness.light
                : WidgetsBinding.instance!.window.platformBrightness;
      } else {
        webConfigurator.currentBrightness =
            WidgetsBinding.instance!.window.platformBrightness;
      }
      webConfigurator.kingState?.setState(() {});
      webConfigurator.rootState?.setState(() {});
    }
  }

  refresh() {
    if (mounted) {
      setState(() {});
    }
  }

  static populateLocalStorage(BuildContext context) async {
    if (webConfigurator.hivePreferencesBox.containsKey("isLeftMenuExpanded")) {
      webConfigurator.isLeftMenuExpanded =
          webConfigurator.hivePreferencesBox.get("isLeftMenuExpanded");
    }
  }

  @override
  Widget build(BuildContext context) {
    KingState.configureAppereance(context);
    KingState.populateLocalStorage(context);
    return MultiProvider(
      providers: (webConfigurator.internalConfigurator.providers) +
          [
            ChangeNotifierProvider<LocalNotificationsChangeNotifier>(
                create: (_) => LocalNotificationsChangeNotifier()),
            ChangeNotifierProvider<RestorationChangeNotifier>(
                create: (_) => RestorationChangeNotifier()),
            ChangeNotifierProvider<HeaderBarChangeNotifier>(
                create: (_) => HeaderBarChangeNotifier()),
          ],
      child: MaterialApp.router(
        color: IGColors.primary(),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            textTheme: webConfigurator.internalConfigurator.customFont != null
                ? webConfigurator.internalConfigurator
                    .customFont!(null, Theme.of(context).textTheme).item2
                : Theme.of(context).textTheme,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            brightness: webConfigurator.currentBrightness,
            colorScheme: ColorScheme.light(
              brightness: webConfigurator.currentBrightness,
              primary: IGColors.primary(),
            ),
            primaryColor: IGColors.primary()),
        darkTheme: ThemeData(
            textTheme: webConfigurator.internalConfigurator.customFont != null
                ? webConfigurator.internalConfigurator
                    .customFont!(null, Theme.of(context).textTheme).item2
                : Theme.of(context).textTheme,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            brightness: webConfigurator.currentBrightness,
            colorScheme: ColorScheme.dark(
              brightness: webConfigurator.currentBrightness,
              primary: IGColors.primary(),
            ),
            primaryColor: IGColors.primary()),
        localizationsDelegates: List<LocalizationsDelegate<dynamic>>.from([
              KingLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              FallbackCupertinoLocalisationsDelegate()
            ] +
            webConfigurator.internalConfigurator.localizationsDelegates
                .toList()),
        supportedLocales: Set<Locale>.from(
                KingLocalizationsDelegate().supportedLocale() +
                    webConfigurator.internalConfigurator.supportedLocales)
            .toList(),
        routerDelegate: webConfigurator.kingRouteDelegate,
        routeInformationParser: KingRouteInformationParser(),
      ),
    );
  }
}
