/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";

class Root extends StatefulWidget {
  final SideItem? sideItem;
  final bool loadDetails;
  Root({Key? key, this.sideItem, this.loadDetails = false}) : super(key: key);
  @override
  RootState createState() => RootState();
}

class RootState extends State<Root> {
  @override
  void initState() {
    super.initState();
    webConfigurator.rootState = this;
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      if (webConfigurator.internalConfigurator.primaryColor != null) {
        webConfigurator.kingState!.refresh();
      }
    });
  }

  refresh() {
    if (mounted) {
      setState(() {});
    }
  }

  String? title = "";
  changeTitle(String? newTitle, {bool notSetState = true}) {
    if (newTitle != title) {
      if (mounted && !notSetState) {
        setState(() {
          title = newTitle;
        });
      } else {
        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
          if (mounted) {
            setState(() {
              title = newTitle;
            });
          }
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    webConfigurator.mainContext = context;
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : Title(
            title: title ?? webConfigurator.internalConfigurator.appName!,
            color: IGColors.primary(),
            child: SizeDetector.isTooSmall(context)
                ? TooSmallController()
                : Scaffold(
                    resizeToAvoidBottomInset: true,
                    body: Row(
                      children: <Widget>[
                        if (SizeDetector.isLargeScreen(context) ||
                            SizeDetector.isMediumScreen(context))
                          SideBar(),
                        if ((SizeDetector.isLargeScreen(context) ||
                            SizeDetector.isMediumScreen(context)))
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 1.0,
                                height: 54,
                                color: IGColors.primaryBackground(),
                              ),
                              Expanded(
                                child: Container(
                                  width: 1.0,
                                  decoration: BoxDecoration(
                                      border: BorderDirectional(
                                          end: BorderSide(
                                              color: IGColors.separator(),
                                              width: 1.0))),
                                ),
                              ),
                            ],
                          ),
                        Flexible(
                          child: Scaffold(body: _returnScreen()),
                        )
                      ],
                    )),
          );
  }

  Widget _returnScreen() {
    if (widget.sideItem != null) {
      return Container(
          child: widget.loadDetails
              ? widget.sideItem!.detailWidget
              : widget.sideItem!.screen);
    } else {
      return Container(
        child: EmptyController(),
      );
    }
  }
}
