/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/widgets.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class KingRouteInformationParser extends RouteInformationParser<KingRoutePath> {
  @override
  Future<KingRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    logger("PARSE ROUTE INFORMATION");
    return EnabledRoutesExtension.kingRoutePath(
        routeInformation: routeInformation);
  }

  @override
  RouteInformation restoreRouteInformation(KingRoutePath path) {
    logger("RESTORE ROUTE INFORMATION");
    return path.routeInformation;
  }
}
