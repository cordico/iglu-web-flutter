/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/widgets.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class KingRoutePath {
  RouteInformation routeInformation;
  final SideItem? sideItem;
  final bool isDetails;
  final EnabledRoutes? route;
  final String? tabName;

  KingRoutePath(
      {required this.routeInformation,
      this.sideItem,
      this.tabName,
      this.isDetails = false,
      this.route}) {
    var information = routeInformation;
    if (!information.location!.startsWith(webConfigurator.internalConfigurator
        .mainRouteToDisplay(forceNotEmpty: true))) {
      routeInformation = RouteInformation(
          state: information.state,
          location: webConfigurator.internalConfigurator.mainRouteToDisplay() +
              information.location!);
    }
  }

  static KingRoutePath loginPath(String? path) {
    return KingRoutePath(
        tabName: "Enter | ${webConfigurator.internalConfigurator.appName}",
        isDetails: false,
        route: EnabledRoutes.login,
        routeInformation: RouteInformation(location: path));
  }

  static KingRoutePath resetPasswordPath(String? path) {
    return KingRoutePath(
        tabName:
            "New Password | ${webConfigurator.internalConfigurator.appName}",
        isDetails: false,
        route: EnabledRoutes.reset_password,
        routeInformation: RouteInformation(location: path));
  }

  static KingRoutePath verifyCodePath(String path) {
    return KingRoutePath(
        tabName: "Verify OTP | ${webConfigurator.internalConfigurator.appName}",
        isDetails: false,
        route: EnabledRoutes.verify_code,
        routeInformation: RouteInformation(location: path));
  }

  static KingRoutePath verifyEmailPath(String path) {
    return KingRoutePath(
        tabName:
            "Verify E-Mail | ${webConfigurator.internalConfigurator.appName}",
        isDetails: false,
        route: EnabledRoutes.verify_email,
        routeInformation: RouteInformation(location: path));
  }

  static KingRoutePath routePath(String? path) {
    String? tabName;
    var sideItem = webConfigurator.sideItemManager.sideItems.hasPath(path);
    if (sideItem != null) {
      var split = path!.split("/");
      split.removeWhere((element) => element.isEmpty);
      if (sideItem.hasDetail &&
          split.length ==
              sideItem.numberOfPathSegment! +
                  webConfigurator
                      .internalConfigurator.numberOfPathSegmentOfMainRoute) {
        tabName =
            "${split.last} | ${webConfigurator.internalConfigurator.appName}";
      } else {
        tabName =
            "${sideItem.title} | ${webConfigurator.internalConfigurator.appName}";
      }
    }
    return KingRoutePath(
        tabName: tabName,
        sideItem: sideItem,
        isDetails: sideItem != null && sideItem.hasDetail ? true : false,
        routeInformation: RouteInformation(location: path));
  }

  ////////
  static KingRoutePath get root {
    return KingRoutePath(
        isDetails: false,
        routeInformation: RouteInformation(
            location: webConfigurator.internalConfigurator
                .mainRouteToDisplay(forceNotEmpty: true)));
  }

  static KingRoutePath get login {
    return KingRoutePath.loginPath(EnabledRoutes.login.value);
  }

  static KingRoutePath get forgotPassword {
    return KingRoutePath(
        tabName:
            "Reset Password | ${webConfigurator.internalConfigurator.appName}",
        isDetails: false,
        route: EnabledRoutes.forgot_password,
        routeInformation:
            RouteInformation(location: EnabledRoutes.forgot_password.value));
  }

  static KingRoutePath get resetPassword {
    return KingRoutePath.resetPasswordPath(EnabledRoutes.reset_password.value);
  }

  static KingRoutePath get verifyCode {
    return KingRoutePath.verifyCodePath(EnabledRoutes.verify_code.value);
  }

  static KingRoutePath get verifyEmail {
    return KingRoutePath.verifyEmailPath(EnabledRoutes.verify_email.value);
  }
}
