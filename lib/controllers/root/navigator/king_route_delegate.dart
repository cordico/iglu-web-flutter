/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:flutter/material.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class KingRouteDelegate extends RouterDelegate<KingRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<KingRoutePath> {
  final GlobalKey<NavigatorState> navigatorKey;

  KingRouteDelegate() : navigatorKey = GlobalKey<NavigatorState>();

  KingRoutePath? currentPath;

  @override
  KingRoutePath? get currentConfiguration {
    return currentPath;
  }

  bool get showLoadingScreen {
    return (webConfigurator.internalConfigurator.remoteConfig == null ||
        !webConfigurator.internalConfigurator.loadedSession);
  }

  List<Page> getPages(KingRoutePath? path) {
    List<Page> pages = [];
    if (webConfigurator.internalConfigurator.showEntryPointKing &&
        webConfigurator.internalConfigurator.entryPointWidget != null) {
      return [
        MaterialPage(
            child: webConfigurator.internalConfigurator.entryPointWidget!,
            maintainState: false)
      ];
    }
    if (showLoadingScreen) {
      pages.add(MaterialPage(child: LoadingController(), maintainState: false));
    }
    if (path != null && !showLoadingScreen) {
      pages.add(EnabledRoutesExtension.materialPage(
          settings: path,
          maintainState:
              path.sideItem != null && !path.isDetails ? true : false,
          key: ValueKey(path.routeInformation.location)) as Page<dynamic>);
    }
    if (pages.isEmpty) {
      pages.add(MaterialPage(child: Controller404(), maintainState: false));
    }
    return pages;
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: getPages(currentPath),
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }
        return true;
      },
    );
  }

  goBack() {
    var current = currentConfiguration!.routeInformation.location ?? "";
    var ar = current.split("/");
    if (ar.length > 0) {
      ar.removeLast();
      current = ar.join("/");
    }
    setNewRoutePath(KingRoutePath.routePath(current));
  }

  @override
  Future<void> setNewRoutePath(KingRoutePath path) async {
    currentPath = path;
    if (webConfigurator.rootState != null) {
      webConfigurator.rootState!.changeTitle(currentPath!.tabName);
    }
    refresh();
    return;
  }

  refresh() {
    notifyListeners();
  }
}
