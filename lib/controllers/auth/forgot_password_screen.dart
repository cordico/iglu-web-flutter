/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  bool isLoading = false;
  bool isEmailValid = true;
  bool isPasswordValid = true;
  bool isLoginButtonEnabled = false;
  bool isAppleSignInAvailable = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      KingState.configureAppereance(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : Title(
            title: webConfigurator
                    .kingRouteDelegate.currentConfiguration!.tabName ??
                webConfigurator.internalConfigurator.appName!,
            color: IGColors.primary(),
            child: Scaffold(
              backgroundColor: IGColors.primaryBackground(),
              body: SafeArea(
                bottom: false,
                child: LayoutBuilder(
                  builder: (context, _) {
                    return GestureDetector(
                      onTap: () => FocusScope.of(context).unfocus(),
                      child: Builder(
                          builder: (ctx) => Stack(
                                  alignment: webConfigurator
                                              .internalConfigurator
                                              .showRandomImageInTheLogin &&
                                          !SizeDetector.isSmallScreen(context)
                                      ? Alignment.centerLeft
                                      : Alignment.center,
                                  children: [
                                    webConfigurator.internalConfigurator
                                            .showRandomImageInTheLogin
                                        ? OptimizedCacheImage(
                                            imageUrl:
                                                "https://source.unsplash.com/random/${MediaQuery.of(context).size.width}x${MediaQuery.of(context).size.height}")
                                        : Container(),
                                    SingleChildScrollView(
                                      child: Form(
                                        key: _formKey,
                                        child: Padding(
                                          padding: EdgeInsets.all(
                                              SizeDetector.isSmallScreen(
                                                      context)
                                                  ? 20
                                                  : 40.0),
                                          child: AbsorbPointer(
                                            absorbing: isLoading,
                                            child: LayoutBuilder(builder:
                                                (context, constraints) {
                                              return Container(
                                                constraints: BoxConstraints(
                                                    maxWidth: 600,
                                                    minHeight: 400),
                                                padding:
                                                    EdgeInsets.only(top: 30),
                                                decoration: BoxDecoration(
                                                    color: IGColors
                                                        .secondaryBackground(),
                                                    boxShadow: webConfigurator
                                                            .internalConfigurator
                                                            .showRandomImageInTheLogin
                                                        ? [
                                                            BoxShadow(
                                                              color:
                                                                  IGColors.text(
                                                                      opacity:
                                                                          0.26),
                                                              blurRadius: 6,
                                                            )
                                                          ]
                                                        : [
                                                            BoxShadow(
                                                              color: IGColors
                                                                  .separator(
                                                                      opacity:
                                                                          0.1),
                                                              blurRadius: 3,
                                                            )
                                                          ],
                                                    border: webConfigurator
                                                            .internalConfigurator
                                                            .showRandomImageInTheLogin
                                                        ? null
                                                        : Border.all(
                                                            color: IGColors
                                                                .separator()),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            kButtonPrimaryCornerRadius)),
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: EdgeInsets.symmetric(
                                                          horizontal: SizeDetector
                                                                  .isSmallScreen(
                                                                      context)
                                                              ? 20
                                                              : 40.0),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .stretch,
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              Container(
                                                                  width: 35,
                                                                  height: 35),
                                                              LogoImage(
                                                                width: 50,
                                                                height: 50,
                                                                lightOnly: webConfigurator
                                                                        .currentBrightness ==
                                                                    Brightness
                                                                        .light,
                                                                darkOnly: webConfigurator
                                                                        .currentBrightness ==
                                                                    Brightness
                                                                        .dark,
                                                              ),
                                                              Container(
                                                                  width: 35,
                                                                  height: 35),
                                                            ],
                                                          ),
                                                          Container(height: 20),
                                                          Text(
                                                            webConfigurator
                                                                .internalConfigurator
                                                                .appName!,
                                                            style: IGTextStyles
                                                                .semiboldStyle(
                                                                    24,
                                                                    IGColors
                                                                        .text()),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                          Container(height: 20),
                                                          Text(
                                                            "Reset your password",
                                                            textAlign: TextAlign
                                                                .center,
                                                            style: IGTextStyles
                                                                .normalStyle(
                                                                    18,
                                                                    IGColors
                                                                        .text()),
                                                          ),
                                                          Container(height: 30),
                                                          Text(
                                                            "Email",
                                                            style: IGTextStyles
                                                                .semiboldStyle(
                                                                    18,
                                                                    IGColors
                                                                        .text()),
                                                          ),
                                                          returnEmailTextFormField(),
                                                          Container(height: 20),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              IGButtonViewDefaults
                                                                  .defaultTextHoverButton(
                                                                      context:
                                                                          context,
                                                                      style: IGButtonViewStyle(
                                                                          text: "Back to login",
                                                                          onTapPositioned: (position) async {
                                                                            _back();
                                                                          })),
                                                            ],
                                                          ),
                                                          Container(height: 20),
                                                          IGButtonViewDefaults.primaryDefaultButton(
                                                              context: context,
                                                              style: IGButtonViewStyle(
                                                                  text: "RESET PASSWORD",
                                                                  textStyle: IGTextStyles.boldStyle(16, Colors.white),
                                                                  onTapPositioned: !isLoginButtonEnabled
                                                                      ? null
                                                                      : (pos) async {
                                                                          await _action();
                                                                        })),
                                                          Container(height: 30),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ])),
                    );
                  },
                ),
              ),
            ),
          );
  }

  Widget returnEmailTextFormField() {
    return TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      autofillHints: [AutofillHints.email],
      cursorColor: webConfigurator.currentBrightness == Brightness.dark
          ? Colors.white
          : IGColors.primary(),
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      validator:
          IGFormFieldValidator(type: IGFormFieldValidatorType.email).validate,
      textInputAction: TextInputAction.done,
      onFieldSubmitted: (value) async {
        await _action();
      },
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: isEmailValid,
          hint: "Enter the email for which you have forgotten the password"),
      onChanged: _onEmailChanged,
    );
  }

  _back() {
    webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
  }

  _onEmailChanged(String text) {
    setState(() {
      isEmailValid = text.isEmpty || text.isEmail;
      isLoginButtonEnabled = (isEmailValid && _emailController.text.isNotEmpty);
    });
  }

  _action() async {
    setState(() => isLoading = true);
    if (this._formKey.currentState!.validate()) {
      //TODD: FORGOT PASSWORD
      var resultMap = {}; //await Shared.queryManager
      //.forgotPasswordUser(_emailController.text, isFromWeb: true);
      setState(() => isLoading = false);
      if (resultMap["error"] == null) {
        await AlertView.showSimpleDialog(context,
            dismissable: false,
            title: "Email correctly sent",
            onClose: () {
              _back();
            },
            message: "Check your email to complete the password reset process",
            okActionPrimaryColor: IGColors.primary(),
            okAction: () {
              _back();
            });
      } else {
        await AlertView.showSimpleErrorDialog(
            context: context, message: resultMap["error"]);
      }
    }
  }
}
