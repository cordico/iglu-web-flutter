/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
///

import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class LoginCodeScreen extends StatefulWidget {
  @override
  _LoginCodeScreenState createState() => _LoginCodeScreenState();
}

class _LoginCodeScreenState extends State<LoginCodeScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _codeController = TextEditingController();
  bool isLoading = false;
  bool isCodeValid = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      KingState.configureAppereance(context);
      if (webConfigurator.isDuringLoginCodeProcess == false) {
        webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : Title(
            title: webConfigurator
                    .kingRouteDelegate.currentConfiguration!.tabName ??
                webConfigurator.internalConfigurator.appName!,
            color: IGColors.primary(),
            child: Scaffold(
              backgroundColor: IGColors.primaryBackground(),
              body: SafeArea(
                bottom: false,
                child: LayoutBuilder(
                  builder: (context, _) {
                    return GestureDetector(
                      onTap: () => FocusScope.of(context).unfocus(),
                      child: Builder(
                        builder: (ctx) => Stack(
                          alignment: webConfigurator.internalConfigurator
                                      .showRandomImageInTheLogin &&
                                  !SizeDetector.isSmallScreen(context)
                              ? Alignment.centerLeft
                              : Alignment.center,
                          children: [
                            webConfigurator.internalConfigurator
                                    .showRandomImageInTheLogin
                                ? OptimizedCacheImage(
                                    imageUrl:
                                        "https://source.unsplash.com/random/${MediaQuery.of(context).size.width}x${MediaQuery.of(context).size.height}")
                                : Container(),
                            SingleChildScrollView(
                              child: Form(
                                key: _formKey,
                                child: Padding(
                                  padding: EdgeInsets.all(
                                      SizeDetector.isSmallScreen(context)
                                          ? 20
                                          : 40.0),
                                  child: AbsorbPointer(
                                    absorbing: isLoading,
                                    child: LayoutBuilder(
                                        builder: (context, constraints) {
                                      return Container(
                                        constraints: BoxConstraints(
                                            maxWidth: 400, minHeight: 400),
                                        padding: EdgeInsets.only(top: 30),
                                        decoration: BoxDecoration(
                                            color:
                                                IGColors.secondaryBackground(),
                                            boxShadow: webConfigurator
                                                    .internalConfigurator
                                                    .showRandomImageInTheLogin
                                                ? [
                                                    BoxShadow(
                                                      color: IGColors.text(
                                                          opacity: 0.26),
                                                      blurRadius: 6,
                                                    )
                                                  ]
                                                : [
                                                    BoxShadow(
                                                      color: IGColors.separator(
                                                          opacity: 0.1),
                                                      blurRadius: 3,
                                                    )
                                                  ],
                                            border: webConfigurator
                                                    .internalConfigurator
                                                    .showRandomImageInTheLogin
                                                ? null
                                                : Border.all(
                                                    color:
                                                        IGColors.separator()),
                                            borderRadius: BorderRadius.circular(
                                                kButtonPrimaryCornerRadius)),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: SizeDetector
                                                          .isSmallScreen(
                                                              context)
                                                      ? 20
                                                      : 40.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Container(
                                                          width: 35,
                                                          height: 35),
                                                      LogoImage(
                                                        width: 50,
                                                        height: 50,
                                                        lightOnly: webConfigurator
                                                                .currentBrightness ==
                                                            Brightness.light,
                                                        darkOnly: webConfigurator
                                                                .currentBrightness ==
                                                            Brightness.dark,
                                                      ),
                                                      Container(
                                                          width: 35,
                                                          height: 35),
                                                    ],
                                                  ),
                                                  Container(height: 20),
                                                  Text(
                                                    webConfigurator
                                                        .internalConfigurator
                                                        .appName!,
                                                    style: IGTextStyles
                                                        .semiboldStyle(24,
                                                            IGColors.text()),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  Container(height: 20),
                                                  Text(
                                                    "Verify Access",
                                                    textAlign: TextAlign.center,
                                                    style: IGTextStyles
                                                        .normalStyle(18,
                                                            IGColors.text()),
                                                  ),
                                                  Container(height: 30),
                                                  Text(
                                                    "Code",
                                                    style: IGTextStyles
                                                        .semiboldStyle(18,
                                                            IGColors.text()),
                                                  ),
                                                  returnCodeTextFormField(),
                                                  Container(height: 20),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      IGButtonViewDefaults
                                                          .defaultTextHoverButton(
                                                              context: context,
                                                              style:
                                                                  IGButtonViewStyle(
                                                                      text:
                                                                          "Back to login",
                                                                      onTapPositioned:
                                                                          (position) async {
                                                                        webConfigurator
                                                                            .kingRouteDelegate
                                                                            .setNewRoutePath(KingRoutePath.login);
                                                                      })),
                                                    ],
                                                  ),
                                                  Container(height: 20),
                                                  IGButtonViewDefaults
                                                      .primaryDefaultButton(
                                                          context: context,
                                                          style:
                                                              IGButtonViewStyle(
                                                                  text: "ENTER",
                                                                  forceLoading:
                                                                      isLoading,
                                                                  textStyle: IGTextStyles
                                                                      .boldStyle(
                                                                          16,
                                                                          Colors
                                                                              .white),
                                                                  onTapPositioned:
                                                                      !isCodeValid
                                                                          ? null
                                                                          : (pos) async {
                                                                              await _codeAction();
                                                                            })),
                                                  Container(height: 30),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          );
  }

  Widget returnCodeTextFormField() {
    return TextFormField(
      controller: _codeController,
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      cursorColor: IGColors.primary(forceWhiteInDarkMode: true),
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      autofillHints: [AutofillHints.oneTimeCode],
      validator: IGFormFieldValidator(
              type: IGFormFieldValidatorType.range, max: 6, min: 6)
          .validate,
      textInputAction: TextInputAction.done,
      onFieldSubmitted: (value) async {
        await _codeAction();
      },
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: true,
          hint: "Enter Code",
          helperMaxLines: 10,
          helperText:
              "We have sent you an email with the code to verify access (also check the Spam folder if it does not arrive). Remember the code is only valid for 5 minutes."),
      onChanged: _onCodeChanged,
    );
  }

  _onCodeChanged(String text) {
    setState(() {
      isCodeValid = text.isNotEmpty && text.length > 5;
    });
  }

  Future _codeAction() async {
    setState(() {
      isLoading = true;
    });
    if (this._formKey.currentState!.validate()) {
      //TODD: FORGOT PASSWORD
      var resultMap =
          {}; //await Shared.queryManager.verifyCodeloginAuth(code: _codeController.text);
      return _manageUserAndError(resultMap["data"], resultMap["error"]);
    } else {
      setState(() {
        isLoading = false;
      });
      return false;
    }
  }

  Future _manageUserAndError(UserSchemaModel? user, String? err) async {
    if (err == null &&
        (webConfigurator.internalConfigurator.allowedUserForLogin.isEmpty ||
            webConfigurator.internalConfigurator.allowedUserForLogin
                .any((element) => user?.roles?.contains(element) ?? false))) {
      webConfigurator.isDuringLoginCodeProcess = false;
      if (webConfigurator.internalConfigurator.manageUserLogin != null) {
        await webConfigurator.internalConfigurator.manageUserLogin!(
            user, context);
      }
      if (webConfigurator
              .kingRouteDelegate.currentConfiguration?.routeInformation !=
          null) {
        var queryParams = webConfigurator
            .kingRouteDelegate.currentConfiguration?.routeInformation.location
            ?.getQueryParameters();
        if (queryParams != null &&
            queryParams.containsKey("redirect") &&
            queryParams["redirect"] != "/") {
          webConfigurator.kingRouteDelegate.setNewRoutePath(
              KingRoutePath.routePath(queryParams["redirect"]));
        } else {
          webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.root);
        }
      } else {
        webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.root);
      }
    } else {
      await _showError("Error", err);
    }
    setState(() {
      isLoading = false;
    });
  }

  _showError(String title, String? message) async {
    await AlertView.showSimpleErrorDialog(context: context, message: message);
  }
}
