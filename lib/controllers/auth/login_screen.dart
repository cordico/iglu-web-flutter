/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class LoginScreen extends StatefulWidget {
  LoginScreen();
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final FocusNode _focusNodePassword = FocusNode();
  bool isLoading = false;
  bool isEmailValid = true;
  bool isPasswordValid = true;
  bool isLoginButtonEnabled = false;
  bool isAppleSignInAvailable = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      KingState.configureAppereance(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : Title(
            title: webConfigurator
                    .kingRouteDelegate.currentConfiguration!.tabName ??
                webConfigurator.internalConfigurator.appName!,
            color: IGColors.primary(),
            child: Scaffold(
              backgroundColor: IGColors.primaryBackground(),
              body: SafeArea(
                bottom: false,
                child: LayoutBuilder(
                  builder: (context, _) {
                    return GestureDetector(
                      onTap: () => FocusScope.of(context).unfocus(),
                      child: Builder(
                        builder: (ctx) => Stack(
                          alignment: webConfigurator.internalConfigurator
                                      .showRandomImageInTheLogin &&
                                  !SizeDetector.isSmallScreen(context)
                              ? Alignment.centerLeft
                              : Alignment.center,
                          children: [
                            webConfigurator.internalConfigurator
                                    .showRandomImageInTheLogin
                                ? OptimizedCacheImage(
                                    imageUrl:
                                        "https://source.unsplash.com/random/${MediaQuery.of(context).size.width}x${MediaQuery.of(context).size.height}")
                                : Container(),
                            SingleChildScrollView(
                              child: Form(
                                key: _formKey,
                                child: Padding(
                                  padding: EdgeInsets.all(
                                      SizeDetector.isSmallScreen(context)
                                          ? 20
                                          : 40.0),
                                  child: AbsorbPointer(
                                    absorbing: isLoading,
                                    child: LayoutBuilder(
                                        builder: (context, constraints) {
                                      return Container(
                                        constraints: BoxConstraints(
                                            maxWidth: 400, minHeight: 400),
                                        padding: EdgeInsets.only(top: 30),
                                        decoration: BoxDecoration(
                                            color:
                                                IGColors.secondaryBackground(),
                                            boxShadow: webConfigurator
                                                    .internalConfigurator
                                                    .showRandomImageInTheLogin
                                                ? [
                                                    BoxShadow(
                                                      color: IGColors.text(
                                                          opacity: 0.26),
                                                      blurRadius: 6,
                                                    )
                                                  ]
                                                : [
                                                    BoxShadow(
                                                      color: IGColors.separator(
                                                          opacity: 0.1),
                                                      blurRadius: 3,
                                                    )
                                                  ],
                                            border: webConfigurator
                                                    .internalConfigurator
                                                    .showRandomImageInTheLogin
                                                ? null
                                                : Border.all(
                                                    color:
                                                        IGColors.separator()),
                                            borderRadius: BorderRadius.circular(
                                                kButtonPrimaryCornerRadius)),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: SizeDetector
                                                          .isSmallScreen(
                                                              context)
                                                      ? 20
                                                      : 40.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Container(
                                                          width: 35,
                                                          height: 35),
                                                      LogoImage(
                                                        width: 50,
                                                        height: 50,
                                                        lightOnly: webConfigurator
                                                                .currentBrightness ==
                                                            Brightness.light,
                                                        darkOnly: webConfigurator
                                                                .currentBrightness ==
                                                            Brightness.dark,
                                                      ),
                                                      Container(
                                                          width: 35,
                                                          height: 35),
                                                    ],
                                                  ),
                                                  Container(height: 20),
                                                  Text(
                                                    "Enter in " +
                                                        webConfigurator
                                                            .internalConfigurator
                                                            .appName!,
                                                    style: IGTextStyles
                                                        .semiboldStyle(24,
                                                            IGColors.text()),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  Container(height: 30),
                                                  Text(
                                                    "Identifier",
                                                    style: IGTextStyles
                                                        .semiboldStyle(18,
                                                            IGColors.text()),
                                                  ),
                                                  returnEmailTextFormField(),
                                                  Container(height: 30),
                                                  Text(
                                                    "Password",
                                                    style: IGTextStyles
                                                        .semiboldStyle(18,
                                                            IGColors.text()),
                                                  ),
                                                  returnPasswordTextFormField(),
                                                  Container(height: 20),
                                                  if (webConfigurator
                                                          .internalConfigurator
                                                          .enabledRoutes
                                                          .contains(EnabledRoutes
                                                              .forgot_password) ==
                                                      true)
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: [
                                                        IGButtonViewDefaults
                                                            .defaultTextHoverButton(
                                                                context:
                                                                    context,
                                                                style:
                                                                    IGButtonViewStyle(
                                                                        text:
                                                                            "Forgot password?",
                                                                        onTapPositioned:
                                                                            (position) async {
                                                                          webConfigurator
                                                                              .kingRouteDelegate
                                                                              .setNewRoutePath(KingRoutePath.forgotPassword);
                                                                        })),
                                                      ],
                                                    ),
                                                  if (webConfigurator
                                                          .internalConfigurator
                                                          .enabledRoutes
                                                          .contains(EnabledRoutes
                                                              .forgot_password) ==
                                                      true)
                                                    Container(height: 20),
                                                  IGButtonViewDefaults
                                                      .primaryDefaultButton(
                                                          context: context,
                                                          style:
                                                              IGButtonViewStyle(
                                                                  text: "ENTER",
                                                                  forceLoading:
                                                                      isLoading,
                                                                  textStyle: IGTextStyles
                                                                      .boldStyle(
                                                                          16,
                                                                          Colors
                                                                              .white),
                                                                  onTapPositioned:
                                                                      !isLoginButtonEnabled
                                                                          ? null
                                                                          : (pos) async {
                                                                              await _signInAction();
                                                                            })),
                                                  Container(height: 30),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          );
  }

  Widget returnEmailTextFormField() {
    return TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      cursorColor: IGColors.primary(forceWhiteInDarkMode: true),
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      autofillHints: [AutofillHints.username, AutofillHints.email],
      validator:
          IGFormFieldValidator(type: IGFormFieldValidatorType.range, min: 4)
              .validate,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) {
        _focusNodePassword.requestFocus();
      },
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: isEmailValid, hint: "Enter Identifier"),
      onChanged: _onEmailChanged,
    );
  }

  Widget returnPasswordTextFormField() {
    return TextFormField(
      controller: _passwordController,
      focusNode: _focusNodePassword,
      obscureText: true,
      keyboardType: TextInputType.visiblePassword,
      autofillHints: [AutofillHints.password],
      cursorColor: IGColors.primary(forceWhiteInDarkMode: true),
      onFieldSubmitted: (value) async {
        await _signInAction();
      },
      textInputAction: TextInputAction.done,
      validator:
          IGFormFieldValidator(type: IGFormFieldValidatorType.range, min: 5)
              .validate,
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: isPasswordValid, hint: "Enter Password"),
      onChanged: _onPasswordChanged,
    );
  }

  _onPasswordChanged(String text) {
    setState(() {
      isPasswordValid = text.isEmpty || !(text.length < 5);
      isLoginButtonEnabled =
          (isEmailValid && _emailController.text.isNotEmpty) &&
              (isPasswordValid && _passwordController.text.isNotEmpty);
    });
  }

  _onEmailChanged(String text) {
    setState(() {
      isEmailValid = text.isEmpty || !(text.length < 4);
      isLoginButtonEnabled =
          (isEmailValid && _emailController.text.isNotEmpty) &&
              (isPasswordValid && _passwordController.text.isNotEmpty);
    });
  }

  Future _signInAction() async {
    setState(() {
      isLoading = true;
    });
    if (this._formKey.currentState!.validate() &&
        webConfigurator
                .internalConfigurator.handlersConfigurations?.loginHandler !=
            null) {
      var user = await webConfigurator
          .internalConfigurator
          .handlersConfigurations
          ?.loginHandler!(_emailController.text, _passwordController.text);
      await _manageUserAndError(
          user, user == null ? "Error during authentication" : null);
    } else {
      setState(() {
        isLoading = false;
      });
      return false;
    }
  }

  Future _manageUserAndError(UserSchemaModel? user, String? err) async {
    if (err == null &&
        (webConfigurator.internalConfigurator.allowedUserForLogin.isEmpty ||
            webConfigurator.internalConfigurator.allowedUserForLogin
                .any((element) => user?.roles?.contains(element) ?? false))) {
      if (webConfigurator.internalConfigurator.manageUserLogin != null) {
        await webConfigurator.internalConfigurator.manageUserLogin!(
            user, context);
      }
      if (webConfigurator
              .kingRouteDelegate.currentConfiguration?.routeInformation !=
          null) {
        var queryParams = webConfigurator
            .kingRouteDelegate.currentConfiguration?.routeInformation.location
            ?.getQueryParameters();
        if (queryParams != null &&
            queryParams.containsKey("redirect") &&
            queryParams["redirect"] != "/") {
          webConfigurator.kingRouteDelegate.setNewRoutePath(
              KingRoutePath.routePath(queryParams["redirect"]));
        } else {
          webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.root);
        }
      } else {
        webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.root);
      }
    } else {
      await _showError("Error", err ?? "Error during authentication");
    }
    setState(() {
      isLoading = false;
    });
  }

  _showError(String title, String? message) async {
    await AlertView.showSimpleErrorDialog(context: context, message: message);
  }
}
