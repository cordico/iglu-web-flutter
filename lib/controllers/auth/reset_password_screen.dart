/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";

class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool isLoading = false;
  bool isConfirmPasswordValid = true;
  bool isPasswordValid = true;
  bool isLoginButtonEnabled = false;

  String? token = "";

  @override
  void initState() {
    super.initState();
    webConfigurator.isInsideResetScreen = true;
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      KingState.configureAppereance(context);
      webConfigurator.resetScreenSettings = webConfigurator
          .kingRouteDelegate.currentConfiguration!.routeInformation;
      var queryParams =
          webConfigurator.resetScreenSettings!.location!.getQueryParameters();
      if (queryParams == null) {
        backToLogin();
      } else {
        if (queryParams.containsKey("token")) {
          token = queryParams["token"];
        } else {
          backToLogin();
        }
      }
    });
  }

  @override
  void dispose() {
    webConfigurator.resetScreenSettings = null;
    webConfigurator.isInsideResetScreen = false;
    super.dispose();
  }

  void backToLogin() {
    webConfigurator.resetScreenSettings = null;
    webConfigurator.isInsideResetScreen = false;
    webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
  }

  @override
  Widget build(BuildContext context) {
    webConfigurator.resetScreenSettings = webConfigurator
        .kingRouteDelegate.currentConfiguration!.routeInformation;
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : Title(
            title: webConfigurator
                    .kingRouteDelegate.currentConfiguration!.tabName ??
                webConfigurator.internalConfigurator.appName!,
            color: IGColors.primary(),
            child: Scaffold(
              backgroundColor: IGColors.primaryBackground(),
              body: SafeArea(
                bottom: false,
                child: LayoutBuilder(
                  builder: (context, _) {
                    return GestureDetector(
                      onTap: () => FocusScope.of(context).unfocus(),
                      child: Builder(
                        builder: (ctx) => Stack(
                          alignment: webConfigurator.internalConfigurator
                                      .showRandomImageInTheLogin &&
                                  !SizeDetector.isSmallScreen(context)
                              ? Alignment.centerLeft
                              : Alignment.center,
                          children: [
                            webConfigurator.internalConfigurator
                                    .showRandomImageInTheLogin
                                ? OptimizedCacheImage(
                                    imageUrl:
                                        "https://source.unsplash.com/random/${MediaQuery.of(context).size.width}x${MediaQuery.of(context).size.height}")
                                : Container(),
                            SingleChildScrollView(
                              child: Form(
                                key: _formKey,
                                child: Padding(
                                  padding: EdgeInsets.all(
                                      SizeDetector.isSmallScreen(context)
                                          ? 20
                                          : 40.0),
                                  child: Center(
                                    child: AbsorbPointer(
                                      absorbing: isLoading,
                                      child: LayoutBuilder(
                                          builder: (context, constraints) {
                                        return Container(
                                          constraints:
                                              BoxConstraints(maxWidth: 400),
                                          padding: EdgeInsets.only(top: 30),
                                          decoration: BoxDecoration(
                                              color: IGColors
                                                  .secondaryBackground(),
                                              boxShadow: webConfigurator
                                                      .internalConfigurator
                                                      .showRandomImageInTheLogin
                                                  ? [
                                                      BoxShadow(
                                                        color: IGColors.text(
                                                            opacity: 0.26),
                                                        blurRadius: 6,
                                                      )
                                                    ]
                                                  : [
                                                      BoxShadow(
                                                        color:
                                                            IGColors.separator(
                                                                opacity: 0.1),
                                                        blurRadius: 3,
                                                      )
                                                    ],
                                              border: webConfigurator
                                                      .internalConfigurator
                                                      .showRandomImageInTheLogin
                                                  ? null
                                                  : Border.all(
                                                      color:
                                                          IGColors.separator()),
                                              borderRadius: BorderRadius.circular(
                                                  kButtonPrimaryCornerRadius)),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: SizeDetector
                                                            .isSmallScreen(
                                                                context)
                                                        ? 20
                                                        : 40.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .stretch,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Container(
                                                            width: 35,
                                                            height: 35),
                                                        LogoImage(
                                                          width: 50,
                                                          height: 50,
                                                          lightOnly: webConfigurator
                                                                  .currentBrightness ==
                                                              Brightness.light,
                                                          darkOnly: webConfigurator
                                                                  .currentBrightness ==
                                                              Brightness.dark,
                                                        ),
                                                        Container(
                                                            width: 35,
                                                            height: 35),
                                                      ],
                                                    ),
                                                    Container(height: 20),
                                                    Text(
                                                      webConfigurator
                                                          .internalConfigurator
                                                          .appName!,
                                                      style: IGTextStyles
                                                          .semiboldStyle(24,
                                                              IGColors.text()),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Container(height: 20),
                                                    Text(
                                                      "Enter your new password",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: IGTextStyles
                                                          .normalStyle(18,
                                                              IGColors.text()),
                                                    ),
                                                    Container(height: 30),
                                                    Text(
                                                      "New Password",
                                                      style: IGTextStyles
                                                          .semiboldStyle(18,
                                                              IGColors.text()),
                                                    ),
                                                    returnPasswordTextFormField(),
                                                    Container(height: 30),
                                                    Text(
                                                      "Confirm Password",
                                                      style: IGTextStyles
                                                          .semiboldStyle(18,
                                                              IGColors.text()),
                                                    ),
                                                    returnConfirmPasswordTextFormField(),
                                                    Container(height: 20),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        IGButtonViewDefaults
                                                            .defaultTextHoverButton(
                                                                context:
                                                                    context,
                                                                style:
                                                                    IGButtonViewStyle(
                                                                        text:
                                                                            "Back to login",
                                                                        onTapPositioned:
                                                                            (position) async {
                                                                          webConfigurator
                                                                              .kingRouteDelegate
                                                                              .setNewRoutePath(KingRoutePath.login);
                                                                        })),
                                                      ],
                                                    ),
                                                    Container(height: 20),
                                                    IGButtonViewDefaults
                                                        .primaryDefaultButton(
                                                            context: context,
                                                            style:
                                                                IGButtonViewStyle(
                                                                    text:
                                                                        "RESET PASSWORD",
                                                                    textStyle: IGTextStyles
                                                                        .boldStyle(
                                                                            16,
                                                                            Colors
                                                                                .white),
                                                                    onTapPositioned:
                                                                        !isLoginButtonEnabled
                                                                            ? null
                                                                            : (pos) async {
                                                                                await _action();
                                                                              })),
                                                    Container(height: 30),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          );
  }

  Widget returnPasswordTextFormField() {
    return TextFormField(
      controller: _passwordController,
      obscureText: true,
      keyboardType: TextInputType.visiblePassword,
      autofillHints: [AutofillHints.newPassword],
      cursorColor: webConfigurator.currentBrightness == Brightness.dark
          ? Colors.white
          : IGColors.primary(),
      onFieldSubmitted: (value) {
        Focus.of(context).nextFocus();
      },
      textInputAction: TextInputAction.next,
      validator:
          IGFormFieldValidator(type: IGFormFieldValidatorType.range, min: 5)
              .validate,
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: isPasswordValid, hint: "Enter New Password"),
      onChanged: _onPasswordChanged,
    );
  }

  Widget returnConfirmPasswordTextFormField() {
    return TextFormField(
      controller: _confirmPasswordController,
      obscureText: true,
      keyboardType: TextInputType.visiblePassword,
      autofillHints: [AutofillHints.newPassword],
      cursorColor: webConfigurator.currentBrightness == Brightness.dark
          ? Colors.white
          : IGColors.primary(),
      onFieldSubmitted: (value) async {
        await _action();
      },
      textInputAction: TextInputAction.done,
      validator:
          IGFormFieldValidator(type: IGFormFieldValidatorType.range, min: 5)
              .validate,
      style: IGTextStyles.normalStyle(15, IGColors.text()),
      decoration: IGTextStyles.returnTextFieldInputDecoration(context,
          check: isPasswordValid, hint: "Enter Confirm Password"),
      onChanged: _onConfirmPasswordChanged,
    );
  }

  _onPasswordChanged(String text) {
    setState(() {
      isPasswordValid = text.isEmpty || !(text.length < 5);
      isLoginButtonEnabled = (isConfirmPasswordValid &&
              _confirmPasswordController.text.isNotEmpty) &&
          (isPasswordValid && _passwordController.text.isNotEmpty);
    });
  }

  _onConfirmPasswordChanged(String text) {
    setState(() {
      isConfirmPasswordValid = text.isEmpty || !(text.length < 5);
      isLoginButtonEnabled = (isConfirmPasswordValid &&
              _confirmPasswordController.text.isNotEmpty) &&
          (isPasswordValid && _passwordController.text.isNotEmpty);
    });
  }

  _action() async {
    setState(() => isLoading = true);
    if (this._formKey.currentState!.validate()) {
      //TODD: FORGOT PASSWORD
      var resultMap =
          {}; //await Shared.queryManager.resetPasswordUser(_passwordController.text, _confirmPasswordController.text, token!);
      setState(() => isLoading = false);
      if (resultMap["error"] == null) {
        await AlertView.showSimpleDialog(context,
            title: "Password updated",
            message:
                "Congratulations. You have successfully updated your password",
            onClose: () {
              webConfigurator.kingRouteDelegate
                  .setNewRoutePath(KingRoutePath.login);
            },
            okActionPrimaryColor: IGColors.primary(),
            okAction: () {
              webConfigurator.kingRouteDelegate
                  .setNewRoutePath(KingRoutePath.login);
            });
      } else {
        await AlertView.showSimpleErrorDialog(
            context: context, message: resultMap["error"]);
      }
    } else {
      setState(() => isLoading = false);
    }
  }
}
