/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";

class VerifyEmailScreen extends StatefulWidget {
  @override
  _VerifyEmailScreenState createState() => _VerifyEmailScreenState();
}

class _VerifyEmailScreenState extends State<VerifyEmailScreen> {
  bool isLoading = false;
  String? token = "";

  @override
  void initState() {
    super.initState();
    webConfigurator.isInsideResetScreen = true;
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      KingState.configureAppereance(context);
      webConfigurator.resetScreenSettings = webConfigurator
          .kingRouteDelegate.currentConfiguration!.routeInformation;
      var queryParams =
          webConfigurator.resetScreenSettings!.location!.getQueryParameters();
      if (queryParams == null) {
        backToLogin();
      } else {
        if (queryParams.containsKey("token")) {
          token = queryParams["token"];
          _action();
        } else {
          backToLogin();
        }
      }
    });
  }

  @override
  void dispose() {
    webConfigurator.resetScreenSettings = null;
    webConfigurator.isInsideResetScreen = false;
    super.dispose();
  }

  void backToLogin() {
    webConfigurator.resetScreenSettings = null;
    webConfigurator.isInsideResetScreen = false;
    webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
  }

  @override
  Widget build(BuildContext context) {
    webConfigurator.resetScreenSettings = webConfigurator
        .kingRouteDelegate.currentConfiguration!.routeInformation;
    return Title(
      title: webConfigurator.kingRouteDelegate.currentConfiguration!.tabName ??
          webConfigurator.internalConfigurator.appName!,
      color: IGColors.primary(),
      child: LeftScreenContainer(generateChild: (constraints) {
        return GestureDetector(
          onTap: () async {},
          child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                Container(
                    height: ((constraints.maxHeight / 2.0) - 150) > 0
                        ? ((constraints.maxHeight / 2.0) - 150)
                        : 0),
                LogoImage(
                    height: 100,
                    width: 100,
                    lightOnly:
                        webConfigurator.currentBrightness == Brightness.light,
                    darkOnly:
                        webConfigurator.currentBrightness == Brightness.dark),
              ])),
        );
      }),
    );
  }

  _action() async {
    setState(() => isLoading = true);
    //TODD: FORGOT PASSWORD
    var resultMap =
        {}; //var resultMap = await Shared.queryManager.verifyEmailUser(token!);
    setState(() => isLoading = false);
    if (resultMap["error"] == null) {
      await AlertView.showSimpleDialog(context,
          title: "Email Confirmed successfully",
          message: "Congratulations. You have confirmed your email correctly",
          okActionPrimaryColor: IGColors.primary(), okAction: () {
        //webConfigurator.internalConfigurator.loggedInUser?.shouldVerifyEmail =
        //    false;
        webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
      });
    } else {
      await AlertView.showSimpleErrorDialog(
          context: context, message: resultMap["error"]);
    }
  }
}
