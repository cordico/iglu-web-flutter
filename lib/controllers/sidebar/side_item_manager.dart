/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class SideItemManager {
  SideItemManager();
  List<SideItem> sideItems = List<SideItem>.from([], growable: true);

  List<Widget> sideItemWidgets(
      {required BuildContext context,
      required bool? isExpandedOutside,
      int index = 0,
      AnimationController? expandController,
      Function(SideItem)? onItemSelected}) {
    var contextSideItem = context.sideItem();
    var isExpanded = expandController == null
        ? isExpandedOutside
        : expandController.value < 1.0
            ? false
            : isExpandedOutside;
    List<Widget> temp = [];
    webConfigurator.sideItemManager.sideItems
        .asMap()
        .forEach((index, sideItem) {
      var isChild =
          sideItem.expandableItems == null || sideItem.expandableItems!.isEmpty;
      var isSelected = contextSideItem != null && contextSideItem == sideItem;
      if (isChild) {
        temp.add(this.sideItem(
            context: context,
            index: index,
            isExpanded: isExpanded!,
            isChild: isChild,
            isSelected: isSelected,
            item: sideItem,
            onItemSelected: onItemSelected));
      } else {
        if (index != 0 &&
            webConfigurator.internalConfigurator.sideBarStyle !=
                SideBarStyle.minimal) {
          temp.add(
              Divider(thickness: 1, height: 1, color: IGColors.separator()));
        }
        if (isExpanded!) {
          temp.add(
            Padding(
              padding: webConfigurator.internalConfigurator.sideBarStyle ==
                      SideBarStyle.minimal
                  ? EdgeInsets.fromLTRB(16, 16.5, 16, 0)
                  : EdgeInsets.fromLTRB(16, 16.5, 16, 16.5),
              child: Text(sideItem.title!,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: IGTextStyles.mediumStyle(
                      15,
                      webConfigurator.internalConfigurator.sideBarStyle ==
                              SideBarStyle.minimal
                          ? IGColors.textSecondary()
                          : IGColors.text())),
            ),
          );
          if (webConfigurator.internalConfigurator.sideBarStyle !=
              SideBarStyle.minimal) {
            temp.add(
                Divider(thickness: 1, height: 1, color: IGColors.separator()));
          }
        } else {
          if (index != 0 &&
              webConfigurator.internalConfigurator.sideBarStyle ==
                  SideBarStyle.minimal) {
            temp.add(Center(
                child: Container(
                    width: 40,
                    child: Divider(
                        thickness: 1,
                        height: 1,
                        color: IGColors.separator()))));
          }
        }
      }
      if (sideItem.expandableItems != null &&
          sideItem.expandableItems!.isNotEmpty) {
        sideItem.expandableItems!.asMap().forEach((idx, v) {
          var isChild = v.expandableItems == null || v.expandableItems!.isEmpty;
          var isSelected = contextSideItem != null && contextSideItem == v;
          temp.add(this.sideItem(
              context: context,
              index: idx,
              isExpanded: isExpanded,
              isChild: isChild,
              isSelected: isSelected,
              item: v,
              onItemSelected: onItemSelected));
        });
      }
    });
    return temp;
  }

  Widget sideItem({
    required BuildContext context,
    required int index,
    required bool isExpanded,
    required bool isChild,
    required bool isSelected,
    required SideItem item,
    required Function(SideItem)? onItemSelected,
  }) {
    var widget = IGButtonRow(
      selected: isSelected,
      mouseCursor: item.screen == null && isChild
          ? SystemMouseCursors.forbidden
          : isSelected
              ? SystemMouseCursors.click
              : null,
      onTap: item.screen == null //|| isSelected
          ? null
          : isChild
              ? () async {
                  onItemSelected!(item);
                }
              : null,
      margin: isChild
          ? EdgeInsets.fromLTRB(8, index == 0 ? 8 : 0, 8, 8)
          : EdgeInsets.zero,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment:
          isExpanded ? MainAxisAlignment.start : MainAxisAlignment.center,
      igViewStyle: IGViewStyleExtension.sidebar(),
      padding: EdgeInsets.only(
        right: isChild ? 8 : 16,
        left: isChild ? 8 : 16,
        top: 14,
        bottom: 14,
      ),
      leading: (item.icon != null)
          ? (states) {
              return Icon(item.icon,
                  color: states.contains(MaterialState.selected)
                      ? IGColors.primary(forceWhiteInDarkMode: true)
                      : IGColors.text(),
                  size: 24);
            }
          : null,
      title: isExpanded ? item.title : null,
    );
    if (isExpanded || item.screen == null || !isChild) {
      return widget;
    } else {
      return WidgetsDefaults.defaultTooltip(
          context: context, message: item.title!, child: widget);
    }
  }

  presentDetailScreen(
      {required BuildContext context,
      required String key,
      required String id,
      bool refreshCurrentPageTable = false}) async {
    var settings = webConfigurator
        .kingRouteDelegate.currentConfiguration!.routeInformation.location;
    var item = webConfigurator.sideItemManager.sideItems.hasKey(key);
    if (item != null) {
      if (settings == item.path()! + "/$id") {
      } else {
        if (refreshCurrentPageTable) {
          item.currentPageTable = null;
        }
        webConfigurator.kingRouteDelegate
            .setNewRoutePath(KingRoutePath.routePath(item.path()! + "/$id"));
      }
    }
  }

  bool isDetailScreenPresented(
      {required BuildContext context,
      required String key,
      required String id}) {
    var settings = webConfigurator
        .kingRouteDelegate.currentConfiguration!.routeInformation.location;
    var item = webConfigurator.sideItemManager.sideItems.hasKey(key);
    if (item != null) {
      if (settings == item.path()! + "/$id") {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  void resetSelectedTabs() {
    sideItems.forEach((element) {
      element.expandableItems!.forEach((element) {});
    });
  }
}
