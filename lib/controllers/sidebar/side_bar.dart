/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:math";

import "package:flutter/material.dart";
import 'package:iglu_web_flutter/helpers/restoration_change_notifier.dart';
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:provider/provider.dart';

class SideBar extends StatefulWidget {
  final Function(SideItem?)? onItemSelected;

  SideBar({this.onItemSelected});

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> with TickerProviderStateMixin {
  double _width = 190.0;
  bool? isExpanded = true;

  AnimationController? expandController;
  late Animation<double> animation;
  late Animation<double> _animation;

  SideItem? selectedItem;

  ScrollController? scrollController;

  var currentSize = "";
  var previousSize = "";

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController(
        initialScrollOffset:
            Provider.of<RestorationChangeNotifier>(context, listen: false)
                .sidebarScrollOffset as double);
    scrollController!.addListener(() {
      Provider.of<RestorationChangeNotifier>(context, listen: false)
          .sidebarScrollOffset = scrollController!.offset;
    });
    expandController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    animation = CurvedAnimation(
      parent: expandController!,
      curve: Curves.fastOutSlowIn,
    );
    _animation = Tween<double>(begin: 0, end: 1).animate(expandController!)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          isExpanded = true;
          webConfigurator.isLeftMenuExpanded = true;
        }
      });
    isExpanded = webConfigurator.isLeftMenuExpanded;
    expandController!.value = isExpanded! ? 1 : 0;

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      changeSize();
    });
  }

  changeSize() {
    previousSize = currentSize;
    if (SizeDetector.isSmallScreen(context)) {
      if (!isExpanded!) {
        setState(() {
          isExpanded = true;
        });
      }
      currentSize = "s";
    } else if (SizeDetector.isMediumScreen(context)) {
      if (isExpanded! && currentSize == "l") {
        setState(() {
          isExpanded = false;
          expandController!.reverse();
        });
      } else if (isExpanded! && (currentSize == "s" || currentSize.isEmpty)) {
        setState(() {
          isExpanded = false;
          expandController!.value = 0;
        });
      }
      currentSize = "m";
    } else if (SizeDetector.isLargeScreen(context)) {
      if (currentSize != "l" &&
          isExpanded != webConfigurator.isLeftMenuExpanded) {
        if (isExpanded!) {
          isExpanded = false;
          webConfigurator.isLeftMenuExpanded = false;
          expandController!.reverse();
        } else {
          expandController!.forward();
        }
      }
      currentSize = "l";
    }
  }

  @override
  Widget build(BuildContext context) {
    changeSize();
    return LayoutBuilder(
      builder: (context, contraints) {
        return SizeDetector.isLargeScreen(context) ||
                SizeDetector.isMediumScreen(context)
            ? Container(
                width: (previousSize == "s" &&
                        SizeDetector.isMediumScreen(context))
                    ? 94
                    : 94 + (_width * animation.value),
                child: AppDrawerState.child(
                    context: context,
                    state: this,
                    angle: -pi * _animation.value,
                    expandController: expandController,
                    showExpandArrow: true,
                    scrollController: scrollController,
                    onItemSelected: widget.onItemSelected))
            : AppDrawerState.child(
                context: context,
                state: this,
                angle: -pi * _animation.value,
                expandController: expandController,
                showExpandArrow: true,
                scrollController: scrollController,
                onItemSelected: widget.onItemSelected);
      },
    );
  }
}
