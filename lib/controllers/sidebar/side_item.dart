/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import 'package:iglu_web_flutter/iglu_web_flutter.dart';

class SideItem<T> {
  String? id;
  IconData? icon;
  String? title;
  List<SideItem>? expandableItems;
  Widget? screen;
  Map<String, String>? pathForLocale;
  bool hasDetail;
  int? numberOfPathSegment;
  Map<String, RegExp>? regexForLocale;
  Map<String, SideItem>? emptySideItemForLocale;
  Widget? detailWidget;
  bool isExpanded;
  int? selectedTab;

  double scrollPageDetailOffset;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  PaginatedQuery<T>? paginatedQuery;
  int? currentPaginatedPageDetail;

  var cacheDataTableViewRow = <int, Map<String, DataTableViewRow<T>>>{};
  var cacheDataTableViewRowArray = <int, List<DataTableViewRow<T>>>{};
  Map<int, Map<String, List<FilterRule<T>>?>> visibleFiltersForHeaderKey = {};

  List<HeaderRule<T>> headerRules = [];
  List<HeaderRule<T>> visibleHeaders = [];

  String? searchQueryTable;
  int? currentPageTable;

  bool settedSortValues = false;
  bool sortAsceding = false;
  int sortColumnIndex = 0;
  String sortColumnName = "";

  clear() {
    currentPaginatedPageDetail = null;
    cacheDataTableViewRow = <int, Map<String, DataTableViewRow<T>>>{};
    cacheDataTableViewRowArray = <int, List<DataTableViewRow<T>>>{};
    visibleFiltersForHeaderKey = {};

    headerRules = [];
    visibleHeaders = [];

    searchQueryTable = null;
    currentPageTable = null;

    settedSortValues = false;
    sortAsceding = false;
    sortColumnIndex = 0;
    sortColumnName = "";
  }

  SideItem({
    this.id,
    this.icon,
    this.title,
    this.expandableItems,
    this.screen,
    this.pathForLocale,
    this.hasDetail = false,
    this.numberOfPathSegment,
    this.detailWidget,
    this.isExpanded = true,
    this.emptySideItemForLocale,
    this.regexForLocale,
    this.searchQueryTable,
    this.currentPageTable,
    this.currentPaginatedPageDetail,
    this.selectedTab = 0,
    this.scrollPageDetailOffset = 0,
    this.paginatedQuery,
  }) {
    if (pathForLocale != null) {
      Map<String, String> newPathForLocale = {};
      pathForLocale!.forEach((key, value) {
        var toAdd = value;
        if (value.startsWith("/")) {
          toAdd = toAdd.substring(1);
        }
        var v =
            "${webConfigurator.internalConfigurator.mainRouteToDisplay()}/$toAdd";
        newPathForLocale[key] = v;
      });
      pathForLocale = newPathForLocale;
    }
  }

  String? path({String locale = "en"}) {
    if (pathForLocale![locale] != null) {
      return pathForLocale![locale];
    } else {
      return webConfigurator.internalConfigurator
          .mainRouteToDisplay(forceNotEmpty: true);
    }
  }

  @override
  bool operator ==(dynamic o) => o is SideItem && o.id == id;

  @override
  int get hashCode => id.hashCode;
}

extension SideItemArray on List<SideItem>? {
  SideItem? hasKey(String key) {
    var result;
    if (this == null) return null;
    for (var i = 0; i < this!.length; i++) {
      var item = this![i];
      if (item.id == key) {
        result = item;
        break;
      } else {
        result = item.expandableItems.hasKey(key);
        if (result != null) {
          break;
        }
      }
    }
    return result;
  }

  SideItem? hasPath(String? path) {
    var result;
    if (this == null) return null;
    var numberOfSegments = path!.split("/");
    numberOfSegments.removeWhere((element) => element.isEmpty);
    for (var i = 0; i < this!.length; i++) {
      var item = this![i];
      var founded = false;
      if (item.pathForLocale != null) {
        item.pathForLocale!.forEach((key, value) {
          if (path == value) {
            founded = true;
          }
        });
      }
      if (founded) {
        result = item;
        break;
      }
    }
    if (result == null) {
      for (var i = 0; i < this!.length; i++) {
        var item = this![i];
        var founded = false;
        if (item.pathForLocale != null) {
          item.pathForLocale!.forEach((key, value) {
            if (path == value ||
                (path.contains(value) &&
                    numberOfSegments.length ==
                        ((item.numberOfPathSegment ?? 0) +
                            webConfigurator.internalConfigurator
                                .numberOfPathSegmentOfMainRoute) &&
                    item.hasDetail)) {
              founded = true;
            }
          });
        }
        if (item.emptySideItemForLocale != null && !founded) {
          item.emptySideItemForLocale!.forEach((key, value) {
            if (path == value.path()) {
              founded = true;
              item = value;
            }
          });
        }
        if (founded) {
          result = item;
          break;
        } else {
          result = item.expandableItems.hasPath(path);
          if (result != null) {
            break;
          }
        }
      }
    }
    return result;
  }
}
