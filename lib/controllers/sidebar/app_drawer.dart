/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class AppDrawer extends StatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  AppDrawerState createState() => AppDrawerState();
}

class AppDrawerState extends State<AppDrawer> {
  bool? oldValue = true;
  double xPosition = 0;
  double yPosition = 0;
  double maxWidth = 0;
  @override
  void initState() {
    super.initState();
    oldValue = webConfigurator.isLeftMenuExpanded;
    webConfigurator.isLeftMenuExpanded = true;
  }

  @override
  void dispose() {
    webConfigurator.isLeftMenuExpanded = oldValue;
    super.dispose();
  }

  closePan() {
    if (xPosition < -(maxWidth / 2.5)) {
      Navigator.of(context).pop();
    } else {
      setState(() {
        xPosition = 0;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if ((SizeDetector.isLargeScreen(context) ||
            SizeDetector.isMediumScreen(context)) &&
        Navigator.of(context).canPop()) {
      Navigator.of(context).pop();
    }
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
      },
      onPanUpdate: (tapInfo) {
        xPosition += tapInfo.delta.dx;
        yPosition += tapInfo.delta.dy;
        setState(() {});
      },
      onPanCancel: () {
        closePan();
      },
      onPanEnd: (details) {
        closePan();
      },
      child: Material(
          color: Colors.transparent,
          child: LayoutBuilder(builder: (context, constraints) {
            maxWidth = SizeDetector.isMediumScreen(context)
                ? constraints.maxWidth * 0.45
                : constraints.maxWidth * 0.75;
            return Stack(
              children: [
                Positioned(
                  left: xPosition > 0 ? 0 : xPosition,
                  child: Container(
                    width: maxWidth,
                    height: constraints.maxHeight,
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: IGColors.text(opacity: 0.20),
                        blurRadius: 6,
                      )
                    ]),
                    child: AppDrawerState.child(context: context, state: this),
                  ),
                )
              ],
            );
          })),
    );
  }

  static Widget child(
      {required BuildContext context,
      required State state,
      double? angle,
      bool showExpandArrow = false,
      ScrollController? scrollController,
      AnimationController? expandController,
      Function(SideItem?)? onItemSelected}) {
    Function(SideItem?) defaultOnItemSelected = (item) {
      if (SizeDetector.isSmallScreen(context)) {
        Navigator.of(context).pop();
      }
      if (item != null) {
        if (webConfigurator.kingRouteDelegate.currentConfiguration!
                .routeInformation.location !=
            item.path()) {
          if (webConfigurator
                  .internalConfigurator.sideItemTabSelectionHandler !=
              null) {
            webConfigurator.internalConfigurator.sideItemTabSelectionHandler!();
          }
          webConfigurator.sideItemManager.resetSelectedTabs();
          webConfigurator.kingRouteDelegate
              .setNewRoutePath(KingRoutePath.routePath(item.path()));
        }
      } else if (webConfigurator.kingRouteDelegate.currentConfiguration!
              .routeInformation.location !=
          webConfigurator.internalConfigurator
              .mainRouteToDisplay(forceNotEmpty: true)) {
        webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.root);
      }
    };
    return Container(
      color: IGColors.primaryBackground(),
      child: Stack(
        children: <Widget>[
          Container(
            color: IGColors.primaryBackground(),
            child: Container(
              color: IGColors.primaryBackground(),
              child: Container(
                color: IGColors.primaryBackground(),
                child: Scrollbar(
                  child: ListView(
                    controller: scrollController,
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.zero,
                    children: <Widget>[Container(height: 55)] +
                        webConfigurator.sideItemManager.sideItemWidgets(
                            context: context,
                            expandController: expandController,
                            isExpandedOutside:
                                SizeDetector.isMediumScreen(context)
                                    ? false
                                    : webConfigurator.isLeftMenuExpanded,
                            onItemSelected: (item) {
                              if (onItemSelected != null) {
                                onItemSelected(item);
                              } else {
                                defaultOnItemSelected(item);
                              }
                            }) +
                        [Container(height: 30)] +
                        (showExpandArrow
                            ? [
                                SizeDetector.isSmallScreen(context) ||
                                        SizeDetector.isMediumScreen(context)
                                    ? Container()
                                    : Container(height: 10),
                                SizeDetector.isSmallScreen(context) ||
                                        SizeDetector.isMediumScreen(context)
                                    ? Container()
                                    : Row(
                                        mainAxisAlignment:
                                            webConfigurator.isLeftMenuExpanded!
                                                ? MainAxisAlignment.end
                                                : MainAxisAlignment.center,
                                        children: <Widget>[
                                          Transform.rotate(
                                              angle: angle!,
                                              child: IGButtonViewDefaults
                                                  .mediumButtonIconOnlyHover(
                                                      context: context,
                                                      style: IGButtonViewStyle(
                                                          icon: Icons
                                                              .chevron_right_rounded,
                                                          circleBorderRadius:
                                                              20,
                                                          tooltipMessage:
                                                              webConfigurator
                                                                      .isLeftMenuExpanded!
                                                                  ? "Collapse"
                                                                  : "Expand",
                                                          onTapPositioned:
                                                              (position) async {
                                                            await webConfigurator
                                                                .setIsLeftMenuExpandedLocally(
                                                                    !webConfigurator
                                                                        .isLeftMenuExpanded!);
                                                            state.setState(() {
                                                              if (webConfigurator
                                                                  .isLeftMenuExpanded!) {
                                                                webConfigurator
                                                                        .isLeftMenuExpanded =
                                                                    false;
                                                                expandController!
                                                                    .reverse();
                                                              } else {
                                                                expandController!
                                                                    .forward();
                                                              }
                                                            });
                                                          }))),
                                          Container(width: 10)
                                        ],
                                      ),
                                SizeDetector.isSmallScreen(context) ||
                                        SizeDetector.isMediumScreen(context)
                                    ? Container()
                                    : Container(height: 30),
                              ]
                            : []),
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 55,
            decoration: BoxDecoration(
                color: IGColors.primaryBackground(opacity: 0.999),
                border: BorderDirectional(
                    bottom:
                        BorderSide(color: IGColors.separator(), width: 1.0))),
            child: IGButtonRow(
              mainAxisAlignment: !webConfigurator.isLeftMenuExpanded! ||
                      SizeDetector.isMediumScreen(context)
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              onTap: () async {
                if (onItemSelected != null) {
                  onItemSelected(null);
                } else {
                  defaultOnItemSelected(null);
                }
              },
              margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
              leading: (states) {
                return LogoImage(
                    height: 30,
                    width: 30,
                    lightOnly:
                        webConfigurator.currentBrightness == Brightness.light,
                    darkOnly:
                        webConfigurator.currentBrightness == Brightness.dark,
                    onTap: () async {
                      if (onItemSelected != null) {
                        onItemSelected(null);
                      } else {
                        defaultOnItemSelected(null);
                      }
                    });
              },
              title: webConfigurator.isLeftMenuExpanded! &&
                      !SizeDetector.isMediumScreen(context)
                  ? webConfigurator.internalConfigurator.appName
                  : null,
              igViewStyle: IGViewStyle(titleStyle:
                  MaterialStateProperty.resolveWith<TextStyle>(
                      (Set<MaterialState> states) {
                return IGTextStyles.semiboldStyle(20, IGColors.text());
              })),
            ),
          ),
        ],
      ),
    );
  }
}
