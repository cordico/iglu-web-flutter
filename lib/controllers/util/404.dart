/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class Controller404 extends StatefulWidget {
  @override
  _Controller404State createState() => _Controller404State();
}

class _Controller404State extends State<Controller404> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: IGColors.primaryBackground(),
      body: SafeArea(
        bottom: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Center(
              child: Container(
                  constraints: BoxConstraints(maxWidth: 600),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        LogoImage(
                            height: 75,
                            width: 75,
                            lightOnly: webConfigurator.currentBrightness ==
                                Brightness.light,
                            darkOnly: webConfigurator.currentBrightness ==
                                Brightness.dark),
                        Container(height: 10),
                        Text("Oops. This page does not exist",
                            textAlign: TextAlign.center,
                            style: IGTextStyles.boldStyle(24, IGColors.text())),
                        Container(height: 15),
                        Container(
                          constraints: BoxConstraints(maxWidth: 320),
                          child: IGButtonViewDefaults.primaryDefaultButton(
                              context: context,
                              style: IGButtonViewStyle(
                                  text: "BACK TO HOME",
                                  onTapPositioned: (pos) async {
                                    webConfigurator.kingRouteDelegate
                                        .setNewRoutePath(KingRoutePath.root);
                                  })),
                        ),
                      ])),
            );
          },
        ),
      ),
    );
  }
}
