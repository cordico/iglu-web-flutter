/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class EmptyController extends StatefulWidget {
  final bool showHeader;
  final bool showTestBoxHeader;
  EmptyController({this.showHeader = true, this.showTestBoxHeader = true});

  @override
  _EmptyControllerState createState() => _EmptyControllerState();
}

class _EmptyControllerState extends State<EmptyController> {
  @override
  Widget build(BuildContext context) {
    return LeftScreenContainer(
        showHeader: widget.showHeader,
        showTestBoxHeader: widget.showTestBoxHeader,
        generateBreadCrumbItems: () {
          return [];
        },
        generateChild: (constraints) {
          return GestureDetector(
            onTap: () async {},
            child: Container(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                  Container(
                      height: ((constraints.maxHeight / 2.0) - 150) > 0
                          ? ((constraints.maxHeight / 2.0) - 150)
                          : 0),
                  LogoImage(
                      height: 100,
                      width: 100,
                      lightOnly:
                          webConfigurator.currentBrightness == Brightness.light,
                      darkOnly:
                          webConfigurator.currentBrightness == Brightness.dark),
                ])),
          );
        });
  }
}
