/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:async";
import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:iglu_web_flutter/widgets/utils/test_data_box_widget.dart';

class LeftScreenContainer extends StatefulWidget {
  final Widget Function(BoxConstraints)? generateChild;
  final List<Widget> Function(BuildContext, BoxConstraints)? additionalWidgets;
  final List<Widget> Function(BuildContext, BoxConstraints)? previousWidgets;
  final CrossAxisAlignment? previousWidgetsCrossAxisAlignment;
  final MainAxisAlignment? previousWidgetsMainAxisAlignment;
  final EdgeInsetsGeometry? previousWidgetPadding;
  final EdgeInsetsGeometry? additionalWidgetPadding;
  final EdgeInsetsGeometry? childWidgetPadding;
  final bool showHeader;
  final Color? backgroundColor;
  final List<BreadCrumbItem> Function()? generateBreadCrumbItems;
  final Function? contextStateRefresh;
  final ScrollController? scrollController;
  final ScrollPhysics? scrollPhysics;
  final bool showTestBoxHeader;
  LeftScreenContainer(
      {this.generateChild,
      this.additionalWidgets,
      this.previousWidgetsCrossAxisAlignment,
      this.previousWidgets,
      this.additionalWidgetPadding,
      this.previousWidgetsMainAxisAlignment,
      this.previousWidgetPadding,
      this.showHeader = true,
      this.childWidgetPadding,
      this.generateBreadCrumbItems,
      this.contextStateRefresh,
      this.scrollController,
      this.scrollPhysics,
      this.showTestBoxHeader = true,
      this.backgroundColor});
  @override
  LeftScreenContainerState createState() => LeftScreenContainerState();
}

class LeftScreenContainerState extends State<LeftScreenContainer> {
  int selectedIndex = 0;
  Timer? endTimer;

  @override
  void initState() {
    super.initState();
  }

  refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : LayoutBuilder(
            builder: (context, constraints) {
              return Scaffold(
                  appBar: widget.showHeader
                      ? HeaderBar(
                          generateBreadCrumbItems:
                              widget.generateBreadCrumbItems,
                          contextStateRefresh: widget.contextStateRefresh)
                      : null,
                  backgroundColor:
                      widget.backgroundColor ?? IGColors.primaryBackground(),
                  body: Stack(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: returnGridView(constraints: constraints),
                      ),
                      if (widget.showTestBoxHeader)
                        Align(
                          child: TestDataBoxWidget(),
                          alignment: Alignment.topCenter,
                        ),
                    ],
                  ));
            },
          );
  }

  List<Widget> returnGridView({required BoxConstraints constraints}) {
    return [
      Flexible(
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scrollbar(
            child: CustomScrollView(
              controller: widget.scrollController,
              physics: widget.scrollPhysics,
              slivers: <Widget>[
                if (widget.previousWidgets != null)
                  SliverPadding(
                      padding:
                          widget.previousWidgetPadding ?? EdgeInsets.all(20),
                      sliver: SliverToBoxAdapter(
                          child: Column(
                              crossAxisAlignment:
                                  widget.previousWidgetsCrossAxisAlignment ??
                                      CrossAxisAlignment.start,
                              mainAxisAlignment:
                                  widget.previousWidgetsMainAxisAlignment ??
                                      MainAxisAlignment.start,
                              children: (widget.previousWidgets != null
                                  ? widget.previousWidgets!(
                                      context, constraints)
                                  : <Widget>[])))),
                if (widget.generateChild != null)
                  SliverPadding(
                      padding: widget.childWidgetPadding ?? EdgeInsets.all(20),
                      sliver: SliverToBoxAdapter(
                          child: widget.generateChild!(constraints))),
                if (widget.additionalWidgets != null)
                  SliverPadding(
                      padding:
                          widget.additionalWidgetPadding ?? EdgeInsets.all(20),
                      sliver: SliverToBoxAdapter(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: (widget.additionalWidgets != null
                            ? widget.additionalWidgets!(context, constraints)
                            : <Widget>[]),
                      ))),
                SliverFillRemaining(
                    child: Container(),
                    fillOverscroll: false,
                    hasScrollBody: false),
                SliverToBoxAdapter(child: FooterBar())
              ],
            ),
          ),
        ),
      )
    ];
  }
}
