/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class LoadingController extends StatefulWidget {
  LoadingController();

  @override
  _LoadingControllerState createState() => _LoadingControllerState();
}

class _LoadingControllerState extends State<LoadingController>
    with SingleTickerProviderStateMixin {
  var showServerNotAvailable = false;
  var value = -1.0;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      if (ModalRoute.of(context)!.isCurrent) {
        if (webConfigurator.internalConfigurator.loadConfig) {
          await webConfigurator.sessionHandler.loadConfig();
          if (webConfigurator.internalConfigurator.remoteConfig == null) {
            if (mounted) {
              setState(() {
                showServerNotAvailable = true;
              });
            }
          }
        }
        if (mounted) {
          setState(() {});
        }
        if (webConfigurator.internalConfigurator.loadSession) {
          webConfigurator.internalConfigurator.loadedSession = false;
          await webConfigurator.sessionHandler.loadSession();
          webConfigurator.internalConfigurator.loadedSession = true;
        }
        if (webConfigurator.internalConfigurator.loadInternalData != null) {
          await webConfigurator.internalConfigurator.loadInternalData!(context);
        }
        _finishAnimation();
        await _pushAccordlyNavigator();
      }
    });
  }

  _finishAnimation() {
    if (mounted) {
      setState(() {
        value = 1;
      });
    }
  }

  _pushAccordlyNavigator() async {
    var pathLocation = webConfigurator.kingRouteDelegate.currentConfiguration
            ?.routeInformation.location ??
        "";
    if (webConfigurator.internalConfigurator.loggedInUser == null) {
      if (webConfigurator.isInsideResetScreen &&
          webConfigurator.resetScreenSettings != null) {
        //RESET SCREEN
        webConfigurator.kingRouteDelegate.setNewRoutePath(
            KingRoutePath.resetPasswordPath(
                webConfigurator.resetScreenSettings!.location));
      } else if (_allowPathIfNotLogged) {
        //OTHER PATHS ALLOWED WHEN NOT LOGGED IN
        webConfigurator.kingRouteDelegate
            .setNewRoutePath(KingRoutePath.routePath(pathLocation));
      } else {
        //404 ROUTE IF IS NOT ROUTE, IS UNKNOWNPATH, AND 404 IS ENABLED
        if (pathLocation != "/" &&
            EnabledRoutesExtension.isUnknownPath(
                routeInformation: RouteInformation(location: pathLocation)) &&
            webConfigurator.internalConfigurator.show404Page) {
          webConfigurator.kingRouteDelegate
              .setNewRoutePath(KingRoutePath.routePath(pathLocation));
        } else {
          //LOGIN ROUTE
          var path =
              _generatePathFromOrigin(EnabledRoutes.login.value, pathLocation);
          webConfigurator.kingRouteDelegate
              .setNewRoutePath(KingRoutePath.loginPath(path));
        }
      }
    } else {
      //404 ROUTE IF IS NOT ROUTE, IS UNKNOWNPATH, AND 404 IS ENABLED
      if (pathLocation != "/" &&
          EnabledRoutesExtension.isUnknownPath(
              routeInformation: RouteInformation(location: pathLocation)) &&
          webConfigurator.internalConfigurator.show404Page) {
        webConfigurator.kingRouteDelegate
            .setNewRoutePath(KingRoutePath.routePath(pathLocation));
      } else {
        //OTHER ROUTES IF USER ARE LOGGED IN
        if (webConfigurator.kingRouteDelegate.currentConfiguration != null &&
            EnabledRoutesExtension.isUnknownPath(
                routeInformation: webConfigurator.kingRouteDelegate
                    .currentConfiguration!.routeInformation)) {
          webConfigurator.kingRouteDelegate.setNewRoutePath(
              KingRoutePath.routePath(
                  webConfigurator.internalConfigurator.loggedInUser == null
                      ? _generatePathFromOrigin(
                          EnabledRoutes.login.value, pathLocation)
                      : webConfigurator.internalConfigurator
                          .mainRouteToDisplay(forceNotEmpty: true)));
        } else {
          webConfigurator.kingRouteDelegate
              .setNewRoutePath(KingRoutePath.routePath(pathLocation));
        }
      }
    }
  }

  String _generatePathFromOrigin(String origin, String pathLocation) {
    var path = origin;
    var fakeURI = Uri.tryParse("https://www.iglu.dev" + pathLocation);
    var alreadyAdded = false;
    if (fakeURI?.queryParameters.isNotEmpty == true) {
      fakeURI?.queryParameters.forEach((key, value) {
        if (value.isNotEmpty) {
          if (!alreadyAdded) {
            alreadyAdded = true;
            path += "?$key=$value";
          } else {
            path += "&$key=$value";
          }
        }
      });
    }
    if (_allowRedirectForPathLocation(pathLocation: pathLocation) &&
        !EnabledRoutesExtension.isUnknownPath(
            routeInformation: RouteInformation(location: pathLocation))) {
      if (!alreadyAdded) {
        alreadyAdded = true;
        path += "?redirect=$pathLocation";
      } else {
        path += "&redirect=$pathLocation";
      }
    }
    return path;
  }

  bool _allowRedirectForPathLocation({required String pathLocation}) {
    var notMain = pathLocation !=
        webConfigurator.internalConfigurator
            .mainRouteToDisplay(forceNotEmpty: true);
    var notLogin = !pathLocation.contains(EnabledRoutes.login.value);
    var notReset = !pathLocation.contains(EnabledRoutes.reset_password.value);
    var notRoot = pathLocation != "/";
    var notVerifyCode =
        !pathLocation.contains(EnabledRoutes.verify_code.value) &&
            !webConfigurator.isDuringLoginCodeProcess;
    return pathLocation.isNotEmpty &&
        notMain &&
        notLogin &&
        notReset &&
        notRoot &&
        notVerifyCode;
  }

  bool get _allowPathIfNotLogged {
    if (webConfigurator.kingRouteDelegate.currentConfiguration == null ||
        webConfigurator
                .kingRouteDelegate.currentConfiguration?.routeInformation ==
            null) {
      return false;
    }
    var name = webConfigurator.kingRouteDelegate.currentConfiguration
            ?.routeInformation.location ??
        "";
    return name.contains(EnabledRoutes.forgot_password.value) ||
        name.contains(EnabledRoutes.reset_password.value) ||
        name.contains(EnabledRoutes.verify_email.value) ||
        (name == EnabledRoutes.verify_code.value &&
            webConfigurator.isDuringLoginCodeProcess);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: IGColors.primaryBackground(),
      body: showServerNotAvailable
          ? ServiceNotAvailableController()
          : Stack(children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: LinearProgressIndicator(
                  value: value == -1 ? null : value,
                  valueColor: AlwaysStoppedAnimation<Color>(IGColors.primary()),
                  backgroundColor: IGColors.primary(opacity: 0.5),
                  minHeight: 3,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: LogoImage(
                    width: 100,
                    height: 100,
                    lightOnly:
                        webConfigurator.currentBrightness == Brightness.light,
                    darkOnly:
                        webConfigurator.currentBrightness == Brightness.dark),
              ),
            ]),
    );
  }
}
