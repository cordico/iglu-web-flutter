/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class ServiceNotAvailableController extends StatefulWidget {
  @override
  _ServiceNotAvailableControllerState createState() =>
      _ServiceNotAvailableControllerState();
}

class _ServiceNotAvailableControllerState
    extends State<ServiceNotAvailableController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: IGColors.primaryBackground(),
      body: SafeArea(
        bottom: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return Center(
              child: Container(
                  constraints: BoxConstraints(maxWidth: 600),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        LogoImage(
                            height: 75,
                            width: 75,
                            lightOnly: webConfigurator.currentBrightness ==
                                Brightness.light,
                            darkOnly: webConfigurator.currentBrightness ==
                                Brightness.dark),
                        Container(height: 10),
                        Image.asset(
                          "assets/images/service_not_available.png",
                          height: 300,
                          width: 400,
                          fit: BoxFit.contain,
                          package: "iglu_web_flutter",
                        ),
                        Container(height: 10),
                        Text("Service currently unavailable",
                            textAlign: TextAlign.center,
                            style: IGTextStyles.boldStyle(24, IGColors.text())),
                        Container(height: 5),
                        Text(
                            "Someone in our offices is already working to fix this\nWe apologize for the inconvenience",
                            textAlign: TextAlign.center,
                            style:
                                IGTextStyles.normalStyle(18, IGColors.text())),
                        Container(height: 10),
                        Container(
                          constraints: BoxConstraints(maxWidth: 320),
                          child: IGButtonViewDefaults.primaryDefaultButton(
                              context: context,
                              style: IGButtonViewStyle(
                                  text: "UPDATE",
                                  onTapPositioned: (pos) async {
                                    await webConfigurator.sessionHandler
                                        .loadConfig();
                                    webConfigurator.kingRouteDelegate
                                        .setNewRoutePath(KingRoutePath.root);
                                  })),
                        ),
                      ])),
            );
          },
        ),
      ),
    );
  }
}
