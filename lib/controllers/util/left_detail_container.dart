/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:async";
import "package:flutter/material.dart";

import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:iglu_web_flutter/widgets/utils/test_data_box_widget.dart';

class LeftDetailContainer<T> extends StatefulWidget {
  final GlobalKey<LeftDetailContainerState<T>>? key;
  final T? fromContextData;
  final Future<T> Function()? loadData;
  final List<DetailViewCellRule> Function(BoxConstraints, T?)? loadRules;
  final List<Widget> Function(BuildContext, BoxConstraints, T?)?
      additionalWidgets;
  final List<Widget> Function(BuildContext, BoxConstraints, T?)?
      previousWidgets;
  final String? title;
  final List<Widget> Function(T)? actions;
  final bool showEditAction;
  final Widget? header;
  final bool showHeader;
  final EdgeInsetsGeometry? headerPadding;
  final Function(bool, T?, BoxConstraints)? editActionTap;
  final Widget? largeLeftWidget;
  final bool showLineUnderHeader;
  final bool showEndSpace;
  final bool showDetailView;
  final bool dismissIfDataNotExists;
  final Widget? mainWidget;
  final bool showLargeLeftWidgetOnMobile;
  final bool showColumnLayoutOnMedium;
  final List<BreadCrumbItem> Function()? generateBreadCrumbItems;
  final Function? contextStateRefresh;
  final bool showBorderDetailView;
  final EdgeInsets padding;
  LeftDetailContainer(
      {this.key,
      this.loadData,
      this.loadRules,
      this.title,
      this.actions,
      this.fromContextData,
      this.additionalWidgets,
      this.previousWidgets,
      this.header,
      this.showEditAction = true,
      this.headerPadding,
      this.showHeader = true,
      this.largeLeftWidget,
      this.showLineUnderHeader = true,
      this.showEndSpace = true,
      this.showDetailView = true,
      this.dismissIfDataNotExists = true,
      this.showLargeLeftWidgetOnMobile = false,
      this.showColumnLayoutOnMedium = false,
      this.showBorderDetailView = true,
      this.padding = const EdgeInsets.all(20),
      this.mainWidget,
      this.generateBreadCrumbItems,
      this.contextStateRefresh,
      this.editActionTap});
  @override
  LeftDetailContainerState<T> createState() => LeftDetailContainerState<T>();
}

class LeftDetailContainerState<T> extends State<LeftDetailContainer<T>> {
  int selectedIndex = 0;
  bool showLoading = false;
  T? data;
  Timer? endTimer;
  double leftMaxWidth = 316;

  var leftController = ScrollController();
  SideItem? sideItem;

  refresh() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      loadData();
      sideItem = context.sideItem();
      if (leftController.hasClients) {
        leftController.jumpTo(sideItem!.scrollPageDetailOffset);
      }
    });
    leftController.addListener(_scrollToStoragePosition);
  }

  _scrollToStoragePosition() {
    if (sideItem != null) {
      sideItem!.scrollPageDetailOffset = leftController.offset;
    }
  }

  @override
  void dispose() {
    leftController.removeListener(_scrollToStoragePosition);
    super.dispose();
  }

  loadData() async {
    if (widget.fromContextData == null && widget.loadData != null) {
      setState(() {
        showLoading = true;
      });
      T? d = await widget.loadData!();
      if (d != null) {
        setState(() {
          showLoading = false;
          data = d;
        });
      } else if (widget.dismissIfDataNotExists) {
        setState(() {
          showLoading = false;
        });
        if (Navigator.of(context).canPop()) {
          Navigator.of(context).pop();
        } else {
          webConfigurator.kingRouteDelegate.goBack();
        }
      } else {
        setState(() {
          showLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return webConfigurator.internalConfigurator.remoteConfig == null
        ? ServiceNotAvailableController()
        : LayoutBuilder(builder: (context, constraints) {
            var _c = returnGridView();
            var maxWidthDivided = constraints.maxWidth / 2.5;
            return Scaffold(
                appBar: HeaderBar(
                    generateBreadCrumbItems: widget.generateBreadCrumbItems,
                    contextStateRefresh: widget.contextStateRefresh),
                backgroundColor: IGColors.primaryBackground(),
                body: Stack(
                  children: [
                    widget.showLargeLeftWidgetOnMobile &&
                            SizeDetector.isSmallScreen(context) &&
                            widget.largeLeftWidget != null
                        ? widget.largeLeftWidget!
                        : (SizeDetector.isLargeScreen(context) ||
                                    (SizeDetector.isMediumScreen(context) &&
                                        widget.showColumnLayoutOnMedium)) &&
                                widget.largeLeftWidget != null
                            ? Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Scrollbar(
                                    child: SingleChildScrollView(
                                      controller: leftController,
                                      child: Container(
                                          constraints: BoxConstraints(
                                              minHeight:
                                                  constraints.maxHeight - 55,
                                              maxWidth:
                                                  maxWidthDivided > leftMaxWidth
                                                      ? leftMaxWidth
                                                      : maxWidthDivided,
                                              minWidth:
                                                  maxWidthDivided > leftMaxWidth
                                                      ? leftMaxWidth
                                                      : maxWidthDivided),
                                          decoration: BoxDecoration(
                                              border: BorderDirectional(
                                                  end: BorderSide(
                                                      color:
                                                          IGColors.separator(),
                                                      width: 1.0))),
                                          child: widget.largeLeftWidget),
                                    ),
                                  ),
                                  Flexible(flex: 9, child: _c!)
                                ],
                              )
                            : _c!,
                    Align(
                      child: TestDataBoxWidget(),
                      alignment: Alignment.topCenter,
                    ),
                  ],
                ));
          });
  }

  _data() {
    return widget.fromContextData ?? data;
  }

  get showEmptyController {
    if (widget.showDetailView) {
      return _data() == null && !showLoading;
    } else if (widget.mainWidget != null) {
      return false;
    } else if (widget.previousWidgets != null ||
        widget.additionalWidgets != null) {
      return false;
    }
    return true;
  }

  Widget? returnGridView() {
    return showEmptyController
        ? EmptyController(showHeader: false)
        : widget.mainWidget != null
            ? widget.mainWidget
            : GestureDetector(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Scrollbar(
                  child: CustomScrollView(
                    slivers: <Widget>[
                      SliverPadding(
                        padding: widget.padding,
                        sliver: SliverToBoxAdapter(child:
                            LayoutBuilder(builder: (context, constraints) {
                          List<BreadCrumbItem> breadCrumbItems = [];
                          if (widget.generateBreadCrumbItems != null) {
                            breadCrumbItems = widget.generateBreadCrumbItems!();
                          }
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: ((SizeDetector.isSmallScreen(context) ||
                                            SizeDetector.isMediumScreen(
                                                context)) &&
                                        breadCrumbItems.length > 1
                                    ? <Widget>[
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 0, 10),
                                          child: FittedBox(
                                            child: IGButtonViewDefaults
                                                .primaryTextHoverBackgroundButton(
                                                    context: context,
                                                    style: IGButtonViewStyle(
                                                        text: (breadCrumbItems[
                                                                    breadCrumbItems.length -
                                                                        2]
                                                                .title) ??
                                                            "Back",
                                                        internalPadding:
                                                            EdgeInsets.fromLTRB(
                                                                5, 0, 5, 0),
                                                        icon: Icons
                                                            .arrow_back_rounded,
                                                        iconSize: 18,
                                                        externalPadding:
                                                            EdgeInsets.zero,
                                                        onTapPositioned: (_) {
                                                          webConfigurator
                                                              .kingRouteDelegate
                                                              .goBack();
                                                        })),
                                          ),
                                        )
                                      ]
                                    : <Widget>[]) +
                                (widget.previousWidgets != null
                                    ? widget.previousWidgets!(
                                        context,
                                        constraints,
                                        showLoading ? null : _data())
                                    : <Widget>[]) +
                                (widget.showDetailView
                                    ? <Widget>[
                                        DetailView(
                                            actions: widget.actions != null
                                                ? widget.actions!(_data())
                                                : [],
                                            header: widget.header,
                                            showBorder:
                                                widget.showBorderDetailView,
                                            showEditAction:
                                                widget.showEditAction,
                                            editActionTap: () async {
                                              if (widget.editActionTap !=
                                                  null) {
                                                await widget.editActionTap!(
                                                    showLoading,
                                                    _data(),
                                                    constraints);
                                              }
                                            },
                                            showHeader: widget.showHeader,
                                            showLoading: showLoading,
                                            title: widget.title ?? "Detail",
                                            cellRules: (constraints) {
                                              return (widget.showLineUnderHeader
                                                      ? <DetailViewCellRule>[
                                                          DetailViewCellRulePreBuild
                                                              .separator0(
                                                                  height: 1),
                                                          DetailViewCellRulePreBuild
                                                              .spaceHeight20(),
                                                        ]
                                                      : <
                                                          DetailViewCellRule>[]) +
                                                  (showLoading
                                                      ? [
                                                          DetailViewCellRule(
                                                              height: 125,
                                                              type:
                                                                  DetailViewCellRuleType
                                                                      .space),
                                                          DetailViewCellRule(
                                                              type:
                                                                  DetailViewCellRuleType
                                                                      .text,
                                                              alignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              contentPadding:
                                                                  EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          20),
                                                              content:
                                                                  "Loading...",
                                                              textStyle: IGTextStyles
                                                                  .normalStyle(
                                                                      30,
                                                                      IGColors
                                                                          .text())),
                                                          DetailViewCellRule(
                                                              height: 125,
                                                              type:
                                                                  DetailViewCellRuleType
                                                                      .space)
                                                        ]
                                                      : (_data() == null ||
                                                              widget.loadRules ==
                                                                  null
                                                          ? []
                                                          : (widget.loadRules!(
                                                                  constraints,
                                                                  _data()) +
                                                              (widget.showEndSpace
                                                                  ? <DetailViewCellRule>[
                                                                      DetailViewCellRulePreBuild
                                                                          .spaceHeight20(),
                                                                    ]
                                                                  : <DetailViewCellRule>[]))));
                                            }),
                                      ]
                                    : <Widget>[]) +
                                (widget.additionalWidgets != null
                                    ? widget.additionalWidgets!(
                                        context,
                                        constraints,
                                        showLoading ? null : _data())
                                    : <Widget>[]),
                          );
                        })),
                      ),
                      SliverFillRemaining(
                          child: Container(),
                          fillOverscroll: false,
                          hasScrollBody: false),
                      SliverToBoxAdapter(child: FooterBar())
                    ],
                  ),
                ),
              );
  }
}
