/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class TooSmallController extends StatefulWidget {
  @override
  _TooSmallControllerState createState() => _TooSmallControllerState();
}

class _TooSmallControllerState extends State<TooSmallController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: IGColors.primaryBackground(),
      body: SafeArea(
        bottom: false,
        child: Center(
          child: Container(
              margin: EdgeInsets.all(20),
              constraints: BoxConstraints(maxWidth: 600),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    LogoImage(
                        height: 75,
                        width: 75,
                        lightOnly: webConfigurator.currentBrightness ==
                            Brightness.light,
                        darkOnly: webConfigurator.currentBrightness ==
                            Brightness.dark),
                    Container(height: 10),
                    Text(
                        "Your device is not currently supported. We apologize for the inconvenience",
                        textAlign: TextAlign.center,
                        maxLines: 6,
                        style: IGTextStyles.boldStyle(24, IGColors.text())),
                  ])),
        ),
      ),
    );
  }
}
