
class Language {
  String? key;
  Map<String, String> nameForLocale;
  String? native;
  String? phone;
  String? continent;
  String? capital;
  String? currency;
  List<String>? languages;
  String? emoji;
  String? emojiU;
  Language(
      {this.capital,
      this.continent,
      this.currency,
      this.emoji,
      this.emojiU,
      this.languages,
      this.native,
      this.phone,
      this.key,
      required this.nameForLocale});

  String? name({required String locale}) {
    if (nameForLocale.containsKey(locale)) {
      return nameForLocale[locale];
    }
    return "";
  }
}
