/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:async";

import "package:flutter/cupertino.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import "package:iglu_web_flutter/l10n/messages_all.dart";
import "package:intl/intl.dart";

class KingLocalizations {
  KingLocalizations(this.localeName);

  static Future<KingLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return KingLocalizations(localeName);
    });
  }

  final String localeName;

  static KingLocalizations? of(BuildContext? context) {
    return Localizations.of<KingLocalizations>(
        context != null ? context : webConfigurator.mainContext!,
        KingLocalizations);
  }
}

class KingLocalizationsDelegate
    extends LocalizationsDelegate<KingLocalizations> {
  const KingLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ["en"].contains(locale.languageCode);
  }

  List<Locale> supportedLocale() {
    return [Locale("en")];
  }

  @override
  Future<KingLocalizations> load(Locale locale) {
    return KingLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<KingLocalizations> old) {
    return false;
  }
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}
