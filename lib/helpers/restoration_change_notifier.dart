/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
import 'package:flutter/widgets.dart';

class RestorationChangeNotifier extends ChangeNotifier {
  num _sidebarScrollOffset = 0;

  num get sidebarScrollOffset => _sidebarScrollOffset;

  set sidebarScrollOffset(num v) {
    _sidebarScrollOffset = v;
    notifyListeners();
  }
}
