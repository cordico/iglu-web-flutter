/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class SessionHandler {
  SessionHandler();
  var alreadyPresented = false;
  showInvalidSessionPopup() {
    if (webConfigurator.mainContext == null) {
      return;
    }
    if (webConfigurator.internalConfigurator.loggedInUser == null) {
      return;
    }
    if (alreadyPresented) {
      return;
    }
    alreadyPresented = true;
    AlertView.showSimpleDialog(webConfigurator.mainContext!,
        title: "Session expired",
        message: "Your session has expired. You must log in again to continue",
        actionTitle: "Log in",
        cancelTitle: "",
        dismissable: false,
        cancelActionPrimaryColor: Colors.transparent,
        okActionPrimaryColor: IGColors.primary(), okAction: () async {
      handleLogout();
    });
  }

  handleLogout({BuildContext? context}) async {
    webConfigurator.internalConfigurator.loggedInUser = null;
    webConfigurator.sideItemManager.resetSelectedTabs();
    webConfigurator.kingRouteDelegate.setNewRoutePath(KingRoutePath.login);
    webConfigurator.isLeftMenuExpanded = true;
    webConfigurator.setIsLeftMenuExpandedLocally(true);
    if (webConfigurator.internalConfigurator.logoutHandler != null) {
      webConfigurator.internalConfigurator.logoutHandler!();
    }
  }

  var isLoadingConfig = false;
  loadConfig() async {
    if (!isLoadingConfig) {
      isLoadingConfig = true;
      if (webConfigurator
              .internalConfigurator.handlersConfigurations?.configHandler !=
          null) {
        webConfigurator.internalConfigurator.remoteConfig =
            await webConfigurator
                .internalConfigurator.handlersConfigurations!.configHandler!();
      } else {
        webConfigurator.internalConfigurator.remoteConfig = Config();
      }
      isLoadingConfig = false;
      webConfigurator.kingState?.refresh();
    }
  }

  var isLoadingSession = false;
  loadSession() async {
    if (!isLoadingSession &&
        webConfigurator
                .internalConfigurator.handlersConfigurations?.sessionHandler !=
            null) {
      isLoadingSession = true;
      webConfigurator.internalConfigurator.loggedInUser = await webConfigurator
          .internalConfigurator.handlersConfigurations?.sessionHandler!();

      if (webConfigurator.internalConfigurator.manageUserSession != null &&
          webConfigurator.internalConfigurator.loggedInUser != null) {
        webConfigurator.internalConfigurator.manageUserSession!(
            webConfigurator.internalConfigurator.loggedInUser);
      }
      isLoadingSession = false;
    }
  }
}
