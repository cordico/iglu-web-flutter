/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///
import 'package:flutter/widgets.dart';
import 'package:iglu_web_flutter/iglu_web_flutter.dart';
import 'package:provider/provider.dart';

class HeaderBarChangeNotifier extends ChangeNotifier {
  void refresh() {
    notifyListeners();
  }

  static HeaderBarChangeNotifier shared({
    BuildContext? context,
    bool listen = false,
  }) {
    try {
      if (context != null || webConfigurator.mainContext != null) {
        return Provider.of<HeaderBarChangeNotifier>(
          (context ?? webConfigurator.mainContext)!,
          listen: listen,
        );
      }
    } catch (e) {
      return HeaderBarChangeNotifier();
    }
    return HeaderBarChangeNotifier();
  }
}
