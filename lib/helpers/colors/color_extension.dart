/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import 'dart:math' as Math;
import "package:iglu_web_flutter/iglu_web_flutter.dart";

extension ColorManipulatorExtension on Color {
  HSVColor toHsv() => _ColorManipulator(this).toHsv();

  HslColor toHsl() => _ColorManipulator(this).toHsl();

  /// Lighten the color a given amount, from 0 to 100. Providing 100 will always return white.
  Color? lighten([int amount = 10]) =>
      _ColorManipulator(this).lighten(amount).color;

  /// Brighten the color a given amount, from 0 to 100.
  Color? brighten([int amount = 10]) =>
      _ColorManipulator(this).brighten(amount).color;

  /// Darken the color a given amount, from 0 to 100. Providing 100 will always return black.
  Color? darken([int amount = 10]) =>
      _ColorManipulator(this).darken(amount).color;

  /// Mix the color with pure white, from 0 to 100. Providing 0 will do nothing, providing 100 will always return white.
  Color? tint([int amount = 10]) => _ColorManipulator(this).tint(amount).color;

  /// Mix the color with pure black, from 0 to 100. Providing 0 will do nothing, providing 100 will always return black.
  Color? shade([int amount = 10]) =>
      _ColorManipulator(this).shade(amount).color;

  /// Desaturate the color a given amount, from 0 to 100. Providing 100 will is the same as calling greyscale.
  Color? desaturate([int amount = 10]) =>
      _ColorManipulator(this).desaturate(amount).color;

  /// Saturate the color a given amount, from 0 to 100.
  Color? saturate([int amount = 10]) =>
      _ColorManipulator(this).saturate(amount).color;

  /// Completely desaturates a color into greyscale. Same as calling desaturate(100).
  Color? get greyscale => _ColorManipulator(this).greyscale().color;

  /// Spin the hue a given amount, from -360 to 360. Calling with 0, 360, or -360 will do nothing (since it sets the hue back to what it was before).
  Color? spin([double amount = 0]) =>
      _ColorManipulator(this).spin(amount).color;

  /// Returns the perceived brightness of a color, from 0-255, as defined by Web Content Accessibility Guidelines (Version 1.0).Returns the perceived brightness of a color, from 0-255, as defined by Web Content Accessibility Guidelines (Version 1.0).
  double get brightness => _ColorManipulator(this).getBrightness();

  /// Return the perceived luminance of a color, a shorthand for flutter Color.computeLuminance
  double get luminance => _ColorManipulator(this).getLuminance();

  /// Return a boolean indicating whether the color's perceived brightness is light.
  bool get isLight => _ColorManipulator(this).isLight();

  /// Return a boolean indicating whether the color's perceived brightness is dark.
  bool get isDark => _ColorManipulator(this).isDark();

  /// Returns the Complimentary Color for dynamic matching
  Color? get compliment => _ColorManipulator(this).complement().color;

  /// Blends the color with another color a given amount, from 0 - 100, default 50.
  Color? mix(Color toColor, [int amount = 50]) =>
      _ColorManipulator(this).mix(input: toColor, amount: amount).color;
}

//
class _ColorManipulator {
  Color? originalColor;
  Color? _color;

  _ColorManipulator(Color color) {
    this.originalColor =
        Color.fromARGB(color.alpha, color.red, color.green, color.blue);
    this._color =
        Color.fromARGB(color.alpha, color.red, color.green, color.blue);
  }

  /*factory _ColorManipulator.fromRGB(
      {@required int r, @required int g, @required int b, int a = 100}) {
    return _ColorManipulator(Color.fromARGB(a, r, g, b));
  }*/

  factory _ColorManipulator.fromHSL(HslColor hsl) {
    return _ColorManipulator(hslToColor(hsl));
  }

  /*factory _ColorManipulator.fromHSV(HSVColor hsv) {
    return _ColorManipulator(hsv.toColor());
  }

  factory _ColorManipulator.fromString(String string) {
    return _ColorManipulator(string.getColor());
  }*/

  bool isDark() {
    return this.getBrightness() < 128.0;
  }

  bool isLight() {
    return !this.isDark();
  }

  double getBrightness() {
    return (_color!.red * 299 + _color!.green * 587 + _color!.blue * 114) /
        1000;
  }

  double getLuminance() {
    return _color!.computeLuminance();
  }

  _ColorManipulator setAlpha(int alpha) {
    _color!.withAlpha(alpha);
    return this;
  }

  _ColorManipulator setOpacity(double opacity) {
    _color!.withOpacity(opacity);
    return this;
  }

  HSVColor toHsv() {
    return colorToHsv(_color!);
  }

  HslColor toHsl() {
    final hsl = rgbToHsl(
      r: _color!.red.toDouble(),
      g: _color!.green.toDouble(),
      b: _color!.blue.toDouble(),
    );
    return HslColor(
        h: hsl.h * 360, s: hsl.s, l: hsl.l, a: _color!.alpha.toDouble());
  }

  _ColorManipulator clone() {
    return _ColorManipulator(_color!);
  }

  _ColorManipulator lighten([int amount = 10]) {
    final hsl = this.toHsl();
    hsl.l += amount / 100;
    hsl.l = clamp01(hsl.l);
    return _ColorManipulator.fromHSL(hsl);
  }

  _ColorManipulator brighten([int amount = 10]) {
    final color = Color.fromARGB(
      _color!.alpha,
      Math.max(0, Math.min(255, _color!.red - (255 * -(amount / 100)).round())),
      Math.max(
          0, Math.min(255, _color!.green - (255 * -(amount / 100)).round())),
      Math.max(
          0, Math.min(255, _color!.blue - (255 * -(amount / 100)).round())),
    );
    return _ColorManipulator(color);
  }

  _ColorManipulator darken([int amount = 10]) {
    final hsl = this.toHsl();
    hsl.l -= amount / 100;
    hsl.l = clamp01(hsl.l);
    return _ColorManipulator.fromHSL(hsl);
  }

  _ColorManipulator tint([int amount = 10]) {
    return this.mix(input: Color.fromRGBO(255, 255, 255, 1.0));
  }

  _ColorManipulator shade([int amount = 10]) {
    return this.mix(input: Color.fromRGBO(0, 0, 0, 1.0));
  }

  _ColorManipulator desaturate([int amount = 10]) {
    final hsl = this.toHsl();
    hsl.s -= amount / 100;
    hsl.s = clamp01(hsl.s);
    return _ColorManipulator.fromHSL(hsl);
  }

  _ColorManipulator saturate([int amount = 10]) {
    final hsl = this.toHsl();
    hsl.s += amount / 100;
    hsl.s = clamp01(hsl.s);
    return _ColorManipulator.fromHSL(hsl);
  }

  _ColorManipulator greyscale() {
    return desaturate(100);
  }

  _ColorManipulator spin(double amount) {
    final hsl = this.toHsl();
    final hue = (hsl.h + amount) % 360;
    hsl.h = hue < 0 ? 360 + hue : hue;
    return _ColorManipulator.fromHSL(hsl);
  }

  _ColorManipulator mix({required Color input, int amount = 50}) {
    final int p = (amount / 100).round();
    final color = Color.fromARGB(
        (input.alpha - _color!.alpha) * p + _color!.alpha,
        (input.red - _color!.red) * p + _color!.red,
        (input.green - _color!.green) * p + _color!.green,
        (input.blue - _color!.blue) * p + _color!.blue);
    return _ColorManipulator(color);
  }

  _ColorManipulator complement() {
    final hsl = this.toHsl();
    hsl.h = (hsl.h + 180) % 360;
    return _ColorManipulator.fromHSL(hsl);
  }

  Color? get color {
    return _color;
  }
}
