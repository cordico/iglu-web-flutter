/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "dart:core";
import "package:flutter/material.dart";
import 'package:hive/hive.dart';
import "package:iglu_web_flutter/iglu_web_flutter.dart";
import 'package:provider/single_child_widget.dart';
import 'package:tuple/tuple.dart';

Manager webConfigurator = Manager();

double kButtonPrimaryCornerRadius = 8.0;
double kDefaultMarginSpace = 20.0;
//

bool isDebugging = true;
logger(dynamic input, {String tag = "[DEBUG]"}) {
  if (isDebugging) {
    debugPrint("$tag -- $input");
  }
}

class Manager {
  Manager();
  late Box hivePreferencesBox;
  Brightness currentBrightness = Brightness.light;
  bool? isLeftMenuExpanded = true;
  bool isInsideResetScreen = false;
  Configuration internalConfigurator = Configuration();
  SideItemManager sideItemManager = SideItemManager();
  SessionHandler sessionHandler = SessionHandler();
  TimeManager timeManager = TimeManager();
  PlacesManager placesManager = PlacesManager();
  double drawerBlurRadius = 0.1;
  RouteInformation? resetScreenSettings;
  BuildContext? mainContext;
  KingState? kingState;
  KingRouteDelegate kingRouteDelegate = KingRouteDelegate();
  RootState? rootState;
  bool initialAppereanceConfigured = false;
  String defaultLanguage = "en";
  Flush? currentFlush;
  List<Language> supportedLanguage = [
    Language(
        key: "it",
        nameForLocale: {"it": "Italiano", "en": "Italian"},
        emoji: "🇮🇹"),
    Language(
        key: "en",
        nameForLocale: {"it": "Inglese", "en": "English"},
        emoji: "🇺🇸")
  ];

  bool isDuringLoginCodeProcess = false;

  initHive() async {
    //var key = Hive.generateSecureKey();
    //print(key);
    await Hive.openBox('iglu.preferencesBox',
        encryptionCipher: HiveAesCipher(
            internalConfigurator.cryptoManager.hiveSecureKeyPreferences));
    hivePreferencesBox = Hive.box('iglu.preferencesBox');
  }

  refreshInternals() {
    if (kingState != null) {
      kingState!.refresh();
    }
    if (rootState != null) {
      rootState!.refresh();
    }
  }

  setIsLeftMenuExpandedLocally(bool value) async {
    hivePreferencesBox.put("isLeftMenuExpanded", value);
  }

  isDark() {
    return currentBrightness == Brightness.dark;
  }

  openUrl(
      {required String url,
      bool? forceSafariVC,
      bool forceWebView = false,
      bool enableJavaScript = false,
      bool enableDomStorage = false,
      bool universalLinksOnly = false,
      Map<String, String> headers = const <String, String>{},
      Brightness? statusBarBrightness,
      String? webOnlyWindowName}) async {
    try {
      if (await canLaunch(url)) {
        await launch(url,
            enableDomStorage: enableDomStorage,
            enableJavaScript: enableJavaScript,
            forceSafariVC: forceSafariVC,
            forceWebView: forceWebView,
            headers: headers,
            statusBarBrightness: statusBarBrightness,
            universalLinksOnly: universalLinksOnly,
            webOnlyWindowName: webOnlyWindowName);
      } else {
        logger('error-----');
      }
    } catch (e) {
      logger('error-----$e');
    }
  }

  showFlush(
      {required BuildContext context,
      required String content,
      Icon? icon,
      IGButtonView? action,
      Duration? duration}) {
    if (currentFlush != null) {
      currentFlush!.dismiss();
    }
    currentFlush = Flush(
        flushbarPosition: FlushbarPosition.TOP,
        backgroundColor: IGColors.secondaryBackground(isOpposite: true),
        borderRadius: kButtonPrimaryCornerRadius,
        blockBackgroundInteraction: false,
        isDismissible: false,
        messageText: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      if (icon != null) icon,
                      if (icon != null) Container(width: 8),
                      Flexible(
                        child: Text(content,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: IGTextStyles.normalStyle(
                                14, IGColors.text(isOpposite: true))),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            if (action != null) Container(width: 8),
            action ?? Container()
          ],
        ),
        duration: duration ?? Duration(seconds: 4),
        animationDuration: Duration(milliseconds: 500),
        margin: SizeDetector.isSmallScreen(context)
            ? EdgeInsets.fromLTRB(20, 20, 20, 20)
            : EdgeInsets.fromLTRB(MediaQuery.of(context).size.width / 4.0, 20,
                MediaQuery.of(context).size.width / 4.0, 20));
    return currentFlush!.show(context);
  }
}

class HandlersConfigurations {
  Future<Config?> Function()? configHandler;
  Future<UserSchemaModel?> Function(String, String)? loginHandler;
  Future<UserSchemaModel?> Function()? sessionHandler;
  Future<bool> Function()? logoutHandler;
  Future<Tuple2<String?, bool>> Function(String)? sendFeedbackHandler;

  HandlersConfigurations(
      {this.loginHandler,
      this.sessionHandler,
      this.configHandler,
      this.logoutHandler,
      this.sendFeedbackHandler});
}

class HeaderBarConfigurations {
  bool showHeaderNotification;
  bool showHeaderFeedback;
  bool showChangePassword;

  List<Widget> customWidgets;

  HeaderBarConfigurations(
      {this.showHeaderFeedback = false,
      this.showChangePassword = false,
      this.showHeaderNotification = false,
      this.customWidgets = const []});
}

class FooterBarConfigurations {
  bool showCreatorsInsideTheFooter;

  FooterBarConfigurations({
    this.showCreatorsInsideTheFooter = false,
  });
}

class Configuration {
  String? version;
  String? appName;
  String? year;
  String? companyName;
  String? companyURL;

  bool clearUnknownURL;
  bool showRandomImageInTheLogin;
  bool loadSession;
  bool loadConfig;
  String? assetPathLight;
  String? assetPathDark;
  Function? logoutHandler;
  Function? sideItemTabSelectionHandler;
  Future Function(UserSchemaModel?, BuildContext)? manageUserLogin;
  Function(UserSchemaModel?)? manageUserSession;
  Function(BuildContext)? loadInternalData;
  UserSchemaModel? loggedInUser;
  Config? remoteConfig;
  Map<String, LoginType> loginTypes;
  List<LocalizationsDelegate<dynamic>> localizationsDelegates;
  List<Locale> supportedLocales;
  List<EnabledRoutes> enabledRoutes;
  Route<dynamic> Function(RouteSettings)? onGenerateRouteCustom;
  List<UserSchemaModelRole> allowedUserForLogin;
  List<SingleChildWidget> providers;
  //MARK: ENTRY POINT VARIABLES
  bool showEntryPointKing = false;
  Widget? entryPointWidget;
  Future<String> Function(String?)? clearEntryPointWidget;
  //MARK: ROUTING
  String _mainRouteToDisplay = "";
  bool show404Page = false;
  //MARK: FONT
  Tuple2<TextStyle, TextTheme> Function(TextStyle?, TextTheme?)? customFont;

  //MARK: FCMTOKEN
  Future<String> Function()? fcmTokenToDelete;

  //MARK: COLORS
  Color? primaryColor;

  //MARK: STYLES
  SideBarStyle sideBarStyle;

  //MARK: HANDLERS
  HandlersConfigurations? handlersConfigurations;
  HeaderBarConfigurations? headerBarConfigurations;
  FooterBarConfigurations? footerBarConfigurations;

  //MARK: API KEYS
  String? googleMapsApiKey;

  //MARK: CRYPTO
  late CryptoManager cryptoManager;

  Configuration(
      {this.version,
      this.appName,
      this.companyName,
      this.year,
      this.companyURL,
      this.loginTypes = const {},
      this.showRandomImageInTheLogin = false,
      this.loadSession = true,
      this.loadConfig = true,
      this.clearUnknownURL = true,
      this.loadInternalData,
      this.manageUserSession,
      this.assetPathDark,
      this.assetPathLight,
      this.logoutHandler,
      this.sideItemTabSelectionHandler,
      this.manageUserLogin,
      this.localizationsDelegates = const [],
      this.supportedLocales = const [],
      this.enabledRoutes = const [],
      this.onGenerateRouteCustom,
      this.providers = const [],
      this.showEntryPointKing = false,
      this.show404Page = false,
      this.entryPointWidget,
      this.clearEntryPointWidget,
      this.customFont,
      String mainRouteToDisplay = "",
      this.sideBarStyle = SideBarStyle.normal,
      this.fcmTokenToDelete,
      this.primaryColor,
      this.allowedUserForLogin = const [],
      this.handlersConfigurations,
      this.headerBarConfigurations,
      this.footerBarConfigurations,
      this.googleMapsApiKey,
      String? passphrase,
      List<int>? hiveSecureKeyPreferences}) {
    this.year = year ?? "${DateTime.now().year}";
    this._mainRouteToDisplay = mainRouteToDisplay;
    cryptoManager = CryptoManager(
        passphrase: passphrase ?? '',
        hiveSecureKeyPreferences: hiveSecureKeyPreferences ?? []);
  }

  int get numberOfPathSegmentOfMainRoute {
    var r = mainRouteToDisplay();
    var r1 = r.split("/");
    r1.removeWhere((element) => element.isEmpty);
    return r1.length;
  }

  set mainRoute(String newValue) {
    this._mainRouteToDisplay = newValue;
  }

  String mainRouteToDisplay({bool forceNotEmpty = false}) {
    if (_mainRouteToDisplay.isEmpty && forceNotEmpty) {
      return "/";
    }
    return _mainRouteToDisplay;
  }

  bool loadedSession = false;
}

enum LoginType { normal, code }
