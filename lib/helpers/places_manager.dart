/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'package:http/http.dart' as http;
import 'package:iglu_web_flutter/extensions/map_extension.dart';
import 'dart:convert' as convert;

import 'package:iglu_web_flutter/helpers/constants.dart';

class PlacesManager {
  PlacesManager();

  Future<List<PlaceDetails>> geocode(
    String address, {
    String language = 'en',
    String components = 'country:us',
    String types = 'address',
    String? region,
    String? bounds,
  }) async {
    List<PlaceDetails> resultsFinal = [];
    if (webConfigurator.internalConfigurator.googleMapsApiKey == null ||
        webConfigurator.internalConfigurator.googleMapsApiKey?.isEmpty ==
            true) {
      return resultsFinal;
    }
    var queryParameters = {
      'address': '${address.replaceAll(' ', '+')}',
      'types': types,
      'language': language,
      'components': components,
      'region': region,
      'bounds': bounds,
      'key': '${webConfigurator.internalConfigurator.googleMapsApiKey}'
    };
    removeNullAndEmptyParams(queryParameters);
    var uri = Uri.https(
        'maps.googleapis.com', '/maps/api/geocode/json', queryParameters);
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var results = jsonResponse['results'] as List<dynamic>;
      results.forEach((p) {
        try {
          var predictions = p['address_components'] as List<dynamic>;
          var result = PlaceDetails();
          predictions.forEach((p) {
            if ((p['types'] as List).contains('route')) {
              result.street = p['short_name'];
            }
            if ((p['types'] as List).contains('street_number')) {
              result.number = p['short_name'];
            }
            if ((p['types'] as List).contains('locality')) {
              result.city = p['short_name'];
            }
            if ((p['types'] as List).contains('administrative_area_level_2')) {
              result.province = p['short_name'];
            }
            if ((p['types'] as List).contains('country')) {
              result.country = p['short_name'];
            }
            if ((p['types'] as List).contains('postal_code')) {
              result.zipCode = p['short_name'];
            }
          });
          result.placeID = p['place_id'];
          List<num> latLng = [];
          latLng.add(p['geometry']['location']['lat']);
          latLng.add(p['geometry']['location']['lng']);
          result.coordinates = latLng;
          //print('RESULT');
          //print(result.street);
          //print(result.number);
          //print(result.city);
          //print(result.zipCode);
          //print(result.province);
          //print(result.placeID);
          //print(result.coordinates);
          resultsFinal.add(result);
        } catch (e) {
          logger(e);
        }
      });
    }
    return resultsFinal;
  }

  Future<List<Place>> autocomplete(
    String address, {
    String language = 'en',
    String components = 'country:us',
    String types = 'address',
    String? location,
    String? radius,
    String? strictbounds,
  }) async {
    List<Place> result = [];

    if (webConfigurator.internalConfigurator.googleMapsApiKey == null ||
        webConfigurator.internalConfigurator.googleMapsApiKey?.isEmpty ==
            true) {
      return result;
    }

    var queryParameters = {
      'input': '${address.replaceAll(' ', '+')}',
      'types': types,
      'language': language,
      'components': components,
      'location': location, //'40.852181,14.268418',
      'radius': radius, //'50000',
      'strictbounds': strictbounds, //'',
      'key': '${webConfigurator.internalConfigurator.googleMapsApiKey}'
    };

    removeNullAndEmptyParams(queryParameters);

    var uri = Uri.https('maps.googleapis.com',
        '/maps/api/place/autocomplete/json', queryParameters);

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var predictions = jsonResponse['predictions'] as List<dynamic>;
      predictions.forEach((p) {
        Place place = Place.fromJson(p);
        result.add(place);
      });
      return result;
    } else {
      return [];
    }
  }

  Future<PlaceDetails?> getPlaceDetails(String id) async {
    PlaceDetails result;

    if (webConfigurator.internalConfigurator.googleMapsApiKey == null ||
        webConfigurator.internalConfigurator.googleMapsApiKey?.isEmpty ==
            true) {
      return null;
    }

    var queryParameters = {
      'place_id': '$id',
      'fields': 'address_component,place_id,geometry',
      'key': '${webConfigurator.internalConfigurator.googleMapsApiKey}'
    };

    var uri = Uri.https(
        'maps.googleapis.com', '/maps/api/place/details/json', queryParameters);

    var response = await http.get(uri);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var predictions =
          jsonResponse['result']['address_components'] as List<dynamic>;
      result = PlaceDetails();
      predictions.forEach((p) {
        if ((p['types'] as List).contains('route')) {
          result.street = p['short_name'];
        }
        if ((p['types'] as List).contains('street_number')) {
          result.number = p['short_name'];
        }
        if ((p['types'] as List).contains('locality')) {
          result.city = p['short_name'];
        }
        if ((p['types'] as List).contains('administrative_area_level_2')) {
          result.province = p['short_name'];
        }
        if ((p['types'] as List).contains('country')) {
          result.country = p['short_name'];
        }
        if ((p['types'] as List).contains('postal_code')) {
          result.zipCode = p['short_name'];
        }
      });
      result.placeID = jsonResponse['result']['place_id'];
      List<num> latLng = [];
      latLng.add(jsonResponse['result']['geometry']['location']['lat']);
      latLng.add(jsonResponse['result']['geometry']['location']['lng']);
      result.coordinates = latLng;

      //print('RESULT');
      //print(result.street);
      //print(result.number);
      //print(result.city);
      //print(result.zipCode);
      //print(result.province);
      //print(result.placeID);
      //print(result.coordinates);

      return result;
    } else {
      return null;
    }
  }
}

class Place {
  String? placeID;
  String? mainString;
  String? secondString;

  Place({this.placeID, this.mainString, this.secondString});

  Place.fromJson(Map<String, dynamic> json) {
    this.placeID = json['place_id'];
    this.mainString = json['structured_formatting']['main_text'];
    this.secondString = json['structured_formatting']['secondary_text'];
  }
}

class PlaceDetails {
  String? placeID;
  String? street;
  String? city;
  String? number;
  String? province;
  String? zipCode;
  String? country;
  List<num>? coordinates;

  PlaceDetails({
    this.street,
    this.city,
    this.number,
    this.province,
    this.country,
    this.zipCode,
    this.placeID,
    this.coordinates,
  });
}
