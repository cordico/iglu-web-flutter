/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import 'dart:ui';
import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

/// NAME         SIZE  WEIGHT  SPACING
/// headline1    96.0  light   -1.5
/// headline2    60.0  light   -0.5
/// headline3    48.0  regular  0.0
/// headline4    34.0  regular  0.25
/// headline5    24.0  regular  0.0
/// headline6    20.0  medium   0.15
/// subtitle1    16.0  regular  0.15
/// subtitle2    14.0  medium   0.1
/// body1        16.0  regular  0.5   (bodyText1)
/// body2        14.0  regular  0.25  (bodyText2)
/// button       14.0  medium   1.25
/// caption      12.0  regular  0.4
/// overline     10.0  regular  1.5

class IGTextStyles {
  static TextStyle custom(
      {bool inherit = true,
      Color? color,
      Color? backgroundColor,
      double? fontSize,
      FontWeight? fontWeight,
      FontStyle? fontStyle,
      double? letterSpacing,
      double? wordSpacing,
      TextBaseline? textBaseline,
      double? height,
      Locale? locale,
      Paint? foreground,
      Paint? background,
      List<Shadow>? shadows,
      List<FontFeature>? fontFeatures,
      TextDecoration? decoration,
      Color? decorationColor,
      TextDecorationStyle? decorationStyle,
      double? decorationThickness,
      String? debugLabel,
      String? fontFamily,
      List<String>? fontFamilyFallback,
      String? package}) {
    var style = TextStyle(
      background: background,
      backgroundColor: backgroundColor,
      color: color,
      debugLabel: debugLabel,
      decoration: decoration,
      decorationColor: decorationColor,
      decorationStyle: decorationStyle,
      decorationThickness: decorationThickness,
      fontFamily: fontFamily,
      fontFamilyFallback: fontFamilyFallback,
      fontFeatures: fontFeatures,
      fontSize: fontSize,
      fontStyle: fontStyle,
      fontWeight: fontWeight,
      foreground: foreground,
      height: height,
      inherit: inherit,
      letterSpacing: letterSpacing,
      locale: locale,
      package: package,
      shadows: shadows,
      textBaseline: textBaseline,
      wordSpacing: wordSpacing,
    );
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline1(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 96.0,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline2(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 60.0,
        fontWeight: FontWeight.w300,
        letterSpacing: -0.5);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline3(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 48.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline4(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 34.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline5(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 24.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle headline6(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 20.0,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle subtitle1(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle subtitle2(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 14.0,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle body1(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 16.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle body2(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 14.0,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle button(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        letterSpacing: 1.25);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle caption(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 12,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle overline(Color color) {
    var style = TextStyle(
        color: color,
        fontSize: 10,
        fontWeight: FontWeight.w400,
        letterSpacing: 1.5);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle lightStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w300);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle normalStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w400);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle mediumStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w500);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle semiboldStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w600);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle boldStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w700);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle extraBoldStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w800);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static TextStyle blackStyle(double size, Color color) {
    var style =
        TextStyle(color: color, fontSize: size, fontWeight: FontWeight.w900);
    if (webConfigurator.internalConfigurator.customFont != null) {
      return webConfigurator
          .internalConfigurator.customFont!(style, null).item1;
    }
    return style;
  }

  static InputDecoration returnTextFieldInputDecoration(BuildContext context,
      {bool? check,
      String? hint,
      EdgeInsets padding =
          const EdgeInsets.only(left: 0, right: 30, top: 8, bottom: 8),
      bool lineVisible = true,
      String? labelText,
      String? helperText,
      int? helperMaxLines,
      int errorMaxLines = 4,
      FloatingLabelBehavior? floatingLabelBehavior}) {
    return InputDecoration(
      contentPadding: padding,
      isDense: true,
      labelStyle: IGTextStyles.semiboldStyle(20, IGColors.text()),
      hintText: hint,
      labelText: labelText,
      helperText: helperText,
      errorMaxLines: errorMaxLines,
      helperMaxLines: helperMaxLines,
      floatingLabelBehavior: floatingLabelBehavior,
      disabledBorder: lineVisible
          ? UnderlineInputBorder(
              borderSide: BorderSide(color: IGColors.separator()),
            )
          : InputBorder.none,
      focusedBorder: lineVisible
          ? UnderlineInputBorder(
              borderSide: BorderSide(color: IGColors.separator()),
            )
          : InputBorder.none,
      enabledBorder: lineVisible
          ? UnderlineInputBorder(
              borderSide: BorderSide(
                  color: (check == null)
                      ? IGColors.separator()
                      : check
                          ? IGColors.separator()
                          : IGColors.red()),
            )
          : InputBorder.none,
    );
  }
}
