/// IGLU WEB FLUTTER
///
/// Copyright © 2021 IGLU. All rights reserved.
/// Copyright © 2021 IGLU
///

import "package:flutter/material.dart";
import "package:iglu_web_flutter/iglu_web_flutter.dart";

class IGColors {
  static Color primaryBackground(
      {double opacity = 1.0,
      bool forceLight = false,
      bool forceDark = false,
      bool isOpposite = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _primaryBackgroundColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))
        .withOpacity(useSelectedOpacity
            ? IGColors.selectedOpacity()
            : useBrightnessOpacity
                ? IGColors.brightnessOpacity()
                : opacity);
  }

  static Color secondaryBackground(
      {double opacity = 1.0,
      bool isOpposite = false,
      bool forceLight = false,
      bool forceDark = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _secondaryBackgroundColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))
        .withOpacity(useSelectedOpacity
            ? IGColors.selectedOpacity()
            : useBrightnessOpacity
                ? IGColors.brightnessOpacity()
                : opacity);
  }

  static Color tertiaryBackground(
      {double opacity = 1.0,
      bool isOpposite = false,
      bool forceLight = false,
      bool forceDark = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _tertiaryBackgroundColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))
        .withOpacity(useSelectedOpacity
            ? IGColors.selectedOpacity()
            : useBrightnessOpacity
                ? IGColors.brightnessOpacity()
                : opacity);
  }

  static Color separator(
      {double opacity = 1.0,
      bool isOpposite = false,
      bool forceLight = false,
      bool forceDark = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _separatorColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))
        .withOpacity(useSelectedOpacity
            ? IGColors.selectedOpacity()
            : useBrightnessOpacity
                ? IGColors.brightnessOpacity()
                : opacity);
  }

  static Color text(
      {double opacity = 1.0,
      bool isOpposite = false,
      bool forceLight = false,
      bool forceDark = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _textColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))!
        .withOpacity(
            useBrightnessOpacity ? IGColors.brightnessOpacity() : opacity);
  }

  static Color textSecondary(
      {double opacity = 1.0,
      bool isOpposite = false,
      bool forceLight = false,
      bool forceDark = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    return _secondaryTextColor(
            lightOnly: forceDark
                ? false
                : (forceLight
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.dark
                        : webConfigurator.currentBrightness ==
                            Brightness.light)),
            darkOnly: forceLight
                ? false
                : (forceDark
                    ? true
                    : (isOpposite
                        ? webConfigurator.currentBrightness == Brightness.light
                        : webConfigurator.currentBrightness ==
                            Brightness.dark)))
        .withOpacity(
            useBrightnessOpacity ? IGColors.brightnessOpacity() : opacity);
  }

  static Color primary(
      {double opacity = 1.0,
      bool forceWhiteInDarkMode = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark &&
        forceWhiteInDarkMode) {
      return Colors.white.withOpacity(useSelectedOpacity
          ? IGColors.selectedOpacity()
          : useBrightnessOpacity
              ? IGColors.brightnessOpacity()
              : opacity);
    }
    return _primaryColor()!.withOpacity(useSelectedOpacity
        ? IGColors.selectedOpacity()
        : useBrightnessOpacity
            ? IGColors.brightnessOpacity()
            : opacity);
  }

  static Color red(
      {double opacity = 1.0,
      bool forceWhiteInDarkMode = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark &&
        forceWhiteInDarkMode) {
      return Colors.white.withOpacity(useSelectedOpacity
          ? IGColors.selectedOpacity()
          : useBrightnessOpacity
              ? IGColors.brightnessOpacity()
              : opacity);
    }
    return _redColor().withOpacity(useSelectedOpacity
        ? IGColors.selectedOpacity()
        : useBrightnessOpacity
            ? IGColors.brightnessOpacity()
            : opacity);
  }

  static Color orange(
      {double opacity = 1.0,
      bool forceWhiteInDarkMode = false,
      bool useBrightnessOpacity = false,
      bool useSelectedOpacity = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark &&
        forceWhiteInDarkMode) {
      return Colors.white.withOpacity(useSelectedOpacity
          ? IGColors.selectedOpacity()
          : useBrightnessOpacity
              ? IGColors.brightnessOpacity()
              : opacity);
    }
    return _orangeColor().withOpacity(useSelectedOpacity
        ? IGColors.selectedOpacity()
        : useBrightnessOpacity
            ? IGColors.brightnessOpacity()
            : opacity);
  }

  static double brightnessOpacity() {
    return webConfigurator.currentBrightness == Brightness.light ? 0.20 : 0.7;
  }

  static double selectedOpacity() {
    return webConfigurator.currentBrightness == Brightness.light ? 0.85 : 0.85;
  }

  static Color blendColor(Color color, {Color? blend}) {
    Color lightBlendColor =
        Color.alphaBlend(blend != null ? blend : Colors.white12, color);
    return lightBlendColor;
  }

  //INTERNAL
  static Color? _primaryColor() {
    if (webConfigurator.internalConfigurator.primaryColor != null) {
      return webConfigurator.internalConfigurator.primaryColor;
    }
    return webConfigurator.internalConfigurator.remoteConfig != null &&
            webConfigurator.internalConfigurator.remoteConfig?.appConfiguration
                    .primaryColor !=
                null
        ? webConfigurator
            .internalConfigurator.remoteConfig?.appConfiguration.primaryColor
        : Color(0xFF004FCE);
  }

  static Color _redColor() {
    return Color(0xFFC10115);
  }

  static Color _orangeColor() {
    return Color(0xFFF5925E);
  }

  static Color _secondaryTextColor(
      {bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return Color.fromRGBO(139, 139, 139, 1);
      }
      return Color.fromRGBO(166, 163, 164, 1);
    } else {
      return Color.fromRGBO(139, 139, 139, 1);
    }
  }

  static Color _separatorColor(
      {bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return Color.fromRGBO(228, 230, 235, 1);
      }
      return Color.fromRGBO(58, 59, 60, 1);
    } else {
      return Color.fromRGBO(228, 230, 235, 1);
    }
  }

  static Color _primaryBackgroundColor(
      {bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return Colors.white;
      }
      return Color.fromRGBO(24, 25, 26, 1);
    } else {
      return Colors.white;
    }
  }

  static Color _secondaryBackgroundColor(
      {bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return Colors.white;
      }
      return Color.fromRGBO(36, 37, 38, 1);
    } else {
      return Colors.white;
    }
  }

  static Color _tertiaryBackgroundColor(
      {bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return Color.fromRGBO(240, 242, 245, 1);
      }
      return Color.fromRGBO(58, 59, 60, 1);
    } else {
      return Color.fromRGBO(240, 242, 245, 1);
    }
  }

  static Color? _textColor({bool lightOnly = false, bool darkOnly = false}) {
    if (webConfigurator.currentBrightness == Brightness.dark || darkOnly) {
      if (lightOnly == true && !darkOnly) {
        return webConfigurator.internalConfigurator.remoteConfig != null &&
                webConfigurator.internalConfigurator.remoteConfig
                        ?.appConfiguration.textColor !=
                    null
            ? webConfigurator
                .internalConfigurator.remoteConfig?.appConfiguration.textColor
            : Color(0xFF232323);
      }
      return webConfigurator.internalConfigurator.remoteConfig != null &&
              webConfigurator.internalConfigurator.remoteConfig
                      ?.appConfiguration.textDarkColor !=
                  null
          ? webConfigurator
              .internalConfigurator.remoteConfig?.appConfiguration.textDarkColor
          : Color(0xFFFFFFFF);
    } else {
      return webConfigurator.internalConfigurator.remoteConfig != null &&
              webConfigurator.internalConfigurator.remoteConfig
                      ?.appConfiguration.textColor !=
                  null
          ? webConfigurator
              .internalConfigurator.remoteConfig?.appConfiguration.textColor
          : Color(0xFF232323);
    }
  }
}
